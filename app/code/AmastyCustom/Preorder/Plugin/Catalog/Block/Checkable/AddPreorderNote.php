<?php

declare(strict_types=1);

namespace AmastyCustom\Preorder\Plugin\Catalog\Block\Checkable;

use Amasty\Preorder\Model\Product\GetPreorderInformation as GetPreorderInformationAlias;
use Amasty\Preorder\Model\Product\RetrieveNote\GetNote;
use Magento\Catalog\Api\Data\ProductCustomOptionValuesInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\View\Options\Type\Select\Checkable;

class AddPreorderNote
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var GetNote
     */
    private $getNote;

    /**
     * @var GetPreorderInformationAlias
     */
    private $getPreorderInformation;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        GetNote $getNote,
        GetPreorderInformationAlias $getPreorderInformation
    ) {
        $this->productRepository = $productRepository;
        $this->getNote = $getNote;
        $this->getPreorderInformation = $getPreorderInformation;
    }

    public function afterFormatPrice(
        Checkable $checkable,
        string $result,
        ProductCustomOptionValuesInterface $value
    ): string {
        $sku = $value->getSku();
        try {
            $product = $this->productRepository->get($sku);
            $isPreorder = $this->getPreorderInformation->execute($product)->isPreorder();
        } catch (\Throwable $exception) {
            $product = null;
        }

        $note = '';

        if ($product && $isPreorder) {
            $note = sprintf('<span><b> | %s</b></span>', $this->getNote->execute($product));
        } elseif ($value->getQty() > 0) {
            $note = '<span><b>' . __('In-Stock') . '</b></span>';
        }

        return $result . $note;
    }
}
