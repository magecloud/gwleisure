<?php

declare(strict_types=1);

namespace AmastyCustom\Preorder\Plugin\Catalog\Block\Product\View\Options;

use Magento\Catalog\Block\Product\View\Options;
use Amasty\Preorder\Model\Product\GetPreorderInformation as GetPreorderInformationAlias;
use Amasty\Preorder\Model\Product\RetrieveNote\GetNote;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Option\Value;
use Magento\Framework\Serialize\Serializer\Json;

class AddPreorderNote
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var GetNote
     */
    private $getNote;

    /**
     * @var GetPreorderInformationAlias
     */
    private $getPreorderInformation;

    /**
     * @var Json
     */
    private $serializer;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        GetNote $getNote,
        GetPreorderInformationAlias $getPreorderInformation,
        Json $serializer
    ) {
        $this->productRepository = $productRepository;
        $this->getNote = $getNote;
        $this->getPreorderInformation = $getPreorderInformation;
        $this->serializer = $serializer;
    }

    public function afterGetJsonConfig(Options $subject, string $result): string
    {
        if (!$subject->hasOptions()) {
            return $result;
        }

        $defaultConfig  = $this->serializer->unserialize($result);

        foreach ($subject->getOptions() as $option) {
            $values = $option->getValues();

            if (!empty($values) && $option->hasValues()) {
                foreach ($values as $value) {
                    if (isset($defaultConfig[$option->getId()][$value->getId()]['name'])) {
                        $name = $defaultConfig[$option->getId()][$value->getId()]['name'];
                        $defaultConfig[$option->getId()][$value->getId()]['name'] =
                            $this->addNote($value, $name);
                    }
                }
            }
        }

        return $this->serializer->serialize($defaultConfig);
    }

    private function addNote(Value $value, string $label): string
    {
        try {
            $product = $this->productRepository->get($value->getSku());
            $isPreorder = $this->getPreorderInformation->execute($product)->isPreorder();
        } catch (\Throwable $exception) {
            $product = null;
        }

        $note = '';

        if ($product && $isPreorder) {
            $note = ' | ' . $this->getNote->execute($product);
        } elseif ($value->getQty() > 0) {
            $note = __(' | In-Stock');
        }


        return $label . $note;
    }
}
