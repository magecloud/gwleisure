<?php

declare(strict_types=1);

namespace AmastyCustom\PreorderAllowQty\Plugin\Quote\Model\Quote;

use AmastyCustom\PreorderAllowQty\Setup\Patch\Data\AddQtyAttribute;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;

class IsQtyAllow
{
    /**
     * @param Quote $subject
     * @param Product $product
     * @param $request
     * @param $processMode
     *
     * @return array
     * @throws LocalizedException
     */
    public function beforeAddProduct(
        Quote $subject,
        Product $product,
        $request = null,
        $processMode = AbstractType::PROCESS_MODE_FULL
    ) {
        if (isset($product->getQuantityAndStockStatus()['qty']) && $product->getQuantityAndStockStatus()['qty'] > 0) {
            return [$product, $request, $processMode];
        }

        $allowPreorderQty = $product->getData(AddQtyAttribute::ATTRIBUTE_NAME);
        if ($allowPreorderQty !== null && $request->getData('qty') > $allowPreorderQty) {
            throw new LocalizedException(__('Adding to the cart is disabled by qty'));
        }

        return [$product, $request, $processMode];
    }
}
