<?php

declare(strict_types=1);

namespace AmastyCustom\PreorderAllowQty\Plugin\Sales\Model\Order;

use AmastyCustom\PreorderAllowQty\Model\ResourceModel\Action\UpdatePreorderQty;
use AmastyCustom\PreorderAllowQty\Setup\Patch\Data\AddQtyAttribute;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as AttributeResource;
use Magento\Sales\Model\Order;

class ChangePreorderQty
{
    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var UpdatePreorderQty
     */
    private $updatePreorderQty;

    public function __construct(
        AttributeResource $attributeResource,
        UpdatePreorderQty $updatePreorderQty
    ) {
        $this->attributeResource = $attributeResource;
        $this->updatePreorderQty = $updatePreorderQty;
    }

    public function afterPlace(Order $subject, Order $result): Order
    {
        $attrId = (int) $this->attributeResource->getIdByCode(Product::ENTITY, AddQtyAttribute::ATTRIBUTE_NAME);
        foreach ($subject->getItems() as $item) {
            $product = $item->getProduct();
            if (isset($product->getQuantityAndStockStatus()['qty'])
                && $product->getQuantityAndStockStatus()['qty'] <= 0
            ) {
                $this->updatePreorderQty->execute($attrId, (int) $item->getProductId(), (int) $item->getQtyOrdered());
            }
        }

        return $result;
    }
}
