<?php

declare(strict_types=1);

namespace AmastyCustom\PreorderAllowQty\Cron;

use AmastyCustom\PreorderAllowQty\Model\ResourceModel\Action\GetProductsByPreorderQty;
use AmastyCustom\PreorderAllowQty\Model\ResourceModel\Action\UpdatePreorderQty;
use AmastyCustom\PreorderAllowQty\Setup\Patch\Data\AddQtyAttribute;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as AttributeResource;

class UpdateStockStatus
{
    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var GetProductsByPreorderQty
     */
    private $getProductsByPreorderQty;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var StockItemRepositoryInterface
     */
    private $stockItemRepository;

    /**
     * @var UpdatePreorderQty
     */
    private $updatePreorderQty;

    public function __construct(
        AttributeResource $attributeResource,
        GetProductsByPreorderQty $getProductsByPreorderQty,
        StockRegistryInterface $stockRegistry,
        StockItemRepositoryInterface $stockItemRepository,
        UpdatePreorderQty $updatePreorderQty
    ) {
        $this->attributeResource = $attributeResource;
        $this->getProductsByPreorderQty = $getProductsByPreorderQty;
        $this->stockRegistry = $stockRegistry;
        $this->stockItemRepository = $stockItemRepository;
        $this->updatePreorderQty = $updatePreorderQty;
    }

    public function execute()
    {
        $attrId = (int) $this->attributeResource->getIdByCode(Product::ENTITY, AddQtyAttribute::ATTRIBUTE_NAME);
        $productIds = $this->getProductsByPreorderQty->execute(
            $attrId,
            0
        );

        foreach (array_map('intval', $productIds) as $productId) {
            $stockItem = $this->stockRegistry->getStockItem($productId);
            $stockItem->setIsInStock(false);
            if ($stockItem->getQty() <= 0) {
                $this->updatePreorderQty->execute($attrId, $productId, null);
                $this->stockItemRepository->save($stockItem);
            }
        }
    }
}
