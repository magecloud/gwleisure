<?php

declare(strict_types=1);

namespace AmastyCustom\PreorderAllowQty\Ui\DataProvider\Product\Form\Modifier;

use AmastyCustom\PreorderAllowQty\Setup\Patch\Data\AddQtyAttribute;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Model\Locator\LocatorInterface;

class Preorder extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @param LocatorInterface $locator
     */
    public function __construct(
        LocatorInterface $locator
    ) {
        $this->locator = $locator;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function modifyData(array $data)
    {
        $model = $this->locator->getProduct();
        $modelId = $model->getId();
        $data[$modelId][self::DATA_SOURCE_DEFAULT][AddQtyAttribute::ATTRIBUTE_NAME]
            = $model->getAmastyPreorderAllowQty();

        return $data;
    }

    /**
     * @param array $meta
     *
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }
}
