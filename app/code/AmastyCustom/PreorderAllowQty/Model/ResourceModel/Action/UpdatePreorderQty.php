<?php

declare(strict_types=1);

namespace AmastyCustom\PreorderAllowQty\Model\ResourceModel\Action;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;
use Zend_Db_Expr;

class UpdatePreorderQty
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Magento\Framework\EntityManager\EntityMetadata
     */
    private $metadata;

    public function __construct(
        ResourceConnection $resourceConnection,
        MetadataPool $metadataPool
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->metadata = $metadataPool->getMetadata(ProductInterface::class);
    }

    public function execute(int $attrId, int $productId, ?int $qty): void
    {
        $linkField = $this->metadata->getLinkField();
        $connection = $this->resourceConnection->getConnection();
        $cpeiTable = $this->resourceConnection->getTableName('catalog_product_entity_int');

        $select = $connection->select()
            ->joinInner(
                ['cpe' => $this->resourceConnection->getTableName('catalog_product_entity')],
                sprintf('cpei.%1$s = cpe.%1$s', $linkField),
                []
            )
            ->where('cpei.attribute_id = ?', $attrId)
            ->where('cpe.entity_id = ?', $productId);

        if ($qty === null) {
            $select->from(['cpei' => $cpeiTable], []);
            $sql = $connection->deleteFromSelect($select, 'cpei');
        } else {
            $select->columns(['value' => new Zend_Db_Expr(sprintf('cpei.value - %d', $qty))]);
            $sql = $connection->updateFromSelect($select, ['cpei' => $cpeiTable]);
        }
        $connection->query($sql);
    }
}
