<?php

declare(strict_types=1);

namespace AmastyCustom\PreorderAllowQty\Model\ResourceModel\Action;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;

class GetProductsByPreorderQty
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Magento\Framework\EntityManager\EntityMetadata
     */
    private $metadata;

    public function __construct(
        ResourceConnection $resourceConnection,
        MetadataPool $metadataPool
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->metadata = $metadataPool->getMetadata(ProductInterface::class);
    }

    public function execute(int $attrId, int $qty): array
    {
        $linkField = $this->metadata->getLinkField();
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                ['cpei' => $this->resourceConnection->getTableName('catalog_product_entity_int')],
                []
            )
            ->joinInner(
                ['cpe' => $this->resourceConnection->getTableName('catalog_product_entity')],
                sprintf('cpei.%1$s = cpe.%1$s', $linkField),
                ['cpe.entity_id']
            )
            ->where(sprintf('%s = ?', 'attribute_id'), $attrId)
            ->where(sprintf('%s <= ?', 'value'), $qty);

        return $connection->fetchCol($select) ?? [];
    }
}
