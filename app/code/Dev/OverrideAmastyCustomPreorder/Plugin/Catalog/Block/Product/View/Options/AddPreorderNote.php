<?php

declare(strict_types=1);

namespace Dev\OverrideAmastyCustomPreorder\Plugin\Catalog\Block\Product\View\Options;

use Magento\Catalog\Block\Product\View\Options;
use Amasty\Preorder\Model\Product\GetPreorderInformation as GetPreorderInformationAlias;
use Amasty\Preorder\Model\Product\RetrieveNote\GetNote;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Option\Value;
use Magento\Framework\Serialize\Serializer\Json;

class   AddPreorderNote extends \AmastyCustom\Preorder\Plugin\Catalog\Block\Checkable\AddPreorderNote
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var GetNote
     */
    private $getNote;

    /**
     * @var GetPreorderInformationAlias
     */
    private $getPreorderInformation;

    /**
     * @var Json
     */
    private $serializer;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        GetNote $getNote,
        GetPreorderInformationAlias $getPreorderInformation,
        Json $serializer
    ) {
        $this->productRepository = $productRepository;
        $this->getNote = $getNote;
        $this->getPreorderInformation = $getPreorderInformation;
        $this->serializer = $serializer;
    }

    public function afterGetJsonConfig(Options $subject, string $result): string
    {
        if (!$subject->hasOptions()) {
            return $result;
        }

        $defaultConfig  = $this->serializer->unserialize($result);

        foreach ($subject->getOptions() as $option) {
            $values = $option->getValues();

            if (!empty($values) && $option->hasValues()) {
                foreach ($values as $value) {
                    if (isset($defaultConfig[$option->getId()][$value->getId()]['name'])) {
                        $name = $defaultConfig[$option->getId()][$value->getId()]['name'];
                        $defaultConfig[$option->getId()][$value->getId()]['name'] =
                            $this->addNote($value, $name);
                    }
                }
            }
        }


        return $this->serializer->serialize($defaultConfig);
    }

    private function addNote(Value $value, string $label): string
    {
        try {
            $product = $this->productRepository->get($value->getSku());
            $isPreorder = $this->getPreorderInformation->execute($product)->isPreorder();
        } catch (\Throwable $exception) {
            $product = null;
        }

        $note = '';
        $specialPriceValue = "";
        $regularPriceValue = "";
        $regularPrice = $value->getPrice();
        $specialPrice = $value->getSpecialPrice();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
        $currency = $objectManager->create('\Magento\Directory\Model\CurrencyFactory')->create()->load($currencyCode);
        $currencyCode = $currency->getCurrencySymbol();

        if ($product && $isPreorder) {
            $note = ' | ' . $this->getNote->execute($product);
        } elseif ($value->getQty() > 0) {
            $note = __(' | In Stock');
        }


        if ( is_string($specialPrice) ) {
            if ( strlen($specialPrice) > 0 ) {
                $specialPriceToArray = json_decode(strval($specialPrice), TRUE);
                if ( is_array($specialPriceToArray) ) {
                    foreach ($specialPriceToArray as $items) {
                        foreach ($items as $item => $key) {
                            if ( $item == "price" ) {
                                $specialPriceValue = $key;
                            }
                        }
                    }
                }
            }
        }

        //set values, convert from string to float
        $regularPriceValue = number_format((float)$regularPrice, 2);
        $specialPriceValue = number_format((float)$specialPriceValue, 2);

        //show Regular Price if special price != regular price
        if ( $specialPriceValue > 0 ) {
            if ( $specialPriceValue == $regularPriceValue ) {
                return $label . $note;
            } else {
                return $label . $note . " (Regular price " . $currencyCode . $regularPriceValue . ")";
            }
        } else {
            return $label . $note ;
        }
    }
}
