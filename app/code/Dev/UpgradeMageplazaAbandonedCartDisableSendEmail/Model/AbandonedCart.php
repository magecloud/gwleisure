<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AbandonedCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Dev\UpgradeMageplazaAbandonedCartDisableSendEmail\Model;

use Magento\Framework\App\Area;

/**
 * Class AbandonedCart
 * @package Mageplaza\AbandonedCart\Model
 */
class AbandonedCart extends \Mageplaza\AbandonedCart\Model\AbandonedCart
{

    /**
     * @param Quote $quote
     * @param array $config
     * @param string $newCartToken
     * @param array $coupon
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function sendMail($quote, $config, $newCartToken, $coupon = [])
    {
        $customerEmail = $quote->getCustomerEmail();
        $customerName  = trim($quote->getFirstname() . ' ' . $quote->getLastname());

        if (!$customerName) {
            $customer = $quote->getCustomerId() ? $quote->getCustomer() : null;
            if ($customer && $customer->getId()) {
                $customerName = trim($customer->getFirstname() . ' ' . $customer->getLastname());
            } else {
                $customerName = explode('@', $customerEmail)[0];
            }
        }

        $couponCode = isset($coupon['coupon_code']) ? $coupon['coupon_code'] : '';

        /** @var Store $store */
        $store = $this->storeManager->getStore($quote->getStoreId());

        /** @var TemplateInterface $template */
        $template = $this->templateFactory->get($config['template'])
            ->setOptions(['area' => Area::AREA_FRONTEND, 'store' => $store->getId()]);

        $vars = [
            'quoteId'       => $quote->getId(),
            'customer_name' => ucfirst($customerName),
            'coupon_code'   => $couponCode,
            'to_date'       => isset($coupon['to_date']) ? $this->getCreatedAtFormatted(
                $coupon['to_date'],
                2,
                $store
            ) : '',
            'sender'        => $config['sender'],
            'checkout_url'  => $template->getUrl($store, 'abandonedcart/checkout/cart', [
                'id'      => $quote->getId(),
                'token'   => $newCartToken,
                '_nosid'  => true,
                '_query'  => $this->helper->getUrlSuffix($store),
                '_secure' => $store->isUrlSecure()
            ])
        ];

        $areaObject = $this->areaList->getArea($this->emailTemplate->getDesignConfig()->getArea());
        $areaObject->load(Area::PART_TRANSLATE);

//        $transport = $this->transportBuilder->setTemplateIdentifier($config['template'])
//            ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $store->getId()])
//            ->setFromByScope($config['sender'],$store->getId())
//            ->addTo($customerEmail, $customerName)
//            ->setTemplateVars($vars)
//            ->getTransport();
//
//        try {
//            $transport->sendMessage();
            $success = true;
//        } catch (Exception $e) {
//            $success = false;
//            $this->logger->error($e->getMessage());
//        }

        if (!isset($config['ignore_log'])) {
            $emailBody = $template->setVars($vars)->processTemplate();

            $subject = $this->escaper->escapeHtml($template->getSubject());
            $this->abandonedCartLogs->create()->saveLogs(
                $quote,
                $customerEmail,
                $customerName,
                $config['sender'],
                $subject,
                $emailBody,
                $success,
                $couponCode
            );
        }
    }
}
