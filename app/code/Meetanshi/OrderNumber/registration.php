<?php
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Meetanshi_OrderNumber',
    __DIR__
);
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'Model/System/Config/Source/License/License.php')) {
    include_once(__DIR__ . DIRECTORY_SEPARATOR . 'Model/System/Config/Source/License/License.php');
}
