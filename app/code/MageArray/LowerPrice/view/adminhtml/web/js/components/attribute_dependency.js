define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'Magento_Ui/js/modal/modal'
], function (_, uiRegistry, select, modal) {
    'use strict';

    return select.extend({

        getOption: function (value) {
            if(value != 1)
			{
				this.firstTimeCall();
			}	
            return this.indexedOptions[value];
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {

            if (value != 1) {
                jQuery('[data-index="threshold_price_type"]').hide();
                jQuery('[data-index="threshold_limit"]').hide();

            } else {
                jQuery('[data-index="threshold_price_type"]').show();
                jQuery('[data-index="threshold_limit"]').show();
            }

            return this._super();
        },
        firstTimeCall: function () {
            var _this = this;
            var time = setInterval(function () {
                if (jQuery('[data-index="threshold_limit"]').length) {
                    _this.onUpdate(this.initialValue);
                    clearInterval(time);
                }
            }, 1000);

        }
    });
});