<?php

namespace MageArray\LowerPrice\Helper;

/**
 * Class Data
 * @package MageArray\LowerPrice\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *    To Check IS_ENABLE
     */
    const IS_ENABLE = 'lowerprice/general/enable';
    /**
     * To Check Settings THRESHOLD_LIMIT
     */
    const THRESHOLD_LIMIT = 'lowerprice/additional_setting/threshold_limit';
    /**
     * To Check Settings PRICE_TYPE
     */
    const PRICE_TYPE = 'lowerprice/additional_setting/price_type';
    /**
     *  To Check Settings AUTO_APPROVED_INQUIRIES
     */
    const AUTO_APPROVED_INQUIRIES = 'lowerprice/additional_setting/auto_approved_inquiries';
    /**
     * To Check Settings COUPON_EXPIRY_DAYS
     */
    const COUPON_EXPIRY_DAYS = 'lowerprice/additional_setting/coupon_expiry_days';

    /**
     * To Check Settings LOWER_PRICE_EMAIL_TEMPLATE
     */
    const LOWER_PRICE_EMAIL_TEMPLATE = 'lowerprice/general/emailtemplate';
    /**
     * To Check Settings LOWER_PRICE_APPROVAL_EMAIL_TEMPLATE
     */
    const LOWER_PRICE_APPROVAL_EMAIL_TEMPLATE = 'lowerprice/additional_setting/approvalemailtemplate';
    /**
     *  To Check Settings LOWERPRICE_EMAIL_IDENTITY
     */
    const LOWERPRICE_EMAIL_IDENTITY = 'lowerprice/general/identity';
    /**
     *  To Check Settings LOWERPRICE_THANKYOU_MSG
     */
    const LOWERPRICE_THANKYOU_MSG = 'lowerprice/general/thankyou';
    /**
     *  To Check Settings NEW_FILE
     */
    const NEW_FILE = 'lowerprice/additional_setting/new_file';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_objectManager = $objectManager;
        $this->messageManager = $messageManager;
        $this->_transportBuilder = $transportBuilder;
    }

    /**
     * @return mixed
     */
    public function getBaseUrlMedia()
    {
        return $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
    }

    /**
     * @return mixed
     */
    public function getCurrentStoreId()
    {
        return $this->_storeManager->getStore()->getStoreId();
    }

    /**
     * @return mixed
     */
    public function isModuleEnabled()
    {
        return $this->scopeConfig->getValue(
            self::IS_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getLowerPriceEmailTemplateId()
    {
        return $this->scopeConfig->getValue(
            self::LOWER_PRICE_EMAIL_TEMPLATE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getApprovalEmailTemplateId()
    {
        return $this->scopeConfig->getValue(
            self::LOWER_PRICE_APPROVAL_EMAIL_TEMPLATE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getAutoApprovedInquiries()
    {
        return $this->scopeConfig->getValue(
            self::AUTO_APPROVED_INQUIRIES,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getThresholdPrice()
    {
        $price = $this->scopeConfig->getValue(
            self::THRESHOLD_LIMIT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
        return preg_replace("/[^0-9]/", "", $price);
    }

    /**
     * @return mixed
     */
    public function getCouponExpiryDays()
    {
        $price = $this->scopeConfig->getValue(
            self::COUPON_EXPIRY_DAYS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
        return preg_replace("/[^0-9]/", "", $price);
    }

    /**
     * @return mixed
     */
    public function getPriceType()
    {
        return $this->scopeConfig->getValue(
            self::PRICE_TYPE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getIdentity()
    {
        return $this->scopeConfig->getValue(
            self::LOWERPRICE_EMAIL_IDENTITY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
    
    /**
     * @return mixed
     */
    public function getCsvFile()
    {
        return $this->scopeConfig->getValue(
            self::NEW_FILE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getSenderEmail()
    {
        $path = 'trans_email/ident_' . $this->getIdentity() . '/email';
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getSenderName()
    {
        $path = 'trans_email/ident_' . $this->getIdentity() . '/name';
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getThankYouMsg()
    {
        $thankYou = $this->scopeConfig->getValue(
            self::LOWERPRICE_THANKYOU_MSG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );

        if ($thankYou) {
            return $thankYou;
        } else {
            return 'Thank you..!!!';
        }
    }

    /**
     * @return mixed
     */
    public function sendCouponCodeEmail($post)
    {
        if ($post['approval'] == 0 || $post['discount_amount'] == '') {
            return false;
        }

        try {
            $email = $post['email'];
            $name = $post['name'];

            $senderEmail = $this->getSenderEmail();
            $senderName = $this->getSenderName();
            $sender = [
                'name' => $senderName,
                'email' => $senderEmail,
            ];

            $emaildata = [];
            $emaildata['product_name'] = $post['product_name'];
            $emaildata['name'] = $name;
            $emaildata['coupon_code'] = $post['coupon_code'];

            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($emaildata);

            $this->inlineTranslation->suspend();
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier($this->getApprovalEmailTemplateId())
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->getCurrentStoreId()
                    ]
                )
                ->setTemplateVars(['data' => $postObject])
                ->setFrom($sender)
                ->addTo($email, $name)
                ->setReplyTo($senderEmail)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();

            $this->messageManager->addSuccess(__('The approval email has been send.'));
            return $post['coupon_code'];
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->messageManager->addException($e, __('Something went wrong while send approval email.'));
        }
        return false;
    }
}
