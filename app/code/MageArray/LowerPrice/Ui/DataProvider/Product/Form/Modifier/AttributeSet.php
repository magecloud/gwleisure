<?php

namespace MageArray\LowerPrice\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\UrlInterface;

/**
 * Class AttributeSet
 * @package MageArray\LowerPrice\Ui\DataProvider\Product\Form\Modifier
 */
class AttributeSet extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AttributeSet
{
    /**
     * @var \MageArray\LowerPrice\Helper\Data
     */
    protected $_dataHelper;

    /**
     * AttributeSet constructor.
     * @param LocatorInterface $locator
     * @param CollectionFactory $attributeSetCollectionFactory
     * @param UrlInterface $urlBuilder
     * @param \MageArray\LowerPrice\Helper\Data $dataHelper
     */
    public function __construct(
        LocatorInterface $locator,
        CollectionFactory $attributeSetCollectionFactory,
        UrlInterface $urlBuilder,
        \MageArray\LowerPrice\Helper\Data $dataHelper
    ) {
        $this->_dataHelper = $dataHelper;
        parent::__construct($locator, $attributeSetCollectionFactory, $urlBuilder);
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $meta = parent::modifyMeta($meta);
        if ($this->_dataHelper->isModuleEnabled()) {
            $meta['lower-price-options']['children']['container_auto_approved_inquiries']
            ['children']['auto_approved_inquiries']['arguments']['data']['config']
            ['component'] = 'MageArray_LowerPrice/js/components/attribute_dependency';

            $meta['lower-price-options']['children']['container_auto_approved_inquiries']
            ['children']['auto_approved_inquiries']['arguments']['data']['config']['template'] = 'ui/form/field';
        }
        return $meta;
    }
}
