<?php

namespace MageArray\LowerPrice\Model\ResourceModel\LowerPrice;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MageArray\LowerPrice\Model\ResourceModel\LowerPrice
 */
class Collection extends AbstractCollection
{

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            \MageArray\LowerPrice\Model\LowerPrice::Class,
            \MageArray\LowerPrice\Model\ResourceModel\LowerPrice::Class
        );
    }
}
