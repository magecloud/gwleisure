<?php

namespace MageArray\LowerPrice\Model\ResourceModel;

use Magento\Store\Model\Store;
use Magento\Framework\DB\Select;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class LowerPrice
 * @package MageArray\LowerPrice\Model\ResourceModel
 */
class LowerPrice extends AbstractDb
{
     /**
      *
      */
    protected function _construct()
    {
        $this->_init('magearray_lowerprice', 'lowerprice_id');
    }
}
