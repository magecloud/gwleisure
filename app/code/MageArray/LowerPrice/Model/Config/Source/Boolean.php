<?php

namespace MageArray\LowerPrice\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class Boolean
 * @package MageArray\LowerPrice\Model\Config\Source
 */
class Boolean extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * To Check Yes
     */
    const YES = '1';
    /**
     * To Check No
     */
    const NO = '0';

    /**#@-*/

    public function getOptionArray()
    {
        return [self::YES => __('Yes'), self::NO => __('No')];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [self::YES => __('Yes'), self::NO => __('No')];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @param $optionId
     * @return mixed|null
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
