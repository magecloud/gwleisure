<?php

namespace MageArray\LowerPrice\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class PriceType
 * @package MageArray\LowerPrice\Model\Config\Source
 */
class PriceType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**#@+
     * Price Type values
     */
    const NULL_VALUE = '';
    /**
     *  Price Type values Fix
     */
    const TYPE_FIX = 'fix';

    /**
     * Price Type values Percentage
     */
    const TYPE_PERCENTAGE = 'percentage';

    /**#@-*/

    public function getOptionArray()
    {
        return [
            self::NULL_VALUE => __('-- Please Select --'),
            self::TYPE_FIX => __('Fix Amount'),
            self::TYPE_PERCENTAGE => __('Percentage')
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            self::NULL_VALUE => __('-- Please Select --'),
            self::TYPE_FIX => __('Fix Amount'),
            self::TYPE_PERCENTAGE => __('Percentage')
        ];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @param $optionId
     * @return mixed|null
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
