<?php

namespace MageArray\LowerPrice\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class LowerPrice
 * @package MageArray\LowerPrice\Model
 */
class LowerPrice extends AbstractModel
{

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(\MageArray\LowerPrice\Model\ResourceModel\LowerPrice::Class);
    }
}
