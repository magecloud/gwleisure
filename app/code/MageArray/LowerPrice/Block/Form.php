<?php

namespace MageArray\LowerPrice\Block;

use Magento\Framework\Registry;
use \Magento\Catalog\Helper\Image;

/**
 * Class Form
 * @package MageArray\LowerPrice\Block
 */
class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Registry
     */
    protected $_registry;
    /**
     * @var Image
     */
    protected $imageHelper;
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Form constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Request\Http $request
     * @param Image $imageHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Image $imageHelper,
        array $data = []
    ) {
        $this->request = $request;
        $this->scopeConfig = $context->getScopeConfig();
        $this->_registry = $registry;
        $this->_productFactory = $productFactory;
        $this->imageHelper = $imageHelper;
        $this->_product = $this->_productFactory->create()->load(
            $this->_registry->registry('current_product')->getId()
        );
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_product;
    }

    /**
     * @return mixed
     */
    public function getProductImageUrl()
    {
        $image = 'category_page_grid';
        return $this->imageHelper->init($this->_product, $image)->getUrl();
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->scopeConfig->getValue('lowerprice/general/description');
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->scopeConfig->getValue('lowerprice/general/zipcode');
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        $title = $this->scopeConfig->getValue('lowerprice/general/title');
        $price = $this->getPrice();
        $title = str_replace('{{price}}', $price, $title);
        return $title;
    }

    /**
     * @return mixed
     */
    public function getActionUrl()
    {
        return $this->getUrl('lowerprice/form/post');
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        $price = $this->_product->getFinalPrice();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        $priceHelper = $objectManager->create(\Magento\Framework\Pricing\Helper\Data::Class);
        return $priceHelper->currency($price, true, false);
    }

    /**
     * @return mixed
     */
    public function showOrSection()
    {
        return $this->scopeConfig->getValue('lowerprice/general/or_section');
    }
}
