<?php

namespace MageArray\LowerPrice\Block\Adminhtml;

/**
 * Class LowerPrice
 * @package MageArray\LowerPrice\Block\Adminhtml
 */

class LowerPrice extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @var
     */
    protected $_controller;
    /**
     * @var
     */
    protected $_blockGroup;
    /**
     * @var
     */
    protected $_headerText;
    /**
     * @var
     */
    protected $_addButtonLabel;

    /**
     *
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_lowerPrice';
        $this->_blockGroup = 'MageArray_LowerPrice';
        $this->_headerText = __('Inquiries');
        $this->_addButtonLabel = __('Add New');
        parent::_construct();
        $this->removeButton('add');
    }
}
