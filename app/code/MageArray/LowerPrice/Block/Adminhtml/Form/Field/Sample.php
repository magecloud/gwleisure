<?php

namespace MageArray\LowerPrice\Block\Adminhtml\Form\Field;

use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Escaper;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Sample
 * @package MageArray\LowerPrice\Block\Adminhtml\Form\Field
 */
class Sample extends AbstractElement
{
    /**
     * @var UrlInterface
     */
    protected $_backendUrl;

    /**
     * Sample constructor.
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param UrlInterface $backendUrl
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        UrlInterface $backendUrl,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        $data = []
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
        $this->_backendUrl = $backendUrl;
        $this->_assetRepo = $assetRepo;
    }

    /**
     * @return string
     */
    public function getElementHtml()
    {
        $buttonBlock = $this->getForm()->getParent()
            ->getLayout()->createBlock(
                \Magento\Backend\Block\Widget\Button::Class
            );
        $url = $this->_assetRepo->getUrl(
            'MageArray_LowerPrice::sample.csv'
        );
        $data = [
            'label' => __('Download CSV'),
            'onclick' => "setLocation('" . $url . "')",
            'class' => '',
        ];
        $html = '';
        $html .= $buttonBlock->setData($data)->toHtml();
        return $html;
    }
}
