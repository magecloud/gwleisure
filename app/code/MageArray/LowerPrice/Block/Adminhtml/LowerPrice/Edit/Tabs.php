<?php

namespace MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit;

/**
 * Class Tabs
 * @package MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('lowerprice_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('LowerPrice Information'));
    }
}
