<?php

namespace MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit\Tab;

use MageArray\LowerPrice\Model\Status;

/**
 * Class Form
 * @package MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit\Tab
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Form constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory
    ) {
        $this->_localeDate = $context->getLocaleDate();
        parent::__construct($context, $registry, $formFactory);
    }

    /**
     *
     */
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $model = $this->getLowerPrice();
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldSet = $form->addFieldset('base_fieldset', ['legend' => __('Lower Price Information')]);

        if ($model->getId()) {
            $fieldSet->addField('lowerprice_id', 'hidden', ['name' => 'lowerprice_id']);
        }

        $fieldSet->addField(
            'product_id',
            'text',
            [
                'name' => 'product_id',
                'label' => __('Product Id'),
                'title' => __('Product Id'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'product_link',
            'text',
            [
                'name' => 'product_link',
                'label' => __('Product Link'),
                'title' => __('Product Link'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'product_name',
            'text',
            [
                'name' => 'product_name',
                'label' => __('Product Name'),
                'title' => __('Product Name'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'product_price',
            'text',
            [
                'name' => 'product_price',
                'label' => __('Price'),
                'title' => __('Price'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'price',
            'text',
            [
                'name' => 'price',
                'label' => __('Their Price'),
                'title' => __('Their Price'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'shipping_handling',
            'checkbox',
            [
                'name' => 'shipping_handling',
                'label' => 'Shipping Handling',
                'onclick' => 'this.value = this.checked ? 1 : 0;',
                'data-form-part' => $this->getData('target_form'),
            ]
        )->setIsChecked($model->getShippingHandling());
        
        $fieldSet->addField(
            'sales_tax',
            'checkbox',
            [
                'name' => 'sales_tax',
                'label' => 'Sales Tax',
                'onclick' => 'this.value = this.checked ? 1 : 0;',
                'data-form-part' => $this->getData('target_form'),
            ]
        )->setIsChecked($model->getSalesTax());
        
        $fieldSet->addField(
            'website',
            'text',
            [
                'name' => 'website',
                'label' => __('Website'),
                'title' => __('Website'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );

        $fieldSet->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'email',
            'text',
            [
                'name' => 'email',
                'label' => __('Email'),
                'title' => __('Email'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );
        $fieldSet->addField(
            'phone',
            'text',
            [
                'name' => 'phone',
                'label' => __('Phone'),
                'title' => __('Phone'),
            ]
        );
        $fieldSet->addField(
            'createat',
            'text',
            [
                'name' => 'createat',
                'label' => __('Create At'),
                'title' => __('Create At'),
            ]
        );
        $fieldSet->addField(
            'ip_address',
            'text',
            [
                'name' => 'ip_address',
                'label' => __('IP Address'),
                'title' => __('IP Address'),
            ]
        );
        $fieldSet->addField(
            'store_name',
            'text',
            [
                'name' => 'store_name',
                'label' => __('Store Name'),
                'title' => __('Store Name'),
            ]
        );
        $fieldSet->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
            ]
        );
        $fieldSet->addField(
            'state',
            'text',
            [
                'name' => 'state',
                'label' => __('State'),
                'title' => __('State'),
            ]
        );
        $fieldSet->addField(
            'zip_code',
            'text',
            [
                'name' => 'zip_code',
                'label' => __('Zip Code'),
                'title' => __('Zip Code')
            ]
        );
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * @return mixed
     */
    public function getLowerPrice()
    {
        return $this->_coreRegistry->registry('lowerprice');
    }

    /**
     * @return mixed
     */
    public function getPageTitle()
    {
        return $this->getLowerPrice()->getId() ? __(
            "Edit Lower Price '%1'",
            $this->escapeHtml($this->getLowerPrice()->getProductName())
        ) : __('New');
    }

    /**
     * @return mixed
     */
    public function getTabLabel()
    {
        return __('Lower Price Information');
    }

    /**
     * @return mixed
     */
    public function getTabTitle()
    {
        return __('Lower Price Information');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
