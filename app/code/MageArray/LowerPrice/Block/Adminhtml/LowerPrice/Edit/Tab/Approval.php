<?php

namespace MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit\Tab;

/**
 * Class Approval
 * @package MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit\Tab
 */
class Approval extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * Approval constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \MageArray\LowerPrice\Model\ManageCoupon $manageCoupon,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->systemStore = $systemStore;
        $this->manageCoupon = $manageCoupon;
        parent::__construct($context, $registry, $formFactory);
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'html_id_prefix' => 'page_additional_'
                ]
            ]
        );

        $model = $this->_coreRegistry->registry('lowerprice');
        $isElementDisabled = false;
        $fieldset = $form->addFieldset(
            'Additional_fieldset',
            ['legend' => __('Approval'), 'class' => 'fieldset-wide', 'disabled' => $isElementDisabled]
        );

        $approval = $fieldset->addField(
            'approval',
            'select',
            [
                'name' => 'approval',
                'label' => __('Approval'),
                'title' => __('Approval'),
                'after_element_html' => '<small>Approval This Inquiries</small>',
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        $note = '';
        if ($model->getCouponCode() != '') {
            $ruleId = $this->manageCoupon->loadCouponByCode($model->getCouponCode())->getRuleId();
            $url = $this->getUrl('sales_rule/promo_quote/edit', ['id'=>$ruleId]);
            $note .= '<a href="'. $url .'" target="_blank">Edit Coupon</a>';
            $note .= '<br /><br />';
            $required = true;
        } else {
            $note .= '<span>Leave it empty to auto generate coupon code.</span>';
            $note .= '<br /><br />';
            $required = false;
        }

        $discountAmount = $fieldset->addField(
            'discount_amount',
            'text',
            [
                'name' => 'discount_amount',
                'label' => __('Discount Amount'),
                'title' => __('Discount Amount'),
                'required' => true
            ]
        );

        $coupanCode = $fieldset->addField(
            'coupon_code',
            'text',
            [
                'name' => 'coupon_code',
                'label' => __('Coupon Code'),
                'title' => __('Coupon Code'),
                'note' => $note,
                'required' => $required,
                'readonly' => true
            ]
        );

        $extraNote = $fieldset->addField(
            'extra_note',
            'textarea',
            [
                'name' => 'extra_note',
                'label' => __('Extra note'),
                'title' => __('Extra note')
            ]
        );

        $submit = $fieldset->addField(
            'send_email',
            'checkbox',
            [
                'name' => 'send_email',
                'label' => __('Send Email'),
                'title' => __('Send Email'),
                'onchange' => 'this.value = this.checked;'
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        // field dependencies
        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                \Magento\Backend\Block\Widget\Form\Element\Dependence::Class
            )->addFieldMap(
                $approval->getHtmlId(),
                $approval->getName()
            )->addFieldMap(
                $discountAmount->getHtmlId(),
                $discountAmount->getName()
            )->addFieldMap(
                $coupanCode->getHtmlId(),
                $coupanCode->getName()
            )->addFieldMap(
                $submit->getHtmlId(),
                $submit->getName()
            )->addFieldMap(
                $extraNote->getHtmlId(),
                $extraNote->getName()
            )->addFieldDependence(
                $discountAmount->getName(),
                $approval->getName(),
                1
            )->addFieldDependence(
                $coupanCode->getName(),
                $approval->getName(),
                1
            )->addFieldDependence(
                $submit->getName(),
                $approval->getName(),
                1
            )->addFieldDependence(
                $extraNote->getName(),
                $approval->getName(),
                1
            )/* ->addFieldDependence(
            $coupanCode->getName(),
            $discountAmount->getName(),
            1
            ) */
        );

        return parent::_prepareForm();
    }

    /**
     * @return mixed
     */
    public function getTabLabel()
    {
        return __('Approval');
    }

    /**
     * @return mixed
     */
    public function getApprovalUrl()
    {
        if ($this->_coreRegistry->registry('lowerprice')) {
            $id = $this->_coreRegistry->registry('lowerprice')->getId();
            return $this->getUrl('*/*/approval', ['id' => $id]);
        }
        return $this->getUrl('*/*/approval');
    }

    /**
     * @return mixed
     */
    public function getTabTitle()
    {
        return __('Approval');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @param $resourceId
     * @return mixed
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
