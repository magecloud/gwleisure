<?php

namespace MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit\Tab;

/**
 * Class Note
 * @package MageArray\LowerPrice\Block\Adminhtml\LowerPrice\Edit\Tab
 */
class Note extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * Note constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory
    ) {
        $this->systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory);
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'html_id_prefix' => 'page_additional_'
                ]
            ]
        );
        $model = $this->_coreRegistry->registry('lowerprice');
        $isElementDisabled = false;
        $fieldset = $form->addFieldset(
            'Additional_fieldset',
            ['legend' => __('Note'), 'class' => 'fieldset-wide', 'disabled' => $isElementDisabled]
        );

        $fieldset->addField(
            'note',
            'textarea',
            [
                'name' => 'note',
                'label' => __('Note'),
                'title' => __('Note')
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return mixed
     */
    public function getTabLabel()
    {
        return __('Note');
    }

    /**
     * @return mixed
     */
    public function getTabTitle()
    {
        return __('Note');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @param $resourceId
     * @return mixed
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
