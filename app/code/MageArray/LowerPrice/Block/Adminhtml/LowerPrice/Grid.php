<?php

namespace MageArray\LowerPrice\Block\Adminhtml\LowerPrice;

/**
 * Class Grid
 * @package MageArray\LowerPrice\Block\Adminhtml\LowerPrice
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \MageArray\LowerPrice\Model\LowerPriceFactory
     */
    protected $_lowerPriceFactory;

    /**
     * @var
     */
    protected $boolean;

    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \MageArray\LowerPrice\Model\Config\Source\Boolean $boolean
     * @param \MageArray\LowerPrice\Model\LowerPriceFactory $lowerPriceFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \MageArray\LowerPrice\Model\Config\Source\Boolean $boolean,
        \MageArray\LowerPrice\Model\LowerPriceFactory $lowerPriceFactory
    ) {
        $this->_lowerPriceFactory = $lowerPriceFactory;
        $this->_boolean = $boolean;
        parent::__construct($context, $backendHelper);
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('lowerpriceGrid');
        $this->setDefaultSort('lowerprice_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * @return mixed
     */
    protected function _prepareCollection()
    {
        $collection = $this->_lowerPriceFactory->create()->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return mixed
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'lowerprice_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'lowerprice_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'width' => '50px',
            ]
        );
        /* $this->addColumn(
            'product_id',
            [
                'header' => __('Product ID'),
                'index' => 'product_id',
                'width' => '50px',
            ]
        ); */
        $this->addColumn(
            'product_name',
            [
                'header' => __('Product Name'),
                'index' => 'product_name',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'product_price',
            [
                'header' => __('Our Price'),
                'index' => 'product_price',
                'width' => '50px',
                'type' => 'currency',
            ]
        );
        $this->addColumn(
            'price',
            [
                'header' => __('Their Price'),
                'index' => 'price',
                'width' => '50px',
                'type' => 'currency',
            ]
        );
        /*  $this->addColumn(
             'shipping_handling',
             [
                 'header' => __('Shipping Included'),
                 'index' => 'shipping_handling',
                 'width' => '50px',
                 'filter_index' => 'shipping_handling',
                 'type' => 'options',
                 'options' => ['' => '', '0' => 'No', '1' => 'Yes']

             ]
         );
         $this->addColumn(
             'sales_tax',
             [
                 'header' => __('Tax Included'),
                 'index' => 'sales_tax',
                 'width' => '50px',
                 'filter_index' => 'sales_tax',
                 'type' => 'options',
                 'options' => ['' => '', '0' => 'No', '1' => 'Yes']
             ]
         );
         $this->addColumn(
             'website',
             [
                 'header' => __('Website'),
                 'index' => 'website',
                 'width' => '50px',
             ]
         );
         $this->addColumn(
             'store_name',
             [
                 'header' => __('Store'),
                 'index' => 'store_name',
                 'width' => '50px',
             ]
         ); */
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'email',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'createat',
            [
                'header' => __('Submitted'),
                'index' => 'createat',
                "type" =>   "datetime",
                'width' => '50px',
            ]
        );

        $this->addColumn(
            'approval',
            [
                'header' => __('Approval'),
                'index' => 'approval',
                'width' => '50px',
                'type' => 'options',
                'options' => $this->_boolean->getOptionArray()
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'index' => 'is_active',
                'type' => 'action',
                'getter' => 'getId',
                'width' => '20px',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit',
                        ],
                        'field' => 'lowerprice_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false
            ]
        );
        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('lowerprice_id');
        $this->getMassactionBlock()->setFormFieldName('lowerprice');
        $this->getMassactionBlock()->addItem('delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('*/*/massDelete', ['' => '']),
            'confirm' => __('Are you sure?')
        ]);
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['lowerprice_id' => $row->getId()]);
    }
}
