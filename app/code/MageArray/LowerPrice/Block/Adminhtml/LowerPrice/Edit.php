<?php

namespace MageArray\LowerPrice\Block\Adminhtml\LowerPrice;

/**
 * Class Edit
 * @package MageArray\LowerPrice\Block\Adminhtml\LowerPrice
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_objectId = 'lowerprice_id';
        $this->_blockGroup = 'MageArray_LowerPrice';
        $this->_controller = 'adminhtml_lowerPrice';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save'));
        $this->buttonList->update('delete', 'label', __('Delete'));
        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ],
                    ],
                ],
            ],
            10
        );
    }

    /**
     * @return mixed
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            ['_current' => true, 'back' => 'edit', 'tab' => '{{tab_id}}']
        );
    }
}
