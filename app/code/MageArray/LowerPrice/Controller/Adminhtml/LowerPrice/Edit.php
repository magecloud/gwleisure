<?php

namespace MageArray\LowerPrice\Controller\Adminhtml\LowerPrice;

/**
 * Class Edit
 * @package MageArray\LowerPrice\Controller\Adminhtml\LowerPrice
 */
class Edit extends \MageArray\LowerPrice\Controller\Adminhtml\LowerPrice
{

    /**
     *
     */
    public function execute()
    {

        $id = $this->getRequest()->getParam('lowerprice_id');
        $model = $this->_lowerPriceFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getLowerpriceId()) {
                $this->messageManager->addError(
                    __(
                        'This LowerPrice no longer exists.'
                    )
                );
                $this->_redirect('*/*/');
                return;
            }
        }
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('lowerprice', $model);
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
