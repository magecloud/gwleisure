<?php

namespace MageArray\LowerPrice\Controller\Adminhtml\LowerPrice;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 * @package MageArray\LowerPrice\Controller\Adminhtml\LowerPrice
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $lowerPriceIds = $this->getRequest()->getParam('lowerprice');
        if (!is_array($lowerPriceIds) || empty($lowerPriceIds)) {
            $this->messageManager->addError(__('Please select lowerprices.'));
        } else {
            try {
                
                $collection = $this->_objectManager->create(\MageArray\LowerPrice\Model\LowerPrice::Class)->getCollection()->addFieldToFilter('lowerprice_id', ['in' => $lowerPriceIds]);//phpcs:ignore
                $collection->walk('delete');
                $this->messageManager->addSuccess(
                    __(
                        'A total of %1 record(s) have been deleted.',
                        count($lowerPriceIds)
                    )
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
}
