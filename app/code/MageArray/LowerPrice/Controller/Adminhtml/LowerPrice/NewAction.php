<?php

namespace MageArray\LowerPrice\Controller\Adminhtml\LowerPrice;

/**
 * Class NewAction
 * @package MageArray\LowerPrice\Controller\Adminhtml\LowerPrice
 */

class NewAction extends \MageArray\LowerPrice\Controller\Adminhtml\LowerPrice
{

    /**
     *
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
