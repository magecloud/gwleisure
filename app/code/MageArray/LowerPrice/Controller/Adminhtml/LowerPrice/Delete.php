<?php

namespace MageArray\LowerPrice\Controller\Adminhtml\LowerPrice;

/**
 * Class Delete
 * @package MageArray\LowerPrice\Controller\Adminhtml\LowerPrice
 */

class Delete extends \Magento\Backend\App\Action
{

    /**
     * @return mixed
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('lowerprice_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create(
                    \MageArray\LowerPrice\Model\LowerPrice::Class
                );
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(
                    __(
                        'The LowerPrice has been deleted.'
                    )
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['lowerprice_id' => $id]
                );
            }
        }
        $this->messageManager->addError(__('We can\'t find a LowerPrice to delete.'));
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('MageArray_LowerPrice::lowerprice');
    }
}
