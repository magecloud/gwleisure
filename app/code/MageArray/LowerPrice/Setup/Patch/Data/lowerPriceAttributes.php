<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MageArray\LowerPrice\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Patch is mechanism, that allows to do atomic upgrade data changes
 */
class lowerPriceAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    private $eavSetupFactory;
    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'found_lower_price',
            [
                'group' => 'Lower Price Options',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Lower Price',
                'input' => 'boolean',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'group' => 'Lower Price Options',
                'option' => ['values' => ['']],
                'sort_order' => 1,
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'auto_approved_inquiries',
            [
                'group' => 'Lower Price Options',
                /* Group name in which you want to display your custom attribute */
                'type' => 'int',
                /* Data type in which formate your value save in database*/
                'backend' => '',
                'frontend' => '',
                'label' => 'Auto approved inquiries',
                /* lablel of your attribute*/
                'input' => 'select',
                'class' => '',
                'source' => \MageArray\LowerPrice\Model\Config\Source\Boolean::Class,
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '0',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'sort_order' => 2,
            ]
        );

        // Price Type
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'threshold_price_type',
            [
                'group' => 'Lower Price Options',
                /* Group name in which you want to display your custom attribute */
                'type' => 'varchar',
                /* Data type in which formate your value save in database*/
                'backend' => '',
                'frontend' => '',
                'label' => 'Price Type',
                /* lablel of your attribute*/
                'input' => 'select',
                'class' => '',
                'source' => \MageArray\LowerPrice\Model\Config\Source\PriceType::Class,
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'sort_order' => 3,
            ]
        );

        // Threshold Limit Price
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'threshold_limit',
            [
                'group' => 'Lower Price Options',
                /* Group name in which you want to display your custom attribute */
                'type' => 'varchar',
                /* Data type in which formate your value save in database*/
                'backend' => '',
                'frontend' => '',
                'label' => 'Threshold Limit Price',
                /* lablel of your attribute*/
                'input' => 'text',
                'class' => '',
                'source' => '',
                /* Source of your select type custom attribute options*/
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                /*Scope of your attribute */
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'sort_order' => 4,
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
