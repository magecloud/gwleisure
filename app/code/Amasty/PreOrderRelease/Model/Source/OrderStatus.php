<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Release for Magento 2 (System)
*/

declare(strict_types=1);

namespace Amasty\PreOrderRelease\Model\Source;

use Magento\Sales\Model\Config\Source\Order\Status;

class OrderStatus extends Status
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        array_shift($options);

        return $options;
    }
}
