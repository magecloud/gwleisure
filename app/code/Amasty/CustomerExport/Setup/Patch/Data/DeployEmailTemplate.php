<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Setup\Patch\Data;

use Amasty\ExportPro\Setup\Model\EmailTemplateDeployer;
use Magento\Framework\App\Area;
use Magento\Framework\Module\ResourceInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class DeployEmailTemplate implements DataPatchInterface
{
    /**
     * @var EmailTemplateDeployer
     */
    private $emailTemplateDeployer;

    /**
     * @var ResourceInterface
     */
    private $moduleResource;

    public function __construct(
        EmailTemplateDeployer $emailTemplateDeployer,
        ResourceInterface $moduleResource
    ) {
        $this->emailTemplateDeployer = $emailTemplateDeployer;
        $this->moduleResource = $moduleResource;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply(): DeployEmailTemplate
    {
        $setupDataVersion = $this->moduleResource->getDataVersion('Amasty_CustomerExport');

        // Check if module was already installed or not.
        // If setup_version present in DB then we don't need to install fixtures, because setup_version is a marker.
        if (!$setupDataVersion) {
            $this->emailTemplateDeployer->execute([
                'Amasty Export Customers: Export failed',
                'amcustomerexport_admin_email_template',
                'amcustomerexport/admin_email/template',
                Area::AREA_ADMINHTML
            ]);
        }

        return $this;
    }
}
