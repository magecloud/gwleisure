<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class ExportEvents implements OptionSourceInterface
{
    public const CUSTOMER_REGISTER_ID = 1;

    public const CUSTOMER_REGISTER = 'customer_register_success';

    /**
     * @var int[]
     */
    private $events = [
        self::CUSTOMER_REGISTER => self::CUSTOMER_REGISTER_ID
    ];

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::CUSTOMER_REGISTER_ID,
                'label'=> __('Customer Registration (Event: %1)', self::CUSTOMER_REGISTER)
            ]
        ];
    }

    public function getEventIdByName($eventName)
    {
        return $this->events[strtolower($eventName)] ?? false;
    }
}
