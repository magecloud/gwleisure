<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\OptionSource;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\OptionSourceInterface;

class Tables implements OptionSourceInterface
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    public function __construct(
        ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    public function toOptionArray()
    {
        $tables = [];

        foreach ($this->resource->getConnection()->getTables() as $table) {
            $tables[] = ['label' => $table, 'value' => $table];
        }

        return $tables;
    }
}
