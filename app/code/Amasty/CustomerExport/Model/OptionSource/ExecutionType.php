<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class ExecutionType implements OptionSourceInterface
{
    public const MANUAL = 'manual';
    public const CRON = 'cron';
    public const EVENT = 'event';
    public const EVENT_AND_CRON = 'event_cron';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::MANUAL, 'label'=> __('Manual')],
            ['value' => self::CRON, 'label'=> __('Cron')],
            ['value' => self::EVENT, 'label'=> __('Event')],
            ['value' => self::EVENT_AND_CRON, 'label' => __('Event and Cron')]
        ];
    }
}
