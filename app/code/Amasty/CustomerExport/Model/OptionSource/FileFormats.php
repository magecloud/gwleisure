<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\OptionSource;

use Amasty\ExportCore\Export\Template\TemplateConfig;
use Magento\Framework\Data\OptionSourceInterface;

class FileFormats implements OptionSourceInterface
{
    /**
     * @var TemplateConfig
     */
    private $templateConfig;

    public function __construct(
        TemplateConfig $templateConfig
    ) {
        $this->templateConfig = $templateConfig;
    }

    public function toOptionArray()
    {
        $result = [];

        foreach ($this->templateConfig->all() as $code => $templateConfig) {
            $result[] = [
                'value' => $code,
                'label' => $templateConfig['name']
            ];
        }

        return $result;
    }
}
