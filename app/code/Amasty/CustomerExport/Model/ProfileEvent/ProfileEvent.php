<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\ProfileEvent;

use Amasty\CustomerExport\Api\Data\ProfileEventInterface;
use Magento\Framework\Model\AbstractModel;

class ProfileEvent extends AbstractModel implements ProfileEventInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    public const ID = 'id';
    public const PROFILE_ID = 'profile_id';
    public const EVENT_NAME = 'event_name';
    /**#@-*/

    public function _construct()
    {
        parent::_construct();
        $this->_init(ResourceModel\ProfileEvent::class);
        $this->setIdFieldName(self::ID);
    }

    public function getProfileId(): int
    {
        return (int)$this->getData(self::PROFILE_ID);
    }

    public function setProfileId(int $profileId): ProfileEventInterface
    {
        $this->setData(self::PROFILE_ID, $profileId);

        return $this;
    }

    public function getEventName(): int
    {
        return (int)$this->getData(self::EVENT_NAME);
    }

    public function setEventName(int $eventName): ProfileEventInterface
    {
        $this->setData(self::EVENT_NAME, $eventName);

        return $this;
    }
}
