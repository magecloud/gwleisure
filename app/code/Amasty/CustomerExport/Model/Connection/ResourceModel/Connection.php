<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\Connection\ResourceModel;

use Amasty\CustomerExport\Model\Connection\Connection as CoonectionModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Connection extends AbstractDb
{
    public const TABLE_NAME = 'amasty_customer_export_connection';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, CoonectionModel::ID);
    }
}
