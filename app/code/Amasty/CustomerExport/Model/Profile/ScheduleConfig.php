<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Model\Profile;

class ScheduleConfig
{
    public const DATAPROVIDER_TYPE = 'basic';
}
