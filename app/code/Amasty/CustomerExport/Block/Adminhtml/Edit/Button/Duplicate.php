<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Block\Adminhtml\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Duplicate extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData(): array
    {
        if (!$this->getProfileId() || $this->isDuplicate()) {
            return [];
        }

        return [
            'label' => __('Duplicate'),
            'class' => 'duplicate',
            'on_click' => sprintf("location.href = '%s';", $this->getDuplicateUrl()),
            'sort_order' => 40
        ];
    }

    public function getDuplicateUrl()
    {
        return $this->getUrl('*/*/duplicate', ['id' => $this->getProfileId()]);
    }
}
