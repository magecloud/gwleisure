<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Controller\Adminhtml\History;

class MassDelete extends \Amasty\ExportPro\Controller\Adminhtml\History\MassDelete
{
    public const ADMIN_RESOURCE = 'Amasty_CustomerExport::customer_export_history';
}
