<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Customers for Magento 2
 */

namespace Amasty\CustomerExport\Controller\Adminhtml\Connection;

use Amasty\CustomerExport\Api\ConnectionRepositoryInterface;
use Amasty\CustomerExport\Model\Connection\Connection;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;

class Edit extends Action
{
    public const ADMIN_RESOURCE = 'Amasty_CustomerExport::customer_export_connections';

    /**
     * @var ConnectionRepositoryInterface
     */
    private $connectionRepository;

    public function __construct(
        Action\Context $context,
        ConnectionRepositoryInterface $connectionRepository
    ) {
        parent::__construct($context);
        $this->connectionRepository = $connectionRepository;
    }

    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Amasty_CustomerExport::customer_export_connections');

        if ($connectionId = (int)$this->getRequest()->getParam(Connection::ID)) {
            if ($this->connectionRepository->getById($connectionId)) {
                $resultPage->getConfig()->getTitle()->prepend(__('Edit 3rd Party Link'));
            } else {
                $this->messageManager->addErrorMessage(__('This 3rd party link no longer exists.'));

                return $this->resultRedirectFactory->create()->setPath('*/*/index');
            }
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New 3rd Party Link'));
        }

        return $resultPage;
    }
}
