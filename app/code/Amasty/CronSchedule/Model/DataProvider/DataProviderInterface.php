<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cron Schedule for Magento 2 (System)
 */

namespace Amasty\CronSchedule\Model\DataProvider;

use Amasty\CronSchedule\Api\Data\ScheduleInterface;
use Magento\Framework\App\RequestInterface;

interface DataProviderInterface
{
    public function getData(string $jobType, int $jobId);

    public function getMeta(string $jobType, array $arguments = []);

    public function prepareSchedule(
        RequestInterface $request,
        string $jobType,
        ?int $jobId
    ): ScheduleInterface;
}
