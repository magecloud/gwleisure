<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Customer Export Entity for Magento 2 (System)
 */

namespace Amasty\CustomerExportEntity\Export\SourceOption\Newsletter\Subscriber;

use Magento\Framework\Data\OptionSourceInterface;

class StatusOptions implements OptionSourceInterface
{
    public const STATUS_SUBSCRIBED = 1;
    public const STATUS_NOT_ACTIVE = 2;
    public const STATUS_UNSUBSCRIBED = 3;
    public const STATUS_UNCONFIRMED = 4;

    public function toOptionArray(): array
    {
        $optionArray = [];
        foreach ($this->toArray() as $value => $label) {
            $optionArray[] = ['value' => $value, 'label' => $label];
        }
        return $optionArray;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::STATUS_SUBSCRIBED => __('Subscribed'),
            self::STATUS_NOT_ACTIVE => __('Not Activated'),
            self::STATUS_UNSUBSCRIBED => __('Unsubscribed'),
            self::STATUS_UNCONFIRMED => __('Unconfirmed')
        ];
    }
}
