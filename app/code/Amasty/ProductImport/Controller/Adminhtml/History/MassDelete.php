<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Controller\Adminhtml\History;

class MassDelete extends \Amasty\ImportPro\Controller\Adminhtml\History\MassDelete
{
    public const ADMIN_RESOURCE = 'Amasty_ProductImport::product_import_history';
}
