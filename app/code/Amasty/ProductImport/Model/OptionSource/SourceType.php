<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\OptionSource;

use Amasty\ImportCore\Api\Source\SourceConfigInterface;
use Magento\Framework\Data\OptionSourceInterface;

class SourceType implements OptionSourceInterface
{
    /**
     * @var SourceConfigInterface
     */
    private $sourceConfig;

    public function __construct(
        SourceConfigInterface $sourceConfig
    ) {
        $this->sourceConfig = $sourceConfig;
    }

    public function toOptionArray()
    {
        $result = [];

        foreach ($this->sourceConfig->all() as $code => $sourceConfig) {
            $result[] = [
                'value' => $code,
                'label' => $sourceConfig['name']
            ];
        }

        return $result;
    }
}
