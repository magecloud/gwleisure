<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\UrlRewrite\Product;

use Amasty\Base\Utils\BatchLoader;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Psr\Log\LoggerInterface;

/**
 * Regenerate Url Rewrites for products
 */
class Regenerator
{
    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var UrlPersistInterface
     */
    private $urlPersist;

    /**
     * @var ProductUrlRewriteGenerator
     */
    private $productUrlRewriteGenerator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var BatchLoader
     */
    private $batchLoader;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        UrlPersistInterface $urlPersist,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        LoggerInterface $logger,
        BatchLoader $batchLoader
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->urlPersist = $urlPersist;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->logger = $logger;
        $this->batchLoader = $batchLoader;
    }

    public function regenerate(array $entityIds): void
    {
        /** @var Product $product */
        foreach ($this->getProductCollection($entityIds) as $product) {
            $this->urlPersist->deleteByData([
                UrlRewrite::ENTITY_ID => $product->getId(),
                UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                UrlRewrite::REDIRECT_TYPE => 0,
                UrlRewrite::STORE_ID => $product->getStoreId(),
            ]);

            try {
                $product->setData('url_path', null);
                $urls = $this->productUrlRewriteGenerator->generate($product);

                foreach ($urls as $url) {
                    $this->urlPersist->deleteByData([
                        UrlRewrite::REQUEST_PATH => $url->getRequestPath(),
                        UrlRewrite::STORE_ID => $url->getStoreId()
                    ]);
                }

                $this->urlPersist->replace($urls);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    private function getProductCollection(array $productsFilter = []): \Generator
    {
        $productCollection = $this->productCollectionFactory->create()
            ->addAttributeToSelect(['url_path', 'url_key']);

        if (count($productsFilter) > 0) {
            $productIdentifier = $productCollection->getProductEntityMetadata()->getLinkField();
            $productCollection->addFieldToFilter($productIdentifier, ['in' => $productsFilter]);
        }

        return $this->batchLoader->load($productCollection);
    }
}
