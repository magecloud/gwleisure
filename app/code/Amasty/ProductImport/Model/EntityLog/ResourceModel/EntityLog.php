<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\EntityLog\ResourceModel;

use Amasty\ProductImport\Model\EntityLog\EntityLog as EntityLogModel;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class EntityLog extends AbstractDb
{
    public const TABLE_NAME = 'amasty_product_import_entity_log';

    public function __construct(
        Context $context,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        if ($connectionName === null) {
            $metadata = $metadataPool->getMetadata(ProductInterface::class);
            $connectionName = $metadata->getEntityConnectionName();
        }

        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, EntityLogModel::ID);
    }

    /**
     * Delete entity log entries of given process identity
     *
     * @param string $processIdentity
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteEntriesByProcessIdentity(string $processIdentity)
    {
        $this->getConnection()
            ->delete(
                $this->getMainTable(),
                [EntityLogModel::PROCESS_IDENTITY . ' = ?' => $processIdentity]
            );

        return $this;
    }

    /**
     * @param array $entityIds
     * @param string $processIdentity
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function insertEntries(array $entityIds, string $processIdentity)
    {
        $data = [];
        foreach ($entityIds as $entityId) {
            $data[] = [
                EntityLogModel::ENTITY_ID => $entityId,
                EntityLogModel::PROCESS_IDENTITY => $processIdentity
            ];
        }

        $this->getConnection()->insertOnDuplicate(
            $this->getMainTable(),
            $data,
            [EntityLogModel::ENTITY_ID, EntityLogModel::PROCESS_IDENTITY]
        );
    }
}
