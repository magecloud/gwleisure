<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\EntityLog;

use Amasty\ProductImport\Model\EntityLog\ResourceModel\EntityLog as EntityLogResource;
use Magento\Framework\Exception\CouldNotSaveException;

class EntityLogManager
{
    /**
     * @var EntityLogResource
     */
    private $entityLogResource;

    public function __construct(EntityLogResource $entityLogResource)
    {
        $this->entityLogResource = $entityLogResource;
    }

    /**
     * Add new entity log entries
     *
     * @param array $entityIds
     * @param string $processIdentity
     * @throws CouldNotSaveException
     */
    public function addEntries(array $entityIds, string $processIdentity): void
    {
        try {
            $this->entityLogResource->insertEntries($entityIds, $processIdentity);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Unable to save new log entries batch. Error: %1', $e->getMessage())
            );
        }
    }

    /**
     * Cleanup log entries of given process identity
     *
     * @param string $processIdentity
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function cleanup(string $processIdentity): void
    {
        $this->entityLogResource->deleteEntriesByProcessIdentity($processIdentity);
    }
}
