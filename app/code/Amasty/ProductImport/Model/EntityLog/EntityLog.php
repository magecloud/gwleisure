<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\EntityLog;

use Magento\Framework\Model\AbstractModel;

class EntityLog extends AbstractModel
{
    public const ID = 'id';
    public const ENTITY_ID = 'entity_id';
    public const PROCESS_IDENTITY = 'process_identity';
    public const CREATED_AT = 'created_at';

    public function _construct()
    {
        parent::_construct();
        $this->_init(ResourceModel\EntityLog::class);
        $this->setIdFieldName(self::ID);
    }
}
