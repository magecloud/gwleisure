<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\Profile;

class ScheduleConfig
{
    public const DATAPROVIDER_TYPE = 'basic';
}
