<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\Profile\ResourceModel;

use Amasty\ProductImport\Model\Profile\Profile as ProfileModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Profile extends AbstractDb
{
    public const TABLE_NAME = 'amasty_product_import_profile';

    /**
     * @var array[]
     */
    protected $_serializableFields = [
        ProfileModel::PRODUCT_ACTIONS => [null, []]
    ];

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ProfileModel::ID);
    }
}
