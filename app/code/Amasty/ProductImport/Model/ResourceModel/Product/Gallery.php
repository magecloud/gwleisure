<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Model\ResourceModel\Product;

use Magento\Catalog\Model\ResourceModel\Product\Gallery as CatalogGallery;

class Gallery extends CatalogGallery
{
    public function isImageExists(int $productId, string $image, array $storeIds, ?array $attributeIds): bool
    {
        $select = $this->getConnection()->select()->from(
            ['gallery' => $this->getMainTable()],
            ['value']
        )->joinLeft(
            ['attr' => $this->getTable('eav_attribute')],
            'gallery.attribute_id = attr.attribute_id',
            []
        )->joinLeft(
            ['value' => $this->getTable(self::GALLERY_VALUE_TABLE)],
            'gallery.value_id = value.value_id',
            []
        )->where(
            $this->metadata->getLinkField() . ' = ?',
            $productId
        )->where(
            'value.store_id IN (?)',
            $storeIds,
            \Zend_Db::INT_TYPE
        )->where(
            'gallery.attribute_id IN (?)',
            $attributeIds
        )->where(
            'gallery.value = ?',
            $image
        );

        return count($this->getConnection()->fetchAll($select)) > 0;
    }
}
