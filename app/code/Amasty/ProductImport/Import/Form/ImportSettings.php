<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Form;

use Amasty\ImportCore\Api\Config\EntityConfigInterface;
use Amasty\ImportCore\Api\Config\ProfileConfigInterface;
use Amasty\ImportCore\Api\FormInterface;
use Amasty\ImportCore\Import\Form\Fields\IdentifiersCollector;
use Amasty\ImportCore\Import\OptionSource\ValidationStrategy;
use Amasty\ProductImport\Ui\DataProvider\Profile\CompositeFormType;
use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\UrlInterface;

class ImportSettings extends \Amasty\ImportCore\Import\Form\General
{
    public const CUSTOM_ENTITY_IDENTIFIER = 'custom_entity_identifier';
    public const CUSTOM_ATTRIBUTE = 'custom_attribute';

    /**
     * @var array
     */
    private $allowedAttributeTypes;

    /**
     * @var array
     */
    private $disallowedAttributeCodes;

    /**
     * @var IdentifiersCollector
     */
    private $identifiersCollector;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilder;

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    public function __construct(
        ValidationStrategy $validationStrategy,
        UrlInterface $url,
        IdentifiersCollector $identifiersCollector,
        RequestInterface $request,
        AttributeRepositoryInterface $attributeRepository = null, // TODO move to not optional
        SearchCriteriaBuilderFactory $searchCriteriaBuilder = null, // TODO move to not optional
        MetadataPool $metadataPool = null, // TODO move to not optional
        array $allowedAttributeTypes = [],
        array $disallowedAttributeCodes = []
    ) {
        parent::__construct($validationStrategy, $url);
        $this->identifiersCollector = $identifiersCollector;
        $this->request = $request;
        $this->attributeRepository =
            $attributeRepository ?? ObjectManager::getInstance()->get(AttributeRepositoryInterface::class);
        $this->searchCriteriaBuilder =
            $searchCriteriaBuilder ?? ObjectManager::getInstance()->get(SearchCriteriaBuilderFactory::class);
        $this->metadataPool = $metadataPool ?? ObjectManager::getInstance()->get(MetadataPool::class);
        $this->allowedAttributeTypes = $allowedAttributeTypes;
        $this->disallowedAttributeCodes = $disallowedAttributeCodes;
    }

    public function getMeta(EntityConfigInterface $entityConfig, array $arguments = []): array
    {
        $meta = parent::getMeta($entityConfig, $arguments);

        $options = array_merge(
            $this->identifiersCollector->collect($entityConfig),
            [['value' => self::CUSTOM_ATTRIBUTE, 'label' => __('Custom Attribute')->render()]]
        );
        $meta['import_behavior']['children']['entity_identifier']['arguments']['data']['config'] = [
            'label' =>  __('Product Identifier'),
            'component' => 'Amasty_ImportPro/js/form/element/entity-identifier',
            'visible' => true,
            'dataScope' => 'entity_identifier',
            'dataType' => 'select',
            'formElement' => 'select',
            'componentType' => 'select',
            'additionalClasses' => 'amimportcore-field',
            'sortOrder' => 15,
            'options' => $options,
            'switcherConfig' => [
                'enabled' => true,
                'rules'   => [
                    [
                        'value'   => $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField(),
                        'actions' => [
                            [
                                'target'   => 'index = ' . self::CUSTOM_ENTITY_IDENTIFIER,
                                'callback' => 'hide'
                            ]
                        ]
                    ],
                    [
                        'value'   => 'sku',
                        'actions' => [
                            [
                                'target'   => 'index = ' . self::CUSTOM_ENTITY_IDENTIFIER,
                                'callback' => 'hide'
                            ]
                        ]
                    ],
                    [
                        'value'   => self::CUSTOM_ATTRIBUTE,
                        'actions' => [
                            [
                                'target'   => 'index = ' . self::CUSTOM_ENTITY_IDENTIFIER,
                                'callback' => 'show'
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $meta['import_behavior']['children'][self::CUSTOM_ENTITY_IDENTIFIER]['arguments']['data']['config'] =
            $this->getCustomAttributeElement();

        $meta['import_behavior']['children']['form_type']['arguments']['data']['config']['value']
            = CompositeFormType::TYPE;

        if (!$this->request->getParam('id')) {
            $meta['import_behavior']['children']['autofill']['arguments']['data']['config'] = [
                'label' => __('Enable Autofill for Typical Use Cases'),
                'dataType' => 'boolean',
                'prefer' => 'toggle',
                'visible' => true,
                'dataScope' => 'autofill',
                'formElement' => 'checkbox',
                'componentType' => 'field',
                'additionalClasses' => 'amimportcore-field',
                'sortOrder' => 11,
                'valueMap' => ['true' => 1, 'false' => 0],
                'default' => 0,
                'tooltipTpl' => 'Amasty_ImportCore/form/element/tooltip',
                'tooltip' => [
                    'description' => __(
                        'If enabled, Fields Configuration will be automatically filled in'
                        . ' with the settings to perform the typical use cases for importing'
                        . ' products from third-party systems.'
                    )
                ]
            ];
        }

        return $meta;
    }

    private function getCustomAttributeElement(): array
    {
        $builder = $this->searchCriteriaBuilder->create();
        $criteria = $builder
            ->addFilter(AttributeInterface::FRONTEND_INPUT, $this->allowedAttributeTypes, 'in')
            ->addFilter(AttributeInterface::ATTRIBUTE_CODE, $this->disallowedAttributeCodes, 'nin')
            ->addFilter(EavAttributeInterface::IS_VISIBLE, 1)
            ->create();
        $this->attributeRepository->getList(Product::ENTITY, $criteria);
        $options = [['label' => __('Please Select...'), 'value' => '']];
        foreach ($this->attributeRepository->getList(Product::ENTITY, $criteria)->getItems() as $attribute) {
            $options[] = ['value' => $attribute->getAttributeCode(), 'label' => $attribute->getDefaultFrontendLabel()];
        }

        return [
            'label' => __('Product Identifier Attribute'),
            'visible' => true,
            'dataScope' => self::CUSTOM_ENTITY_IDENTIFIER,
            'source' => self::CUSTOM_ENTITY_IDENTIFIER,
            'validation' => [
                'required-entry' => true
            ],
            'dataType' => 'select',
            'sortOrder' => 18,
            'prefix' => 'source_',
            'formElement' => 'select',
            'componentType' => 'select',
            'options' => $options
        ];
    }

    public function getData(ProfileConfigInterface $profileConfig): array
    {
        $importBehavior = parent::getData($profileConfig);

        if ($entityIdentifier = $profileConfig->getEntityIdentifier()) {
            $importBehavior['entity_identifier'] = $entityIdentifier;
        }
        if ($customEntityIdentifier = $profileConfig->getExtensionAttributes()->getCustomEntityIdentifier()) {
            $importBehavior[self::CUSTOM_ENTITY_IDENTIFIER] = $customEntityIdentifier;
            $importBehavior['entity_identifier'] = self::CUSTOM_ATTRIBUTE;
        }

        return ['import_behavior' => $importBehavior];
    }

    public function prepareConfig(ProfileConfigInterface $profileConfig, RequestInterface $request): FormInterface
    {
        $params = $request->getParams();
        $importBehavior = $params['import_behavior'] ?? [];
        unset($params['import_behavior']);
        $params = array_merge_recursive($params, $importBehavior);
        $request->setParams($params);

        parent::prepareConfig($profileConfig, $request);

        if ($entityIdentifier = $request->getParam('entity_identifier')) {
            $profileConfig->setEntityIdentifier($entityIdentifier);
        }
        if ($entityIdentifier === self::CUSTOM_ATTRIBUTE
            && $customEntityIdentifier = $request->getParam(self::CUSTOM_ENTITY_IDENTIFIER)
        ) {
            $profileConfig->getExtensionAttributes()->setCustomEntityIdentifier($customEntityIdentifier);
            $profileConfig->setEntityIdentifier('');
        }

        return $this;
    }
}
