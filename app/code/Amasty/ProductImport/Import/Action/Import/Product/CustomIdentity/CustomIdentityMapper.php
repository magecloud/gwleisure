<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Action\Import\Product\CustomIdentity;

use Amasty\ImportCore\Api\ImportProcessInterface;
use Amasty\ImportCore\Import\Source\SourceDataStructure;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Store\Model\Store;
use RuntimeException;

class CustomIdentityMapper
{
    public const PRODUCT_ATTRIBUTE_ENTITY_CODE = 'catalog_product_entity_attribute';

    /**
     * @var array
     */
    private $idAttributeValueMapping = [];

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    public function execute(ImportProcessInterface $importProcess)
    {
        $attributeValues = [];
        $customEntityIdentifier =
            $importProcess->getProfileConfig()->getExtensionAttributes()->getCustomEntityIdentifier();
        $productsData = $importProcess->getData();
        foreach ($productsData as $productData) {
            $attributeValues[] = $this->getRowAttributeValue($productData, $customEntityIdentifier);
        }
        if ($attributeValues !== array_unique($attributeValues)) {
            throw new RuntimeException('Custom Identifier field value in the import file needs to be unique.');
        }
        $this->validate($importProcess, $attributeValues);

        $productCollection = $this->collectionFactory->create();
        $products = $productCollection->addAttributeToSelect(
            $customEntityIdentifier
        )->addAttributeToFilter($customEntityIdentifier, ['in' => $attributeValues])->getItems();
        $productIdentifier = $productCollection->getProductEntityMetadata()->getLinkField();
        foreach ($productsData as &$productData) {
            $customAttributeValue = $this->getRowAttributeValue($productData, $customEntityIdentifier);
            $productData[$productIdentifier] = null;
            foreach ($products as $item) {
                if ($customAttributeValue == $item->getData($customEntityIdentifier)) {
                    $productData[$productIdentifier] = $item->getId();
                }
            }
        }
        $importProcess->setData($productsData);
    }

    private function validate(ImportProcessInterface $importProcess, array $attributeValues): void
    {
        $customEntityIdentifier =
            $importProcess->getProfileConfig()->getExtensionAttributes()->getCustomEntityIdentifier();
        /** @var Collection $productCollection */
        $productCollection = $this->collectionFactory->create();
        $productCollection->addAttributeToSelect(
            $customEntityIdentifier
        )->addAttributeToFilter($customEntityIdentifier, ['in' => $attributeValues]);
        $productCollection->getSelect()->group($customEntityIdentifier)->having("COUNT(*) > 1");
        $result = $productCollection->getResource()->getConnection()->fetchAll($productCollection->getSelect());
        if ($result) {
            throw new RuntimeException('There are more than one product with imported attribute value.');
        }
    }

    private function getRowAttributeValue(array $productData, string $customEntityIdentifier): string
    {
        if (empty($productData[SourceDataStructure::SUB_ENTITIES_DATA_KEY][self::PRODUCT_ATTRIBUTE_ENTITY_CODE])) {
            throw new RuntimeException(
                'You need to configure Product Attribute in Fields Configuration '
                . ' section for using this attribute as Product Identifier.'
            );
        }
        $storeAttributeData =
            $productData[SourceDataStructure::SUB_ENTITIES_DATA_KEY][self::PRODUCT_ATTRIBUTE_ENTITY_CODE];
        $defaultStoreData = [];
        foreach ($storeAttributeData as $storeData) {
            if (isset($storeData['store_id']) && $storeData['store_id'] == Store::DEFAULT_STORE_ID) {
                $defaultStoreData = $storeData;
            }
        }
        if (empty($defaultStoreData)) {
            $defaultStoreData = reset($storeAttributeData);
        }
        if (empty($defaultStoreData[$customEntityIdentifier])) {
            throw new RuntimeException('Custom Identifier field cannot be empty.');
        }

        return $defaultStoreData[$customEntityIdentifier];
    }
}
