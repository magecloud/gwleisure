<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Action\Import\Product\ProductActions;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\ImportProcessInterface;
use Amasty\ProductImport\Model\UrlRewrite\Product\Regenerator;

class RegenerateUrlRewrite extends AbstractAction
{
    /**
     * @var Regenerator
     */
    private $regenerator;

    public function __construct(
        Regenerator $regenerator
    ) {
        $this->regenerator = $regenerator;
    }

    public function execute(ImportProcessInterface $importProcess): void
    {
        /** @var BehaviorResultInterface $productResult */
        $productResult = $importProcess->getProcessedEntityResult('catalog_product_entity');
        if (!$productResult) {
            return;
        }

        $affectedProductIds = $productResult->getAffectedIds();
        if (empty($affectedProductIds)) {
            return;
        }

        $this->regenerator->regenerate($affectedProductIds);
    }
}
