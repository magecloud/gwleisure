<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Action\Import\Product\ProductActions;

use Amasty\ProductImport\Api\ProductActionInterface;

abstract class AbstractAction implements ProductActionInterface
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @inheritdoc
     */
    public function setOption(string $name, $value): ProductActionInterface
    {
        $this->options[$name] = $value;

        return $this;
    }
}
