<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Action\Import\Product\CustomIdentity;

use Amasty\ImportCore\Api\ActionInterface;
use Amasty\ImportCore\Api\ImportProcessInterface;

class CustomIdentityAction implements ActionInterface
{
    /**
     * @var CustomIdentityMapper
     */
    private $customIdentityMapper;

    public function __construct(
        CustomIdentityMapper $customIdentityMapper
    ) {
        $this->customIdentityMapper = $customIdentityMapper;
    }

    public function execute(ImportProcessInterface $importProcess): void
    {
        if ($importProcess->getProfileConfig()->getExtensionAttributes()->getCustomEntityIdentifier()) {
            $this->customIdentityMapper->execute($importProcess);
        }
    }

    //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock.DetectedFunction
    public function initialize(ImportProcessInterface $importProcess): void
    {
    }
}
