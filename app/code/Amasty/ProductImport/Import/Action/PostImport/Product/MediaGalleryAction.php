<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Action\PostImport\Product;

use Amasty\ImportCore\Api\ActionInterface;
use Amasty\ImportCore\Api\ImportProcessInterface;
use Amasty\ImportCore\Import\Config\EntityConfigProvider;
use Amasty\ImportCore\Import\Source\SourceDataStructure;
use Amasty\ProductImport\Model\ResourceModel\Product\Gallery;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\Store;

class MediaGalleryAction implements ActionInterface
{
    public const PRODUCT_ATTRIBUTE_ENTITY_CODE = 'catalog_product_entity_attribute';

    /**
     * @var array
     */
    private $uploadRows;

    /**
     * @var EntityConfigProvider
     */
    private $entityConfigProvider;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var Gallery
     */
    private $gallery;

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(
        EntityConfigProvider $entityConfigProvider,
        ProductAttributeRepositoryInterface $attributeRepository,
        Gallery $gallery,
        MetadataPool $metadataPool,
        ProductRepositoryInterface $productRepository
    ) {
        $this->entityConfigProvider = $entityConfigProvider;
        $this->attributeRepository = $attributeRepository;
        $this->gallery = $gallery;
        $this->metadataPool = $metadataPool;
        $this->productRepository = $productRepository;
    }

    public function initialize(ImportProcessInterface $importProcess): void
    {
        $fieldsConfig = $this->entityConfigProvider->get(self::PRODUCT_ATTRIBUTE_ENTITY_CODE)->getFieldsConfig();
        foreach ($fieldsConfig->getFields() as $field) {
            if ($field->isFile()) {
                $this->uploadRows[] = $field->getName();
            }
        }
    }

    public function execute(ImportProcessInterface $importProcess): void
    {
        foreach ($importProcess->getData() as $productData) {
            $this->processProductData($importProcess, $productData);
        }
    }

    private function processProductData(ImportProcessInterface $importProcess, array $productData)
    {
        $productAttributesData =
            $productData[SourceDataStructure::SUB_ENTITIES_DATA_KEY][self::PRODUCT_ATTRIBUTE_ENTITY_CODE] ?? [];
        if (empty($productAttributesData) || empty($productData['sku'])) {
            return;
        }
        $position = 0;
        foreach ($productAttributesData as $attributesData) {
            foreach ($this->uploadRows as $uploadRow) {
                if (!empty($attributesData[$uploadRow])) {
                    try {
                        $storeId = $attributesData['store_id'] ?? Store::DEFAULT_STORE_ID;
                        $productSku = $productData['sku'];
                        $linkField = $this->getProductEntityLinkField();
                        $productId = (int)$this->productRepository->get($productSku)->getData($linkField);
                        $attributeId = $this->getMediaGalleryAttributeId();
                        $uploadedFileName = $attributesData[$uploadRow];
                        if ($this->gallery->isImageExists($productId, $uploadedFileName, [$storeId], [$attributeId])) {
                            continue;
                        }

                        $galleryData = [
                            'attribute_id' => $attributeId,
                            'store_id' => $storeId,
                            'value' => $uploadedFileName,
                            $linkField => $productId
                        ];
                        $lastId = $this->gallery->insertGallery($galleryData);

                        $galleryValueInStoreData = [
                            'value_id' => $lastId,
                            'label' => $attributesData[$uploadRow . '_label'] ?? '',
                            'position' => ++$position,
                            'disabled' => '0',
                            $linkField => $productId
                        ];
                        $this->gallery->insertGalleryValueInStore($galleryValueInStoreData);

                        $this->gallery->bindValueToEntity($lastId, $productId);
                    } catch (LocalizedException $e) {
                        $importProcess->addErrorMessage($e->getMessage());
                    }
                }
            }
        }
    }

    private function getMediaGalleryAttributeId(): int
    {
        return (int)$this->attributeRepository->get('media_gallery')->getAttributeId();
    }

    private function getProductEntityLinkField(): string
    {
        return $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField();
    }
}
