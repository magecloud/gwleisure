<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Products for Magento 2
 */

namespace Amasty\ProductImport\Import\Action\PostImport\Product;

use Amasty\ProductImport\Api\ProductActionInterface;
use Amasty\ProductImport\Import\Action\Import\Product\ProductActions as BaseProductActions;

class ProductActions extends BaseProductActions
{
    /**
     * @var string
     */
    protected $actionGroup = ProductActionInterface::GROUP_FULL_SET;
}
