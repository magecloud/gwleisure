<?php

namespace Amasty\CustomTabs\Model\Tabs\Indexer;

use Magento\Framework\Indexer\AbstractProcessor;

class ProductProcessor extends AbstractProcessor
{
    /**
     * Indexer id
     */
    public const INDEXER_ID = 'amasty_tabs_product_tab';
}
