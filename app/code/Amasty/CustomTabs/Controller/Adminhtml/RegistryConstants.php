<?php

namespace Amasty\CustomTabs\Controller\Adminhtml;

use Amasty\CustomTabs\Api\Data\TabsInterface;

class RegistryConstants
{
    /**#@+
     * Constants defined for dataPersistor
     */
    public const TAB_DATA = 'amcustomtabs_data';
    /**#@-*/
}
