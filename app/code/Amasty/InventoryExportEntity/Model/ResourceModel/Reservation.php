<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Inventory Export Entity for Magento 2 (System)
 */

namespace Amasty\InventoryExportEntity\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Reservation extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('inventory_reservation', 'reservation_id');
    }
}
