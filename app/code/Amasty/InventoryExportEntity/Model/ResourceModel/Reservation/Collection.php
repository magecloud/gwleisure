<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Inventory Export Entity for Magento 2 (System)
 */

namespace Amasty\InventoryExportEntity\Model\ResourceModel\Reservation;

use Amasty\InventoryExportEntity\Model\ResourceModel\Reservation as ReservationResource;
use Magento\Framework\DataObject;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(DataObject::class, ReservationResource::class);
    }
}
