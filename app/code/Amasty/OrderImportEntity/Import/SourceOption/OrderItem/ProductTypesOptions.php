<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Import Entity for Magento 2 (System)
 */

namespace Amasty\OrderImportEntity\Import\SourceOption\OrderItem;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Catalog\Api\ProductTypeListInterface;

class ProductTypesOptions implements OptionSourceInterface
{
    /**
     * @var ProductTypeListInterface
     */
    private $productTypeList;

    public function __construct(ProductTypeListInterface $productTypeList)
    {
        $this->productTypeList = $productTypeList;
    }

    public function toOptionArray(): array
    {
        $result = [];
        if ($productTypes = $this->productTypeList->getProductTypes()) {
            foreach ($productTypes as $productType) {
                $result[] = ['value' => $productType->getName(), 'label' => $productType->getLabel()];
            }
        }

        return $result;
    }
}
