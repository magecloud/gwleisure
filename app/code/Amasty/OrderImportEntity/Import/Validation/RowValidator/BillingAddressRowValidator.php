<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Import Entity for Magento 2 (System)
 */

namespace Amasty\OrderImportEntity\Import\Validation\RowValidator;

class BillingAddressRowValidator extends AddressRowValidator
{
    protected function getAddressType(): string
    {
        return 'billing';
    }
}
