<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Sales Rule Import Entity for Magento 2 (system)
 */

namespace Amasty\CartPriceRuleImportEntity\Import\Validation;

use Amasty\ImportCore\Api\Validation\FieldValidatorInterface;
use Magento\Framework\Data\OptionSourceInterface;

class OptionValueValidator implements FieldValidatorInterface
{
    /**
     * @var OptionSourceInterface
     */
    private $optionSource;

    /**
     * @var array|null
     */
    private $optionsList = null;

    /**
     * @var array
     */
    private $validationResult = [];

    public function __construct(array $config)
    {
        $optionSource = $config['optionSource'] ?? null;

        if (!$optionSource instanceof OptionSourceInterface) {
            throw new \LogicException(
                sprintf('Option Source must implement %s', OptionSourceInterface::class)
            );
        }

        $this->optionSource = $optionSource;
    }

    public function validate(array $row, string $field): bool
    {
        if (isset($row[$field])) {
            $optionValue = trim($row[$field]);

            if (!empty($optionValue)) {
                if (!isset($this->validationResult[$optionValue])) {
                    $options = $this->getOptionsList();
                    $this->validationResult[$optionValue] = in_array($optionValue, $options);
                }

                return $this->validationResult[$optionValue];
            }
        }

        return true;
    }

    private function getOptionsList(): array
    {
        if ($this->optionsList) {
            return $this->optionsList;
        }

        return $this->optionsList = array_column($this->optionSource->toOptionArray(), 'value');
    }
}
