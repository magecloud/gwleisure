<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Setup\Patch\Data;

use Amasty\OrderExport\Setup\Module\Management as SetupModuleManagement;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class DeleteDbVersion implements DataPatchInterface
{
    public const OLD_MODULE_NAME = 'Amasty_Orderexport';

    /**
     * @var SetupModuleManagement
     */
    private $setupModuleManagement;

    public function __construct(SetupModuleManagement $setupModuleManagement)
    {
        $this->setupModuleManagement = $setupModuleManagement;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $this->setupModuleManagement->assertIsDisabled(self::OLD_MODULE_NAME);
        $this->setupModuleManagement->deleteDbVersion(self::OLD_MODULE_NAME);
    }
}
