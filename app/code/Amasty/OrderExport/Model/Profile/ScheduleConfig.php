<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Model\Profile;

class ScheduleConfig
{
    public const DATAPROVIDER_TYPE = 'basic';
}
