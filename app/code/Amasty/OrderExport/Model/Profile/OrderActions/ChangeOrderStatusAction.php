<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Model\Profile\OrderActions;

use Amasty\OrderExport\Api\OrderActionInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class ChangeOrderStatusAction implements OrderActionInterface
{
    public function execute(OrderInterface $order, array $actionData = [])
    {
        if ($value = $actionData['value'] ?? null) {
            $order->setStatus($value);
        }
    }
}
