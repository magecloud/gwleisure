<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Model\Profile;

use Amasty\OrderExport\Api\OrderActionInterface;

class OrderActionResolver
{
    /**
     * @var array
     */
    protected $actions;

    public function __construct(
        array $actions = []
    ) {
        $this->actions = $actions;
    }

    public function resolve(string $type = ''): ?OrderActionInterface
    {
        $action = null;

        if (key_exists($type, $this->actions)) {
            $action = $this->actions[$type];
        }

        return $action;
    }
}
