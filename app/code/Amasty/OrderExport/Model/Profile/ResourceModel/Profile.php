<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Model\Profile\ResourceModel;

use Amasty\OrderExport\Model\Profile\Profile as ProfileModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Profile extends AbstractDb
{
    public const TABLE_NAME = 'amasty_order_export_profile';

    /**
     * @var array[]
     */
    protected $_serializableFields = [
        ProfileModel::ORDER_ACTIONS => [null, []]
    ];

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ProfileModel::ID);
    }
}
