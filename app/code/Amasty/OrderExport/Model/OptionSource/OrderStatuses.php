<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\Order\Config;

class OrderStatuses implements OptionSourceInterface
{
    /**
     * @var Config
     */
    private $orderConfig;

    public function __construct(
        Config $orderConfig
    ) {
        $this->orderConfig = $orderConfig;
    }

    public function toOptionArray()
    {
        $values = [['value' => '', 'label' => __('-- Please Select --')]];

        foreach ($this->orderConfig->getStatuses() as $key => $status) {
            $values[] = [
                'value' => $key,
                'label' => $status
            ];
        }

        return $values;
    }
}
