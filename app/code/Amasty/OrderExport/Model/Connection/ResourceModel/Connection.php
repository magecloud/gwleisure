<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Model\Connection\ResourceModel;

use Amasty\OrderExport\Model\Connection\Connection as CoonectionModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Connection extends AbstractDb
{
    public const TABLE_NAME = 'amasty_order_export_connection';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, CoonectionModel::ID);
    }
}
