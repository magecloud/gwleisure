<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Controller\Adminhtml\Connection;

use Amasty\OrderExport\Api\ConnectionRepositoryInterface;
use Amasty\OrderExport\Model\Connection\Connection;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

class Delete extends Action
{
    public const ADMIN_RESOURCE = 'Amasty_OrderExport::order_export_connections';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ConnectionRepositoryInterface
     */
    private $repository;

    public function __construct(
        Action\Context $context,
        ConnectionRepositoryInterface $repository,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->repository = $repository;
    }

    public function execute()
    {
        $id = (int)$this->getRequest()->getParam(Connection::ID);

        if ($id) {
            try {
                $this->repository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The connection has been deleted.'));

                return $this->resultRedirectFactory->create()->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(
                    __('Can\'t delete the connection right now. Please review the log and try again.')
                );
                $this->logger->critical($e);
            }

            return $this->resultRedirectFactory->create()->setPath('*/*/edit', [Connection::ID => $id]);
        } else {
            $this->messageManager->addErrorMessage(__('Can\'t find a resolution to delete.'));
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
