<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Controller\Adminhtml\History;

use Amasty\ExportPro\Model\History\Repository;
use Amasty\OrderExport\Model\ModuleType;
use Magento\Backend\App\Action;

class Clear extends Action
{
    public const ADMIN_RESOURCE = 'Amasty_OrderExport::order_export_history';

    /**
     * @var Repository
     */
    private $repository;

    public function __construct(
        Action\Context $context,
        Repository $repository
    ) {
        parent::__construct($context);
        $this->repository = $repository;
    }

    public function execute()
    {
        $result = $this->repository->clearHistory(ModuleType::TYPE);

        if ($result) {
            $this->messageManager->addSuccessMessage(__("History has been cleared."));
        } else {
            $this->messageManager->addErrorMessage(__("Something went wrong."));
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
