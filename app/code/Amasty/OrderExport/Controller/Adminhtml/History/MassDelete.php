<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Controller\Adminhtml\History;

class MassDelete extends \Amasty\ExportPro\Controller\Adminhtml\History\MassDelete
{
    public const ADMIN_RESOURCE = 'Amasty_OrderExport::order_export_history';
}
