<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Orders for Magento 2
 */

namespace Amasty\OrderExport\Api;

use Magento\Sales\Api\Data\OrderInterface;

interface OrderActionInterface
{
    public function execute(OrderInterface $order, array $actionData = []);
}
