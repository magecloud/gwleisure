<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Search Import Entity for Magento 2 (System)
 */

namespace Amasty\SearchImportEntity\Import\Validation\RowValidator\SearchTerm;

use Amasty\ImportCore\Api\Validation\RowValidatorInterface;
use Amasty\ImportCore\Import\Utils\DuplicateFieldChecker;

class SearchTermValidator implements RowValidatorInterface
{
    /**
     * @var string|null
     */
    private $message;

    /**
     * @var DuplicateFieldChecker
     */
    private $duplicateFieldChecker;

    public function __construct(
        DuplicateFieldChecker $duplicateFieldChecker
    ) {
        $this->duplicateFieldChecker = $duplicateFieldChecker;
    }

    public function validate(array $row): bool
    {
        $this->message = null;
        if ($this->duplicateFieldChecker->hasDuplicateFields('search_query', $row)) {
            $this->message = __(
                'A duplicate query_text \'%1\' was found for the %2 store.',
                $row['query_text'],
                $row['store_id']
            )->render();

            return false;
        }

        return true;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }
}
