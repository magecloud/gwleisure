<?php

declare(strict_types=1);

namespace Amasty\ImportCore\Exception;

/**
 * Occurs when count of rows in source is multiple of batch size, and last batch turned out to be empty
 * For example, batch size = 2, count of rows = 4. The 3rd batch will be empty
 */
class SourceEndException extends \RuntimeException
{
}