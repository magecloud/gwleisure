<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Catalog Rule Export Entity for Magento 2 (system)
 */

namespace Amasty\CatalogPriceRuleExportEntity\Export\DataHandling\FieldModifier;

use Amasty\CatalogPriceRuleExportEntity\Export\DataHandling\FieldModifier\SourceOption\Value2Label;
use Amasty\ExportCore\Export\SourceOption\CustomerGroupOptions;

class GroupId2GroupCode extends Value2Label
{
    /**
     * @var CustomerGroupOptions
     */
    private $sourceModel;

    public function __construct($config, CustomerGroupOptions $sourceModel)
    {
        parent::__construct($config);
        $this->sourceModel = $sourceModel;
    }

    protected function getSourceModel()
    {
        return $this->sourceModel;
    }

    public function getLabel(): string
    {
        return __('Convert Group Id To Group Code')->getText();
    }
}
