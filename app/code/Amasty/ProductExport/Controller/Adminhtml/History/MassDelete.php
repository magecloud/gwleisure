<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Controller\Adminhtml\History;

class MassDelete extends \Amasty\ExportPro\Controller\Adminhtml\History\MassDelete
{
    public const ADMIN_RESOURCE = 'Amasty_ProductExport::product_export_history';
}
