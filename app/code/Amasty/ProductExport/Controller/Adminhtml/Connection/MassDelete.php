<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Controller\Adminhtml\Connection;

use Amasty\ProductExport\Api\ConnectionRepositoryInterface;
use Amasty\ProductExport\Model\Connection\ResourceModel\Collection;
use Amasty\ProductExport\Model\Connection\ResourceModel\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

class MassDelete extends Action
{
    public const ADMIN_RESOURCE = 'Amasty_ProductExport::product_export_connections';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ConnectionRepositoryInterface
     */
    private $repository;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        Action\Context $context,
        ConnectionRepositoryInterface $repository,
        LoggerInterface $logger,
        CollectionFactory $collectionFactory,
        Filter $filter
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->repository = $repository;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
    }

    public function execute()
    {
        $this->filter->applySelectionOnTargetProvider();
        /** @var Collection $collection */
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        if ($collection->getSize()) {
            foreach ($collection->getItems() as $connection) {
                try {
                    $this->repository->delete($connection);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                    $this->logger->critical($e);
                }
            }
        }

        $this->messageManager->addSuccessMessage(__('Connection(s) was successfully removed.'));

        return $this->resultRedirectFactory->create()->setPath('*/*');
    }
}
