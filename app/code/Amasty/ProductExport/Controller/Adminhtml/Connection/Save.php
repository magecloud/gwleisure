<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Controller\Adminhtml\Connection;

use Amasty\ProductExport\Api\ConnectionRepositoryInterface;
use Amasty\ProductExport\Api\Data\ConnectionInterfaceFactory;
use Amasty\ProductExport\Model\Connection\Connection;
use Amasty\ProductExport\Ui\DataProvider\Connection\Form;
use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

class Save extends Action
{
    public const ADMIN_RESOURCE = 'Amasty_ProductExport::product_export_connections';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ConnectionRepositoryInterface
     */
    private $connectionRepository;

    /**
     * @var ConnectionInterfaceFactory
     */
    private $connectionFactory;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    public function __construct(
        Action\Context $context,
        ConnectionRepositoryInterface $connectionRepository,
        ConnectionInterfaceFactory $connectionFactory,
        LoggerInterface $logger,
        DataPersistorInterface $dataPersistor
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->connectionRepository = $connectionRepository;
        $this->connectionFactory = $connectionFactory;
        $this->dataPersistor = $dataPersistor;
    }

    public function execute()
    {
        try {
            if ($data = $this->getRequest()->getPostValue()) {
                if ($connectionId = (int)$this->getRequest()->getParam(Connection::ID)) {
                    $model = $this->connectionRepository->getById($connectionId);
                } else {
                    $model = $this->connectionRepository->getEmptyConnectionModel();
                }
                $model->addData($data);
                $this->connectionRepository->save($model);

                $this->messageManager->addSuccessMessage(__('You saved the connection.'));

                if ($this->getRequest()->getParam('back')) {
                    return $this->resultRedirectFactory->create()
                        ->setPath('*/*/edit', [Connection::ID => $model->getConnectionId()]);
                }
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set(Form::CONNECTION_DATA, $data);

            if (isset($model) && $model->getConnectionId()) {
                return $this->resultRedirectFactory->create()
                    ->setPath('*/*/edit', [Connection::ID => $model->getConnectionId()]);
            } else {
                return $this->resultRedirectFactory->create()->setPath('*/*/edit');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('An error has occurred'));
            $this->logger->critical($e);

            return $this->resultRedirectFactory->create()->setPath('*/*');
        }

        return $this->resultRedirectFactory->create()->setPath('*/*');
    }
}
