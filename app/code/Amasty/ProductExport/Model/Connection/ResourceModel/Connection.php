<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Model\Connection\ResourceModel;

use Amasty\ProductExport\Model\Connection\Connection as CoonectionModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Connection extends AbstractDb
{
    public const TABLE_NAME = 'amasty_product_export_connection';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, CoonectionModel::ID);
    }
}
