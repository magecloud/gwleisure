<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Model\Profile;

class ScheduleConfig
{
    public const DATAPROVIDER_TYPE = 'basic';
}
