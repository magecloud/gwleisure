<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Model\ProfileEvent\ResourceModel;

use Amasty\ProductExport\Model\ProfileEvent\ProfileEvent as ProfileEventModel;
use Amasty\ProductExport\Model\ProfileEvent\ResourceModel\ProfileEvent as ProfileEventResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(ProfileEventModel::class, ProfileEventResource::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
