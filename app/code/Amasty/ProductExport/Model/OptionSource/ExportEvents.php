<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Products for Magento 2
 */

namespace Amasty\ProductExport\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class ExportEvents implements OptionSourceInterface
{
    public const PRODUCT_AFTER_SAVE_ID = 1;

    public const PRODUCT_AFTER_SAVE_EVENT = 'catalog_product_save_after';

    /**
     * @var int[]
     */
    private $events = [
        self::PRODUCT_AFTER_SAVE_EVENT => self::PRODUCT_AFTER_SAVE_ID,
    ];

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::PRODUCT_AFTER_SAVE_ID,
                'label'=> __('Product Save (Event: %1)', self::PRODUCT_AFTER_SAVE_EVENT)
            ]
        ];

        return $options;
    }

    public function getEventIdByName($eventName)
    {
        return $this->events[strtolower($eventName)] ?? false;
    }
}
