<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package URL Rewrite Import Entity for Magento 2 (system)
 */

namespace Amasty\UrlRewriteImportEntity\Import\DataHandling\FieldModifier\Product;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;

class Sku2ProductId extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var array
     */
    private $skuToEntityIdMap = [];

    public function __construct($config, ProductResource $productResource)
    {
        parent::__construct($config);
        $this->productResource = $productResource;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        $productId = $this->getIdBySku($value);
        if ($productId) {
            return $productId;
        }

        return null;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Product Sku to Product Id')->getText();
    }

    /**
     * Get product entity Id by sku
     *
     * @param string $sku
     * @return int|false
     */
    private function getIdBySku($sku)
    {
        if (!isset($this->skuToEntityIdMap[$sku])) {
            $this->skuToEntityIdMap[$sku] = $this->productResource->getIdBySku($sku);
        }

        return $this->skuToEntityIdMap[$sku];
    }
}
