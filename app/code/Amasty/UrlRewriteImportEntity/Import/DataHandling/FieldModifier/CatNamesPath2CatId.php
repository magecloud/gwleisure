<?php

declare(strict_types=1);

namespace Amasty\UrlRewriteImportEntity\Import\DataHandling\FieldModifier;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\UrlRewriteImportEntity\Import\DataHandling\Entity\Category\NamesPath2EntityId;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;

class CatNamesPath2CatId extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var NamesPath2EntityId
     */
    private $namesPath2EntityId;

    public function __construct($config, NamesPath2EntityId $namesPath2EntityId)
    {
        parent::__construct($config);
        $this->namesPath2EntityId = $namesPath2EntityId;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        if (!empty($value)) {
            return $this->namesPath2EntityId->execute($value);
        }

        return $value;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Category Paths to Category Id')->getText();
    }
}
