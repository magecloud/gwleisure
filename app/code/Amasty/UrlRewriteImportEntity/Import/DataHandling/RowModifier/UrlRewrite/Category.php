<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package URL Rewrite Import Entity for Magento 2 (system)
 */

namespace Amasty\UrlRewriteImportEntity\Import\DataHandling\RowModifier\UrlRewrite;

use Amasty\ImportCore\Api\Modifier\RowModifierInterface;
use Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator;

class Category implements RowModifierInterface
{
    public const CATEGORY_ID_FIELD_NAME = 'categories';

    /**
     * @inheritDoc
     */
    public function transform(array &$row): array
    {
        $row['entity_type'] = CategoryUrlRewriteGenerator::ENTITY_TYPE;

        if (isset($row[self::CATEGORY_ID_FIELD_NAME])) {
            $row['entity_id'] = $row[self::CATEGORY_ID_FIELD_NAME];
            $row['target_path'] = 'catalog/category/view/id/' . $row[self::CATEGORY_ID_FIELD_NAME];
        }

        return $row;
    }
}
