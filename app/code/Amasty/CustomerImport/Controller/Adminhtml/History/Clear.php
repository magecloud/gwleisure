<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Customers for Magento 2
 */

namespace Amasty\CustomerImport\Controller\Adminhtml\History;

use Amasty\CustomerImport\Model\ModuleType;
use Amasty\ImportPro\Model\History\Repository;
use Magento\Backend\App\Action;

class Clear extends Action
{
    public const ADMIN_RESOURCE = 'Amasty_CustomerImport::customer_import_history';

    /**
     * @var Repository
     */
    private $repository;

    public function __construct(
        Action\Context $context,
        Repository $repository
    ) {
        parent::__construct($context);
        $this->repository = $repository;
    }

    public function execute()
    {
        $result = $this->repository->clearHistory(ModuleType::TYPE);

        if ($result) {
            $this->messageManager->addSuccessMessage(__("History has been cleared."));
        } else {
            $this->messageManager->addErrorMessage(__("Something went wrong."));
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
