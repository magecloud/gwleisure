<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Customers for Magento 2
 */

namespace Amasty\CustomerImport\Model\OptionSource;

use Magento\Store\Ui\Component\Listing\Column\Store\Options as StoreOptions;

class CustomerStore extends StoreOptions
{
    /**
     * No selection value
     */
    public const NO_SELECTION = 0;

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->currentOptions['No Selection'] = [
            'label' => __('-- Please Select --'),
            'value' => self::NO_SELECTION
        ];

        $this->generateCurrentOptions();

        $this->options = array_values($this->currentOptions);

        return $this->options;
    }
}
