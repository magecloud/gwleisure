<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cms Export Entity for Magento 2 (System)
 */

namespace Amasty\CmsExportEntity\Model\ResourceModel\Page\Store;

use Amasty\CmsExportEntity\Model\ResourceModel\AbstractStoreCollection;
use Amasty\CmsExportEntity\Model\Page\Store as PageStore;
use Amasty\CmsExportEntity\Model\ResourceModel\Page\Store as PageStoreResource;

class Collection extends AbstractStoreCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(PageStore::class, PageStoreResource::class);
    }
}
