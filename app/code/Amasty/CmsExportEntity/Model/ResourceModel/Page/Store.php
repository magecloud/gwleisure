<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cms Export Entity for Magento 2 (System)
 */

namespace Amasty\CmsExportEntity\Model\ResourceModel\Page;

use Magento\Cms\Api\Data\PageInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Store extends AbstractDb
{
    /**
     * @var MetadataPool
     */
    private $metadataPool;

    public function __construct(
        Context $context,
        MetadataPool $metadataPool,
        ?string $connectionName = null
    ) {
        $this->metadataPool = $metadataPool;
        parent::__construct($context, $connectionName);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $metadata = $this->metadataPool->getMetadata(PageInterface::class);
        $this->_init('cms_page_store', $metadata->getLinkField());
    }

    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        $metadata = $this->metadataPool->getMetadata(PageInterface::class);

        return $this->_resources->getConnectionByName($metadata->getEntityConnectionName());
    }
}
