<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cms Export Entity for Magento 2 (System)
 */

namespace Amasty\CmsExportEntity\Model\Page;

use Amasty\CmsExportEntity\Model\ResourceModel\Page\Store as PageStore;
use Magento\Framework\Model\AbstractModel;

class Store extends AbstractModel
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(PageStore::class);
    }
}
