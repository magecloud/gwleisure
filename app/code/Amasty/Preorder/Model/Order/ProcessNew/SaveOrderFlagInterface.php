<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Base for Magento 2
*/

declare(strict_types=1);

namespace Amasty\Preorder\Model\Order\ProcessNew;

use Magento\Sales\Api\Data\OrderInterface;

interface SaveOrderFlagInterface
{
    public function execute(OrderInterface $order): void;
}
