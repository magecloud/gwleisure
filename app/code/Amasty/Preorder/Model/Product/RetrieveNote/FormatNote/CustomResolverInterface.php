<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Base for Magento 2
*/

declare(strict_types=1);

namespace Amasty\Preorder\Model\Product\RetrieveNote\FormatNote;

use Magento\Catalog\Api\Data\ProductInterface;

interface CustomResolverInterface
{
    public function execute(ProductInterface $product): string;
}
