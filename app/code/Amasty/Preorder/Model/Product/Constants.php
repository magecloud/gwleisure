<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Base for Magento 2
*/

declare(strict_types=1);

namespace Amasty\Preorder\Model\Product;

class Constants
{
    public const NOTE = 'amasty_preorder_note';
    public const CART_LABEL = 'amasty_preorder_cart_label';
    public const BACKORDERS_PREORDER_OPTION = 101;
}
