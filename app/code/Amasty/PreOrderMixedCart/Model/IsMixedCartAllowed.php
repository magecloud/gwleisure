<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Mixed Cart for Magento 2 (System)
*/

declare(strict_types=1);

namespace Amasty\PreOrderMixedCart\Model;

class IsMixedCartAllowed
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(ConfigProvider $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    public function execute(): bool
    {
        return $this->configProvider->isMixedCartAllowed();
    }
}
