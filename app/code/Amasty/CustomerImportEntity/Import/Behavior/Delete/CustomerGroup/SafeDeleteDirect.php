<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Customer Import Entity for Magento 2 (System)
 */

namespace Amasty\CustomerImportEntity\Import\Behavior\Delete\CustomerGroup;

use Amasty\ImportCore\Import\Behavior;

class SafeDeleteDirect extends Behavior\Delete\Table
{
    public function getUniqueIds(array &$data)
    {
        return array_filter(parent::getUniqueIds($data), function ($groupId) {
            return (int)$groupId !== 1; // Prevent deletion of General customer group
        });
    }
}
