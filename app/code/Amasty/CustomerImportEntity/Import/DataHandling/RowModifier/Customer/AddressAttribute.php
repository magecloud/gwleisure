<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Customer Import Entity for Magento 2 (System)
 */

namespace Amasty\CustomerImportEntity\Import\DataHandling\RowModifier\Customer;

use Amasty\ImportCore\Api\Modifier\RowModifierInterface;

class AddressAttribute implements RowModifierInterface
{
    /**
     * @var RegionConverter
     */
    private $regionConverter;

    public function __construct(
        RegionConverter $regionConverter
    ) {
        $this->regionConverter = $regionConverter;
    }

    /**
     * @inheritDoc
     */
    public function transform(array &$row): array
    {
        return $this->regionConverter->executeRow($row);
    }
}
