<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Customer Import Entity for Magento 2 (System)
 */

namespace Amasty\CustomerImportEntity\Import\DataHandling\RowModifier\Customer;

use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\ResourceModel\Region as RegionResource;

class RegionConverter
{
    /**
     * @var RegionFactory
     */
    private $regionFactory;

    /**
     * @var RegionResource
     */
    private $regionResource;

    public function __construct(
        RegionFactory $regionFactory,
        RegionResource $regionResource
    ) {
        $this->regionFactory = $regionFactory;
        $this->regionResource = $regionResource;
    }

    public function executeRow(array &$row): array
    {
        if (empty($row['region']) && !empty($row['region_id'])) {
            $region = $this->regionFactory->create();
            $this->regionResource->load($region, $row['region_id']);
            $row['region'] = $region->getDefaultName();
        }

        if (!empty($row['region']) && empty($row['region_id'])) {
            $region = $this->regionFactory->create();
            $this->regionResource->load($region, $row['region'], 'default_name');
            $row['region_id'] = $region->getId();
        }

        return $row;
    }
}
