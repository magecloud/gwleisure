<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Customer Import Entity for Magento 2 (System)
 */

namespace Amasty\CustomerImportEntity\Import\Validation\EntityValidator;

use Amasty\ImportCore\Api\Validation\FieldValidatorInterface;

class EmailFormat implements FieldValidatorInterface
{
    public function validate(array $row, string $field): bool
    {
        if (!isset($row[$field]) || !filter_var($row[$field], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }
}
