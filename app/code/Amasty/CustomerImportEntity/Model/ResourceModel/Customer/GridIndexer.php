<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Customer Import Entity for Magento 2 (System)
 */

namespace Amasty\CustomerImportEntity\Model\ResourceModel\Customer;

use Magento\Customer\Model\Customer;
use Magento\Framework\Indexer\IndexerInterface;
use Magento\Framework\Indexer\IndexerRegistry;

class GridIndexer
{
    /**
     * @var IndexerInterface
     */
    private $indexer;

    public function __construct(
        IndexerRegistry $indexerRegistry
    ) {
        $this->indexer = $indexerRegistry->get(Customer::CUSTOMER_GRID_INDEXER_ID);
    }

    public function update($value): void
    {
        $this->indexer->reindexList($value);
    }
}
