<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\DataHandling;

use Magento\Framework\App\ResourceConnection;

class AttributeCodeToId
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var array
     */
    private $attrIdsByCodeAndEntityType = [];

    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Retrieves attribute_id by attribute code and updates corresponding attribute_id data column
     *
     * @param array $row
     * @param string $attrCodeKey
     * @return array
     */
    public function executeRow(array &$row, $attrCodeKey = 'attribute_code'): array
    {
        if (isset($row['entity_type_id']) && isset($row[$attrCodeKey])) {
            $attributeCode = trim($row[$attrCodeKey]);
            if (empty($attributeCode)) {
                return $row;
            }

            $attributeId = $this->getAttributeIdByCode(
                $row['entity_type_id'],
                $attributeCode
            );
            if ($attributeId) {
                $row['attribute_id'] = $attributeId;
            }
        }

        return $row;
    }

    /**
     * Get attribute Id by attribute code
     *
     * @param int $entityTypeId
     * @param int $attributeCode
     * @return int
     */
    private function getAttributeIdByCode($entityTypeId, $attributeCode)
    {
        if (!isset($this->attrIdsByCodeAndEntityType[$entityTypeId][$attributeCode])) {
            $connection = $this->resourceConnection->getConnection();

            $select = $connection->select()
                ->from(
                    ['attribute' => $this->resourceConnection->getTableName('eav_attribute')],
                    ['attribute.attribute_id']
                )->join(
                    ['entity_type' => $this->resourceConnection->getTableName('eav_entity_type')],
                    'attribute.entity_type_id = entity_type.entity_type_id',
                    []
                )->where(
                    'entity_type.entity_type_id = ?',
                    $entityTypeId
                )->where(
                    'attribute.attribute_code = ?',
                    $attributeCode
                );

            $this->attrIdsByCodeAndEntityType[$entityTypeId][$attributeCode] = $connection->fetchOne($select);
        }

        return $this->attrIdsByCodeAndEntityType[$entityTypeId][$attributeCode];
    }
}
