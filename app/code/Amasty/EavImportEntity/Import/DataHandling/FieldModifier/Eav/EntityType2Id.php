<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\DataHandling\FieldModifier\Eav;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\Exception\LocalizedException;

class EntityType2Id extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var EavConfig
     */
    private $eavConfig;

    public function __construct($config, EavConfig $eavConfig)
    {
        parent::__construct($config);
        $this->eavConfig = $eavConfig;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        try {
            $entityType = $this->eavConfig->getEntityType($value);

            return $entityType->getEntityTypeId() ?: $value;
        } catch (LocalizedException $exception) {
            return $value;
        }
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Eav Entity Type to Id')->getText();
    }
}
