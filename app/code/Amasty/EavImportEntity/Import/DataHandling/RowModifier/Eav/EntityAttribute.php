<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\DataHandling\RowModifier\Eav;

use Amasty\ImportCore\Api\Modifier\RowModifierInterface;
use Amasty\EavImportEntity\Import\DataHandling\AttributeCodeToId;

class EntityAttribute implements RowModifierInterface
{
    /**
     * @var AttributeCodeToId
     */
    private $attributeCodeToId;

    public function __construct(AttributeCodeToId $attributeCodeToId)
    {
        $this->attributeCodeToId = $attributeCodeToId;
    }

    /**
     * @inheritDoc
     */
    public function transform(array &$row): array
    {
        return $this->attributeCodeToId->executeRow($row, 'attribute_id');
    }
}
