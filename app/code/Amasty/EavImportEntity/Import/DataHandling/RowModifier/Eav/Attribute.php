<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\DataHandling\RowModifier\Eav;

use Amasty\EavImportEntity\Import\DataHandling\AttributeCodeToId;
use Amasty\ImportCore\Api\Modifier\RowModifierInterface;
use Amasty\ImportCore\Import\Source\SourceDataStructure;

class Attribute implements RowModifierInterface
{
    public const AMASTY_DEFAULT_VALUE_KEY = 'amasty_default_value';

    /**
     * @var AttributeCodeToId
     */
    private $attributeCodeToId;

    public function __construct(AttributeCodeToId $attributeCodeToId)
    {
        $this->attributeCodeToId = $attributeCodeToId;
    }

    /**
     * @inheritDoc
     */
    public function transform(array &$row): array
    {
        $this->transferDefaultValue($row);

        return $this->attributeCodeToId->executeRow($row);
    }

    /**
     * Transfer default_value from main entity to eav_attribute_option_entity
     *
     * @param array &$row
     */
    public function transferDefaultValue(array &$row)
    {
        if (isset($row['default_value'])
            && !empty($row[SourceDataStructure::SUB_ENTITIES_DATA_KEY]['eav_attribute_option_entity'])
        ) {
            foreach ($row[SourceDataStructure::SUB_ENTITIES_DATA_KEY]['eav_attribute_option_entity'] as &$option) {
                if (isset($option['option_id']) && $option['option_id'] == $row['default_value']) {
                    $option[self::AMASTY_DEFAULT_VALUE_KEY] = $row['default_value'];
                }
            }
        }
    }
}
