<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Validation\RowValidator\Eav;

use Amasty\ImportCore\Api\Validation\RowValidatorInterface;
use Amasty\ImportCore\Import\Utils\CompositePKeyValidator;

class AttributeSetValidator implements RowValidatorInterface
{
    /**
     * @var string|null
     */
    private $message;

    /**
     * @var CompositePKeyValidator
     */
    private $compositePKeyValidator;

    public function __construct(CompositePKeyValidator $compositePKeyValidator)
    {
        $this->compositePKeyValidator = $compositePKeyValidator;
    }

    /**
     * @inheritDoc
     */
    public function validate(array $row): bool
    {
        $this->message = null;

        if (isset($row['attribute_set_id'])) {
            if (isset($row['entity_type_id']) && isset($row['attribute_set_name'])) {
                $isPKeyDuplicated = $this->compositePKeyValidator->isUniquePartDuplicated(
                    $row,
                    ['attribute_set_id', 'entity_type_id', 'attribute_set_name'],
                    ['attribute_set_id'],
                    'eav_attribute_set'
                );
                if ($isPKeyDuplicated) {
                    $this->message = (string)__(
                        'Attribute set \'attribute_set_id\' value %1 is already exists. ',
                        $row['attribute_set_id']
                    );

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}
