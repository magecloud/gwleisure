<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Behavior\Eav\Attribute\Save;

class Label extends AbstractSaveValueBehavior
{
    /**
     * @inheritDoc
     */
    protected function getIdentityKeys()
    {
        return ['attribute_id', 'store_id'];
    }

    /**
     * @inheritDoc
     */
    protected function getEntityTable()
    {
        return 'eav_attribute_label';
    }

    /**
     * @inheritDoc
     */
    protected function getIdFieldName()
    {
        return 'attribute_label_id';
    }

    /**
     * @inheritDoc
     */
    protected function getEntityFields()
    {
        return [
            'attribute_label_id',
            'attribute_id',
            'store_id',
            'value'
        ];
    }
}
