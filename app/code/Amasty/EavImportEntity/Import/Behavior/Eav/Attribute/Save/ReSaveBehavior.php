<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Behavior\Eav\Attribute\Save;

use Amasty\EavImportEntity\Import\Behavior\CustomAddUpdateDirectBehavior;
use Amasty\EavImportEntity\Import\DataHandling\RowModifier\Eav\Attribute;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Magento\Framework\App\ResourceConnection;

class ReSaveBehavior extends CustomAddUpdateDirectBehavior
{
    /**
     * @var IdsMap
     */
    private $idsMap;

    public function __construct(
        ResourceConnection $resourceConnection,
        BehaviorResultInterfaceFactory $behaviorResultFactory,
        IdsMap $idsMap,
        array $config
    ) {
        parent::__construct(
            $resourceConnection,
            $behaviorResultFactory,
            $config
        );
        $this->idsMap = $idsMap;
    }

    /**
     * @inheritDoc
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $result = $this->behaviorResultFactory->create();
        if (empty($data)) {
            return $result;
        }

        $connection = $this->getConnection();
        $connection->beginTransaction();

        $mainTable = $this->getMainTable();
        $idFieldName = $this->getIdFieldName($mainTable);
        try {
            $this->deleteOrigData($data);

            foreach ($data as $item) {
                if (!isset($item[$idFieldName])) {
                    continue;
                }

                $itemId = $this->saveItem($item);
                if (isset($item[Attribute::AMASTY_DEFAULT_VALUE_KEY], $item['attribute_id'])) {
                    $this->updateDefaultValue((int)$item['attribute_id'], $itemId);
                }
                $this->idsMap->add($item[$idFieldName], $itemId, $this->config['tableName']);
            }

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

        return $result;
    }

    /**
     * Deletes original data
     *
     * @param array $data
     * @throws \Exception
     */
    private function deleteOrigData(array $data)
    {
        $attributeIds = array_column($data, 'attribute_id');

        $connection = $this->getConnection();
        $connection->delete(
            $this->getMainTable(),
            $connection->quoteInto('attribute_id IN (?)', array_unique($attributeIds))
        );
    }

    /**
     * Save item
     *
     * @param array $item
     * @return int|null
     * @throws \Exception
     */
    private function saveItem(array $item)
    {
        $mainTable = $this->getMainTable();
        $idFieldName = $this->getIdFieldName($mainTable);

        $itemData = $this->prepareDataForTable([$item], $mainTable);
        $itemData = $this->unsetColumns($itemData, [$idFieldName]);
        if (empty($itemData)) {
            return null;
        }

        $connection = $this->getConnection();
        $connection->insert(
            $mainTable,
            $itemData[0]
        );

        return (int)$connection->lastInsertId();
    }

    /**
     * Update default_value in Eav Attribute Entity after getting lastInsertId
     *
     * @param int $attributeId
     * @param int $lastInsertId
     */
    private function updateDefaultValue(int $attributeId, int $lastInsertId)
    {
        $connection = $this->getConnection();
        $connection->update(
            $this->getTableName('eav_attribute'),
            ['default_value' => $lastInsertId],
            $connection->quoteInto('attribute_id = ?', $attributeId)
        );
    }
}
