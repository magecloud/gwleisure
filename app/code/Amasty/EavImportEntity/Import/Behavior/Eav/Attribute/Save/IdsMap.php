<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Behavior\Eav\Attribute\Save;

class IdsMap
{
    /**
     * @var array
     */
    private $map = [];

    /**
     * Add old Id to new Id mapping
     *
     * @param int $oldId
     * @param int $newId
     * @param string $tableName
     * @return void
     */
    public function add($oldId, $newId, $tableName): void
    {
        $this->map[$tableName][$oldId] = (int)$newId;
    }

    /**
     * Get new Id value by old Id
     *
     * @param int $oldId
     * @param string $tableName
     * @return int|null
     */
    public function getNewId($oldId, $tableName): ?int
    {
        return $this->map[$tableName][$oldId] ?? null;
    }

    /**
     * Deletes mapping records
     *
     * @param array $oldIds
     * @param string $tableName
     * @return void
     */
    public function delete(array $oldIds, $tableName): void
    {
        foreach ($oldIds as $oldId) {
            unset($this->map[$tableName][$oldId]);
        }
    }
}
