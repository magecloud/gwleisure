<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Behavior\Eav\Attribute\Save;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Magento\Framework\App\ResourceConnection;

class OptionValue extends AbstractSaveValueBehavior
{
    /**
     * @var IdsMap
     */
    private $idsMap;

    public function __construct(
        ResourceConnection $resourceConnection,
        BehaviorResultInterfaceFactory $resultFactory,
        IdsMap $idsMap
    ) {
        parent::__construct($resourceConnection, $resultFactory);
        $this->idsMap = $idsMap;
    }

    /**
     * @inheritDoc
     */
    protected function prepareValuesData(array &$data): void
    {
        foreach ($data as &$item) {
            if (!isset($item['option_id'])) {
                continue;
            }

            $newId = $this->idsMap->getNewId($item['option_id'], 'eav_attribute_option');
            if (!$newId) {
                continue;
            }

            $item['option_id'] = $newId;
        }
    }

    /**
     * @inheritDoc
     */
    protected function getIdentityKeys()
    {
        return ['option_id', 'store_id'];
    }

    /**
     * @inheritDoc
     */
    protected function getEntityTable()
    {
        return 'eav_attribute_option_value';
    }

    /**
     * @inheritDoc
     */
    protected function getIdFieldName()
    {
        return 'value_id';
    }

    /**
     * @inheritDoc
     */
    protected function getEntityFields()
    {
        return [
            'value_id',
            'option_id',
            'store_id',
            'value'
        ];
    }
}
