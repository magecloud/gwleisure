<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Behavior\Eav\Attribute\Save;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ImportCore\Api\BehaviorInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

abstract class AbstractSaveValueBehavior implements BehaviorInterface
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var BehaviorResultInterfaceFactory
     */
    private $resultFactory;

    public function __construct(
        ResourceConnection $resourceConnection,
        BehaviorResultInterfaceFactory $resultFactory
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->resultFactory = $resultFactory;
    }

    /**
     * @return array
     */
    abstract protected function getIdentityKeys();

    /**
     * @return string
     */
    abstract protected function getEntityTable();

    /**
     * @return string
     */
    abstract protected function getIdFieldName();

    /**
     * @return array
     */
    abstract protected function getEntityFields();

    /**
     * Prepares values data before save.
     * Extension point for specific cases
     *
     * @param array $data
     * @return void
     */
    // phpcs:ignore Magento2.CodeAnalysis.EmptyBlock.DetectedFunction
    protected function prepareValuesData(array &$data): void
    {
    }

    /**
     * @inheritDoc
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $result = $this->resultFactory->create();
        if (empty($data)) {
            return $result;
        }

        $this->prepareValuesData($data);
        $values = $this->getValues($data);

        $newIds = [];
        $updatedIds = [];
        $connection = $this->getConnection();
        foreach ($data as &$item) {
            $tableName = $this->getTableName($this->getEntityTable());
            $identityKey = $this->isItemIdentifiable($item)
                ? $this->getIdentityKey($item)
                : null;

            $idFieldName = $this->getIdFieldName();
            if ($identityKey && isset($values[$identityKey])) {
                $existentValueId = (int)$values[$identityKey][$idFieldName];

                $updatedIds[] = $existentValueId;
                $item[$idFieldName] = $existentValueId;

                if (isset($item['value'])
                    && $item['value'] != $values[$identityKey]['value']
                ) {
                    $connection->update(
                        $tableName,
                        ['value' => $item['value']],
                        [$idFieldName . ' = ?' => $existentValueId]
                    );
                }

                continue;
            }

            if ($identityKey && isset($item['value'])) {
                $bind = [];
                foreach ($this->getEntityFields() as $fieldName) {
                    if ($fieldName == $idFieldName) {
                        continue;
                    }
                    if ($this->isIdentityKeyPart($fieldName) || isset($item[$fieldName])) {
                        $bind[$fieldName] = $item[$fieldName];
                    }
                }

                $connection->insert($tableName, $bind);
                $newValueId = $connection->lastInsertId($tableName);

                $newIds[] = $newValueId;
                $item[$idFieldName] = $newValueId;
            }
        }

        $result->setUpdatedIds($updatedIds);
        $result->setNewIds($newIds);

        return $result;
    }

    /**
     * Get stored values
     *
     * @param array $data
     * @return array
     * @throws \Exception
     */
    private function getValues(array $data): array
    {
        $connection = $this->getConnection();

        $conditions = [];
        foreach ($data as $item) {
            if ($this->isItemIdentifiable($item)) {
                $identityConditions = $this->getIdentityConditions($item, $connection);
                $conditions[] = implode(' AND ', $identityConditions);
            }
        }
        $select = $connection->select()->from(
            $this->getTableName($this->getEntityTable()),
            $this->getEntityFields()
        )->where(
            '(' . implode(' OR ', $conditions) . ')'
        );

        $values = [];
        $valueRows = $connection->fetchAll($select);
        foreach ($valueRows as $valuesData) {
            $identityKey = $this->getIdentityKey($valuesData);
            $values[$identityKey] = $valuesData;
        }

        return $values;
    }

    /**
     * Checks if item is identifiable
     *
     * @param array $item
     * @return bool
     */
    private function isItemIdentifiable(array $item)
    {
        foreach ($this->getIdentityKeys() as $key) {
            if (!isset($item[$key])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get identity conditions
     *
     * @param array $item
     * @param AdapterInterface $connection
     * @return array
     */
    private function getIdentityConditions(array $item, AdapterInterface $connection)
    {
        $conditions = [];
        foreach ($this->getIdentityKeys() as $key) {
            $conditions[] = $connection->quoteInto($key . ' = ?', $item[$key]);
        }

        return $conditions;
    }

    /**
     * Get identity key for an item
     *
     * @param array $item
     * @return string
     */
    private function getIdentityKey(array $item)
    {
        $keyParts = [];
        foreach ($this->getIdentityKeys() as $key) {
            if (isset($item[$key])) {
                $keyParts[] = $item[$key];
            }
        }

        return implode('-', $keyParts);
    }

    /**
     * Checks if specified field is identity part
     *
     * @param string $fieldName
     * @return bool
     */
    private function isIdentityKeyPart($fieldName)
    {
        $identityKeys = $this->getIdentityKeys();

        return in_array($fieldName, $identityKeys);
    }

    /**
     * Get resource connection
     *
     * @return AdapterInterface
     * @throws \Exception
     */
    private function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }

    /**
     * Get table name
     *
     * @param string $table
     * @return string
     * @throws \Exception
     */
    protected function getTableName($table)
    {
        return $this->resourceConnection->getTableName($table);
    }
}
