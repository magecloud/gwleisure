<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\Behavior;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ImportCore\Api\BehaviorInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class CustomAddUpdateDirectBehavior implements BehaviorInterface
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var BehaviorResultInterfaceFactory
     */
    protected $behaviorResultFactory;

    /**
     * @var array
     */
    protected $config;

    public function __construct(
        ResourceConnection $resourceConnection,
        BehaviorResultInterfaceFactory $behaviorResultFactory,
        array $config
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->behaviorResultFactory = $behaviorResultFactory;
        if (!isset($config['tableName'])) {
            throw new \RuntimeException('tableName isn\'t specified.');
        }
        $this->config = $config;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $result = $this->behaviorResultFactory->create();

        $mainTable = $this->getMainTable();

        $maxId = $this->getMaxId($mainTable);
        $preparedData = $this->prepareDataForTable($data, $mainTable);
        if (!empty($preparedData)) {
            $this->getConnection()->insertOnDuplicate($mainTable, $preparedData);
        }

        $newIds = $this->getNewIds($maxId, $mainTable);
        $uniqueIds = $this->getUniqueIds($preparedData, $mainTable);

        $result->setUpdatedIds(array_diff($uniqueIds, $newIds));
        $result->setNewIds($newIds);

        return $result;
    }

    /**
     * Get resource connection
     *
     * @return AdapterInterface
     * @throws \Exception
     */
    protected function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }

    /**
     * Prepares data for table
     *
     * @param array $data
     * @param string $tableName
     * @return array
     * @throws \Exception
     */
    protected function prepareDataForTable(array $data, string $tableName)
    {
        if (empty($data)) {
            return $data;
        }

        $columns = $this->getConnection()->describeTable($tableName);
        $columnsToUnset = current($data) ? array_keys(current($data)) : [];
        foreach ($columns as $column => $value) {
            if (false !== $key = array_search($column, $columnsToUnset)) {
                unset($columnsToUnset[$key]);
            }
        }

        if (!empty($columnsToUnset)) {
            $data = $this->unsetColumns($data, $columnsToUnset);
        }

        return $data;
    }

    /**
     * Get unique ids
     *
     * @param array $data
     * @param string $tableName
     * @return array
     * @throws \Exception
     */
    protected function getUniqueIds(array &$data, $tableName)
    {
        return array_filter(array_unique(array_column($data, $this->getIdFieldName($tableName))));
    }

    /**
     * Get new inserted Ids
     *
     * @param int $minId
     * @param string $tableName
     * @return array
     * @throws \Exception
     */
    protected function getNewIds(int $minId, $tableName): array
    {
        $select = $this->getConnection()->select()
            ->from($tableName, $this->getIdFieldName($tableName))
            ->where($this->getIdFieldName($tableName) . ' > ' . $minId);

        return $this->getConnection()->fetchCol($select);
    }

    /**
     * Unset specified columns in the rows
     *
     * @param array $data
     * @param array $columns
     * @return array
     */
    protected function unsetColumns(array $data, array $columns): array
    {
        foreach ($data as &$row) {
            foreach ($columns as $column) {
                unset($row[$column]);
            }
        }

        return $data;
    }

    /**
     * Get entity main table name
     *
     * @return string
     * @throws \Exception
     */
    protected function getMainTable()
    {
        return $this->getTableName($this->config['tableName']);
    }

    /**
     * Get table name
     *
     * @param string $table
     * @return string
     * @throws \Exception
     */
    protected function getTableName($table)
    {
        return $this->resourceConnection->getTableName($table);
    }

    /**
     * Get Id field name of specified table
     *
     * @param string $tableName
     * @return string
     * @throws \Exception
     */
    protected function getIdFieldName($tableName)
    {
        return $this->getConnection()->getAutoIncrementField($tableName);
    }

    /**
     * Get max Id in the specified table
     *
     * @param string $tableName
     * @return int
     * @throws \Exception
     */
    protected function getMaxId($tableName): int
    {
        $select = $this->getConnection()->select()
            ->from($tableName, 'MAX(' . $this->getIdFieldName($tableName) . ')')
            ->limit(1);

        return (int)$this->getConnection()->fetchOne($select);
    }
}
