<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Import Entity for Magento 2 (system)
 */

namespace Amasty\EavImportEntity\Import\SourceOption\Eav;

use Magento\Eav\Model\Entity\Attribute\Set;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class AttributeSets implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var array
     */
    private $options;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = [];

            /** @var Collection $collection */
            $collection = $this->collectionFactory->create();
            $collection->join(
                ['entity_type' => 'eav_entity_type'],
                'main_table.entity_type_id = entity_type.entity_type_id',
                ['entity_type.entity_type_code']
            )->setOrder(
                'main_table.attribute_set_name',
                Collection::SORT_ORDER_ASC
            );
            /** @var Set $attributeSet */
            foreach ($collection as $attributeSet) {
                $this->options[] = [
                    'value' => $attributeSet->getAttributeSetId(),
                    'label' => $attributeSet->getAttributeSetName() . ' - ' . $attributeSet->getEntityTypeCode()
                ];
            }
        }

        return $this->options;
    }
}
