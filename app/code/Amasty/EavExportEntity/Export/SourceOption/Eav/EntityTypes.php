<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Export Entity for Magento 2 (system)
 */

namespace Amasty\EavExportEntity\Export\SourceOption\Eav;

use Magento\Eav\Model\ResourceModel\Entity\Type\Collection;
use Magento\Eav\Model\ResourceModel\Entity\Type\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class EntityTypes implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var array
     */
    private $options;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = [];

            /** @var Collection $collection */
            $collection = $this->collectionFactory->create();
            foreach ($collection as $entityType) {
                $this->options[] = [
                    'value' => $entityType->getEntityTypeId(),
                    'label' => $entityType->getEntityTypeCode()
                ];
            }
        }

        return $this->options;
    }
}
