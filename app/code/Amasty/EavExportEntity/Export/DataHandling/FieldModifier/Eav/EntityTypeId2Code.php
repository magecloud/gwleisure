<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Eav Export Entity for Magento 2 (system)
 */

namespace Amasty\EavExportEntity\Export\DataHandling\FieldModifier\Eav;

use Amasty\EavExportEntity\Export\SourceOption\Eav\EntityTypes;
use Amasty\EavExportEntity\Export\DataHandling\FieldModifier\SourceOption\Value2Label;

class EntityTypeId2Code extends Value2Label
{
    /**
     * @var EntityTypes
     */
    private $sourceModel;

    public function __construct($config, EntityTypes $sourceModel)
    {
        parent::__construct($config);
        $this->sourceModel = $sourceModel;
    }

    /**
     * @inheritdoc
     */
    protected function getSourceModel()
    {
        return $this->sourceModel;
    }

    /**
     * @inheritdoc
     */
    public function getLabel(): string
    {
        return __('Entity Type Id to Entity Type Code')->getText();
    }
}
