<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package URL Rewrite Export Entity for Magento 2 (System)
 */

namespace Amasty\UrlRewriteExportEntity\Export\UrlRewrite\SourceOption;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\UrlRewrite\Model\OptionProvider;

class RewriteOptions implements OptionSourceInterface
{
    /**
     * @var OptionProvider
     */
    private $optionProvider;

    /**
     * @var array|null
     */
    private $options;

    public function __construct(
        OptionProvider $optionProvider
    ) {
        $this->optionProvider = $optionProvider;
    }

    public function toOptionArray()
    {
        if (null === $this->options) {
            $options = $this->optionProvider->toOptionArray();
            $this->options = [];
            if (is_array($options)
                && count($options)
                && !is_array(reset($options))
            ) {
                foreach ($options as $value => $label) {
                    $this->options[] = [
                        'label' => $label,
                        'value' => $value
                    ];
                }
            } else {
                $this->options = $options;
            }
        }

        return $this->options;
    }
}
