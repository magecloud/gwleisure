<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package URL Rewrite Export Entity for Magento 2 (System)
 */

namespace Amasty\UrlRewriteExportEntity\Export\DataHandling\FieldGenerator;

use Amasty\Base\Model\Serializer;
use Amasty\ExportCore\Api\VirtualField\GeneratorInterface;
use Amasty\ExportCore\Export\DataHandling\FieldModifier\Catalog\CategoriesPathResolver;
use Magento\Store\Model\Store;

class Categories implements GeneratorInterface
{
    /**
     * @var CategoriesPathResolver
     */
    private $categoriesPathResolver;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        CategoriesPathResolver $categoriesPathResolver,
        Serializer $serializer
    ) {
        $this->categoriesPathResolver = $categoriesPathResolver;
        $this->serializer = $serializer;
    }

    public function generateValue(array $currentRecord)
    {
        if (isset($currentRecord['metadata'])
            && stripos($currentRecord['metadata'], 'category_id') !== false
        ) {
            $categoryId = $this->serializer->unserialize($currentRecord['metadata'])['category_id'] ?? null;
        } else {
            $categoryId = null;
        }

        if (isset($currentRecord['store_id'])) {
            $storeId = (int)$currentRecord['store_id'];
        } else {
            $storeId = Store::DEFAULT_STORE_ID;
        }

        if ($path = $this->categoriesPathResolver->getNamePathByEntityId((int)$categoryId, $storeId)) {
            return $path;
        }

        return '';
    }
}
