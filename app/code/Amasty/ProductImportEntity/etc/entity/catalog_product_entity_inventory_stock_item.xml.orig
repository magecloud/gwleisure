<?xml version="1.0"?>
<!--
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Product Import Entity for Magento 2 (System)
*/-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:amasty:module:Amasty_ImportCore:etc/am_import.xsd">
    <entity code="catalog_product_entity_inventory_stock_item">
        <name>Catalog Inventory Stock Item</name>
        <group>Product</group>
        <description>Catalog Inventory Stock Item</description>
        <fieldsConfig>
            <fields>
                <field name="item_id">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <nonNegative/>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                    <required/>
                </field>
                <field name="product_id">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <nonNegative/>
                        <custom class="Amasty\ProductImportEntity\Import\Validation\EntityValidator\Product\LinkField"
                                error="Product entity with specified 'product_id' doesn't exist"
                                rootOnly="true">
                            <excludeBehaviors>
                                <deleteDirect/>
                            </excludeBehaviors>
                        </custom>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                    <required/>
                </field>
                <field name="product_sku">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <notEmpty/>
                    </validation>
                    <required/>
                </field>
                <field name="stock_id">
                    <actions>
                        <trim/>
                        <custom class="Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\CatalogInventory\StockName2StockId"
                                apply="beforeValidate"/>
                    </actions>
                    <validation>
                        <tableRowExists tableName="cataloginventory_stock"
                                        idFieldName="stock_id"
                                        error="Catalog Inventory Stock with specified 'stock_id' doesn't exist.">
                            <excludeBehaviors>
                                <deleteDirect/>
                            </excludeBehaviors>
                        </tableRowExists>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                    <required/>
                </field>
                <field name="qty">
                    <actions>
                        <trim/>
                        <emptyToNull/>
                    </actions>
                    <filter>
                        <type>text</type>
                    </filter>
                </field>
                <field name="min_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <nonNegative/>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                </field>
                <field name="use_config_min_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="is_qty_decimal">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="backorders">
                    <actions>
                        <trim/>
                    </actions>
                    <filter>
                        <type>select</type>
                        <options>
                            <class>Magento\CatalogInventory\Model\Source\Backorders</class>
                        </options>
                    </filter>
                </field>
                <field name="use_config_backorders">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="min_sale_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <nonNegative/>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                </field>
                <field name="use_config_min_sale_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="max_sale_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <nonNegative/>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                </field>
                <field name="use_config_max_sale_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="is_in_stock">
                    <actions>
                        <trim/>
                    </actions>
                    <filter>
                        <type>select</type>
                        <options>
                            <class>Magento\CatalogInventory\Model\Source\Stock</class>
                        </options>
                    </filter>
                </field>
                <field name="low_stock_date">
                    <actions>
                        <trim/>
                        <emptyToNull/>
                    </actions>
                    <filter>
                        <type>date</type>
                    </filter>
                </field>
                <field name="notify_stock_qty">
                    <actions>
                        <trim/>
                        <emptyToNull/>
                    </actions>
                    <filter>
                        <type>text</type>
                    </filter>
                </field>
                <field name="use_config_notify_stock_qty">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="manage_stock">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="use_config_manage_stock">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="stock_status_changed_auto">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="use_config_qty_increments">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="qty_increments">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <nonNegative/>
                    </validation>
                    <filter>
                        <type>text</type>
                    </filter>
                </field>
                <field name="use_config_enable_qty_inc">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="enable_qty_increments">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="is_decimal_divided">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="website_id">
                    <actions>
                        <trim/>
                        <websiteCode2WebsiteId/>
                    </actions>
                    <validation>
                        <websiteExists>
                            <excludeBehaviors>
                                <deleteDirect/>
                            </excludeBehaviors>
                        </websiteExists>
                    </validation>
                    <filter>
                        <type>select</type>
                        <options>
                            <class>Amasty\ImportCore\Import\SourceOption\WebsiteOptions</class>
                        </options>
                    </filter>
                </field>
                <field name="deferred_stock_update">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
                <field name="use_config_deferred_stock_update">
                    <actions>
                        <trim/>
                    </actions>
                    <validation>
                        <boolean/>
                    </validation>
                    <filter>
                        <type>toggle</type>
                    </filter>
                </field>
            </fields>
            <sampleData>
                <row>
                    <field name='item_id'>1</field>
                    <field name='product_id'>1</field>
                    <field name='product_sku'>24-WG085</field>
                    <field name='stock_id'>1</field>
                    <field name='qty'>100</field>
                    <field name='min_qty'>0</field>
                    <field name='use_config_min_qty'>1</field>
                    <field name='is_qty_decimal'>0</field>
                    <field name='backorders'>0</field>
                    <field name='use_config_backorders'>1</field>
                    <field name='min_sale_qty'>1</field>
                    <field name='use_config_min_sale_qty'>1</field>
                    <field name='max_sale_qty'>0</field>
                    <field name='use_config_max_sale_qty'>1</field>
                    <field name='is_in_stock'>1</field>
                    <field name='low_stock_date'></field>
                    <field name='notify_stock_qty'></field>
                    <field name='use_config_notify_stock_qty'>1</field>
                    <field name='manage_stock'>0</field>
                    <field name='use_config_manage_stock'>1</field>
                    <field name='stock_status_changed_auto'>0</field>
                    <field name='use_config_qty_increments'>1</field>
                    <field name='qty_increments'>0</field>
                    <field name='use_config_enable_qty_inc'>1</field>
                    <field name='enable_qty_increments'>0</field>
                    <field name='is_decimal_divided'>0</field>
                    <field name='website_id'>All Websites</field>
                    <field name='deferred_stock_update'>0</field>
                    <field name='use_config_deferred_stock_update'>1</field>
                </row>
            </sampleData>
        </fieldsConfig>
        <behaviors>
            <custom class="Amasty\ProductImportEntity\Import\Behavior\Product\Save\CatalogInventory\CatalogInventory"
                    code="save_inventory_stock_item"
                    name="Add/Update">
                <arguments>
                    <argument name="tableName" xsi:type="string">cataloginventory_stock_item</argument>
                </arguments>
                <executeOnParent>
                    <addDirect/>
                    <addUpdateDirect/>
                    <updateDirect/>
                </executeOnParent>
            </custom>
            <deleteDirect name="Delete">
                <tableName>cataloginventory_stock_item</tableName>
            </deleteDirect>
        </behaviors>
    </entity>
    <relation code="catalog_product_entity_inventory_stock_item">
        <parent_entity>catalog_product_entity</parent_entity>
        <child_entity>catalog_product_entity_inventory_stock_item</child_entity>
        <sub_entity_field_name>inventory_stock_item</sub_entity_field_name>
        <skip_relation_fields_update>true</skip_relation_fields_update>
        <type>one_to_many</type>
        <arguments>
            <argument name="parent_field_name" xsi:type="string">entity_id</argument>
            <argument name="child_field_name" xsi:type="string">product_id</argument>
        </arguments>
    </relation>
</config>
