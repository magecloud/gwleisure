<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Filesystem;

use Amasty\ImportCore\Api\Action\FileUploaderInterface;
use Amasty\ImportCore\Api\ImportProcessInterface;
use Amasty\ImportCore\Import\Config\EntityConfigProvider;
use Amasty\ImportCore\Import\Source\SourceDataStructure;
use Magento\CatalogImportExport\Model\Import\Uploader;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Math\Random;
use Magento\MediaStorage\Helper\File\Storage;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\MediaStorage\Model\File\Validator\NotProtectedExtension;

class FileUploader extends Uploader implements FileUploaderInterface
{
    private const HASH_ALGORITHM = 'sha256';

    /**
     * @var EntityConfigProvider
     */
    private $entityConfigProvider;

    /**
     * @var string|null
     */
    private $newFileName = null;

    /**
     * @var array
     */
    private $uploadedImages = [];

    /**
     * @var array
     */
    private $fileFields = [];

    public function __construct(
        Database $coreFileStorageDb,
        Storage $coreFileStorage,
        AdapterFactory $imageFactory,
        NotProtectedExtension $validator,
        Filesystem $filesystem,
        Filesystem\File\ReadFactory $readFactory,
        EntityConfigProvider $entityConfigProvider,
        $filePath = null,
        Random $random = null
    ) {
        parent::__construct(
            $coreFileStorageDb,
            $coreFileStorage,
            $imageFactory,
            $validator,
            $filesystem,
            $readFactory,
            $filePath,
            $random
        );
        $this->entityConfigProvider = $entityConfigProvider;
    }

    public function initialize(ImportProcessInterface $importProcess): void
    {
        parent::init();
        $this->setTmpDir(trim($importProcess->getProfileConfig()->getImagesFileDirectory(), DIRECTORY_SEPARATOR));
        $this->setDestDir($importProcess->getEntityConfig()->getFileUploaderConfig()->getStoragePath());
    }

    public function execute(ImportProcessInterface $importProcess): void
    {
        $data = $importProcess->getData();
        foreach ($data as &$productData) {
            if (empty($productData[SourceDataStructure::SUB_ENTITIES_DATA_KEY])) {
                continue;
            }
            $subEntitiesData = &$productData[SourceDataStructure::SUB_ENTITIES_DATA_KEY];
            foreach ($subEntitiesData as $entityCode => &$subEntityData) {
                $this->processProductData($importProcess, $subEntityData, $entityCode);
            }
        }
        $importProcess->setData($data);
    }

    /**
     * Proceed moving a file from TMP to destination folder
     *
     * @param string $fileName
     * @param bool $renameFileOff
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function move($fileName, $renameFileOff = false)
    {
        $this->newFileName = null;
        if (!filter_var($fileName, FILTER_VALIDATE_URL)) {
            return parent::move($fileName, $renameFileOff);
        }

        $hash = $this->getFileHash($fileName);
        if (isset($this->uploadedImages[$hash])) {
            return $this->uploadedImages[$hash];
        }

        $this->newFileName = $this->getFilenameByUrl($fileName);

        return $this->uploadedImages[$hash] = parent::move($fileName, $renameFileOff);
    }

    /**
     * Used to save uploaded file into destination folder with original or new file name (if specified).
     *
     * @param string $destinationFolder
     * @param string $newFileName
     * @return array
     * @throws \Exception
     */
    public function save($destinationFolder, $newFileName = null)
    {
        return parent::save($destinationFolder, $this->newFileName ?: $newFileName);
    }

    private function processProductData(
        ImportProcessInterface $importProcess,
        array &$entityData,
        string $entityCode
    ): void {
        foreach ($entityData as &$subEntityData) {
            foreach ($this->getIsFileFields($entityCode) as $uploadRow) {
                if (!empty($subEntityData[$uploadRow])) {
                    try {
                        $uploadedFile = $this->move($subEntityData[$uploadRow], true);
                        $subEntityData[$uploadRow] = $uploadedFile['file'] ?? '';
                    } catch (LocalizedException $e) {
                        $importProcess->addErrorMessage($e->getMessage());
                    }
                }
            }
        }

        if (!empty($subEntityData[SourceDataStructure::SUB_ENTITIES_DATA_KEY])) {
            foreach ($subEntityData[SourceDataStructure::SUB_ENTITIES_DATA_KEY] as $entityCode => &$data) {
                $this->processProductData($importProcess, $data, $entityCode);
            }
        }
    }

    private function getIsFileFields(string $entityCode): array
    {
        if (array_key_exists($entityCode, $this->fileFields)) {
            return $this->fileFields[$entityCode];
        }
        $result = [];
        foreach ($this->entityConfigProvider->get($entityCode)->getFieldsConfig()->getFields() as $field) {
            if ($field->isFile()) {
                $result[] = $field->getName();
            }
        }
        $this->fileFields[$entityCode] = $result;

        return $this->fileFields[$entityCode];
    }

    private function getFilenameByUrl(string $url): string
    {
        // phpcs:disable Magento2.Functions.DiscouragedFunction.Discouraged
        $parsedUrlPath = parse_url($url, PHP_URL_PATH);
        $urlPathValues = explode('/', $parsedUrlPath);

        return preg_replace('/[^a-z0-9\._-]+/i', '', end($urlPathValues));
    }

    private function getFileHash(string $path): string
    {
        return hash_file(self::HASH_ALGORITHM, $path);
    }
}
