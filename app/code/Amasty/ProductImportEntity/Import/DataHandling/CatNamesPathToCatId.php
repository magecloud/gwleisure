<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling;

use Amasty\ImportCore\Import\DataHandling\Entity\Catalog\CategoryNamesPath2EntityId;

class CatNamesPathToCatId
{
    /**
     * @var CategoryNamesPath2EntityId
     */
    private $catNamesPath2EntityId;

    public function __construct(CategoryNamesPath2EntityId $catNamesPath2EntityId)
    {
        $this->catNamesPath2EntityId = $catNamesPath2EntityId;
    }

    /**
     * Converts category names path to entity Id
     *
     * @param string $namesPath
     * @return int|null
     */
    public function executeValue($namesPath)
    {
        return $this->catNamesPath2EntityId->execute($namesPath);
    }

    /**
     * Retrieves category_id by category names path and update corresponding category_id data column
     *
     * @param array $row
     * @param string $categoryIdKey
     * @param string $namesPathKey
     * @return array
     */
    public function executeRow(array &$row, $categoryIdKey = 'category_id', $namesPathKey = 'category'): array
    {
        if (isset($row[$namesPathKey])) {
            $path = trim($row[$namesPathKey]);
            if (empty($path)) {
                return $row;
            }

            $categoryId = $this->executeValue($path);
            if ($categoryId) {
                $row[$categoryIdKey] = $categoryId;
            }
        }

        return $row;
    }

    /**
     * Apply executeRows() method for multiple rows
     *
     * @param array $rows
     * @param string $categoryIdKey
     * @param string $namesPathKey
     * @return array
     */
    public function executeRows(array &$rows, $categoryIdKey = 'category_id', $namesPathKey = 'category'): array
    {
        foreach ($rows as &$row) {
            $this->executeRow($row, $categoryIdKey, $namesPathKey);
        }

        return $rows;
    }
}
