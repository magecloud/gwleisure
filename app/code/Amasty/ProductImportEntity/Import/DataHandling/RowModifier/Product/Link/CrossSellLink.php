<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\RowModifier\Product\Link;

use Amasty\ImportCore\Api\Modifier\RowModifierInterface;
use Amasty\ProductImportEntity\Import\DataHandling\SkuToProductId;

class CrossSellLink implements RowModifierInterface
{
    /**
     * @var SkuToProductId
     */
    private $skuToProductId;

    public function __construct(SkuToProductId $skuToProductId)
    {
        $this->skuToProductId = $skuToProductId;
    }

    /**
     * @inheritDoc
     */
    public function transform(array &$row): array
    {
        return $this->skuToProductId->executeRow($row, 'linked_product_id');
    }
}
