<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Category;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Framework\EntityManager\EntityMetadataInterface;
use Magento\Framework\EntityManager\MetadataPool;

class CategoryPathNormalizer
{
    /**
     * Category path delimiter
     */
    public const PATH_DELIMITER = '/';

    public const PATH_FIELD_NAME = 'path';

    /**
     * @var EntityMetadataInterface
     */
    private $metadata;

    public function __construct(
        MetadataPool $metadataPool
    ) {
        $this->metadata = $metadataPool->getMetadata(CategoryInterface::class);
    }

    /**
     * Apply executeRows() method for multiple rows if the identifier field
     * and the link field are different
     *
     * @param array $rows
     * @param string $identifierKey
     * @return array
     */
    public function executeRows(array &$rows, string $identifierKey = 'entity_id'): array
    {
        if ($this->metadata->getIdentifierField() != $this->metadata->getLinkField()) {
            foreach ($rows as &$row) {
                $this->executeRow($row, $identifierKey);
            }
        }

        return $rows;
    }

    /**
     * Checks that the last value in the "path" data column matches the value in the "entity_id" column.
     * If not, it will update the data column "path".
     *
     * @param array $row
     * @param string $identifierKey
     * @return array
     */
    public function executeRow(array &$row, string $identifierKey = 'entity_id'): array
    {
        if (!empty($row[$identifierKey]) && !empty($row[self::PATH_FIELD_NAME])) {
            $identifierValue = (int)trim($row[$identifierKey]);
            $structure = explode(self::PATH_DELIMITER, trim($row[self::PATH_FIELD_NAME]));
            if ((int)array_pop($structure) != $identifierValue) {
                $structure[] = $identifierValue;
                $row[self::PATH_FIELD_NAME] = implode(self::PATH_DELIMITER, $structure);
            }
        }

        return $row;
    }
}
