<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\CatalogInventory;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Magento\CatalogInventory\Api\StockCriteriaInterface;
use Magento\CatalogInventory\Api\StockCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockRepositoryInterface;

class StockName2StockId extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var StockCriteriaInterfaceFactory
     */
    private $stockCriteriaFactory;

    /**
     * @var StockRepositoryInterface
     */
    private $stockRepository;

    /**
     * @var array|null
     */
    private $map;

    public function __construct(
        $config,
        StockCriteriaInterfaceFactory $stockCriteriaFactory,
        StockRepositoryInterface $stockRepository
    ) {
        parent::__construct($config);
        $this->stockCriteriaFactory = $stockCriteriaFactory;
        $this->stockRepository = $stockRepository;
    }

    public function transform($value)
    {
        $map = $this->getMap();
        return $map[$value] ?? $value;
    }

    private function getMap()
    {
        if (!$this->map) {
            $this->map = [];

            /** @var StockCriteriaInterface $stockCriteria */
            $stockCriteria = $this->stockCriteriaFactory->create();
            $stockCollection = $this->stockRepository->getList($stockCriteria);
            foreach ($stockCollection->getItems() as $stock) {
                $this->map[$stock->getStockName()] = $stock->getStockId();
            }
        }
        return $this->map;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Stock Name to Stock Id')->getText();
    }
}
