<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Review;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Magento\Review\Helper\Data as StatusSource;

class StatusCode2StatusId extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var StatusSource
     */
    private $source;

    /**
     * @var array|null
     */
    private $map;

    public function __construct($config, StatusSource $source)
    {
        parent::__construct($config);
        $this->source = $source;
    }

    public function transform($value)
    {
        if (!is_array($value)) {
            $map = $this->getMap();
            return $map[$value] ?? $value;
        }

        return $value;
    }

    private function getMap(): ?array
    {
        if (!$this->map) {
            $statuses = array_map(function ($status) {
                return $status->getText();
            }, $this->source->getReviewStatuses());
            $this->map = array_flip($statuses);
        }
        return $this->map;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Status Code to Status Id')->getText();
    }
}
