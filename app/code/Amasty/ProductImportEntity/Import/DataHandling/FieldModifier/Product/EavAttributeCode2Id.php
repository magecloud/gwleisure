<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Product;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavResource;

class EavAttributeCode2Id extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var EavResource
     */
    private $eavResource;

    public function __construct($config, EavResource $eavResource)
    {
        parent::__construct($config);
        $this->eavResource = $eavResource;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        $attributeId = $this->eavResource->getIdByCode(Product::ENTITY, trim($value));

        return $attributeId ?: $value;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Eav Attribute Code to Id')->getText();
    }
}
