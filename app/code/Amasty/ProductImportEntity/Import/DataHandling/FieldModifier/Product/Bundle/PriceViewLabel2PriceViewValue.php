<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Product\Bundle;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Magento\Bundle\Model\Product\Attribute\Source\Price\View;

class PriceViewLabel2PriceViewValue extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var View
     */
    private $view;

    public function __construct($config, View $view)
    {
        parent::__construct($config);
        $this->view = $view;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        if (!is_array($value) && !empty($value)) {
            return $this->view->getOptionId($value) ?? $value;
        }

        return $value;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Price View Label to Price View Value')->getText();
    }
}
