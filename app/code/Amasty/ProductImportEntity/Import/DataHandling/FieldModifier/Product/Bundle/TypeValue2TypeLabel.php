<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Product\Bundle;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;

class TypeValue2TypeLabel extends AbstractModifier implements FieldModifierInterface
{
    public const TYPE_DYNAMIC = 0;
    public const TYPE_FIXED = 1;

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        if (!is_array($value)) {
            $map = $this->getMap();
            return $map[$value] ?? $value;
        }

        return $value;
    }

    private function getMap(): array
    {
        return [
            'dynamic' => self::TYPE_DYNAMIC,
            'fixed' => self::TYPE_FIXED,
        ];
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Type Value to Type Label')->getText();
    }
}
