<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Product;

use Amasty\ImportCore\Api\Modifier\FieldModifierInterface;
use Amasty\ImportCore\Import\DataHandling\AbstractModifier;
use Amasty\ImportCore\Import\DataHandling\ModifierProvider;
use Amasty\ProductImportEntity\Import\DataHandling\SkuToProductId;

class Sku2ProductId extends AbstractModifier implements FieldModifierInterface
{
    /**
     * @var SkuToProductId
     */
    private $skuToProductId;

    public function __construct($config, SkuToProductId $skuToProductId)
    {
        parent::__construct($config);
        $this->skuToProductId = $skuToProductId;
    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        $productId = $this->skuToProductId->executeValue($value);
        if ($productId) {
            return $productId;
        }

        return null;
    }

    public function getGroup(): string
    {
        return ModifierProvider::CUSTOM_GROUP;
    }

    public function getLabel(): string
    {
        return __('Sku to Product Id')->getText();
    }
}
