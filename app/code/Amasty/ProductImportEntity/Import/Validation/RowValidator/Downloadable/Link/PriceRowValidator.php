<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Validation\RowValidator\Downloadable\Link;

use Amasty\ImportCore\Api\Validation\RowValidatorInterface;
use Magento\Framework\App\ResourceConnection;

class PriceRowValidator implements RowValidatorInterface
{
    public const DOWNLOADABLE_LINK_PRICE_TABLE_NAME = 'downloadable_link_price';
    public const PRICE_ID_COLUMN_NAME = 'price_id';
    public const LINK_ID_COLUMN_NAME = 'link_id';

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    public function validate(array $row): bool
    {
        $this->message = null;

        $priceId = (int)($row[self::PRICE_ID_COLUMN_NAME] ?? 0);
        if ($priceId && $this->isRecordExists(self::PRICE_ID_COLUMN_NAME, $priceId)) {
            return true;
        }

        $linkId = (int)($row[self::LINK_ID_COLUMN_NAME] ?? 0);
        if ($linkId && $this->isRecordExists(self::LINK_ID_COLUMN_NAME, $linkId)) {
            $this->message = (string)__(
                'Link Price with %1 "%2" is already exists',
                self::LINK_ID_COLUMN_NAME,
                $linkId
            );

            return false;
        }

        return true;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    private function isRecordExists(string $columnName, int $value): bool
    {
        $select = $this->resourceConnection->getConnection()->select()
            ->from($this->resourceConnection->getTableName(self::DOWNLOADABLE_LINK_PRICE_TABLE_NAME))
            ->where(sprintf('%s = %d', $columnName, $value))
            ->limit(1);

        return (bool)$this->resourceConnection->getConnection()->fetchOne($select);
    }
}
