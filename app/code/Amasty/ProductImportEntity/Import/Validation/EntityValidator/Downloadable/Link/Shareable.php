<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Validation\EntityValidator\Downloadable\Link;

use Amasty\ImportCore\Api\Validation\FieldValidatorInterface;
use Magento\Downloadable\Model\Link;

class Shareable implements FieldValidatorInterface
{
    /**
     * @var \Magento\Downloadable\Model\Source\Shareable
     */
    private $shareableOptions;

    public function __construct(
        \Magento\Downloadable\Model\Source\Shareable $shareableOptions
    ) {
        $this->shareableOptions = $shareableOptions;
    }

    public function validate(array $row, string $field): bool
    {
        if (isset($row[$field])) {
            return in_array(
                $row[$field],
                array_column($this->shareableOptions->toOptionArray(), 'value')
            );
        }

        return true;
    }
}
