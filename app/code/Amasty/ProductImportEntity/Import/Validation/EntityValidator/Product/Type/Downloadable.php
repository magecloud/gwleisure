<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Validation\EntityValidator\Product\Type;

use Amasty\ImportCore\Api\Validation\FieldValidatorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Downloadable\Model\Product\Type;
use Magento\Framework\EntityManager\EntityMetadataInterface;
use Magento\Framework\EntityManager\MetadataPool;

class Downloadable implements FieldValidatorInterface
{
    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var array
     */
    private $validationResult;

    /**
     * @var EntityMetadataInterface
     */
    private $productMetadata;

    public function __construct(
        ProductResource $productResource,
        MetadataPool $metadataPool
    ) {
        $this->productResource = $productResource;
        $this->productMetadata = $metadataPool->getMetadata(ProductInterface::class);
    }

    public function validate(array $row, string $field): bool
    {
        if (isset($row[$field])) {
            $id = trim($row[$field]);
            if (!empty($id)) {
                if (!isset($this->validationResult[$id])) {
                    $typeId = $this->getTypeId((int)$id);
                    $this->validationResult[$id] = empty($typeId) || ($typeId === Type::TYPE_DOWNLOADABLE);
                }

                return $this->validationResult[$id];
            }
        }

        return true;
    }

    private function getTypeId(int $productId): string
    {
        $connection = $this->productResource->getConnection();

        $linkField = $this->productMetadata->getLinkField();
        $select = $connection->select()->from($this->productResource->getEntityTable(), 'type_id')
            ->where(sprintf('%s = %d', $linkField, $productId))
            ->limit(1);

        return (string)$connection->fetchOne($select);
    }
}
