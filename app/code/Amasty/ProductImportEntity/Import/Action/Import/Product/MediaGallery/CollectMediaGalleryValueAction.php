<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Action\Import\Product\MediaGallery;

use Amasty\ImportCore\Api\ActionInterface;
use Amasty\ImportCore\Api\ImportProcessInterface;
use Amasty\ImportCore\Import\Source\SourceDataStructure;
use Amasty\ProductExportEntity\Model\ResourceModel\Product\MediaGallery\Collection;
use Amasty\ProductImportEntity\Import\Behavior\Product\Save\MediaGallery\MediaGalleryMappingRegistry;
use Amasty\ProductImportEntity\Import\Behavior\Product\Save\MediaGallery\MediaGalleryRelation;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;

class CollectMediaGalleryValueAction implements ActionInterface
{
    public const MEDIA_GALLERY_ENTITY = 'catalog_product_entity_media_gallery';

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var MediaGalleryMappingRegistry
     */
    private $mediaGalleryMappingRegistry;

    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection,
        MediaGalleryMappingRegistry $mediaGalleryMappingRegistry
    ) {
        $this->metadataPool = $metadataPool;
        $this->resourceConnection = $resourceConnection;
        $this->mediaGalleryMappingRegistry = $mediaGalleryMappingRegistry;
    }

    public function execute(ImportProcessInterface $importProcess): void
    {
        $collectedProductIds = [];
        $importData = $importProcess->getData();
        $linkField = $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField();
        if (!empty($importData)) {
            foreach ($importData as $value) {
                if (empty($value[$linkField])
                    || empty($value[SourceDataStructure::SUB_ENTITIES_DATA_KEY][self::MEDIA_GALLERY_ENTITY])
                ) {
                    continue;
                }
                $collectedProductIds[] = $value[$linkField];
            }
        }
        if (empty($collectedProductIds)) {
            return;
        }
        $connection = $this->resourceConnection->getConnection();
        $query = $connection->select()->from(
            ['media_gallery' => $this->resourceConnection->getTableName(MediaGalleryRelation::TABLE_NAME)],
            ['value_id', 'value']
        )->joinLeft(
            ['relation' => $this->resourceConnection->getTableName(Collection::MEDIA_GALLERY_RELATION_TABLE)],
            'media_gallery.value_id = relation.value_id',
            [$linkField]
        )->where('relation. ' . $linkField . ' IN (?)', $collectedProductIds)->group('value_id');
        $result = [];
        foreach ($connection->fetchAssoc($query) as $item) {
            $result[$item[$linkField]][] = $item;
        }
        $this->mediaGalleryMappingRegistry->save($result);
    }

    //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock.DetectedFunction
    public function initialize(ImportProcessInterface $importProcess): void
    {
    }
}
