<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\FieldsClass\Product;

use Amasty\ImportCore\Api\Config\Entity\FieldsConfigInterface;
use Amasty\ImportCore\Import\FieldsClass\Describe as CoreDescribe;

class Describe extends CoreDescribe
{
    /**
     * @inheritDoc
     */
    public function execute(FieldsConfigInterface $existingConfig): FieldsConfigInterface
    {
        $fieldsConfig = parent::execute($existingConfig);

        $fields = $fieldsConfig->getFields();
        $rowIdField = $this->getFieldByName('row_id', $fields);
        if ($rowIdField) {
            $entityIdField = $this->getFieldByName('entity_id', $fields);
            $entityIdField->setIdentification(null);
        }

        return $fieldsConfig;
    }
}
