<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Observer\Gallery;

use Amasty\ImportCore\Api\Behavior\BehaviorObserverInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\EntityMetadataInterface;
use Magento\Framework\EntityManager\MetadataPool;

class AddUpdateAfterObserver implements BehaviorObserverInterface
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var EntityMetadataInterface
     */
    private $productMetadata;

    public function __construct(
        ResourceConnection $resourceConnection,
        MetadataPool $metadataPool
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->productMetadata = $metadataPool->getMetadata(ProductInterface::class);
    }

    public function execute(array &$data): void
    {
        $update = [];
        $linkField = $this->productMetadata->getLinkField();
        foreach ($data as $record) {
            if (isset($record['value_id'], $record[$linkField])) {
                $update[$record['value_id']] = [
                    'value_id' => $record['value_id'],
                    $linkField => $record[$linkField]
                ];
            }
        }

        $linkTableName = $this->resourceConnection->getTableName(Gallery::GALLERY_VALUE_TO_ENTITY_TABLE);
        $this->resourceConnection->getConnection()->insertOnDuplicate(
            $linkTableName,
            array_values($update)
        );
    }
}
