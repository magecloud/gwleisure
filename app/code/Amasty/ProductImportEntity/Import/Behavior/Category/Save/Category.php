<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Category\Save;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ImportCore\Import\Behavior\AddUpdate\Table as TableBehavior;
use Amasty\ImportCore\Import\Behavior\UniqueConstraintsProcessor;
use Amasty\ImportCore\Import\Utils\DuplicateFieldChecker;
use Amasty\ProductImportEntity\Import\DataHandling\FieldModifier\Category\CategoryPathNormalizer;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Category extends TableBehavior
{
    /**
     * @var CategoryPathNormalizer
     */
    private $categoryPathNormalizer;

    public function __construct(
        ObjectManagerInterface $objectManager,
        ResourceConnection $resourceConnection,
        SerializerInterface $serializer,
        BehaviorResultInterfaceFactory $behaviorResultFactory,
        DuplicateFieldChecker $duplicateFieldChecker,
        CategoryPathNormalizer $categoryPathNormalizer,
        UniqueConstraintsProcessor $uniqueConstraintsProcessor,
        array $config
    ) {
        parent::__construct(
            $objectManager,
            $resourceConnection,
            $serializer,
            $behaviorResultFactory,
            $duplicateFieldChecker,
            $uniqueConstraintsProcessor,
            $config
        );
        $this->categoryPathNormalizer = $categoryPathNormalizer;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $this->categoryPathNormalizer->executeRows($data);
        return parent::execute($data, $customIdentifier);
    }
}
