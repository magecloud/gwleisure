<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Inventory\Save;

class SourceStockLink extends AbstractInventory
{
    /**
     * @var string
     */
    protected $identityKey = 'link_id';

    protected function getMainTable()
    {
        return 'inventory_source_stock_link';
    }
}
