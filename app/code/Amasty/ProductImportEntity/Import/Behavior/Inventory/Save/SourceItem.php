<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Inventory\Save;

class SourceItem extends AbstractInventory
{
    /**
     * @var string
     */
    protected $identityKey = 'source_item_id';

    protected function getMainTable()
    {
        return 'inventory_source_item';
    }
}
