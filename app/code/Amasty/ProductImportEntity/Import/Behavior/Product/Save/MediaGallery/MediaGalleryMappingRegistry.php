<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\MediaGallery;

class MediaGalleryMappingRegistry
{
    /**
     * @var array<string, array{value_id: string, value: string, row_id: string}>
     */
    private $mapping = [];

    public function save(array $mapping): void
    {
        $this->mapping = $mapping;
    }

    /**
     * @return array<string, array{value_id: string, value: string, row_id: string}>
     */
    public function get(): array
    {
        return $this->mapping;
    }

    public function clear(): void
    {
        $this->mapping = [];
    }
}
