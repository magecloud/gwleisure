<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\Bundle;

use Amasty\ProductImportEntity\Import\Behavior\Product\AbstractScopedBehavior;

class BundleOptionValue extends AbstractScopedBehavior
{
    /**
     * @inheritDoc
     */
    protected function insertData(array $data, string $tableName)
    {
        $this->getConnection()->insertOnDuplicate($tableName, $data, ['title']);
    }

    /**
     * @inheritDoc
     */
    protected function getMainTable()
    {
        return 'catalog_product_bundle_option_value';
    }

    /**
     * @inheritDoc
     */
    protected function getScopedKeys()
    {
        return ['title', 'parent_product_id'];
    }
}
