<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\CustomOption;

class CustomOptionValue extends AbstractCustomOption
{
    /**
     * @var array
     */
    protected $linkedTableToFieldsMap = [
        'catalog_product_option_type_title' => ['title'],
        'catalog_product_option_type_price' => [
            'price',
            'price_type'
        ]
    ];

    /**
     * @var string[]
     */
    protected $identityKeys = ['option_type_id', 'option_id'];

    /**
     * @inheritDoc
     */
    protected function getMainTable()
    {
        return 'catalog_product_option_type_value';
    }

    /**
     * @inheritDoc
     */
    protected function isNestedDataRowPersisted(array $row)
    {
        $identity = $this->getIdentityKey($row, ['option_id']);

        return $this->identityRegistry->isPersisted($identity);
    }

    /**
     * @inheritDoc
     */
    protected function markRowsPersisted(array $data)
    {
        foreach ($data as $row) {
            $identity = $this->getIdentityKey($row, ['option_id']);
            $this->identityRegistry->markPersisted($identity);
        }
    }
}
