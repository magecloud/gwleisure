<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\MediaGallery;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ImportCore\Import\Behavior\Add\Table as AddDirectTable;
use Amasty\ImportCore\Import\Source\SourceDataStructure;
use Amasty\ImportCore\Import\Utils\DuplicateFieldChecker;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\EntityMetadataInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;

class AddDirect extends AddDirectTable
{
    public const GALLERY_VALUE_ENTITY = 'catalog_product_entity_media_gallery_value';
    /**
     * @var EntityMetadataInterface
     */
    private $productMetadata;

    public function __construct(
        ObjectManagerInterface $objectManager,
        ResourceConnection $resourceConnection,
        SerializerInterface $serializer,
        BehaviorResultInterfaceFactory $behaviorResultFactory,
        DuplicateFieldChecker $duplicateFieldChecker,
        MetadataPool $metadataPool,
        array $config
    ) {
        parent::__construct(
            $objectManager,
            $resourceConnection,
            $serializer,
            $behaviorResultFactory,
            $duplicateFieldChecker,
            $config
        );
        $this->productMetadata = $metadataPool->getMetadata(ProductInterface::class);
    }

    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $this->prepareBeforeExecute($data);
        $result = parent::execute($data, $customIdentifier);
        // for correct relation update
        $result->setAffectedIds($result->getNewIds());

        return $result;
    }

    private function prepareBeforeExecute(array &$mediaGalleryData): void
    {
        $linkField = $this->productMetadata->getLinkField();
        foreach ($mediaGalleryData as &$gallery) {
            $row[MediaGalleryRelation::MEDIA_GALLERY_VALUE_ID_NAME] = null;
            if (!empty($gallery['product_id'])
                && !empty($gallery[SourceDataStructure::SUB_ENTITIES_DATA_KEY][self::GALLERY_VALUE_ENTITY])
            ) {
                $galleryValues = &$gallery[SourceDataStructure::SUB_ENTITIES_DATA_KEY][self::GALLERY_VALUE_ENTITY];
                foreach ($galleryValues as &$galleryValue) {
                    $galleryValue[$linkField] = $gallery['product_id'];
                }
            }
        }
    }
}
