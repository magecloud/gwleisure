<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\CatalogInventory;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ProductImportEntity\Import\Behavior\Product\AbstractDirectBehavior;
use Amasty\ProductImportEntity\Import\DataHandling\SkuToProductId;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;

class CatalogInventory extends AbstractDirectBehavior
{
    /**
     * @var SkuToProductId
     */
    private $skuToProductId;

    /**
     * @var array
     */
    private $config;

    public function __construct(
        ResourceConnection $resourceConnection,
        StoreManagerInterface $storeManager,
        BehaviorResultInterfaceFactory $resultFactory,
        SkuToProductId $skuToProductId,
        $config = []
    ) {
        parent::__construct(
            $resourceConnection,
            $storeManager,
            $resultFactory
        );

        $this->config = $config;
        $this->skuToProductId = $skuToProductId;

        if (!isset($config['tableName'])) {
            throw new \RuntimeException('tableName isn\'t specified.');
        }
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $result = $this->resultFactory->create();
        $data = $this->skuToProductId->executeRows($data, 'product_id', 'product_sku');

        $tableName = $this->getTableName($this->config['tableName']);
        $preparedData = $this->prepareDataForTable($data, $tableName);
        if (!$this->hasDataToInsert($preparedData)) {
            return $result;
        }

        $this->getConnection()->insertOnDuplicate(
            $tableName,
            $preparedData
        );

        return $result;
    }

    private function hasDataToInsert(array $data): bool
    {
        return count(current($data) ?: []) >= 1;
    }
}
