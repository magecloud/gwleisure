<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\SuperAttribute;

use Amasty\ProductImportEntity\Import\Behavior\Product\AbstractScopedBehavior;

class SuperAttributeLabel extends AbstractScopedBehavior
{
    /**
     * @inheritDoc
     */
    protected function getMainTable()
    {
        return 'catalog_product_super_attribute_label';
    }

    /**
     * @inheritDoc
     */
    protected function getScopedKeys()
    {
        return ['value', 'use_default'];
    }
}
