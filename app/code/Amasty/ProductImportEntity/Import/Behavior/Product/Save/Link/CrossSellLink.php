<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\Link;

use Magento\Catalog\Model\Product\Link;

class CrossSellLink extends AbstractLink
{
    /**
     * @inheritDoc
     */
    protected function getLinkTypeId()
    {
        return Link::LINK_TYPE_CROSSSELL;
    }
}
