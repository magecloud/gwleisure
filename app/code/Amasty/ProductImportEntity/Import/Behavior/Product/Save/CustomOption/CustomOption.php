<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\CustomOption;

use Amasty\ImportCore\Import\Source\SourceDataStructure;

class CustomOption extends AbstractCustomOption
{
    public const CUSTOM_OPTION_VALUE_ENTITY_KEY = 'catalog_product_custom_option_value';

    /**
     * @var array
     */
    protected $linkedTableToFieldsMap = [
        'catalog_product_option_title' => ['title'],
        'catalog_product_option_price' => [
            'price',
            'price_type'
        ]
    ];

    /**
     * @var string[]
     */
    protected $identityKeys = ['option_id', 'product_id'];

    /**
     * @inheritDoc
     */
    protected function getMainTable()
    {
        return 'catalog_product_option';
    }

    /**
     * @inheritDoc
     */
    protected function registerSubEntities(array $data)
    {
        foreach ($data as $row) {
            $subEntitiesData = $row[SourceDataStructure::SUB_ENTITIES_DATA_KEY]
                [self::CUSTOM_OPTION_VALUE_ENTITY_KEY] ?? [];
            if (empty($subEntitiesData)) {
                continue;
            }

            foreach ($subEntitiesData as $subEntityRow) {
                $subIdentity = $this->getIdentityKey($subEntityRow, ['option_id']);
                $this->identityRegistry->add($subIdentity);
            }
        }
    }
}
