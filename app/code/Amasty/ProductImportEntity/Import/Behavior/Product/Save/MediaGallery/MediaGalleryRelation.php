<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\MediaGallery;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ProductImportEntity\Import\Behavior\Product\AbstractDirectBehavior;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;

class MediaGalleryRelation extends AbstractDirectBehavior
{
    public const TABLE_NAME = 'catalog_product_entity_media_gallery';
    public const MEDIA_GALLERY_VALUE_ID_NAME = 'value_id';

    /**
     * @var MediaGalleryMappingRegistry
     */
    private $mediaGalleryMappingRegistry;

    public function __construct(
        ResourceConnection $resourceConnection,
        StoreManagerInterface $storeManager,
        BehaviorResultInterfaceFactory $resultFactory,
        MediaGalleryMappingRegistry $mediaGalleryMappingRegistry
    ) {
        parent::__construct(
            $resourceConnection,
            $storeManager,
            $resultFactory
        );
        $this->mediaGalleryMappingRegistry = $mediaGalleryMappingRegistry;
    }

    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $result = $this->resultFactory->create();
        if (empty($data)) {
            return $result;
        }
        $mainTable = $this->getTableName(self::TABLE_NAME);
        $maxId = $this->getMaxId($mainTable);

        $galleryMapping = $this->mediaGalleryMappingRegistry->get();
        $updatedIds = [];
        foreach ($data as &$row) {
            $row[self::MEDIA_GALLERY_VALUE_ID_NAME] = null;
            if (isset($galleryMapping[$row['product_id']])) {
                foreach ($galleryMapping[$row['product_id']] as $key => $value) {
                    if ($value['value'] === $row['value']) {
                        $row[self::MEDIA_GALLERY_VALUE_ID_NAME] = $value[self::MEDIA_GALLERY_VALUE_ID_NAME];
                        $updatedIds[] = $value[self::MEDIA_GALLERY_VALUE_ID_NAME];
                    }
                }
            }
            unset($row);
        }

        $preparedData = $this->prepareDataForTable($data, $mainTable);
        $this->getConnection()->insertOnDuplicate($mainTable, $preparedData);

        $newIds = $this->getNewIds($maxId, $mainTable);
        $result->setNewIds($newIds);
        $result->setUpdatedIds($updatedIds);
        $this->fillValuesIds($data, $newIds);
        // for correct relation update
        $result->setAffectedIds(array_column($data, self::MEDIA_GALLERY_VALUE_ID_NAME));

        return $result;
    }

    private function fillValuesIds(array &$data, array $newIds): void
    {
        foreach ($data as &$row) {
            if (empty($row[self::MEDIA_GALLERY_VALUE_ID_NAME])) {
                $row[self::MEDIA_GALLERY_VALUE_ID_NAME] = array_shift($newIds);
            }
        }
    }
}
