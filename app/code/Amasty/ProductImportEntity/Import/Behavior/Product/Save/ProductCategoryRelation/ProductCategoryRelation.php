<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Import Entity for Magento 2 (System)
 */

namespace Amasty\ProductImportEntity\Import\Behavior\Product\Save\ProductCategoryRelation;

use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Amasty\ProductImportEntity\Import\Behavior\Product\AbstractDirectBehavior;
use Amasty\ProductImportEntity\Import\DataHandling\CatNamesPathToCatId;
use Amasty\ProductImportEntity\Import\DataHandling\SkuToProductId;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;

class ProductCategoryRelation extends AbstractDirectBehavior
{
    public const TABLE_NAME = 'catalog_category_product';
    public const CATEGORY_ID_FIELD_NAME = 'category_id';
    public const CATEGORIES_PATH_FIELD_NAME = 'categories';
    public const PRODUCT_ID_FIELD_NAME = 'product_id';
    public const PRODUCT_SKU_FIELD_NAME = 'product_sku';

    /**
     * @var SkuToProductId
     */
    private $skuToProductId;

    /**
     * @var CatNamesPathToCatId
     */
    private $catNamesPathToCatId;

    public function __construct(
        ResourceConnection $resourceConnection,
        StoreManagerInterface $storeManager,
        BehaviorResultInterfaceFactory $resultFactory,
        SkuToProductId $skuToProductId,
        CatNamesPathToCatId $catNamesPathToCatId
    ) {
        parent::__construct(
            $resourceConnection,
            $storeManager,
            $resultFactory
        );
        $this->skuToProductId = $skuToProductId;
        $this->catNamesPathToCatId = $catNamesPathToCatId;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $result = $this->resultFactory->create();
        $data = $this->prepareData($data);
        if (empty($data)) {
            return $result;
        }

        $tableName = $this->getTableName(self::TABLE_NAME);
        $preparedData = $this->prepareDataForTable($data, $tableName);
        $this->getConnection()->insertOnDuplicate(
            $tableName,
            $preparedData
        );

        return $result;
    }

    private function prepareData(array &$data): array
    {
        foreach ($data as $key => &$row) {
            unset($row[self::CATEGORY_ID_FIELD_NAME], $row[self::PRODUCT_ID_FIELD_NAME]);
            $this->skuToProductId->executeRow(
                $row,
                self::PRODUCT_ID_FIELD_NAME,
                self::PRODUCT_SKU_FIELD_NAME
            );
            $this->catNamesPathToCatId->executeRow(
                $row,
                self::CATEGORY_ID_FIELD_NAME,
                self::CATEGORIES_PATH_FIELD_NAME
            );

            if (!isset($row[self::PRODUCT_ID_FIELD_NAME]) || !isset($row[self::CATEGORY_ID_FIELD_NAME])) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
