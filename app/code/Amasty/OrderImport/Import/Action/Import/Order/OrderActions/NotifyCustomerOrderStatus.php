<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Import\Action\Import\Order\OrderActions;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use Magento\Sales\Model\Order\Email\Sender\OrderCommentSender;

class NotifyCustomerOrderStatus
{
    /**
     * @var OrderCommentSender
     */
    private $commentSender;

    /**
     * @var State
     */
    private $appState;

    public function __construct(
        OrderCommentSender $commentSender,
        State $appState
    ) {
        $this->commentSender = $commentSender;
        $this->appState = $appState;
    }

    public function execute(OrderInterface $order, OrderStatusHistoryInterface $history)
    {
        // for compatibility with Amasty_OrderStatus amasty_order_status_model_order_plugin_status_plugin_history
        $history->setIsCustomerNotified(true);

        if ($history->getIsCustomerNotified()) {
            //need to emulate frontend area because of theme full path problem
            // in \Magento\Framework\View\Design\Fallback\Rule\Theme::getPatternDirs
            $this->appState->emulateAreaCode(
                Area::AREA_FRONTEND,
                [$this->commentSender, 'send'],
                [
                    $order,
                    true
                ]
            );
            $history->setComment(
                __('The customer was notified about the order status changing.')
            );
        }
    }
}
