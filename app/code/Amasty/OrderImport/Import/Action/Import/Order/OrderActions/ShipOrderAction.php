<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Import\Action\Import\Order\OrderActions;

use Amasty\OrderImport\Api\OrderActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Model\Convert\Order;
use Magento\Sales\Model\Order\ShipmentFactory;

class ShipOrderAction implements OrderActionInterface
{
    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @var NotifyCustomerShipment
     */
    private $notifyCustomerShipment;

    /**
     * @var ShipmentFactory
     */
    private $shipmentFactory;

    public function __construct(
        Order $convertOrder, //@deprecated
        ShipmentRepositoryInterface $shipmentRepository,
        NotifyCustomerShipment $notifyCustomerShipment,
        ShipmentFactory $shipmentFactory = null
    ) {
        $this->shipmentRepository = $shipmentRepository;
        $this->notifyCustomerShipment = $notifyCustomerShipment;
        $this->shipmentFactory = $shipmentFactory ?? ObjectManager::getInstance()->get(ShipmentFactory::class);
    }

    public function execute(OrderInterface $order, array $actionData = []): void
    {
        if (!$order->canShip()
            || ($actionData['ship_order_new'] && !$actionData['isNew'])
        ) {
            return;
        }
        $shipmentItems = [];

        foreach ($order->getAllItems() as $orderItem) {
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $shipmentItems[$orderItem->getItemId()] = $orderItem->getQtyOrdered();
        }

        $shipment = $this->shipmentFactory->create($order, $shipmentItems);
        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);
        $this->shipmentRepository->save($shipment);

        if ($actionData['notify_customer_shipment'] ?? null) {
            $this->notifyCustomerShipment->execute($order, $shipment);
        }
    }
}
