<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Import\Action\Import\Order\OrderActions;

use Amasty\OrderImport\Api\OrderActionInterface;
use Magento\Sales\Api\Data\OrderInterface;

class CancelOrderAction implements OrderActionInterface
{
    public function execute(OrderInterface $order, array $actionData = []): void
    {
        if ($actionData['cancel_order_new'] && !$actionData['isNew']) {
            return;
        }
        $order->cancel();
    }
}
