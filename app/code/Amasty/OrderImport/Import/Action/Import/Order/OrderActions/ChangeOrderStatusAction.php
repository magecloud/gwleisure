<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Import\Action\Import\Order\OrderActions;

use Amasty\OrderImport\Api\OrderActionInterface;
use Magento\Sales\Api\Data\OrderInterface;

class ChangeOrderStatusAction implements OrderActionInterface
{
    /**
     * @var NotifyCustomerOrderStatus
     */
    private $notifyCustomerOrderStatus;

    public function __construct(
        NotifyCustomerOrderStatus $notifyCustomerOrderStatus
    ) {
        $this->notifyCustomerOrderStatus = $notifyCustomerOrderStatus;
    }

    public function execute(OrderInterface $order, array $actionData = []): void
    {
        if (!empty($actionData['change_status_new']) && empty($actionData['isNew'])) {
            return;
        }

        $value = $actionData['value'] ?? null;
        if ($value && $order->getStatus() !== $value) {
            $order->setStatus($value);
            $history = $order->addCommentToStatusHistory(
                __('The customer was not notified about the order status changing.'),
                $value
            );

            if ($actionData['notify_customer_status'] ?? null) {
                $this->notifyCustomerOrderStatus->execute($order, $history);
            }
        }
    }
}
