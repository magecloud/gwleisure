<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Controller\Adminhtml\History;

class MassDelete extends \Amasty\ImportPro\Controller\Adminhtml\History\MassDelete
{
    public const ADMIN_RESOURCE = 'Amasty_OrderImport::order_import_history';
}
