<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Model\Profile;

class ScheduleConfig
{
    public const DATAPROVIDER_TYPE = 'basic';
}
