<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\Order\Config;

class OrderStatuses implements OptionSourceInterface
{
    /**
     * @var Config
     */
    private $orderConfig;

    public function __construct(
        Config $orderConfig
    ) {
        $this->orderConfig = $orderConfig;
    }

    public function toOptionArray(): array
    {
        $values = [];

        foreach ($this->orderConfig->getStatuses() as $key => $status) {
            $values[] = [
                'value' => $key,
                'label' => $status
            ];
        }

        return $values;
    }
}
