<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Api;

use Magento\Sales\Api\Data\OrderInterface;

interface OrderActionInterface
{
    public function execute(OrderInterface $order, array $actionData = []): void;
}
