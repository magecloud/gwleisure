<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Orders for Magento 2
 */

namespace Amasty\OrderImport\Cron;

use Amasty\ImportPro\Model\History\Repository;
use Amasty\OrderImport\Model\ConfigProvider;
use Amasty\OrderImport\Model\ModuleType;

class CleanLogs
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var Repository
     */
    private $repository;

    public function __construct(
        ConfigProvider $configProvider,
        Repository $repository
    ) {
        $this->configProvider = $configProvider;
        $this->repository = $repository;
    }

    public function execute(): void
    {
        if ($this->configProvider->getLogCleaning()) {
            $logPeriod = $this->configProvider->getLogPeriod();
            $this->repository->clearHistoryByDays(ModuleType::TYPE, $logPeriod);
        }
    }
}
