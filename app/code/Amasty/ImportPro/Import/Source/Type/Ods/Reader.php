<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Import\Source\Type\Ods;

use Amasty\ImportCore\Api\ImportProcessInterface;

class Reader extends \Amasty\ImportPro\Import\Source\Type\Spout\Reader
{
    public const TYPE_ID = 'ods';

    public function estimateRecordsCount(): int
    {
        $rows = 0;
        $sheetIterator = $this->fileReader->getSheetIterator();
        $rowIterator = $sheetIterator->current()->getRowIterator();

        while ($rowIterator->valid()) {
            if (!$this->readAndValidateEntityRow()) {
                break;
            }

            $rows++;
        }

        $sheetIterator->rewind();
        $this->clearNextRowCache();
        $this->readTypeMatchedRow(); //skip reading header row

        return $rows;
    }

    protected function getSourceConfig(ImportProcessInterface $importProcess)
    {
        return $importProcess->getProfileConfig()->getExtensionAttributes()->getOdsSource();
    }

    protected function readTypeMatchedRow()
    {
        do {
            $sheetIterator = $this->fileReader->getSheetIterator();
            //todo: Remove after updating library "box/spout"
            $currentErrorLevel = error_reporting();
            error_reporting($currentErrorLevel & ~E_DEPRECATED);
            $rowIterator = $sheetIterator->current()->getRowIterator();
            error_reporting($currentErrorLevel);
            $rowIterator->next();
            $rowData = $rowIterator->current()->toArray();

            if (empty($rowData)) {
                return false;
            }
            $rowData = $this->prepareRowData($rowData);
        } while ($this->isRowEmpty($rowData));

        return $rowData;
    }
}
