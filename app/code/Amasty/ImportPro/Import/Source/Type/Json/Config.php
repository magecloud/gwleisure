<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Import\Source\Type\Json;

use Magento\Framework\DataObject;

class Config extends DataObject implements ConfigInterface
{
    public const HEADER = 'header';
    public const XPATH = 'xpath';
    public const FOOTER = 'footer';

    public function getHeader(): ?string
    {
        return $this->_getData(self::HEADER);
    }

    public function setHeader(?string $header): void
    {
        $this->setData(self::HEADER, $header);
    }

    public function getXpath(): ?string
    {
        return $this->_getData(self::XPATH);
    }

    public function setXpath(?string $xpath): void
    {
        $this->setData(self::XPATH, $xpath);
    }

    public function getFooter(): ?string
    {
        return $this->_getData(self::FOOTER);
    }

    public function setFooter(?string $footer): void
    {
        $this->setData(self::FOOTER, $footer);
    }
}
