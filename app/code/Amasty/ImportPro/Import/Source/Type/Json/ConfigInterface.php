<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Import\Source\Type\Json;

interface ConfigInterface
{
    /**
     * @return string|null
     */
    public function getHeader(): ?string;

    /**
     * @param string|null $header
     *
     * @return void
     */
    public function setHeader(?string $header): void;

    /**
     * @return string|null
     */
    public function getXpath(): ?string;

    /**
     * @param string|null $xpath
     * @return void
     */
    public function setXpath(?string $xpath): void;

    /**
     * @return string|null
     */
    public function getFooter(): ?string;

    /**
     * @param string|null $footer
     *
     * @return void
     */
    public function setFooter(?string $footer): void;
}
