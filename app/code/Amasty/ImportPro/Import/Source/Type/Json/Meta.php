<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Import\Source\Type\Json;

use Amasty\ImportCore\Api\Config\EntityConfigInterface;
use Amasty\ImportCore\Api\Config\ProfileConfigInterface;
use Amasty\ImportCore\Api\FormInterface;
use Magento\Framework\App\RequestInterface;

class Meta implements FormInterface
{
    public const DATASCOPE = 'extension_attributes.json_source.';

    /**
     * @var ConfigFactory
     */
    private $configFactory;

    public function __construct(
        ConfigInterfaceFactory $configFactory
    ) {
        $this->configFactory = $configFactory;
    }

    public function getMeta(EntityConfigInterface $entityConfig, array $arguments = []): array
    {
        return [
            'json.header' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Header'),
                            'dataType' => 'text',
                            'formElement' => 'textarea',
                            'additionalClasses' => 'amimportcore-textarea',
                            'visible' => true,
                            'sortOrder' => 10,
                            'default' => '[',
                            'componentType' => 'field',
                            'notice' => __('Please note that if you are using Amasty Export extension, '
                                . 'it is necessary to set the Header in the same way as the header '
                                . ' during the export process to ensure correct import.'),
                            'dataScope' => self::DATASCOPE . 'header'
                        ]
                    ]
                ]
            ],
            'json.xpath' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Items Container Name'),
                            'dataType' => 'text',
                            'formElement' => 'input',
                            'default' => '',
                            'visible' => true,
                            'sortOrder' => 20,
                            'componentType' => 'field',
                            'notice' => __('Specify the path to the container with items (optional). '
                                . 'For example, data/items or items.'),
                            'dataScope' => self::DATASCOPE . 'xpath'
                        ]
                    ]
                ]
            ],
            'json.footer' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Footer'),
                            'dataType' => 'text',
                            'formElement' => 'textarea',
                            'additionalClasses' => 'amimportcore-textarea',
                            'default' => ']',
                            'visible' => true,
                            'sortOrder' => 30,
                            'componentType' => 'field',
                            'notice' => __('Please note that if you are using Amasty Export extension, '
                                . 'it is necessary to set the Footer in the same way as the footer during '
                                . ' the export process to ensure correct import.'),
                            'dataScope' => self::DATASCOPE . 'footer'
                        ]
                    ]
                ]
            ],
        ];
    }

    public function prepareConfig(ProfileConfigInterface $profileConfig, RequestInterface $request): FormInterface
    {
        $config = $this->configFactory->create();
        $requestConfig = $request->getParam('extension_attributes')['json_source'] ?? [];
        if (isset($requestConfig[Config::HEADER])) {
            $config->setHeader((string) $requestConfig[Config::HEADER]);
        }
        if (isset($requestConfig[Config::XPATH])) {
            $config->setXpath((string) $requestConfig[Config::XPATH]);
        }
        if (isset($requestConfig[Config::FOOTER])) {
            $config->setFooter((string) $requestConfig[Config::FOOTER]);
        }

        $profileConfig->getExtensionAttributes()->setJsonSource($config);

        return $this;
    }

    public function getData(ProfileConfigInterface $profileConfig): array
    {
        $data = [];

        if ($config = $profileConfig->getExtensionAttributes()->getJsonSource()) {
            $data = [
                'extension_attributes' => [
                    'json_source' => [
                        Config::HEADER => $config->getHeader(),
                        Config::XPATH => $config->getXpath(),
                        Config::FOOTER => $config->getFooter()
                    ]
                ]
            ];
        }

        return $data;
    }
}
