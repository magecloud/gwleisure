<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Import\Source\Type\Json;

use Amasty\ImportCore\Api\ImportProcessInterface;
use Amasty\ImportCore\Api\Source\SourceDataStructureInterface;
use Amasty\ImportCore\Api\Source\SourceReaderInterface;
use Amasty\ImportCore\Import\FileResolver\FileResolverAdapter;
use Amasty\ImportCore\Import\Source\Data\DataStructureProvider;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\File\ReadInterface as FileReader;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\ArrayManager;

class Reader implements SourceReaderInterface
{
    public const TYPE_ID = 'json';

    /**
     * @var FileReader
     */
    private $fileReader;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var FileResolverAdapter
     */
    private $fileResolverAdapter;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var DataStructureProvider
     */
    private $dataStructureProvider;

    /**
     * @var array
     */
    private $document;

    /**
     * @var \Generator
     */
    private $generator;

    /**
     * @var SourceDataStructureInterface
     */
    private $dataStructure;

    /**
     * @var array
     */
    private $entityNodes;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var Json
     */
    private $json;

    public function __construct(
        FileResolverAdapter $fileResolverAdapter,
        Filesystem $filesystem,
        DataStructureProvider $dataStructureProvider,
        ArrayManager $arrayManager,
        Json $json
    ) {
        $this->fileResolverAdapter = $fileResolverAdapter;
        $this->filesystem = $filesystem;
        $this->dataStructureProvider = $dataStructureProvider;
        $this->arrayManager = $arrayManager;
        $this->json = $json;
    }

    /**
     * @throws FileSystemException
     */
    public function initialize(ImportProcessInterface $importProcess)
    {
        $fileName = $this->fileResolverAdapter->getFileResolver(
            $importProcess->getProfileConfig()->getFileResolverType()
        )->execute($importProcess);
        $this->config = $importProcess->getProfileConfig()->getExtensionAttributes()->getJsonSource();

        $directoryRead = $this->filesystem->getDirectoryRead(DirectoryList::ROOT);
        $this->fileReader = $directoryRead->openFile($fileName);

        $this->dataStructure = $this->dataStructureProvider->getDataStructure(
            $importProcess->getEntityConfig(),
            $importProcess->getProfileConfig()
        );
    }

    public function readRow()
    {
        if ($this->document === null) {
            $this->initDocument();
        }
        $row = (array) $this->generator->current();

        $this->generator->next();

        if ($this->isRowEmpty($row)) {
            return false;
        }

        return $this->parseSubEntities($row, $this->dataStructure);
    }

    public function estimateRecordsCount(): int
    {
        if ($this->document === null) {
            $this->initDocument();
        }

        return $this->entityNodes ? count($this->entityNodes) : 0;
    }

    private function parseSubEntities(array $entity, SourceDataStructureInterface $dataStructure): array
    {
        $formattedEntity = [];
        $fields = $dataStructure->getFields();

        foreach ($entity as $key => $row) {
            if (!empty($row) && is_array($row)) {
                continue;
            }
            if (in_array($key, $fields)) {
                $formattedEntity[$key] = (string) $row;
            }
        }

        foreach ($dataStructure->getSubEntityStructures() as $subEntityStructure) {
            $subEntities = $entity[$subEntityStructure->getMap()] ?? null;
            if ($subEntities) {
                if (is_array($subEntities)) {
                    foreach ($subEntities as $subEntity) {
                        $formattedEntity[$subEntityStructure->getMap()][] = $this->parseSubEntities(
                            (array) $subEntity,
                            $subEntityStructure
                        );
                    }
                }
            } else {
                $formattedEntity[$subEntityStructure->getMap()] = []; //empty element
            }
        }

        return $formattedEntity;
    }

    private function initDocument(): void
    {
        $contents = $this->fileReader->readAll();
        $contents = ltrim($contents, $this->config->getHeader());
        $contents = rtrim($contents, $this->config->getFooter());
        $contents = '[' . $contents . ']';
        $this->document = $this->json->unserialize($contents);
        $this->generator = $this->fetchRecord($this->config->getXpath());

        if (!$this->generator->valid()) {
            throw new \RuntimeException('Wrong file content.');
        }
    }

    private function fetchRecord(?string $xpath = null): \Generator
    {
        if ($xpath) {
            $this->entityNodes = $this->arrayManager->get($xpath, $this->document, []);
        } else {
            $this->entityNodes = $this->document;
        }

        foreach ($this->entityNodes as $entityNode) {
            yield $entityNode;
        }
    }

    private function isRowEmpty(array $row): bool
    {
        return empty(array_filter($row));
    }
}
