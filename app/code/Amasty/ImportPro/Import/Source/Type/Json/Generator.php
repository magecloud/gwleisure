<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Import\Source\Type\Json;

use Amasty\ImportCore\Api\Source\SourceGeneratorInterface;
use Amasty\ImportCore\Import\Config\ProfileConfig;

class Generator implements SourceGeneratorInterface
{
    public function generate(ProfileConfig $profileConfig, array $data): string
    {
        return json_encode($data, JSON_PRETTY_PRINT);
    }

    public function getExtension(): string
    {
        return 'json';
    }
}
