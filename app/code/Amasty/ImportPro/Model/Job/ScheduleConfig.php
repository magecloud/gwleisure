<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Model\Job;

class ScheduleConfig
{
    public const DATAPROVIDER_TYPE = 'basic';
}
