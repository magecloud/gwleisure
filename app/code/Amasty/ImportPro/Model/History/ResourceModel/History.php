<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Model\History\ResourceModel;

use Amasty\ImportPro\Model\History\History as HistoryModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class History extends AbstractDb
{
    public const TABLE_NAME = 'amasty_import_history';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, HistoryModel::HISTORY_ID);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function clearTable()
    {
        $this->getConnection()->truncateTable($this->getMainTable());
    }

    public function clearHistoryByDate(string $jobType, string $dateToDelete)
    {
        $where = [
            HistoryModel::TYPE . ' = ?' => $jobType,
            HistoryModel::FINISHED_AT . ' < ?' => $dateToDelete
        ];
        $this->getConnection()->delete($this->getMainTable(), $where);
    }
}
