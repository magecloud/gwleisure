<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Import Pro for Magento 2 (System)
 */

namespace Amasty\ImportPro\Api\Import;

use Amasty\ImportCore\Api\ImportProcessInterface;

interface NotifierInterface
{
    /**
     * Perform notification process.
     *
     * @param ImportProcessInterface $importProcess
     */
    public function notify(ImportProcessInterface $importProcess): void;
}
