<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Model\History;

class HistoryStrategies
{
    /**
     * @var array
     */
    private $strategies;

    public function __construct(array $strategies)
    {
        $this->strategies = $strategies;
    }

    public function isLogStrategy(string $strategy): bool
    {
        return in_array($strategy, $this->strategies);
    }
}
