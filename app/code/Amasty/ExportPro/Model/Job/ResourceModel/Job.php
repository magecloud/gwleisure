<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Model\Job\ResourceModel;

use Amasty\ExportPro\Model\Job\Job as JobModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Job extends AbstractDb
{
    public const TABLE_NAME = 'amasty_export_cron_job';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, JobModel::JOB_ID);
    }
}
