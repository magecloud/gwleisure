<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Model\Email\VariableProcessor;

use Amasty\ExportCore\Api\ExportProcessInterface;

interface VariableProcessorInterface
{
    /**
     * @param ExportProcessInterface $exportProcess
     * @param array $variables
     * @return array
     */
    public function prepareVariables(ExportProcessInterface $exportProcess, array $variables): array;
}
