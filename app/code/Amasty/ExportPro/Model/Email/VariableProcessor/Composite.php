<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Model\Email\VariableProcessor;

use Amasty\ExportCore\Api\ExportProcessInterface;

class Composite
{
    /**
     * @var VariableProcessorInterface[]
     */
    private $processors;

    public function __construct(array $processors = [])
    {
        $this->processors = $processors;
    }

    public function prepareVariables(ExportProcessInterface $exportProcess, array $variables): array
    {
        foreach ($this->processors as $processor) {
            $variables = $processor->prepareVariables($exportProcess, $variables);
        }

        return $variables;
    }
}
