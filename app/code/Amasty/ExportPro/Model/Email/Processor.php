<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Model\Email;

use Amasty\ExportCore\Api\ExportProcessInterface;
use Amasty\ExportPro\Model\Email\VariableProcessor\Composite as VariableProcessorComposite;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Processor
{
    /**
     * @var VariableProcessorComposite
     */
    private $variableProcessorComposite;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    public function __construct(
        VariableProcessorComposite $variableProcessorComposite,
        TimezoneInterface $timezone
    ) {
        $this->variableProcessorComposite = $variableProcessorComposite;
        $this->timezone = $timezone;
    }

    public function process(ExportProcessInterface $exportProcess): array
    {
        $profileConfig = $exportProcess->getProfileConfig();
        $emailData = $profileConfig->getExtensionAttributes()->getEmailFileDestination();
        $templateVariables = [
            'profile_name' => $profileConfig->getExtensionAttributes()->getName() ?: $profileConfig->getEntityCode(),
            'date' => $this->timezone->formatDate(),
            'subject' => $emailData->getEmailSubject()
        ];

        return $this->variableProcessorComposite->prepareVariables($exportProcess, $templateVariables);
    }
}
