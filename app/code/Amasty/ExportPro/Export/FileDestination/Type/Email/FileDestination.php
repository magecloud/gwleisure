<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Email;

use Amasty\ExportCore\Api\ExportProcessInterface;
use Amasty\ExportCore\Api\FileDestination\FileDestinationInterface;
use Amasty\ExportCore\Export\Utils\TmpFileManagement;
use Amasty\ExportPro\Model\Email\Processor;
use Amasty\ExportPro\Utils\Email\TransportBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\File\Mime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;

class FileDestination implements FileDestinationInterface
{
    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var TmpFileManagement
     */
    private $tmpFileManagement;

    /**
     * @var Mime
     */
    private $mime;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var Processor
     */
    private $variableProcessor;

    public function __construct(
        TmpFileManagement $tmpFileManagement,
        TransportBuilder $transportBuilder,
        TimezoneInterface $timezone,
        StoreManagerInterface $storeManager,
        Mime $mime,
        Processor $variableProcessor
    ) {
        $this->timezone = $timezone;
        $this->storeManager = $storeManager;
        $this->tmpFileManagement = $tmpFileManagement;
        $this->mime = $mime;
        $this->transportBuilder = $transportBuilder;
        $this->variableProcessor = $variableProcessor;
    }

    public function execute(ExportProcessInterface $exportProcess)
    {
        $exportProcess->addInfoMessage('Started sending the file to E-mail.');
        $profileConfig = $exportProcess->getProfileConfig();
        $storeId = $this->storeManager->getDefaultStoreView()->getId();
        $emailData = $profileConfig->getExtensionAttributes()->getEmailFileDestination();
        $data = $this->variableProcessor->process($exportProcess);
        $processIdentity = $exportProcess->getIdentity();
        $tempDirectory = $this->tmpFileManagement->getTempDirectory($processIdentity);
        $content = $tempDirectory->readFile(
            $this->tmpFileManagement->getResultTempFileName($processIdentity)
        );
        $absolutePath =
            $tempDirectory->getAbsolutePath($this->tmpFileManagement->getResultTempFileName($processIdentity));
        $mimeType = $this->mime->getMimeType($absolutePath);
        $fileName = $exportProcess->getExportResult()->getResultFileName();
        $templateIdentifier = $emailData->getTemplate();
        $emailTo = array_filter(explode(',', $emailData->getEmailRecipients()));

        foreach ($emailTo as $reciever) {
            $transportBuild = $this->transportBuilder
                ->setTemplateIdentifier($templateIdentifier)
                ->setTemplateOptions(['area' => Area::AREA_ADMINHTML, 'store' => $storeId])
                ->setTemplateVars($data)
                ->setFromByScope($emailData->getSender())
                ->addTo($reciever)
                ->addAttachment($content, $fileName, $mimeType);
            $transportBuild->getTransport()->sendMessage();
        }
        $exportProcess->addInfoMessage('The file is successfully sent to the E-mail.');
    }
}
