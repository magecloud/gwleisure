<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Email;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ConfigInterface extends ExtensibleDataInterface
{
    /**
     * @return string|null
     */
    public function getSender(): ?string;

    /**
     * @param string|null $sender
     *
     * @return \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigInterface
     */
    public function setSender(?string $sender): ConfigInterface;

    /**
     * @return string|null
     */
    public function getEmailRecipients(): ?string;

    /**
     * @param string|null $emailRecipients
     *
     * @return \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigInterface
     */
    public function setEmailRecipients(?string $emailRecipients): ConfigInterface;

    /**
     * @return string|null
     */
    public function getEmailSubject(): ?string;

    /**
     * @param string|null $emailSubject
     *
     * @return \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigInterface
     */
    public function setEmailSubject(?string $emailSubject): ConfigInterface;

    /**
     * @return string|null
     */
    public function getTemplate(): ?string;

    /**
     * @param string|null $template
     *
     * @return \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigInterface
     */
    public function setTemplate(?string $template): ConfigInterface;

    /**
     * @return \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigExtensionInterface
     */
    public function getExtensionAttributes()
    : \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigExtensionInterface;

    /**
     * @param \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigExtensionInterface $extensionAttributes
     *
     * @return \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigInterface
     */
    public function setExtensionAttributes(
        \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigExtensionInterface $extensionAttributes
    ): ConfigInterface;
}
