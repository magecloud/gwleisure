<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Email;

use Magento\Framework\DataObject;

class Config extends DataObject implements ConfigInterface
{
    public const SENDER = 'sender';
    public const EMAIL_RECIPIENTS = 'email_recipients';
    public const EMAIL_SUBJECT = 'email_subject';
    public const EMAIL_TEMPLATE = 'template';

    /**
     * @var ConfigExtensionInterfaceFactory
     */
    private $extensionAttributesFactory;

    public function __construct(
        ConfigExtensionInterfaceFactory $extensionAttributesFactory,
        array $data = []
    ) {
        parent::__construct($data);
        $this->extensionAttributesFactory = $extensionAttributesFactory;
    }

    public function getSender(): ?string
    {
        return $this->getData(self::SENDER);
    }

    public function setSender(?string $sender): ConfigInterface
    {
        $this->setData(self::SENDER, $sender);

        return $this;
    }

    public function getEmailRecipients(): ?string
    {
        return $this->getData(self::EMAIL_RECIPIENTS);
    }

    public function setEmailRecipients(?string $emailRecipients): ConfigInterface
    {
        $this->setData(self::EMAIL_RECIPIENTS, $emailRecipients);

        return $this;
    }

    public function getEmailSubject(): ?string
    {
        return $this->getData(self::EMAIL_SUBJECT);
    }

    public function setEmailSubject(?string $emailSubject): ConfigInterface
    {
        $this->setData(self::EMAIL_SUBJECT, $emailSubject);

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->getData(self::EMAIL_TEMPLATE);
    }

    public function setTemplate(?string $template): ConfigInterface
    {
        $this->setData(self::EMAIL_TEMPLATE, $template);

        return $this;
    }

    public function getExtensionAttributes()
    : \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigExtensionInterface
    {
        if (null === $this->getData(self::EXTENSION_ATTRIBUTES_KEY)) {
            $this->setExtensionAttributes($this->extensionAttributesFactory->create());
        }

        return $this->getData(self::EXTENSION_ATTRIBUTES_KEY);
    }

    public function setExtensionAttributes(
        \Amasty\ExportPro\Export\FileDestination\Type\Email\ConfigExtensionInterface $extensionAttributes
    ): ConfigInterface {
        return $this->setData(self::EXTENSION_ATTRIBUTES_KEY, $extensionAttributes);
    }
}
