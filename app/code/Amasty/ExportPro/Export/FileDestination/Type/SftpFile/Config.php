<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\SftpFile;

use Magento\Framework\DataObject;

class Config extends DataObject implements ConfigInterface
{
    public const HOST = 'host';
    public const USER = 'user';
    public const PASSWORD = 'password';
    public const PATH = 'path';
    public const FILE_NAME = 'filename';

    public function getHost(): ?string
    {
        return $this->getData(self::HOST);
    }

    public function setHost(?string $host): ConfigInterface
    {
        $this->setData(self::HOST, $host);

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->getData(self::USER);
    }

    public function setUser(?string $user): ConfigInterface
    {
        $this->setData(self::USER, $user);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->getData(self::PASSWORD);
    }

    public function setPassword(?string $password): ConfigInterface
    {
        $this->setData(self::PASSWORD, $password);

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->getData(self::PATH);
    }

    public function setPath(?string $path): ConfigInterface
    {
        $this->setData(self::PATH, $path);

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->getData(self::FILE_NAME);
    }

    public function setFilename(?string $filename): ConfigInterface
    {
        $this->setData(self::FILE_NAME, $filename);

        return $this;
    }
}
