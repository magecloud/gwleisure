<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Rest\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class Methods implements OptionSourceInterface
{
    public const POST = 0;
    public const PUT = 1;

    public function toOptionArray(): array
    {
        return [
            ['value' => self::POST, 'label' => __('POST')],
            ['value' => self::PUT, 'label' => __('PUT')],
        ];
    }
}
