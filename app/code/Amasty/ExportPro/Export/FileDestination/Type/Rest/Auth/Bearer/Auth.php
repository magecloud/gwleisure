<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Rest\Auth\Bearer;

use Amasty\ExportCore\Api\ExportProcessInterface;
use Amasty\ExportPro\Export\FileDestination\Type\Rest\Auth\AuthInterface;
use Magento\Framework\HTTP\ClientInterface;

class Auth implements AuthInterface
{
    public function process(ExportProcessInterface $exportProcess, ClientInterface $curl)
    {
        if ($exportProcess->getProfileConfig()->getExtensionAttributes()->getRestFileDestination()
            && ($config = $exportProcess->getProfileConfig()->getExtensionAttributes()
                ->getRestFileDestination()->getExtensionAttributes()->getBearer())
        ) {
            $curl->addHeader('Authorization', 'Bearer ' . $config->getToken());
        }
    }
}
