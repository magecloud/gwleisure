<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Rest\Auth;

use Amasty\ExportCore\Api\ExportProcessInterface;
use Magento\Framework\HTTP\ClientInterface;

interface AuthInterface
{
    /**
     * @param ExportProcessInterface $exportProcess
     * @param ClientInterface $curl
     *
     * @return void
     */
    public function process(ExportProcessInterface  $exportProcess, ClientInterface $curl);
}
