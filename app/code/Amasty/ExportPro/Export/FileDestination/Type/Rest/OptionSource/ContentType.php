<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\FileDestination\Type\Rest\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class ContentType implements OptionSourceInterface
{
    public const JSON = 0;
    public const XML = 1;

    public function toOptionArray(): array
    {
        return [
            ['value' => self::JSON, 'label' => __('JSON')],
            ['value' => self::XML, 'label' => __('XML')],
        ];
    }
}
