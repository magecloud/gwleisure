<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\Template\Type\Xlsx;

use Amasty\ExportCore\Api\ExportProcessInterface;
use Amasty\ExportCore\Api\Template\RendererInterface;
use Amasty\ExportPro\Export\Template\Type\Spout\Renderer as SpoutRenderer;

class Renderer extends SpoutRenderer implements RendererInterface
{
    public const TYPE_ID = 'xlsx';
    public const EXTENSION = 'xlsx';
    public const WRITER_TYPE = \Box\Spout\Common\Type::XLSX;

    public function getConfig(ExportProcessInterface $exportProcess)
    {
        return $exportProcess->getProfileConfig()->getExtensionAttributes()->getXlsxTemplate();
    }
}
