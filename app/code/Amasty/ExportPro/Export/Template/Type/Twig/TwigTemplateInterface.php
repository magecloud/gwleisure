<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\Template\Type\Twig;

interface TwigTemplateInterface
{
    public function getName(): string;

    public function getHeader(): string;

    public function getContent(): string;

    public function getSeparator(): string;

    public function getFooter(): string;

    public function getExtension(): string;
}
