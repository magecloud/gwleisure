<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Export\PostProcessing\Type\Compress;

use Amasty\ExportCore\Api\Config\EntityConfigInterface;
use Amasty\ExportCore\Api\Config\ProfileConfigInterface;
use Amasty\ExportCore\Api\FormInterface;
use Amasty\ExportCore\Export\Config\ProfileConfig;
use Magento\Framework\App\RequestInterface;

class Meta implements FormInterface
{
    public const TYPE_ID = 'compress';

    public function getMeta(EntityConfigInterface $entityConfig, array $arguments = []): array
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Compress Export File With Gzip'),
                        'dataType' => 'boolean',
                        'prefer' => 'toggle',
                        'valueMap' => ['true' => 'compress', 'false' => ''],
                        'default' => '',
                        'formElement' => 'checkbox',
                        'processorFieldset' => 'processor_compress',
                        'visible' => true,
                        'componentType' => 'field',
                        'dataScope' => 'post_processors.' . 'compress'
                    ]
                ]
            ]
        ];
    }

    public function prepareConfig(ProfileConfigInterface $profileConfig, RequestInterface $request): FormInterface
    {
        $profileConfig->getExtensionAttributes()->setCompress(
            $request->getParam(ProfileConfig::POST_PROCESSORS)[self::TYPE_ID]
        );

        return $this;
    }

    public function getData(ProfileConfigInterface $profileConfig): array
    {
        return [self::TYPE_ID => (string)$profileConfig->getExtensionAttributes()->getCompress()];
    }
}
