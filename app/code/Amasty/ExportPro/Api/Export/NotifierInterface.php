<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Export Pro for Magento 2 (System)
 */

namespace Amasty\ExportPro\Api\Export;

use Amasty\ExportCore\Api\ExportProcessInterface;

interface NotifierInterface
{
    /**
     * Perform notification process.
     *
     * @param ExportProcessInterface $exportProcess
     */
    public function notify(ExportProcessInterface $exportProcess): void;
}
