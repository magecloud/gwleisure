<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Analytics for Magento 2 (System)
*/

declare(strict_types=1);

namespace Amasty\PreOrderAnalytic\ViewModel;

use Amasty\PreOrderAnalytic\Model\IsPreorderOrdersExist;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class PreorderAnalytic implements ArgumentInterface
{
    /**
     * @var IsPreorderOrdersExist
     */
    private $isPreorderOrdersExist;

    public function __construct(IsPreorderOrdersExist $isPreorderOrdersExist)
    {
        $this->isPreorderOrdersExist = $isPreorderOrdersExist;
    }

    public function isDataExist(): bool
    {
        return $this->isPreorderOrdersExist->execute();
    }
}
