<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Analytics for Magento 2 (System)
*/

declare(strict_types=1);

namespace Amasty\PreOrderAnalytic\Model\GetAnalyticCounter;

interface GetAnalyticCounterInterface
{
    public function execute(array $params): int;
}
