<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Pre Order Analytics for Magento 2 (System)
*/

declare(strict_types=1);

namespace Amasty\PreOrderAnalytic\Model\GetAnalyticCounter;

use Amasty\PreOrderAnalytic\Model\ResourceModel\LoadCountPreorderProducts;

class GetCountPreorderProducts implements GetAnalyticCounterInterface
{
    /**
     * @var LoadCountPreorderProducts
     */
    private $loadCountPreorderProducts;

    public function __construct(LoadCountPreorderProducts $loadCountPreorderProducts)
    {
        $this->loadCountPreorderProducts = $loadCountPreorderProducts;
    }

    public function execute(array $params): int
    {
        return $this->loadCountPreorderProducts->execute($params);
    }
}
