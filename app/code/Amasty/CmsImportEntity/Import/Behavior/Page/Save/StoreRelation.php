<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cms Import Entity for Magento 2 (System)
 */

namespace Amasty\CmsImportEntity\Import\Behavior\Page\Save;

use Amasty\CmsImportEntity\Model\ResourceModel\Store\RelationPersistence;
use Amasty\ImportCore\Api\BehaviorInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterface;
use Amasty\ImportCore\Api\Behavior\BehaviorResultInterfaceFactory;
use Magento\Cms\Api\Data\PageInterface;

class StoreRelation implements BehaviorInterface
{
    /**
     * @var BehaviorResultInterfaceFactory
     */
    private $resultFactory;

    /**
     * @var RelationPersistence
     */
    private $relationPersistence;

    public function __construct(
        BehaviorResultInterfaceFactory $resultFactory,
        RelationPersistence $relationPersistence
    ) {
        $this->resultFactory = $resultFactory;
        $this->relationPersistence = $relationPersistence;
    }

    /**
     * @inheritDoc
     */
    public function execute(array &$data, ?string $customIdentifier = null): BehaviorResultInterface
    {
        $this->relationPersistence->save($data, PageInterface::class);

        return $this->resultFactory->create();
    }
}
