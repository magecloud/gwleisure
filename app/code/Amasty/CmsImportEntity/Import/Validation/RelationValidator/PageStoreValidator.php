<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cms Import Entity for Magento 2 (System)
 */

namespace Amasty\CmsImportEntity\Import\Validation\RelationValidator;

use Amasty\CmsImportEntity\Model\ResourceModel\Store\RelationUniqueness;
use Amasty\ImportCore\Api\Validation\RelationValidatorInterface;
use Magento\Cms\Api\Data\PageInterface;

class PageStoreValidator implements RelationValidatorInterface
{
    /**
     * @var RelationUniqueness
     */
    private $relationUniqueness;

    /**
     * @var string|null
     */
    private $message;

    public function __construct(
        RelationUniqueness $relationUniqueness
    ) {
        $this->relationUniqueness = $relationUniqueness;
    }

    /**
     * @inheritDoc
     */
    public function validate(array $entityRow, array $subEntityRows): bool
    {
        if (isset($entityRow['identifier'])) {
            $storeIds = array_column($subEntityRows, 'store_id');
            if (!$this->relationUniqueness->isIdentifierUnique(
                PageInterface::class,
                $entityRow['identifier'],
                $storeIds
            )) {
                $this->message = (string)__(
                    'A page identifier %1 already exists in the specified stores: %2.',
                    $entityRow['identifier'],
                    implode(',', $storeIds)
                );

                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}
