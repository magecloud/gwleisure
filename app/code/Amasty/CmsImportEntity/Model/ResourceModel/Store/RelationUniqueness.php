<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Cms Import Entity for Magento 2 (System)
 */

namespace Amasty\CmsImportEntity\Model\ResourceModel\Store;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;

class RelationUniqueness
{
    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection
    ) {
        $this->metadataPool = $metadataPool;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Checks if identifier is unique in given scopes
     *
     * @param string $entityType
     * @param string $identifier
     * @param array $storeIds
     * @return bool
     * @throws \Exception
     */
    public function isIdentifierUnique(string $entityType, string $identifier, array $storeIds)
    {
        if (empty($storeIds)) {
            return true;
        }

        $metadata = $this->metadataPool->getMetadata($entityType);
        $connectionName = $metadata->getEntityConnectionName();
        $connection = $this->resourceConnection->getConnectionByName($connectionName);
        $entityTable = $this->resourceConnection->getTableName(
            $metadata->getEntityTable(),
            $connectionName
        );
        $linkField = $metadata->getLinkField();
        $select = $connection->select()
            ->from(['e' => $entityTable])
            ->join(
                ['es' => $entityTable . '_store'],
                'e.' . $linkField . ' = es.' . $linkField,
                []
            )
            ->where('e.identifier = ?  ', $identifier)
            ->where('es.store_id IN (?)', $storeIds);

        if ($connection->fetchRow($select)) {
            return false;
        }

        return true;
    }
}
