# Pre Order MFTF3
18 Pre Order specific tests, grouped by purpose, for greater convenience.

To run "AdminAmastyReleaseDateTest" test  change crontab.xml in PreOrderRelease module so that amasty_preorder_release_date cron job runs every minute. 
To run "AdminAmastyReleaseNotificationTest" test add MAILHOG_BASE_URL=http://mail.docker.loc:8025 to the magento/dev/tests/acceptance/.eav file 

    Tests are divided into following groups:
    - PreOrderLite (is used for running all tests related to the Lite version. E.g. vendor/bin/mftf run:group PreOrderLite -r)
    - PreOrderPro (is used for running all tests related to the Pro version (it includes all PreOrderLite tests). E.g. vendor/bin/mftf run:group PreOrderPro -r)
