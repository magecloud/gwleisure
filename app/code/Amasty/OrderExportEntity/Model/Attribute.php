<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Model;

class Attribute extends \Magento\Framework\Model\AbstractModel
{
    public const ATTRIBUTE_ID = 'entity_id';

    protected function _construct()
    {
        $this->_init(\Amasty\OrderExportEntity\Model\ResourceModel\Attribute::class);
    }
}
