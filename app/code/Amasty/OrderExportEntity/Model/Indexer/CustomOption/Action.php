<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Model\Indexer\CustomOption;

use Amasty\OrderExportEntity\Model\Indexer\CustomOption\Action\DataProvider;
use Generator;
use Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot;
use Magento\Sales\Model\Order\Item;
use Psr\Log\LoggerInterface;

/**
 * Custom options indexer action
 */
class Action
{
    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Snapshot
     */
    private $snapshot;

    public function __construct(
        DataProvider $dataProvider,
        LoggerInterface $logger,
        Snapshot $snapshot
    ) {
        $this->dataProvider = $dataProvider;
        $this->logger = $logger;
        $this->snapshot = $snapshot;
    }

    /**
     * Rebuild index
     */
    public function rebuildIndex(array $ids = []): Generator
    {
        $lastOrderItemId = 0;

        try {
            $orderItems = $this->dataProvider->getOrderItemsForIndex($ids, $lastOrderItemId);

            while (count($orderItems) > 0) {
                $lastOrderItemId = end($orderItems)['item_id'];

                /**
                 * @param Item $orderItem
                 * @return bool
                 */
                $filterAvailableForIndexCallback = function ($orderItem) {
                    $options = $orderItem->getProductOptionByCode('options');

                    return is_array($options);
                };
                $orderItems = array_filter($orderItems, $filterAvailableForIndexCallback);

                foreach ($orderItems as $orderItem) {
                    if ($index = $this->dataProvider->prepareCustomOptionsIndex($orderItem)) {
                        yield $orderItem['item_id'] => $index;
                    }
                }

                $orderItems = $this->dataProvider->getOrderItemsForIndex($ids, (int) $lastOrderItemId);

                if (method_exists($this->snapshot, 'clear') && end($orderItems)) {
                    // Because collection extends \Magento\Framework\Model\ResourceModel\Db\VersionControl\Collection
                    // we must clear snapshot data to avoid storing items in memory due problems on big data
                    $this->snapshot->clear(end($orderItems));
                }
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
