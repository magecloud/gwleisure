<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Model\Indexer\CustomOption\Action;

use Magento\Catalog\Api\Data\CustomOptionInterface;
use Magento\Catalog\Api\Data\ProductCustomOptionInterface;
use Magento\Catalog\Model\Product\Option;
use Magento\Catalog\Model\ResourceModel\Product\Option\Collection as ProductOptionCollection;
use Magento\Catalog\Model\ResourceModel\Product\Option\CollectionFactory as ProductOptionCollectionFactory;
use Magento\Framework\Data\Collection;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory as OrderItemCollectionFactory;

/**
 * Custom options indexer data provider
 */
class DataProvider
{
    /**
     * Default option type to resolve
     */
    public const DEFAULT_OPTION_TYPE = ProductCustomOptionInterface::OPTION_TYPE_FIELD;

    /**
     * Custom option types without option values
     */
    public const SIMPLE_CUSTOM_OPTION_TYPES = [
        ProductCustomOptionInterface::OPTION_TYPE_FIELD,
        ProductCustomOptionInterface::OPTION_GROUP_TEXT
    ];

    /**
     * Custom option types with option values
     */
    public const SELECT_CUSTOM_OPTION_TYPES = [
        ProductCustomOptionInterface::OPTION_TYPE_DROP_DOWN,
        ProductCustomOptionInterface::OPTION_TYPE_RADIO,
        ProductCustomOptionInterface::OPTION_TYPE_CHECKBOX,
        ProductCustomOptionInterface::OPTION_TYPE_MULTIPLE
    ];

    /**
     * Order item's options required keys
     */
    public const OPTIONS_REQUIRED_KEYS = ['value', 'label', 'option_id'];

    /**
     * @var OrderItemCollectionFactory
     */
    private $orderItemCollectionFactory;

    /**
     * @var ProductOptionCollectionFactory
     */
    private $productOptionCollectionFactory;

    /**
     * @var array
     */
    private $productOptionsByProductIdAndStoreId = [];

    public function __construct(
        OrderItemCollectionFactory $orderItemCollectionFactory,
        ProductOptionCollectionFactory $productOptionCollectionFactory
    ) {
        $this->orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->productOptionCollectionFactory = $productOptionCollectionFactory;
    }

    /**
     * Get order items data for index
     *
     * @param array $orderItemIds
     * @param int|null $lastOrderItemId
     * @param int|null $batchSize
     * @return Item[]
     */
    public function getOrderItemsForIndex(
        array $orderItemIds = [],
        ?int $lastOrderItemId = 0,
        ?int $batchSize = 200
    ): array {
        $collection = $this->orderItemCollectionFactory->create();
        $collection->getSelect()->joinInner(
            ['product' => $collection->getTable('catalog_product_entity')],
            'main_table.product_id = product.entity_id',
            []
        );
        $collection->addFieldToFilter('main_table.item_id', ['gt' => $lastOrderItemId])
            ->addOrder('main_table.item_id', Collection::SORT_ORDER_ASC)
            ->setPageSize($batchSize);

        if (count($orderItemIds)) {
            $collection->addFieldToFilter('main_table.item_id', ['in' => $orderItemIds]);
        }

        return $collection->getItems();
    }

    /**
     * Prepare custom options index
     */
    public function prepareCustomOptionsIndex(Item $orderItem): array
    {
        $indexData = [];

        $itemStoreId = (int) $orderItem->getStoreId();
        foreach ($orderItem->getProductOptionByCode('options') as $option) {
            if ($this->isOptionDataValidForIndex($option)) {
                $indexRow = [
                    'order_item_id' => $orderItem->getId(),
                    'option_title' => $option['label'],
                    'option_value' => $option['value'],
                    'price' => null,
                    'sku' => null
                ];

                $optionType = $option['option_type'] ?? self::DEFAULT_OPTION_TYPE;

                if (in_array($optionType, self::SIMPLE_CUSTOM_OPTION_TYPES)) {
                    $productOptions = $this->getProductOptions((int) $orderItem->getProductId(), $itemStoreId);
                    $productOption = $productOptions[$option[CustomOptionInterface::OPTION_ID]] ?? null;

                    if ($productOption) {
                        $indexRow['price'] = $productOption->getPrice();
                        $indexRow['sku'] = $productOption->getSku();
                    }
                } elseif (in_array($optionType, self::SELECT_CUSTOM_OPTION_TYPES)) {
                    $productOptions = $this->getProductOptions((int) $orderItem->getProductId(), $itemStoreId);
                    $productOption = $productOptions[$option[CustomOptionInterface::OPTION_ID]] ?? null;

                    if ($productOption
                        && $optionValue = $productOption->getValueById($option[CustomOptionInterface::OPTION_VALUE])
                    ) {
                        $indexRow['price'] = $optionValue->getPrice();
                        $indexRow['sku'] = $optionValue->getSku();
                    }
                }

                $indexData[] = $indexRow;
            }
        }

        return $indexData;
    }

    /**
     * Checks if option data is valid for indexing
     */
    private function isOptionDataValidForIndex(array $option): bool
    {
        foreach (self::OPTIONS_REQUIRED_KEYS as $key) {
            if (!isset($option[$key])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get product options by product ID and store ID
     *
     * @param int $productId
     * @param int $storeId
     * @return Option[]
     */
    private function getProductOptions(int $productId, int $storeId): array
    {
        $key = $productId . '-' . $storeId;
        if (!isset($this->productOptionsByProductIdAndStoreId[$key])) {
            $productOptionCollection = $this->productOptionCollectionFactory->create();
            $this->productOptionsByProductIdAndStoreId[$key] = $productOptionCollection->getProductOptions(
                $productId,
                $storeId
            );
        }

        return $this->productOptionsByProductIdAndStoreId[$key];
    }
}
