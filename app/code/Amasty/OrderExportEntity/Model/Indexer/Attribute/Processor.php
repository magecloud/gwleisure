<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Model\Indexer\Attribute;

use Amasty\OrderExportEntity\Model\Indexer\Attribute as AttributeIndexer;

class Processor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * {@inheritdoc}
     */
    public const INDEXER_ID = AttributeIndexer::INDEXER_ID;
}
