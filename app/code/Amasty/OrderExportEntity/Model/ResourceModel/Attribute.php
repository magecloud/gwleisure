<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Model\ResourceModel;

use Amasty\OrderExportEntity\Model\Attribute as AttributeModel;

class Attribute extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public const TABLE_NAME = 'amasty_order_export_attribute';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, AttributeModel::ATTRIBUTE_ID);
    }
}
