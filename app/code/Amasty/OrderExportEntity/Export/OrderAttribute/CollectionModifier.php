<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\OrderAttribute;

use Amasty\ExportCore\Api\CollectionModifierInterface;

class CollectionModifier implements CollectionModifierInterface
{
    public function apply(\Magento\Framework\Data\Collection $collection): CollectionModifierInterface
    {
        $collection->addFieldToFilter('parent_entity_type', 1);

        return $this;
    }
}
