<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\Filter\Type\CustomOption;

/**
 * Filter value item data interface
 */
interface ValueItemInterface
{
    /**
     * @return string
     */
    public function getKey(): ?string;

    /**
     * @param string $key
     * @return ValueItemInterface
     */
    public function setKey(?string $key): ValueItemInterface;

    /**
     * @return string
     */
    public function getValue(): ?string;

    /**
     * @param string $value
     * @return ValueItemInterface
     */
    public function setValue(?string $value): ValueItemInterface;
}
