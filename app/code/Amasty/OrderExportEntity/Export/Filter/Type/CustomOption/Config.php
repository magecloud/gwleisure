<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\Filter\Type\CustomOption;

class Config implements ConfigInterface
{
    /**
     * @var ValueItemInterface[]
     */
    private $valueItems;

    public function getValueItems(): ?array
    {
        return $this->valueItems;
    }

    public function setValueItems(?array $valueItems): ConfigInterface
    {
        $this->valueItems = $valueItems;
        return $this;
    }
}
