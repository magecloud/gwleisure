<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\Filter\Type\CustomOption;

use Amasty\ExportCore\Api\Config\Profile\FieldFilterInterface;
use Amasty\ExportCore\Api\Filter\FilterInterface;
use Amasty\ExportCore\Export\Filter\Utils\AfterFilterApplier;
use Magento\Framework\Data\Collection;
use Magento\Framework\DB\Select;

/**
 * Custom option filter
 */
class Filter implements FilterInterface
{
    public const TYPE_ID = 'custom_option';

    /**
     * @var AfterFilterApplier
     */
    private $afterFilterApplier;

    public function __construct(
        AfterFilterApplier $afterFilterApplier
    ) {
        $this->afterFilterApplier = $afterFilterApplier;
    }

    public function apply(Collection $collection, FieldFilterInterface $filter)
    {
        /** @var ConfigInterface $config */
        $config = $filter->getExtensionAttributes()->getCustomOptionFilter();
        if ($config) {
            $conditions = [];
            $filterCondition = $filter->getCondition() ?? 'eq';

            /** @var \Magento\Framework\Data\Collection\AbstractDb $collection */
            $connection = $collection->getConnection();
            foreach ($config->getValueItems() as $valueItem) {
                $valueItemConditions = [
                    $connection->prepareSqlCondition('option_title', $valueItem->getKey()),
                    $connection->prepareSqlCondition(
                        'option_value',
                        [$filterCondition => $valueItem->getValue()]
                    )
                ];
                $conditions[] = implode(' ' . Select::SQL_AND . ' ', $valueItemConditions);
            }

            $collection->getSelect()
                ->where(
                    '(' . implode(') ' . Select::SQL_OR . ' (', $conditions) . ')',
                    null,
                    Select::TYPE_CONDITION
                );
        }
    }

    public function applyAfter(array $row, FieldFilterInterface $filter): bool
    {
        $value = $row[$filter->getField()] ?? null;
        $config = $filter->getExtensionAttributes()->getCustomOptionFilter();
        if (!$config || !$value) {
            return false;
        }

        $condition = [];
        $filterCondition = $filter->getCondition() ?? 'eq';
        foreach ($config->getValueItems() as $valueItem) {
            $condition[] = [$filterCondition => $valueItem->getValue()];
        }

        return $this->afterFilterApplier->apply($condition, $value, null);
    }
}
