<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\Filter\Type\CustomOption;

interface ConfigInterface
{
    /**
     * @return \Amasty\OrderExportEntity\Export\Filter\Type\CustomOption\ValueItemInterface[]|null
     */
    public function getValueItems(): ?array;

    /**
     * @param \Amasty\OrderExportEntity\Export\Filter\Type\CustomOption\ValueItemInterface[] $valueItems
     * @return ConfigInterface
     */
    public function setValueItems(?array $valueItems): ConfigInterface;
}
