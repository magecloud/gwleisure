<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\Filter\Type\CustomOption;

class ValueItem implements ValueItemInterface
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $value;

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(?string $key): ValueItemInterface
    {
        $this->key = $key;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): ValueItemInterface
    {
        $this->value = $value;
        return $this;
    }
}
