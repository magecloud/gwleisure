<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Order Export Entity for Magento 2 (System)
 */

namespace Amasty\OrderExportEntity\Export\Order\SourceOption;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Directory\Model\Currency;

class CurrencyOptions implements OptionSourceInterface
{
    /**
     * @var Currency
     */
    private $currency;

    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }

    public function toOptionArray(): array
    {
        $result = [];
        if ($currencies = $this->currency->getConfigAllowCurrencies()) {
            foreach ($currencies as $key => $currency) {
                $result[] = ['value' => $currency, 'label' => $currency];
            }
        }

        return $result;
    }
}
