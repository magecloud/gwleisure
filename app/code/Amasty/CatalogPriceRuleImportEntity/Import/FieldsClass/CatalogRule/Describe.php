<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Catalog Rule Import Entity for Magento 2 (system)
 */

namespace Amasty\CatalogPriceRuleImportEntity\Import\FieldsClass\CatalogRule;

use Amasty\ImportCore\Api\Config\Entity\FieldsConfigInterface;
use Amasty\ImportCore\Import\FieldsClass\Describe as CoreDescribe;

class Describe extends CoreDescribe
{
    /**
     * @inheritDoc
     */
    public function execute(FieldsConfigInterface $existingConfig): FieldsConfigInterface
    {
        $fieldsConfig = parent::execute($existingConfig);

        $fields = $fieldsConfig->getFields();
        $rowIdField = $this->getFieldByName('row_id', $fields);
        if ($rowIdField) {
            $entityIdField = $this->getFieldByName('rule_id', $fields);
            $entityIdField->setIdentification(null);
        }

        return $fieldsConfig;
    }
}
