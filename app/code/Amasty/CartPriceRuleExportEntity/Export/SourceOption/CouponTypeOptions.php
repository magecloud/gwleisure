<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Sales Rule Export Entity for Magento 2 (system)
 */

namespace Amasty\CartPriceRuleExportEntity\Export\SourceOption;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\SalesRule\Model\Rule;

class CouponTypeOptions implements OptionSourceInterface
{
    /**
     * @var Rule
     */
    private $rule;

    public function __construct(Rule $rule)
    {
        $this->rule = $rule;
    }

    public function toOptionArray(): array
    {
        $optionArray = [];
        foreach ($this->rule->getCouponTypes() as $value => $label) {
            $optionArray[] = ['value' => $value, 'label' => $label];
        }

        return $optionArray;
    }
}
