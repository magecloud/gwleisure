<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Model\ResourceModel\ScopedEntity;

use Magento\Framework\Data\Collection\AbstractDb;

interface ItemCollectorInterface
{
    public const STORE_ID_FIELD = 'store_id';

    /**
     * Set fields to select
     *
     * @param array $fieldsToSelect
     * @return $this
     */
    public function setFieldsToSelect(array $fieldsToSelect);

    /**
     * Set fields to filter
     *
     * @param array $fieldsToFilter
     * @return $this
     */
    public function setFieldsToFilter(array $fieldsToFilter);

    /**
     * Collect scoped entities
     *
     * @param AbstractDb $collection
     * @param array $defaultItems
     * @param int|int[] $scope
     * @return array
     */
    public function collect(AbstractDb $collection, array $defaultItems, $scope);

    /**
     * Collect scoped entities size
     *
     * @param AbstractDb $collection
     * @param int|int[] $scope
     * @return int
     */
    public function collectSize(AbstractDb $collection, $scope);
}
