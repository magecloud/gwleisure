<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Model\ResourceModel\CategoryProduct;

use Magento\Framework\DataObject;
use Magento\Catalog\Model\ResourceModel\CategoryProduct as CategoryProductResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(DataObject::class, CategoryProductResource::class);
    }

    /**
     * @inheritdoc
     */
    public function addFieldToSelect($field, $alias = null)
    {
        if (!is_array($field)) {
            if ($field == 'product_sku') {
                return $this->joinProductSku();
            }
        }

        return parent::addFieldToSelect($field, $alias);
    }

    /**
     * Joins product SKU to collection
     *
     * @return \Amasty\ProductExportEntity\Model\ResourceModel\CategoryProduct\Collection
     */
    private function joinProductSku()
    {
        if (!$this->getFlag('sku_joined')) {
            $this->getSelect()
                ->joinLeft(
                    ['product_entity_table' => $this->getTable('catalog_product_entity')],
                    'main_table.product_id = product_entity_table.entity_id',
                    [
                        'product_sku' => 'product_entity_table.sku'
                    ]
                );
            $this->setFlag('sku_joined', true);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field == 'product_sku') {
            $this->addFilter('product_entity_table.sku', $condition, 'public');

            return $this;
        }

        return parent::addFieldToFilter($field, $condition);
    }
}
