<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Model\ResourceModel\Product\MediaGallery;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\Gallery as GalleryResource;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DataObject;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Psr\Log\LoggerInterface;

class Collection extends AbstractCollection
{
    public const MEDIA_GALLERY_RELATION_TABLE = 'catalog_product_entity_media_gallery_value_to_entity';

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        MetadataPool $metadataPool,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->metadataPool = $metadataPool;
    }

    protected function _construct()
    {
        $this->_init(DataObject::class, GalleryResource::class);
    }

    public function addFieldToSelect($field, $alias = null)
    {
        if (!is_array($field)) {
            if ($field === 'product_id') {
                return $this->joinProductId();
            }
        }

        return parent::addFieldToSelect($field, $alias);
    }

    private function joinProductId(): Collection
    {
        if (!$this->getFlag('amasty_product_id_joined')) {
            $this->getSelect()
                ->joinLeft(
                    [
                        'media_gallery_relation' => $this->getTable(self::MEDIA_GALLERY_RELATION_TABLE)
                    ],
                    'main_table.value_id = media_gallery_relation.value_id',
                    [
                        'product_id' => 'media_gallery_relation.' . $this->getProductLinkField()
                    ]
                );
            $this->addFilterToMap('value_id', 'main_table.value_id');
            $this->setFlag('amasty_product_id_joined', true);
        }

        return $this;
    }

    public function addFieldToFilter($field, $condition = null)
    {
        if ($field === 'product_id') {
            $this->addFilter('media_gallery_relation.' . $this->getProductLinkField(), $condition, 'public');

            return $this;
        }

        return parent::addFieldToFilter($field, $condition);
    }

    private function getProductLinkField(): ?string
    {
        return $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField();
    }
}
