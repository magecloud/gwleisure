<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Model\ResourceModel\Product\Type\Bundle\Option;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Value extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('catalog_product_bundle_option_value', 'value_id');
    }
}
