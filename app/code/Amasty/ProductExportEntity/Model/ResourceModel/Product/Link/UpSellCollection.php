<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Model\ResourceModel\Product\Link;

use Magento\Catalog\Model\Product\Link;

class UpSellCollection extends AbstractCollection
{
    protected function getLinkTypeId()
    {
        return Link::LINK_TYPE_UPSELL;
    }
}
