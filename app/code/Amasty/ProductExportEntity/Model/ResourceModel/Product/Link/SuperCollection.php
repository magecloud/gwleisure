<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Model\ResourceModel\Product\Link;

use Magento\GroupedProduct\Model\ResourceModel\Product\Link;

class SuperCollection extends AbstractCollection
{
    protected function getLinkTypeId()
    {
        return Link::LINK_TYPE_GROUPED;
    }
}
