<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Export\DataHandling\FieldGenerator;

use Amasty\ExportCore\Api\VirtualField\GeneratorInterface;
use Amasty\ExportCore\Export\DataHandling\FieldModifier\Catalog\CategoriesPathResolver;
use Magento\Store\Model\Store;

class Categories implements GeneratorInterface
{
    /**
     * @var CategoriesPathResolver
     */
    private $categoriesPathResolver;

    public function __construct(
        CategoriesPathResolver $categoriesPathResolver
    ) {
        $this->categoriesPathResolver = $categoriesPathResolver;
    }

    public function generateValue(array $currentRecord)
    {
        $path = $this->categoriesPathResolver->getNamePathByEntityId(
            (int)$currentRecord['category_id'] ?? 0,
            Store::DEFAULT_STORE_ID
        );
        if ($path) {
            return $path;
        }

        return '';
    }
}
