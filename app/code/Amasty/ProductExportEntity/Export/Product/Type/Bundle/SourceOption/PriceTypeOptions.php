<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Export\Product\Type\Bundle\SourceOption;

use Magento\Framework\Data\OptionSourceInterface;

class PriceTypeOptions implements OptionSourceInterface
{
    public const PRICE_TYPE_FIXED = 0;
    public const PRICE_TYPE_PERCENT = 1;

    /**
     * @var array
     */
    private $options;

    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = [
                [
                    'value' => self::PRICE_TYPE_FIXED,
                    'label' => __('fixed')
                ],
                [
                    'value' => self::PRICE_TYPE_PERCENT,
                    'label' => __('percent')
                ]
            ];
        }
        return $this->options;
    }
}
