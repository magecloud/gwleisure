<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Product Export Entity for Magento 2 (System)
 */

namespace Amasty\ProductExportEntity\Export\Product\Type\Bundle\SourceOption;

use Magento\Framework\Data\OptionSourceInterface;

class TypeOptions implements OptionSourceInterface
{
    public const TYPE_DYNAMIC = '0';

    public const TYPE_FIXED = '1';

    /**
     * @var array
     */
    private $options;

    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = [
                [
                    'value' => self::TYPE_DYNAMIC,
                    'label' => __('dynamic')
                ],
                [
                    'value' => self::TYPE_FIXED,
                    'label' => __('fixed')
                ]
            ];
        }
        return $this->options;
    }
}
