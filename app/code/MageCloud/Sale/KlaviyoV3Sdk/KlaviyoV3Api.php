<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
declare(strict_types=1);

namespace MageCloud\Sale\KlaviyoV3Sdk;

use Klaviyo\Reclaim\KlaviyoV3Sdk\Exception\KlaviyoApiException;
use Klaviyo\Reclaim\KlaviyoV3Sdk\Exception\KlaviyoAuthenticationException;
use Klaviyo\Reclaim\KlaviyoV3Sdk\Exception\KlaviyoRateLimitException;
use Klaviyo\Reclaim\KlaviyoV3Sdk\KlaviyoV3Api as DefaultKlaviyoV3Api;

/**
 * Class KlaviyoV3Api
 * @package MageCloud\Sale\KlaviyoV3Sdk
 */
class KlaviyoV3Api extends DefaultKlaviyoV3Api
{
    /**
     * Request methods
     */
    const HTTP_PATCH = 'PATCH';

    /**
     * Create a new Profile in Klaviyo
     * https://developers.klaviyo.com/en/reference/create_profile
     *
     * @param array $attributes
     * @return array
     * @throws KlaviyoApiException
     * @throws KlaviyoAuthenticationException
     * @throws KlaviyoRateLimitException
     */
    public function createCustomerProfile(array $attributes): array
    {
        $body = [
            self::DATA_KEY_PAYLOAD => [
                self::TYPE_KEY_PAYLOAD => self::PROFILE_KEY_PAYLOAD,
                self::ATTRIBUTE_KEY_PAYLOAD => $attributes
            ]
        ];

        $response = $this->requestV3('api/profiles/', self::HTTP_POST, $body);
        $id = $response[self::DATA_KEY_PAYLOAD][self::ID_KEY_PAYLOAD] ?? null;

        return [
            'data' => $response[self::DATA_KEY_PAYLOAD],
            'profile_id' => $id
        ];
    }

    /**
     * Update a Profile in Klaviyo
     * https://developers.klaviyo.com/en/reference/update_profile
     *
     * @param string $profileId
     * @param array $attributes
     * @return void
     * @throws KlaviyoApiException
     * @throws KlaviyoAuthenticationException
     * @throws KlaviyoRateLimitException
     */
    public function updateCustomerProfile(string $profileId, array $attributes): void
    {
        $body = [
            self::DATA_KEY_PAYLOAD => [
                self::TYPE_KEY_PAYLOAD => self::PROFILE_KEY_PAYLOAD,
                self::ID_KEY_PAYLOAD => $profileId,
                self::ATTRIBUTE_KEY_PAYLOAD => $attributes
            ]
        ];

        $this->requestV3("api/profiles/{$profileId}", self::HTTP_PATCH, $body);
    }

    /**
     * Add a Profile to a list using profile id
     * https://developers.klaviyo.com/en/v2023-08-15/reference/create_list_relationships
     *
     * @param string $listId
     * @param string $profileId
     * @return void
     * @throws KlaviyoApiException
     * @throws KlaviyoAuthenticationException
     * @throws KlaviyoRateLimitException
     */
    public function addCustomerProfileToList(string $listId, string $profileId): void
    {
        $body = [
            self::DATA_KEY_PAYLOAD => [
                [
                    self::TYPE_KEY_PAYLOAD => self::PROFILE_KEY_PAYLOAD,
                    self::ID_KEY_PAYLOAD => $profileId
                ]
            ]
        ];

        $this->requestV3("api/lists/{$listId}/relationships/profiles/", self::HTTP_POST, $body);
    }
}