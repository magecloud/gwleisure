<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
namespace MageCloud\Sale\Controller\Subscriber;

use Magento\Customer\Api\AccountManagementInterface as CustomerAccountManagement;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Validator\EmailAddress as EmailValidator;
use Magento\Newsletter\Controller\Subscriber as SubscriberController;
use Magento\Newsletter\Model\Subscriber;
use Magento\Newsletter\Model\SubscriptionManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use MageCloud\Sale\Model\AccessProcessor;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

/**
 * Class Processor
 * @package MageCloud\Sale\Controller\Subscriber
 */
class Processor extends SubscriberController implements HttpPostActionInterface
{
    /**
     * @var CustomerAccountManagement
     */
    private $customerAccountManagement;

    /**
     * @var EmailValidator
     */
    private $emailValidator;

    /**
     * @var SubscriptionManagerInterface
     */
    private $subscriptionManager;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var RawFactory
     */
    private $resultRawFactory;

    /**
     * @var AccessProcessor
     */
    private $accessProcessor;

    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param Context $context
     * @param SubscriberFactory $subscriberFactory
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     * @param CustomerUrl $customerUrl
     * @param CustomerAccountManagement $customerAccountManagement
     * @param SubscriptionManagerInterface $subscriptionManager
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param AccessProcessor $accessProcessor
     * @param JsonSerializer $jsonSerializer
     * @param EmailValidator|null $emailValidator
     * @param CustomerRepositoryInterface|null $customerRepository
     */
    public function __construct(
        Context $context,
        SubscriberFactory $subscriberFactory,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        CustomerUrl $customerUrl,
        CustomerAccountManagement $customerAccountManagement,
        SubscriptionManagerInterface $subscriptionManager,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        AccessProcessor $accessProcessor,
        JsonSerializer $jsonSerializer,
        EmailValidator $emailValidator = null,
        CustomerRepositoryInterface $customerRepository = null
    ) {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->subscriptionManager = $subscriptionManager;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->accessProcessor = $accessProcessor;
        $this->jsonSerializer = $jsonSerializer;
        $this->emailValidator = $emailValidator ?: ObjectManager::getInstance()->get(EmailValidator::class);
        $this->customerRepository = $customerRepository ?: ObjectManager::getInstance()
            ->get(CustomerRepositoryInterface::class);
        parent::__construct(
            $context,
            $subscriberFactory,
            $customerSession,
            $storeManager,
            $customerUrl
        );
    }

    /**
     * Validates that the email address isn't being used by a different account.
     *
     * @param string $email
     * @param array $response
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function validateEmailAvailable(string $email, array &$response = []): void
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        if ($this->_customerSession->isLoggedIn()
            && ($this->_customerSession->getCustomerDataObject()->getEmail() !== $email
                && !$this->customerAccountManagement->isEmailAvailable($email, $websiteId))
        ) {
            $response['message'] = (string)__('This email address is already assigned to another user.');
            $response['errors'] = true;
        }
    }

    /**
     * Validates the format of the email address
     *
     * @param string $email
     * @param array $response
     * @return void
     */
    protected function validateEmailFormat(string $email, array &$response = []): void
    {
        if (!$this->emailValidator->isValid($email)) {
            $response['message'] = (string)__('Please enter a valid email address.');
            $response['errors'] = true;
        }
    }

    /**
     * Get result URL
     *
     * @param string|null $routePath
     * @return string
     */
    private function getResultUrl(string $routePath = null): string
    {
        return $this->_url->getUrl($routePath);
    }

    /**
     * @return Raw|mixed|null
     */
    private function getEmail()
    {
        $httpBadRequestCode = 400;

        /** @var Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        try {
            $formData = $this->jsonSerializer->unserialize(($this->getRequest()->getContent()));
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        if (!$formData || $this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        return $formData['email'] ?? null;
    }

    /**
     * @return Json|Raw
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     */
    public function execute()
    {
        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        $response = [
            'errors' => false,
            'message' => ''
        ];

        if (!$email = (string)$this->getEmail()) {
            $response = [
                'errors' => true,
                'message' => (string)__('Email is required and not provided.')
            ];
            return $resultJson->setData($response);
        }

        try {
            $this->validateEmailFormat($email, $response);
            $this->validateEmailAvailable($email, $response);

            if (isset($response['message']) && !empty($response['message'])) {
                return $resultJson->setData($response);
            }

            $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
            /** @var Subscriber $subscriber */
            $subscriber = $this->_subscriberFactory->create()->loadBySubscriberEmail($email, $websiteId);
            if (
                $subscriber->getId()
                && (int)$subscriber->getSubscriberStatus() === Subscriber::STATUS_SUBSCRIBED
            ) {
                $response['redirectUrl'] = $this->_redirect->success($this->getResultUrl('sale'));
                return $resultJson->setData($response);
            }

            $storeId = (int)$this->_storeManager->getStore()->getId();
            $currentCustomerId = $this->getCustomerId($email, $websiteId);

            $subscriber = $currentCustomerId
                ? $this->subscriptionManager->subscribeCustomer($currentCustomerId, $storeId)
                : $this->subscriptionManager->subscribe($email, $storeId);
            $message = $this->getSuccessMessage((int)$subscriber->getSubscriberStatus());
        } catch (LocalizedException $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => (string)__('Something went wrong with the subscribe.')
            ];
        }

        if (empty($response['errors']) && empty($response['message'])) {
            $this->accessProcessor->setAccessGranted();
            $response['redirectUrl'] = $this->_redirect->success($this->getResultUrl('sale'));
        }

        return $resultJson->setData($response);
    }

    /**
     * Check if customer with provided email exists and return its id
     *
     * @param string $email
     * @param int $websiteId
     * @return int|null
     * @throws LocalizedException
     */
    private function getCustomerId(string $email, int $websiteId): ?int
    {
        try {
            $customer = $this->customerRepository->get($email, $websiteId);
            return (int)$customer->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Get success message
     *
     * @param int $status
     * @return Phrase
     */
    private function getSuccessMessage(int $status): Phrase
    {
        if ($status === Subscriber::STATUS_NOT_ACTIVE) {
            return __('The confirmation request has been sent.');
        }

        return __('Thank you for your subscription.');
    }
}