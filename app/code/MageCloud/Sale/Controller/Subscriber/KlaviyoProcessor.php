<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
namespace MageCloud\Sale\Controller\Subscriber;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Validator\EmailAddress as EmailValidator;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use MageCloud\Sale\Model\AccessProcessor;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use MageCloud\Sale\Model\KlaviyoSubscription as Subscription;
use MageCloud\Sale\Model\KlaviyoSubscriptionFactory as SubscriptionFactory;

/**
 * Class KlaviyoProcessor
 * @package MageCloud\Sale\Controller\Subscriber
 */
class KlaviyoProcessor extends Action implements HttpPostActionInterface
{
    /**
     * @var SubscriptionFactory
     */
    private $subscriptionFactory;

    /**
     * @var AccessProcessor
     */
    private $accessProcessor;

    /**
     * @var EmailValidator
     */
    private $emailValidator;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var RawFactory
     */
    private $resultRawFactory;

    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param Context $context
     * @param SubscriptionFactory $subscriptionFactory
     * @param AccessProcessor $accessProcessor
     * @param EmailValidator $emailValidator
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        Context $context,
        SubscriptionFactory $subscriptionFactory,
        AccessProcessor $accessProcessor,
        EmailValidator $emailValidator,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        JsonSerializer $jsonSerializer
    ) {
        parent::__construct($context);
        $this->subscriptionFactory = $subscriptionFactory;
        $this->accessProcessor = $accessProcessor;
        $this->emailValidator = $emailValidator;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * Get result URL
     *
     * @param string|null $routePath
     * @return string
     */
    private function getResultUrl(string $routePath = null): string
    {
        return rtrim($this->_url->getUrl($routePath), '/');
    }

    /**
     * @return mixed
     */
    private function getFormData(): mixed
    {
        $httpBadRequestCode = 400;

        /** @var Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        try {
            $formData = $this->jsonSerializer->unserialize(($this->getRequest()->getContent()));
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        if (!$formData || $this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        return $formData;
    }

    /**
     * @return Json|Raw
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     */
    public function execute()
    {
        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        $response = [
            'errors' => false,
            'message' => ''
        ];

        $formData = $this->getFormData();
        if (empty($formData)) {
            $response = [
                'errors' => true,
                'message' => (string)__('Email is required and not provided.')
            ];
            return $resultJson->setData($response);
        }

        $email = $formData[Subscription::PARAM_KEY_EMAIL] ?? null;
        if (!$this->emailValidator->isValid($email)) {
            $response = [
                'errors' => true,
                'message' => (string)__('Please enter a valid email address.')
            ];
            return $resultJson->setData($response);
        }

        try {
            /** @var Subscription $subscription */
            $subscription = $this->subscriptionFactory->create(['formData' => $formData]);
            $subscriptionResult = $subscription->execute();
            if (isset($subscriptionResult['errors'])) {
                return $resultJson->setData($subscriptionResult);
            }
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => (string)__('Something went wrong with the subscription. ' . $e->getMessage())
            ];
        }

        if (empty($response['errors']) && empty($response['message'])) {
            $this->accessProcessor->setAccessGranted();
            $response['redirectUrl'] = $this->_redirect->success($this->getResultUrl('sale'));
        }

        return $resultJson->setData($response);
    }
}