/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */

define([
    'jquery',
    'mage/storage',
    'Magento_Ui/js/model/messageList',
    'mage/translate'
], function ($, storage, globalMessageList, $t) {
    'use strict';

    let callbacks = [],

        /**
         * @param {Object} formData
         * @param {String} redirectUrl
         * @param {*} isGlobal
         * @param {Object} messageContainer
         */
        action = function (formData, redirectUrl, isGlobal, messageContainer) {
            messageContainer = messageContainer || globalMessageList;
            let customerKlaviyoSubscribeUrl = 'mc_sale/subscriber/klaviyoProcessor';

            if (formData.customerKlaviyoSubscribeUrl) {
                customerKlaviyoSubscribeUrl = formData.customerKlaviyoSubscribeUrl;
                delete formData.customerKlaviyoSubscribeUrl;
            }

            return storage.post(
                customerKlaviyoSubscribeUrl,
                JSON.stringify(formData),
                formData,
                isGlobal
            ).done(function (response) {
                if (response.errors) {
                    messageContainer.addErrorMessage(response);
                    callbacks.forEach(function (callback) {
                        callback(formData);
                    });
                } else {
                    callbacks.forEach(function (callback) {
                        callback(formData);
                    });

                    if (response.redirectUrl) {
                        window.location.href = response.redirectUrl;
                    } else if (redirectUrl) {
                        window.location.href = redirectUrl;
                    } else {
                        location.reload();
                    }
                }
            }).fail(function () {
                messageContainer.addErrorMessage({
                    'message': $t('Could not proceed. Please try again later')
                });
                callbacks.forEach(function (callback) {
                    callback(formData);
                });
            });
        };

    /**
     * @param {Function} callback
     */
    action.registerSubscribeCallback = function (callback) {
        callbacks.push(callback);
    };

    return action;
});
