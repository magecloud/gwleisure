/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */

define([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function ($, modal) {
    'use strict';

    return {
        modalWindow: null,

        /**
         * Create popUp window for provided element
         *
         * @param {HTMLElement} element
         */
        createPopUp: function (element) {
            let options = {
                'type': 'popup',
                'modalClass': 'popup-sale-subscribe',
                'focus': '[name=email]',
                'responsive': true,
                'innerScroll': true,
                'trigger': '[data-role="process-sale-access"]',
                'buttons': []
            };

            this.modalWindow = element;
            modal(options, $(this.modalWindow));
        },

        /**
         * Show subscribe popup window
         */
        showModal: function () {
            if (
                !window.magecloudSaleSubscribePopup.isPopupEnabled
                || window.magecloudSaleSubscribePopup.isAccessGranted
            ) {
                window.location.href = window.magecloudSaleSubscribePopup.customerDestinationUrl;
                return;
            }

            $(this.modalWindow).modal('openModal').trigger('contentUpdated');
        }
    };
});