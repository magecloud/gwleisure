/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */

define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/form',
    'MageCloud_Sale/js/action/subscribe-popup',
    'Magento_Customer/js/customer-data',
    'MageCloud_Sale/js/model/subscribe-popup',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'mage/url',
    'Magento_Ui/js/modal/alert',
    'mage/validation',
    'mage/cookies'
], function (
    $,
    ko,
    Component,
    subscribePopupAction,
    customerData,
    subscribePopup,
    messageContainer,
    $t,
    url,
    alert
) {
    'use strict';

    return Component.extend({
        modalWindow: null,
        notAllowedUrlParam: 'sale_not_allowed',
        beforeProcessSaleAccessTrigger: '[data-role="before-process-sale-access"]',
        processSaleAccessTrigger: '[data-role="process-sale-access"]',
        isLoading: ko.observable(false),

        defaults: {
            template: 'MageCloud_Sale/subscribe-popup'
        },

        /**
         * Init
         */
        initialize: function () {
            let self = this;

            this._super();
            url.setBaseUrl(window.magecloudSaleSubscribePopup.baseUrl);
            subscribePopupAction.registerSubscribeCallback(function () {
                self.isLoading(false);
            });

            $(document).on('click', this.beforeProcessSaleAccessTrigger, function (event) {
                if (
                    !window.magecloudSaleSubscribePopup.isPopupEnabled
                    || (
                        window.magecloudSaleSubscribePopup.isAccessGranted
                        || $.mage.cookies.get(window.magecloudSaleSubscribePopup.accessCookieName)
                    )
                ) {
                    window.location.href = window.magecloudSaleSubscribePopup.customerDestinationUrl;
                    return true;
                }
                $(self.processSaleAccessTrigger).trigger('click');
            });
        },

        /**
         * Init popup subscribe window
         * @param element
         */
        setModalElement: function (element) {
            if (subscribePopup.modalWindow == null) {
                subscribePopup.createPopUp(element);
            }

            let saleNotAllowed = this.getUrlParam(this.notAllowedUrlParam);
            if (saleNotAllowed) {
                messageContainer.addErrorMessage({
                    'message': $t('Please first fill out the form to access the sale.')
                });
                subscribePopup.showModal();
            }
        },

        /**
         * Process source assess
         */
        showModal: function () {
            if (
                !window.magecloudSaleSubscribePopup.isPopupEnabled
                || window.magecloudSaleSubscribePopup.isAccessGranted
            ) {
                window.location.href = window.magecloudSaleSubscribePopup.customerDestinationUrl;
                return;
            }

            if (this.modalWindow) {
                $(this.modalWindow).modal('openModal');
            } else {
                alert({
                    content: $t('Subscribe modal is not defined.')
                });
            }
        },

        /**
         * Provide subscribe action
         *
         * @return {Boolean}
         */
        subscribe: function (formUiElement, event) {
            let formData = {},
                formElement = $(event.currentTarget),
                formDataArray = formElement.serializeArray(),
                shoppingForKey = 'shopping_for';

            event.stopPropagation();
            formDataArray.forEach(function (entry, i) {
                let entryName = entry.name,
                    entryValue = entry.value;

                if (entryName.indexOf(shoppingForKey) !== -1) {
                    if (!formData.hasOwnProperty(shoppingForKey)) {
                        formData[shoppingForKey] = [];
                    }
                    if (entryValue) {
                        formData[shoppingForKey][i] = entryValue;
                    }
                } else {
                    if (entryValue) {
                        formData[entryName] = entryValue;
                    }
                }
            });
            formData['customerKlaviyoSubscribeUrl'] = window.magecloudSaleSubscribePopup.customerKlaviyoSubscribeUrl;
            if (formElement.validation() &&
                formElement.validation('isValid')
            ) {
                this.isLoading(true);
                subscribePopupAction(formData);
            }

            return false;
        },

        /**
         * @param name
         * @return {string|number|string}
         */
        getUrlParam: function getUrlParam(name) {
            let results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);

            if (results === null) {
                return '';
            } else {
                return results[1] || 0;
            }
        }
    });
});
