<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
declare(strict_types=1);

namespace MageCloud\Sale\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use MageCloud\Sale\Helper\Data as HelperData;

/**
 * Class AccessProcessor
 * @package MageCloud\Sale\Model
 */
class AccessProcessor
{
    /**
     * Name of cookie that holds private content version
     */
    const COOKIE_NAME = 'sale_subscribe_state';

    /**
     * Ten years cookie period
     */
    const COOKIE_PERIOD = 315360000;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var array
     */
    private $restrictedPaths;

    /**
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param RequestInterface $request
     * @param RedirectFactory $redirectFactory
     * @param StoreManagerInterface $storeManager
     * @param MessageManagerInterface $messageManager
     * @param HelperData $helperData
     * @param array $restrictedPaths
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        RequestInterface $request,
        RedirectFactory $redirectFactory,
        StoreManagerInterface $storeManager,
        MessageManagerInterface $messageManager,
        HelperData $helperData,
        array $restrictedPaths = []
    ) {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->request = $request;
        $this->redirectFactory = $redirectFactory;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->helperData = $helperData;
        $this->restrictedPaths = $restrictedPaths;
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isUrlAllowed(): bool
    {
        if (!$this->helperData->isEnabled($this->storeManager->getStore()->getId())) {
            return true;
        }
        if (!in_array(trim($this->request->getRequestString(), '/'), $this->restrictedPaths)) {
            return true;
        }
        if ($this->getIsAccessGranted()) {
            return true;
        }

        return false;
    }

    /**
     * @return Redirect
     * @throws NoSuchEntityException
     */
    public function getRedirect(): Redirect
    {
        $resultRedirect = $this->redirectFactory->create();
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $path = $baseUrl . '?sale_not_allowed=1';
        $this->messageManager->addErrorMessage(__('Please first fill out the form to access the sale.'));

        return $resultRedirect->setPath($path);
    }

    /**
     * Generate unique identifier
     *
     * @return string
     */
    protected function generateUniqueAccessValue(): string
    {
        return md5(rand() . time());
    }

    /**
     * @return bool
     */
    public function getIsAccessGranted(): bool
    {
        return (bool)$this->cookieManager->getCookie(self::COOKIE_NAME);
    }

    /**
     * @return void
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function setAccessGranted(): void
    {
        $publicCookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setDuration(self::COOKIE_PERIOD)
            ->setPath('/')
            ->setSecure($this->request->isSecure())
            ->setHttpOnly(false)
            ->setSameSite('Lax');

        $this->cookieManager->setPublicCookie(
            self::COOKIE_NAME,
            $this->generateUniqueAccessValue(),
            $publicCookieMetadata
        );
    }
}