<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
declare(strict_types=1);

namespace MageCloud\Sale\Model;

use Klaviyo\Reclaim\KlaviyoV3Sdk\Exception\KlaviyoApiException;
use Klaviyo\Reclaim\KlaviyoV3Sdk\Exception\KlaviyoAuthenticationException;
use Klaviyo\Reclaim\KlaviyoV3Sdk\Exception\KlaviyoRateLimitException;
use MageCloud\Sale\KlaviyoV3Sdk\KlaviyoV3Api;
use Klaviyo\Reclaim\Helper\ScopeSetting;
use MageCloud\Sale\Helper\Data as HelperData;

/**
 * Class KlaviyoSubscription
 * @package MageCloud\Sale\Model
 */
class KlaviyoSubscription
{
    const PARAM_KEY_EMAIL = 'email';
    const PARAM_KEY_SHOPPING_FOR = 'shopping_for';
    const KEY_PROFILE_ID = 'profile_id';

    const TARGET_LIST_NAME = 'Black Friday';

    /**
     * @var ScopeSetting
     */
    private $klaviyoScopeSetting;

    /**
     * V3 API Wrapper
     * @var KlaviyoV3Api $api
     */
    private $api;

    /**
     * @var array
     */
    private $formData;

    /**
     * @param ScopeSetting $klaviyoScopeSetting
     * @param array $formData
     */
    public function __construct(
        ScopeSetting $klaviyoScopeSetting,
        array $formData = []
    ) {
        $this->klaviyoScopeSetting = $klaviyoScopeSetting;
        $this->api = new KlaviyoV3Api(
            $this->klaviyoScopeSetting->getPublicApiKey(),
            $this->klaviyoScopeSetting->getPrivateApiKey(),
            $klaviyoScopeSetting
        );
        $this->formData = $formData;
    }

    /**
     * @param string|null $key
     * @return array|null
     */
    private function getFormData(string $key = null): ?array
    {
        if ($key) {
            return $this->formData[$key] ?? null;
        }

        return $this->formData;
    }

    /**
     * @param array $formData
     * @return KlaviyoSubscription
     */
    private function setFormData(array $formData = []): KlaviyoSubscription
    {
        $this->formData = $formData;
        return $this;
    }

    /**
     * @param string $email
     * @param array $properties
     * @return array|null
     * @throws KlaviyoApiException
     * @throws KlaviyoAuthenticationException
     * @throws KlaviyoRateLimitException
     */
    public function subscribeEmailToKlaviyoList(string $email, array $properties = []): ?array
    {
        $lists = $this->api->getLists();
        if (empty($lists)) {
            return [
                'errors' => true,
                'message' => (string)__('Empty subscription lists.')
            ];
        }

        $listData = array_filter($lists, function ($list) {
            return isset($list['attributes']['name'])
                && ((string)$list['attributes']['name'] === self::TARGET_LIST_NAME);
        });

        if (empty($listData)) {
            return [
                'errors' => true,
                'message' => (string)__('Can\'t find target subscription list.')
            ];
        }

        $listData = array_shift($listData);
        $listId = $listData['id'] ?? null;
        if (empty($listId)) {
            return [
                'errors' => true,
                'message' => (string)__('Can\'t find target subscription list ID.')
            ];
        }

        $attributes = [];
        $attributes[self::PARAM_KEY_EMAIL] = $email;
        if (!empty($properties)) {
            $attributes[KlaviyoV3Api::PROPERTIES] = $properties;
        }

        try {
            $response = (array)$this->api->searchProfileByEmail($email);
            $profileId = $response[self::KEY_PROFILE_ID] ?? null;
            if ($profileId) {
                $this->api->addCustomerProfileToList($listId, $profileId);
                // update customer profile in case if some additional properties was selected
                if (isset($attributes[KlaviyoV3Api::PROPERTIES])) {
                    $this->api->updateCustomerProfile($profileId, $attributes);
                }

                return [
                    'success' => true
                ];
            }

            $newProfile = (array)$this->api->createCustomerProfile($attributes);
            $profileId = $newProfile[self::KEY_PROFILE_ID] ?? null;
            if (!$profileId) {
                return [
                    'errors' => true,
                    'message' => (string)__('Can\'t get profile ID from new created profile.')
                ];
            }

            $this->api->addCustomerProfileToList($listId, $profileId);

            return [
                'success' => true
            ];
        } catch (\Exception $e) {
            return [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @return array|null
     * @throws KlaviyoApiException
     * @throws KlaviyoAuthenticationException
     * @throws KlaviyoRateLimitException
     */
    public function execute(): ?array
    {
        $formData = $this->getFormData();
        $email = $formData[self::PARAM_KEY_EMAIL] ?? null;
        if (!$email) {
            return [
                'errors' => true,
                'message' => (string)__('Please enter a valid email address.')
            ];
        }

        $properties = [];
        $shoppingFor = $formData[self::PARAM_KEY_SHOPPING_FOR] ?? null;
        if ($shoppingFor) {
            $shoppingFor = array_filter($shoppingFor, function ($param) {
                return $param !== '' && $param !== null;
            });
            if (!empty($shoppingFor)) {
                $properties[self::PARAM_KEY_SHOPPING_FOR] = implode(', ', $shoppingFor);
            }
        }

        return $this->subscribeEmailToKlaviyoList($email, $properties);
    }
}