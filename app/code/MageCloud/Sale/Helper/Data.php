<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
declare(strict_types=1);

namespace MageCloud\Sale\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package MageCloud\Sale\Helper
 */
class Data extends AbstractHelper
{
    /**
     * XML path
     *
     * general settings
     */
    const XML_PATH_GENERAL_ENABLED = 'magecloud_sale/general/enabled';

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * @param Context $context
     * @param DateTimeFactory $dateTimeFactory
     */
    public function __construct(
        Context $context,
        DateTimeFactory $dateTimeFactory
    ) {
        parent::__construct($context);
        $this->dateTimeFactory = $dateTimeFactory;
    }

    /**
     * @param $store
     * @return bool
     */
    public function isEnabled($store = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }
}
