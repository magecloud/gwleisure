<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */
declare(strict_types=1);

namespace MageCloud\Sale\Plugin\Catalog\Controller\Category;

use Magento\Catalog\Controller\Category\View as DefaultView;
use MageCloud\Sale\Model\AccessProcessor;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class View
 * @package MageCloud\Sale\Plugin\Catalog\Controller\Category
 */
class View
{
    /**
     * @var AccessProcessor
     */
    private $accessProcessor;

    /**
     * @param AccessProcessor $accessProcessor
     */
    public function __construct(
        AccessProcessor $accessProcessor
    ) {
        $this->accessProcessor = $accessProcessor;
    }

    /**
     * @param DefaultView $subject
     * @param \Closure $proceed
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function aroundExecute(
        DefaultView $subject,
        \Closure $proceed
    ) {
        if (!$this->accessProcessor->isUrlAllowed()) {
            return $this->accessProcessor->getRedirect();
        }

        return $proceed();
    }
}