<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Sale
 */

namespace MageCloud\Sale\Block;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use MageCloud\Sale\Helper\Data as HelperData;
use MageCloud\Sale\Model\AccessProcessor;

/**
 * Class SubscribePopup
 * @package MageCloud\Sale\Block
 */
class SubscribePopup extends Template
{
    /**
     * @var array
     */
    protected $jsLayout;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var AccessProcessor
     */
    private $accessProcessor;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @param Context $context
     * @param HelperData $helperData
     * @param AccessProcessor $accessProcessor
     * @param array $data
     * @param Json|null $serializer
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        AccessProcessor $accessProcessor,
        array $data = [],
        Json $serializer = null
    ) {
        parent::__construct($context, $data);
        $this->helperData = $helperData;
        $this->accessProcessor = $accessProcessor;
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->serializer = $serializer ?: ObjectManager::getInstance()
            ->get(Json::class);
    }

    /**
     *  Returns serialize jsLayout
     *
     * @return string
     */
    public function getJsLayout()
    {
        return $this->serializer->serialize($this->jsLayout);
    }

    /**
     * Returns popup config
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getConfig(): array
    {
        return [
            'isPopupEnabled' => $this->helperData->isEnabled($this->_storeManager->getStore()->getId()),
            'isAccessGranted' => $this->accessProcessor->getIsAccessGranted(),
            'accessCookieName' => AccessProcessor::COOKIE_NAME,
            'baseUrl' => $this->escapeUrl($this->getBaseUrl()),
            'customerSubscribeUrl' => $this->getUrl('mc_sale/subscriber/processor'),
            'customerKlaviyoSubscribeUrl' => $this->getUrl('mc_sale/subscriber/klaviyoProcessor'),
            'customerDestinationUrl' => $this->getUrl('sale')
        ];
    }

    /**
     * @return bool|string
     * @throws NoSuchEntityException
     */
    public function getSerializedConfig(): bool|string
    {
        return $this->serializer->serialize($this->getConfig());
    }

    /**
     * Return base url.
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getBaseUrl(): string
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }
}