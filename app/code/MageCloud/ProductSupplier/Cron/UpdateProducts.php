<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Cron;

use MageCloud\ProductSupplier\Model\SupplierProcessor;
use MageCloud\ProductSupplier\Model\SupplierProcessorFactory;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ProductSync
 * @package MageCloud\Purity\Cron
 */
class UpdateProducts
{
    /**
     * @var SupplierProcessorFactory
     */
    private $supplierProcessorFactory;

    /**
     * @param SupplierProcessorFactory $supplierProcessorFactory
     */
    public function __construct(
        SupplierProcessorFactory $supplierProcessorFactory
    ) {
        $this->supplierProcessorFactory = $supplierProcessorFactory;
    }

    /**
     * Update products by schedule
     *
     * @throws LocalizedException
     */
    public function execute(): void
    {
        /** @var SupplierProcessor $processor */
        $processor = $this->supplierProcessorFactory->create();
        $processor->execute();
    }
}