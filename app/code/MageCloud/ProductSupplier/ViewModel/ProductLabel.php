<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\ViewModel;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use MageCloud\ProductSupplier\Model\ProductLabel as ProductLabelModel;
use Magento\Catalog\Model\Product;

/**
 * Class ProductLabel
 * @package MageCloud\ProductSupplier\ViewModel
 */
class ProductLabel extends DataObject implements ArgumentInterface
{
    /**
     * @var ProductLabelModel
     */
    private $productLabelModel;

    /**
     * @param ProductLabelModel $productLabelModel
     * @param array $data
     */
    public function __construct(
        ProductLabelModel $productLabelModel,
        array $data = []
    ) {
        parent::__construct(
            $data
        );
        $this->productLabelModel = $productLabelModel;
    }

    /**
     * @param Product|null $product
     * @return bool
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function isAvailable(Product $product = null): bool
    {
        return (bool)$this->productLabelModel->isShippingLabelAvailable($product);
    }
}