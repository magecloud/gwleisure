<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;
use MageCloud\ProductSupplier\Model\ResourceModel\Supplier\CollectionFactory;
use MageCloud\ProductSupplier\Model\OverwritingProcessor;
use MageCloud\ProductSupplier\Model\OverwritingProcessorFactory;
use MageCloud\ProductSupplier\Model\ReportProcessor;
use MageCloud\ProductSupplier\Model\ReportProcessorFactory;

/**
 * Class MassSyncProductStock
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class MassSyncProductStock extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MageCloud_ProductSupplier::supplier';

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var OverwritingProcessorFactory
     */
    private $overwritingProcessorFactory;

    /**
     * @var ReportProcessorFactory
     */
    private $reportProcessorFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param OverwritingProcessorFactory $overwritingProcessorFactory
     * @param ReportProcessorFactory $reportProcessorFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OverwritingProcessorFactory $overwritingProcessorFactory,
        ReportProcessorFactory $reportProcessorFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->overwritingProcessorFactory = $overwritingProcessorFactory;
        $this->reportProcessorFactory = $reportProcessorFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return Redirect
     * @throws LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        $reportData = [];
        foreach ($collection as $supplier) {
            $supplier->syncProductStock($reportData);
        }

        /** @var OverwritingProcessor $overwritingProcessor */
        $overwritingProcessor = $this->overwritingProcessorFactory->create();
        $overwritingProcessor->execute();

        if (!empty($reportData)) {
            /** @var ReportProcessor $reportProcessor */
            $reportProcessor = $this->reportProcessorFactory->create(
                [
                    'reportData' => $reportData
                ]
            );
            if ($reportFilePath = $reportProcessor->execute()) {
                $reportFile = pathinfo($reportFilePath, PATHINFO_BASENAME);

                $downloadLink = $this->getUrl(
                    'product_supplier/supplier/downloadReport',
                    [
                        'report_file' => $reportFile
                    ]
                );

                $this->messageManager->addSuccess(
                    __(
                        'The report is available at %1. <a href="%2">Download report</a>.',
                        $reportFilePath,
                        $downloadLink
                    )
                );
            }
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 supplier(s) have been synced.', $collectionSize));

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}