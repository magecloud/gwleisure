<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use MageCloud\ProductSupplier\Controller\Adminhtml\AbstractSupplier;
use MageCloud\ProductSupplier\Model\Supplier;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use MageCloud\ProductSupplier\Model\RegistryConstants;

/**
 * Class Edit
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class Edit extends AbstractSupplier
{
    /**
     * Edit option
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create(Supplier::class);
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This supplier no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->registry->register(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER, $model);

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Supplier') : __('New Supplier'),
            $id ? __('Edit Supplier') : __('New Supplier')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Suppliers'));
        $resultPage->getConfig()->getTitle()->prepend(
            $model->getId() ? $model->getName() : __('New Supplier')
        );
        return $resultPage;
    }
}