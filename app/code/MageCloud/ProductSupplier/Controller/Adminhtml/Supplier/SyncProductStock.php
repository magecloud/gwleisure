<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use MageCloud\ProductSupplier\Controller\Adminhtml\AbstractSupplier;
use MageCloud\ProductSupplier\Model\OverwritingProcessor;
use MageCloud\ProductSupplier\Model\OverwritingProcessorFactory;
use MageCloud\ProductSupplier\Model\ReportProcessor;
use MageCloud\ProductSupplier\Model\ReportProcessorFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use MageCloud\ProductSupplier\Model\Supplier;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class SyncProductStock
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class SyncProductStock extends AbstractSupplier implements HttpPostActionInterface
{
    /**
     * @var OverwritingProcessorFactory
     */
    private $overwritingProcessorFactory;

    /**
     * @var ReportProcessorFactory
     */
    private $reportProcessorFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param OverwritingProcessorFactory $overwritingProcessorFactory
     * @param ReportProcessorFactory $reportProcessorFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        OverwritingProcessorFactory $overwritingProcessorFactory,
        ReportProcessorFactory $reportProcessorFactory
    ) {
        parent::__construct($context, $resultPageFactory, $registry);
        $this->overwritingProcessorFactory = $overwritingProcessorFactory;
        $this->reportProcessorFactory = $reportProcessorFactory;
    }

    /**
     * Sync product stock action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $reportData = [];
                $model = $this->_objectManager->create(Supplier::class);
                $model->load($id);
                $model->syncProductStock($reportData);
                $model->setSyncedAt(date(DateTime::DATETIME_PHP_FORMAT));
                $model->save();

                /** @var OverwritingProcessor $overwritingProcessor */
                $overwritingProcessor = $this->overwritingProcessorFactory->create();
                $overwritingProcessor->execute();

                if (!empty($reportData)) {
                    /** @var ReportProcessor $reportProcessor */
                    $reportProcessor = $this->reportProcessorFactory->create(
                        [
                            'reportData' => $reportData
                        ]
                    );
                    if ($reportFilePath = $reportProcessor->execute()) {
                        $reportFile = pathinfo($reportFilePath, PATHINFO_BASENAME);

                        $downloadLink = $this->getUrl(
                            'product_supplier/supplier/downloadReport',
                            [
                                'report_file' => $reportFile
                            ]
                        );

                        $this->messageManager->addSuccess(
                            __(
                                'The report is available at %1. <a href="%2">Download report</a>.',
                                $reportFilePath,
                                $downloadLink
                            )
                        );
                    }
                }

                $this->messageManager->addSuccessMessage(__('The supplier stock data has been synced successfully.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a supplier to proceed.'));
        return $resultRedirect->setPath('*/*/');
    }
}