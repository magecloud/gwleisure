<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use MageCloud\ProductSupplier\Api\SupplierRepositoryInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use MageCloud\ProductSupplier\Api\Data\SupplierInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class InlineEdit
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class InlineEdit extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MageCloud_ProductSupplier::supplier';

    /**
     * @var SupplierRepositoryInterface
     */
    private $supplierRepository;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * @param Context $context
     * @param SupplierRepositoryInterface $supplierRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        SupplierRepositoryInterface $supplierRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->supplierRepository = $supplierRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $id) {
                    /** @var SupplierInterface $model */
                    $model = $this->supplierRepository->getById((int)$id);
                    try {
                        $model->setData(array_merge($model->getData(), $postItems[$id]));
                        $this->supplierRepository->save($model);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithId(
                            $model,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add option name to error message
     *
     * @param SupplierInterface $model
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithId(SupplierInterface $model, $errorText): string
    {
        return '[Supplier ID: ' . $model->getId() . '] ' . $errorText;
    }
}
