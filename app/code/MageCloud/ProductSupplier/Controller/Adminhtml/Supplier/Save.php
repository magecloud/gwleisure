<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use MageCloud\ProductSupplier\Controller\Adminhtml\AbstractSupplier;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use MageCloud\ProductSupplier\Api\SupplierRepositoryInterface;
use MageCloud\ProductSupplier\Model\Supplier;
use MageCloud\ProductSupplier\Model\SupplierFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use MageCloud\ProductSupplier\Model\RegistryConstants;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Save
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class Save extends AbstractSupplier implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var SupplierFactory
     */
    private $supplierFactory;

    /**
     * @var SupplierRepositoryInterface
     */
    private $supplierRepository;

    /**
     * @var SerializerInterface
     */
    private $json;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param DataPersistorInterface $dataPersistor
     * @param SupplierFactory $supplierFactory
     * @param SupplierRepositoryInterface $supplierRepository
     * @param SerializerInterface $json
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        DataPersistorInterface $dataPersistor,
        SupplierFactory $supplierFactory,
        SupplierRepositoryInterface $supplierRepository,
        SerializerInterface $json
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->supplierFactory = $supplierFactory;
        $this->supplierRepository = $supplierRepository;
        $this->json = $json;
        parent::__construct($context, $resultPageFactory, $registry);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Supplier::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }

            if (isset($data['mapped_fields'])) {
                if (is_array($data['mapped_fields'])) {
                    $data['mapped_fields'] = $this->json->serialize($data['mapped_fields']);
                }
            }

            if (isset($data['code'])) {
                $data['code'] = str_replace(' ', '_', strtolower($data['code']));
            }

            /** @var Supplier $model */
            $model = $this->supplierFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->supplierRepository->getById((int)$id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This supplier no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->supplierRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the supplier.'));
                $this->dataPersistor->clear(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER);
                return $this->processOptionReturn($model, $data, $resultRedirect);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the supplier.'));
            }
            $this->dataPersistor->set(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER, $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the supplier return
     *
     * @param $model
     * @param $data
     * @param $resultRedirect
     * @return mixed
     * @throws LocalizedException
     */
    private function processOptionReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';
        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        } else if ($redirect === 'duplicate') {
            $duplicateModel = $this->supplierFactory->create(['data' => $data]);
            $duplicateModel->setId(null);
            $duplicateModel->setStatus(Supplier::STATUS_DISABLED);
            $this->supplierRepository->save($duplicateModel);
            $id = $duplicateModel->getId();
            $this->messageManager->addSuccessMessage(__('You duplicated the supplier.'));
            $this->dataPersistor->set(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER, $data);
            $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect;
    }
}
