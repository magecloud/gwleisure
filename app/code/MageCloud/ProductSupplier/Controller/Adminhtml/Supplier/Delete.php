<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use MageCloud\ProductSupplier\Controller\Adminhtml\AbstractSupplier;
use Magento\Framework\App\Action\HttpPostActionInterface;
use MageCloud\ProductSupplier\Model\Supplier;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Delete
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class Delete extends AbstractSupplier implements HttpPostActionInterface
{
    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->_objectManager->create(Supplier::class);
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the supplier.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a supplier to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
