<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;

/**
 * Class DownloadReport
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class DownloadReport extends Action
{
    /**
     * @var FileFactory
     */
    public $fileFactory;

    /**
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var ReadInterface
     */
    private $mediaDirectory;

    /**
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param ForwardFactory $resultForwardFactory
     * @param Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        ForwardFactory $resultForwardFactory,
        Filesystem $filesystem
    ) {
        $this->fileFactory = $fileFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->filesystem = $filesystem;
        $this->mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $reportFile = $this->getRequest()->getParam('report_file');
        if (!$reportFile) {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('noroute');
            return $resultForward;
        }

        $reportFilePath = sprintf('import/supplier_reports/%s', $reportFile);
        $fileContent = $this->mediaDirectory->readFile($reportFilePath);

        return $this->fileFactory->create(
            $reportFile,
            $fileContent,
            DirectoryList::VAR_DIR,
            'text/csv'
        );
    }
}
