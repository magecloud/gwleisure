<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Controller\Adminhtml\Supplier;

use MageCloud\ProductSupplier\Controller\Adminhtml\AbstractSupplier;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use MageCloud\ProductSupplier\Model\OverwritingProcessor;

/**
 * Class SyncOverwriteProductStock
 * @package MageCloud\ProductSupplier\Controller\Adminhtml\Supplier
 */
class SyncOverwriteProductStock extends AbstractSupplier implements HttpPostActionInterface
{
    /**
     * @var OverwritingProcessor
     */
    private $overwritingProcessor;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param OverwritingProcessor $overwritingProcessor
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        OverwritingProcessor $overwritingProcessor
    ) {
        parent::__construct($context, $resultPageFactory, $registry);
        $this->overwritingProcessor = $overwritingProcessor;
    }

    /**
     * Sync overwrite product stock action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            $this->overwritingProcessor->execute();
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $resultRedirect->setPath('*/*/');
    }
}