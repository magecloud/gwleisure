<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Controller\Adminhtml;

use MageCloud\ProductSupplier\Model\Supplier;
use MageCloud\ProductSupplier\Model\RegistryConstants;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class AbstractSupplier
 * @package MageCloud\ProductSupplier\Controller\Adminhtml
 */
abstract class AbstractSupplier extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MageCloud_ProductSupplier::supplier';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $registry;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $registry
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
    }

    /**
     * Initiate supplier
     *
     * @return void
     */
    protected function initSupplier(): void
    {
        $this->registry->register(
            RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER,
            $this->_objectManager->create(Supplier::class)
        );

        if ($id = (int)$this->getRequest()->getParam('id')) {
            $this->registry->registry(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER)->load($id);
        }
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    protected function initPage(Page $resultPage): Page
    {
        $resultPage->setActiveMenu('MageCloud_ProductSupplier::supplier')
            ->addBreadcrumb(__('Suppliers'), __('Suppliers'));
        return $resultPage;
    }
}