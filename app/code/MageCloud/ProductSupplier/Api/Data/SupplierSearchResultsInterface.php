<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface SupplierSearchResultsInterface
 * @package MageCloud\ProductSupplier\Api\Data
 */
interface SupplierSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get supplier.
     *
     * @return \MageCloud\ProductSupplier\Api\Data\SupplierInterface[]
     */
    public function getItems();

    /**
     * Set suppliers.
     *
     * @param \MageCloud\ProductSupplier\Api\Data\SupplierInterface[] $items
     * @return $this
     */
    public function setItems(array $items = null);
}
