<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Api\Data;

/**
 * Interface SupplierInterface
 * @package MageCloud\ProductSupplier\Api\Data
 */
interface SupplierInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID = 'id';
    const NAME = 'name';
    const CODE = 'code';
    const RELATIVE_SOURCE_PATH = 'relative_source_path';
    const DEFAULT_DELIVERY_DAYS = 'default_delivery_days';
    const USE_PRODUCT_DELIVERY_CONDITIONS = 'use_product_delivery_conditions';
    const DELIVERY_DAYS_PER_CONDITIONS = 'delivery_days_per_conditions';
    const USE_DEFAULT_STOCK_HANDLER = 'use_default_stock_handler';
    const ADDITIONAL_INFORMATION = 'additional_information';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const SYNCED_AT = 'synced_at';
    const STATUS = 'status';
    const MAPPED_FIELDS = 'mapped_fields';
    const ADD_DAYS = 'add_days';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get code
     *
     * @return string
     */
    public function getCode(): string;

    /**
     * Get relative source path
     *
     * @return string
     */
    public function getRelativeSourcePath(): string;

    /**
     * Get default delivery days
     *
     * @return string
     */
    public function getDefaultDeliveryDays(): string;

    /**
     * Get use product delivery conditions
     *
     * @return int
     */
    public function getUseProductDeliveryConditions(): int;

    /**
     * Get delivery days per conditions
     *
     * @return string
     */
    public function getDeliveryDaysPerConditions(): string;

    /**
     * Get use default stock handler
     *
     * @return int
     */
    public function getUseDefaultStockHandler(): int;

    /**
     * Get additional information
     *
     * @return string
     */
    public function getAdditionalInformation(): string;

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Get synced at
     *
     * @return string
     */
    public function getSyncedAt(): string;

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus(): int;

    /**
     * Get mapped fields
     *
     * @return string
     */
    public function getMappedFields(): string;

    /**
     * Get add days
     *
     * @return int
     */
    public function getAddDays(): int;

    /**
     * Set ID
     *
     * @param int $id
     * @return SupplierInterface
     */
    public function setId(int $id): SupplierInterface;

    /**
     * Set name
     *
     * @param string $name
     * @return SupplierInterface
     */
    public function setName(string $name): SupplierInterface;

    /**
     * Set code
     *
     * @param string $code
     * @return SupplierInterface
     */
    public function setCode(string $code): SupplierInterface;

    /**
     * Set path
     *
     * @param string $path
     * @return SupplierInterface
     */
    public function setRelativeSourcePath(string $path): SupplierInterface;

    /**
     * Set default delivery days
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setDefaultDeliveryDays(string $value): SupplierInterface;

    /**
     * Set use product delivery conditions
     *
     * @param int $value
     * @return SupplierInterface
     */
    public function setUseProductDeliveryConditions(int $value): SupplierInterface;

    /**
     * Set delivery days per conditions
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setDeliveryDaysPerConditions(string $value): SupplierInterface;

    /**
     * Set use default stock handler
     *
     * @param int $value
     * @return SupplierInterface
     */
    public function setUseDefaultStockHandler(int $value): SupplierInterface;

    /**
     * Set additional information
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setAdditionalInformation(string $value): SupplierInterface;

    /**
     * Set created at
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setCreatedAt(string $value): SupplierInterface;

    /**
     * Set updated at
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setUpdatedAt(string $value): SupplierInterface;

    /**
     * Set synced at
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setSyncedAt(string $value): SupplierInterface;

    /**
     * Set status
     *
     * @param int $status
     * @return SupplierInterface
     */
    public function setStatus(int $status): SupplierInterface;

    /**
     * Set mapped fields
     *
     * @param mixed $value
     * @return SupplierInterface
     */
    public function setMappedFields($value): SupplierInterface;

    /**
     * Set add days
     *
     * @param int $value
     * @return SupplierInterface
     */
    public function setAddDays(int $value): SupplierInterface;
}
