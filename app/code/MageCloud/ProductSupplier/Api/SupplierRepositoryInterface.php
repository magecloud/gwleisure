<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Api;

/**
 * Supplier CRUD interface
 *
 * @api
 */
interface SupplierRepositoryInterface
{
    /**
     * Save supplier.
     *
     * @param \MageCloud\ProductSupplier\Api\Data\SupplierInterface $supplier
     * @return \MageCloud\ProductSupplier\Api\Data\SupplierInterface
     * @throws \Magento\Framework\Exception\InputException If there is a problem with the input
     * @throws \Magento\Framework\Exception\NoSuchEntityException If a supplier ID is sent but the supplier does not exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\MageCloud\ProductSupplier\Api\Data\SupplierInterface $supplier);

    /**
     * Get supplier by ID.
     *
     * @param int $supplierId
     * @return \MageCloud\ProductSupplier\Api\Data\SupplierInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $id is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById(int $supplierId);

    /**
     * Retrieve suppliers that match te specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageCloud\ProductSupplier\Api\Data\SupplierSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete supplier by ID.
     *
     * @param int $supplierId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById(int $supplierId);
}
