<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_EnhancedEcommerce
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Plugin\Amasty\Preorder\Model\Quote\Item;

use Amasty\Preorder\Model\Quote\Item\GetNote as DefaultGetProduct;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use MageCloud\ProductSupplier\Helper\Product as ProductHelper;
use MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager\Product as ResourceProduct;

/**
 * Class GetNote
 * @package MageCloud\ProductSupplier\Plugin\Amasty\Preorder\Model\Quote\Item
 */
class GetNote
{
    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @param ProductHelper $productHelper
     */
    public function __construct(
        ProductHelper $productHelper
    ) {
        $this->productHelper = $productHelper;
    }

    /**
     * @param DefaultGetProduct $subject
     * @param $result
     * @param QuoteItem $quoteItem
     * @return string
     */
    public function afterExecute(
        DefaultGetProduct $subject,
        $result,
        QuoteItem $quoteItem
    ): string {
        $product = $quoteItem->getProduct();
        $productId = (int)$product->getId();
        $modifiedResult = $this->productHelper->renderInStockInfoHtml();
        $isInStock = $this->productHelper->isInStock($productId);
        if (!$isInStock) {
            $modifiedResult = $this->productHelper->renderOutOfStockInfoHtml(
                $result,
                (int)$product->getData(ResourceProduct::ATTRIBUTE_PREORDER_ALLOW_QTY),
                (string)$product->getData(ResourceProduct::ATTRIBUTE_PREORDER_RELEASE_DATE)
            );
        }

        return $modifiedResult;
    }
}