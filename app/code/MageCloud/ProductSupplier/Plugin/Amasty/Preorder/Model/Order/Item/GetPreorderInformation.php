<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_EnhancedEcommerce
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Plugin\Amasty\Preorder\Model\Order\Item;

use Amasty\Preorder\Model\Order\Item\GetPreorderInformation as DefaultGetPreorderInformation;
use Magento\Sales\Api\Data\OrderItemInterface;
use MageCloud\ProductSupplier\Helper\Product as ProductHelper;

/**
 * Class GetPreorderInformation
 * @package MageCloud\ProductSupplier\Plugin\Amasty\Preorder\Model\Order\Item
 */
class GetPreorderInformation
{
    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @param ProductHelper $productHelper
     */
    public function __construct(
        ProductHelper $productHelper
    ) {
        $this->productHelper = $productHelper;
    }

    /**
     * @param DefaultGetPreorderInformation $subject
     * @param $result
     * @param OrderItemInterface $orderItem
     * @return string
     */
    public function afterExecute(
        DefaultGetPreorderInformation $subject,
        $result,
        OrderItemInterface $orderItem
    ) {
        if ($result->isPreorder()) {
            if ($note = $this->productHelper->getOrderItemNote($orderItem, $result->getNote())) {
                $result->setNote($note);
            }
        } else {
            if ($note = $this->productHelper->getOrderItemNote($orderItem, $result->getNote(), true)) {
                $result->setNote($note);
            }
        }

        return $result;
    }
}