<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_EnhancedEcommerce
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Plugin\Checkout\CustomerData;

use MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager\Product as ResourceProduct;
use Magento\Checkout\CustomerData\AbstractItem as DefaultAbstractItem;
use Magento\Quote\Model\Quote\Item;
use Amasty\Preorder\Model\Quote\Item\Processor;
use MageCloud\ProductSupplier\Helper\Product as ProductHelper;

/**
 * Class AbstractItem
 * @package MageCloud\ProductSupplier\Plugin\Checkout\CustomerData
 */
class AbstractItem
{
    /**
     * @var Processor
     */
    private $processor;

    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @param Processor $processor
     * @param ProductHelper $productHelper
     */
    public function __construct(
        Processor $processor,
        ProductHelper $productHelper
    ) {
        $this->processor = $processor;
        $this->productHelper = $productHelper;
    }

    /**
     * @param DefaultAbstractItem $subject
     * @param array $result
     * @param Item $item
     * @return array
     */
    public function afterGetItemData(
        DefaultAbstractItem $subject,
        array $result,
        Item $item
    ): array {
        $this->processor->execute([$item]);
        $note = $item->getExtensionAttributes()->getPreorderInfo()->getNote();
        if (str_contains($note, 'additional-note')) {
            $result['preorder_note'] = $note;
            return $result;
        }

        $product = $item->getProduct();
        $productId = (int)$product->getId();

        $modifiedResult = $this->productHelper->renderInStockInfoHtml();
        if (!$this->productHelper->isInStock($productId)) {
            $modifiedResult = $this->productHelper->renderOutOfStockInfoHtml(
                $note,
                (int)$product->getData(ResourceProduct::ATTRIBUTE_PREORDER_ALLOW_QTY),
                (string)$product->getData(ResourceProduct::ATTRIBUTE_PREORDER_RELEASE_DATE)
            );
        }
        $result['preorder_note'] = $modifiedResult;

        return $result;
    }
}
