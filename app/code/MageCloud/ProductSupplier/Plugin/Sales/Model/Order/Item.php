<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Plugin\Sales\Model\Order;

use Magento\Sales\Model\Order\Item as DefaultItem;
use Amasty\Preorder\Model\Order\Item\GetPreorderInformation;

/**
 * Class Item
 * @package MageCloud\ProductSupplier\Plugin\Sales\Model\Order
 */
class Item
{
    /**
     * @var GetPreorderInformation
     */
    private $getPreorderInformation;

    /**
     * @param GetPreorderInformation $getPreorderInformation
     */
    public function __construct(
        GetPreorderInformation $getPreorderInformation
    ) {
        $this->getPreorderInformation = $getPreorderInformation;
    }

    /**
     * @param DefaultItem $subject
     * @param string|null $result
     * @return string|null
     */
    public function afterGetName(
        DefaultItem $subject,
        ?string $result
    ): ?string {
        if ($result !== null) {
            $preorderInformation = $this->getPreorderInformation->execute($subject);
            if ($preorderInformation->isPreorder()) {
                $note = $preorderInformation->getNote();
                if (stripos($result, $note) === false) {
                    $result .= ' / ' . $note;
                }
            }
        }

        return $result;
    }
}