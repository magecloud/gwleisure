<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Block\Product\View\Shipping;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Label
 * @package MageCloud\ProductSupplier\Block\Product\View\Shipping
 */
class Label extends Template
{
    /**
     * @param $store
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getStore($store = null): StoreInterface
    {
        return $this->_storeManager->getStore($store = null);
    }
}