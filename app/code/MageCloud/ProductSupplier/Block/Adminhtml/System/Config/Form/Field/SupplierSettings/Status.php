<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field\SupplierSettings;

use Magento\Config\Model\Config\Source\Enabledisable;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class Status
 * @package MageCloud\ProductSupplier\Block\Adminhtml\Form\Field\SupplierSettings
 */
class Status extends Select
{
    /**
     * @var Enabledisable
     */
    protected $enableDisable;

    /**
     * @param Context $context
     * @param Enabledisable $enableDisable
     * @param array $data
     */
    public function __construct(
        Context $context,
        Enabledisable $enableDisable,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->enableDisable = $enableDisable;
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $options = $this->enableDisable->toOptionArray();
            foreach ($options as $option) {
                if (isset($option['value']) && isset($option['label'])) {
                    $this->addOption($option['value'], $option['label']);
                }
            }
        }

        return parent::_toHtml();
    }
}