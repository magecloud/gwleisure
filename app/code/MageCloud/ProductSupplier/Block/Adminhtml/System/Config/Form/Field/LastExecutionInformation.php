<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Filesystem;
use Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface;
use MageCloud\ProductSupplier\Helper\Data as HelperData;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class LastExecutionInformation
 * @package MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field
 */
class LastExecutionInformation extends Field
{
    /**
     * @var DateTimeFormatterInterface
     */
    private $dateTimeFormatter;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param DateTimeFormatterInterface $dateTimeFormatter
     * @param Filesystem $filesystem
     * @param HelperData $helperData
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        DateTimeFormatterInterface $dateTimeFormatter,
        Filesystem $filesystem,
        HelperData $helperData,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->dateTimeFormatter = $dateTimeFormatter;
        $this->filesystem = $filesystem;
        $this->helperData = $helperData;
        $this->storeManager = $storeManager;
    }

    /**
     * @param AbstractElement $element
     * @return \Magento\Framework\Phrase|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $format = $this->_localeDate->getDateTimeFormat(\IntlDateFormatter::MEDIUM);
        if ($lastExecutionDate = $this->helperData->getLastExecutionDate($storeId)) {
            $info = '<div class="info-wrapper">';
            $lastExecutionDate = $this->dateTimeFormatter->formatObject(
                $this->_localeDate->date($lastExecutionDate), $format
            );
            $info .= '<div class="info-row">';
            $info .= '<span class="row-label">Date: </span>';
            $info .= '<span class="row-value">' . $lastExecutionDate . '</span>';
            $info .= '</div>';

            if ($lastExecutionTime = $this->helperData->getLastExecutionTime($storeId)) {
                $info .= '<div class="info-row">';
                $info .= '<span class="row-label">Time:</span>';
                $info .= '<span class="row-value">' . sprintf('%ss.', $lastExecutionTime) . '</span>';
                $info .= '</div>';
            }

            $varDir = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
            $logFile = $varDir->getAbsolutePath('log/product-supplier.log');
            $info .= '<div class="info-row">';
            $info .= '<span class="row-label">Log:</span>';
            $info .= '<span class="row-value">' . $logFile . '</span>';
            $info .= '</div>';

            if ($lastExecutionStatus = (string)$this->helperData->getStatus($storeId)) {
                $class = ($lastExecutionStatus == 'Error') ? 'minor' : 'notice';
                $info .= '<div class="info-row">';
                $info .= '<span class="row-label">Status:</span>';
                $info .= '<span class="grid-severity grid-severity-'. $class . '"><span>' . $lastExecutionStatus . '</span></span>';
                $info .= '</div>';
            }
            $info .= '</div>';

            return $info;
        }
        return __('Not Performed');
    }
}