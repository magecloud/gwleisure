<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field;

use MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field\SupplierSettings\Status;
use MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field\SupplierSettings\UseProductDeliveryConditions;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\Factory as ElementFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class SupplierSettings
 * @package MageCloud\ProductSupplier\Block\Adminhtml\Form\Field
 */
class SupplierSettings extends AbstractFieldArray
{
    /**
     * @var ElementFactory
     */
    protected $elementFactory;

    /**
     * @var Status
     */
    private $statusRenderer = null;

    /**
     * @var UseProductDeliveryConditions
     */
    private $useProductDeliveryConditionsRenderer = null;

    /**
     * WarehouseShippingMethods constructor.
     * @param Context $context
     * @param ElementFactory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ElementFactory $elementFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Retrieve status column renderer
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws LocalizedException
     */
    private function getStatusRenderer()
    {
        if (!$this->statusRenderer) {
            $this->statusRenderer = $this->getLayout()->createBlock(
                Status::class,
                'product.supplier.settings.status',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->statusRenderer->setClass('select admin__control-select');
        }
        return $this->statusRenderer;
    }

    /**
     * Retrieve use product delivery conditions column renderer
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws LocalizedException
     */
    private function getUseProductDeliveryConditionsRenderer()
    {
        if (!$this->useProductDeliveryConditionsRenderer) {
            $this->useProductDeliveryConditionsRenderer = $this->getLayout()->createBlock(
                UseProductDeliveryConditions::class,
                'product.supplier.settings.use-product-delivery-conditions',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->useProductDeliveryConditionsRenderer->setClass('select admin__control-select');
        }
        return $this->useProductDeliveryConditionsRenderer;
    }

    /**
     * @inheritdoc
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->addColumn(
            'supplier',
            [
                'label' => __('Supplier')
            ]
        );
        $this->addColumn(
            'delivery_days',
            [
                'label' => __('Delivery Days')
            ]
        );
        $this->addColumn(
            'use_product_delivery_conditions',
            [
                'label' => __('Use Conditions'),
                'renderer'  => $this->getUseProductDeliveryConditionsRenderer()
            ]
        );
        $this->addColumn(
            'relative_path',
            [
                'label' => __('Relative Path')
            ]
        );
        $this->addColumn(
            'is_enabled',
            [
                'label' => __('Status'),
                'renderer'  => $this->getStatusRenderer()
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Supplier');
        parent::_construct();
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $optionExtraAttr = [];
        $optionExtraAttr['option_' . $this->getStatusRenderer()->calcOptionHash(
            $row->getData('is_enabled')
        )] = 'selected="selected"';
        $optionExtraAttr['option_' . $this->getUseProductDeliveryConditionsRenderer()->calcOptionHash(
            $row->getData('is_enabled')
        )] = 'selected="selected"';

        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}