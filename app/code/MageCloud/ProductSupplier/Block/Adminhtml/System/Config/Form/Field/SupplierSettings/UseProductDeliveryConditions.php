<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Block\Adminhtml\System\Config\Form\Field\SupplierSettings;

use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class Status
 * @package MageCloud\ProductSupplier\Block\Adminhtml\Form\Field\SupplierSettings
 */
class UseProductDeliveryConditions extends Select
{
    /**
     * @var Yesno
     */
    protected $yesno;

    /**
     * @param Context $context
     * @param Yesno $yesno
     * @param array $data
     */
    public function __construct(
        Context $context,
        Yesno $yesno,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->yesno = $yesno;
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $options = $this->yesno->toOptionArray();
            $options = array_reverse($options);
            foreach ($options as $option) {
                if (isset($option['value']) && isset($option['label'])) {
                    $this->addOption($option['value'], $option['label']);
                }
            }
        }

        return parent::_toHtml();
    }
}