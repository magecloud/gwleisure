<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Block\Adminhtml\Supplier;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use MageCloud\ProductSupplier\Block\Adminhtml\Supplier\Edit\GenericButton;

/**
 * Class SyncOverwriteButton
 * @package MageCloud\ProductSupplier\Block\Adminhtml\Supplier
 */
class SyncOverwriteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Sync Overwrite Product Stock'),
            'class' => 'primary',
            'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to overwrite product stock?'
                ) . '\', \'' . $this->urlBuilder->getUrl('*/*/syncOverwriteProductStock') . '\', {data: {}})',
        ];
    }
}