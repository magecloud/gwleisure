<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Block\Adminhtml\Supplier\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SyncButton
 * @package MageCloud\ProductSupplier\Block\Adminhtml\Supplier\Edit
 */
class SyncButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        $supplierId = $this->getSupplierId();
        if ($supplierId && $this->canRender('sync')) {
            $data = [
                'label' => __('Sync Product Stock'),
                'class' => 'sync',
                'on_click' => 'deleteConfirm(\'' . __(
                        'Are you sure you want to sync product stock?'
                    ) . '\', \'' . $this->urlBuilder->getUrl('*/*/syncProductStock', ['id' => $supplierId]) . '\', {data: {}})',
                'sort_order' => 20,
            ];
        }
        return $data;
    }
}