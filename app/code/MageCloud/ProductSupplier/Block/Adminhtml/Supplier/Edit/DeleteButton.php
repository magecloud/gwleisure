<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Block\Adminhtml\Supplier\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 * @package MageCloud\ProductSupplier\Block\Adminhtml\Supplier\Edit
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        $supplierId = $this->getSupplierId();
        if ($supplierId && $this->canRender('delete')) {
            $data = [
                'label' => __('Delete'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to delete this?'
                ) . '\', \'' . $this->urlBuilder->getUrl('*/*/delete', ['id' => $supplierId]) . '\', {data: {}})',
                'sort_order' => 20,
            ];
        }
        return $data;
    }
}
