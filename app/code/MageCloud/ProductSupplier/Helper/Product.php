<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Helper;

use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager\Product as ResourceProduct;

/**
 * Class Data
 * @package MageCloud\ProductSupplier\Helper
 */
class Product extends AbstractHelper
{
    /**
     * Shipping note needle
     */
    const CLICK_COLLECT_SHIPPING_METHOD = 'InStore_s003';

    const FASTEST_SHIPPING_SERVICE_SHIPPING_METHOD = 'FSS_fss1';

    /**
     * @var Json
     */
    private $json;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var ReinitableConfigInterface
     */
    private $reinitableConfig;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @param Context $context
     * @param Json $json
     * @param ConfigInterface $configInterface
     * @param ReinitableConfigInterface $reinitableConfig
     * @param DateTimeFactory $dateTimeFactory
     * @param StockStatusInterface $stockStatus
     */
    public function __construct(
        Context $context,
        Json $json,
        ConfigInterface $configInterface,
        ReinitableConfigInterface $reinitableConfig,
        DateTimeFactory $dateTimeFactory,
        StockRegistryInterface $stockRegistry
    ) {
        parent::__construct($context);
        $this->json = $json;
        $this->configInterface = $configInterface;
        $this->reinitableConfig = $reinitableConfig;
        $this->dateTimeFactory = $dateTimeFactory;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @param int $productId
     * @return bool
     */
    public function isInStock(int $productId): bool
    {
        try {
            $stockItem = $this->stockRegistry->getStockItem($productId);
        } catch (\Exception $e) {
            return false;
        }

        return ($stockItem->getQty() > 0) && $stockItem->getIsInStock();
    }

    /**
     * @return string
     */
    public function getOrderConfirmationClickCollectDefaultNote(): string
    {
        return 'Please don’t travel until we have confirmed item(s) ready.';
    }

    /**
     * @param string $shippingNote
     * @return string|null
     */
    public function getOrderConfirmationNotAvailableNote(string $shippingNote): ?string
    {
        if ($dueDate = preg_replace('/.*?:\s*/', '', $shippingNote)) {
            return sprintf(
                'PRE-ORDER - Currently Due %s, we will keep you posted if there are any changes to this date and 
                contact you when the product is available and ready for your chosen Shipping/Collection service.',
                $dueDate
            );
        }
        return null;
    }

    /**
     * @return string
     */
    public function renderInStockInfoHtml(): string
    {
        $html = '';
        $html .= '<div class="additional-note">';
        $html .= '<div class="item fss">';
        $html .= '<strong>';
        $html .= 'Fastest Shipping Service: ';
        $html .= '</strong>';
        $html .= '<span>';
        $html .= 'Express 1-2 Days';
        $html .= '</span>';
        $html .= '</div>';
        $html .= '<div class="item cc">';
        $html .= '<strong>';
        $html .= 'Click & Collect: ';
        $html .= '</strong>';
        $html .= '<span>';
        $html .= 'Collect from store approx 1 Hr.';
        $html .= '</span>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * @param string|null $shippingNote
     * @param int|null $preOrderAllowedQty
     * @param string|null $preOrderReleaseDate
     * @return string|null
     */
    public function getFastestShippingServiceInfo(
        string $shippingNote = null,
        int $preOrderAllowedQty = null,
        string $preOrderReleaseDate = null
    ): ?string {
        $result = null;
        if (str_contains($shippingNote, ResourceProduct::PRE_ORDER_NOTE_ESTIMATED_DELIVERY_TEXT)) {
            $result = $shippingNote;
        }

        if (!$preOrderAllowedQty && $preOrderReleaseDate) {
            $preOrderReleaseDate = date('d/m/Y', strtotime($preOrderReleaseDate));
            $result = 'Express 1-2 Days after ' . $preOrderReleaseDate;
        }

        return $result;
    }

    /**
     * @param string|null $shippingNote
     * @param int|null $preOrderAllowedQty
     * @param string|null $preOrderReleaseDate
     * @return string|null
     */
    public function getClickCollectInfo(
        string $shippingNote = null,
        int $preOrderAllowedQty = null,
        string $preOrderReleaseDate = null
    ): ?string {
        if ($shippingNote && !str_contains($shippingNote, ResourceProduct::PRE_ORDER_NOTE_ESTIMATED_DELIVERY_TEXT)) {
            if ($preOrderReleaseDate) {
                $preOrderReleaseDate = date('d/m/Y', strtotime($preOrderReleaseDate));
                return sprintf('Collect from store approx %s', $preOrderReleaseDate);
            }
            return null;
        }

        $result = $shippingNote;
        if ($preOrderAllowedQty && $preOrderReleaseDate) {
            $minusDays = preg_replace('/[^0-9\-]/', '', $shippingNote);
            $minusDays = explode('-', $minusDays, 2);
            foreach ($minusDays as &$day) {
                $day -= 1;
            }
            $minusDays = implode('-', $minusDays);
            $result = sprintf('Collect from store in approx %s Days', $minusDays);

        } elseif (!$preOrderAllowedQty && $preOrderReleaseDate) {
            $preOrderReleaseDate = date('d/m/Y', strtotime($preOrderReleaseDate));
            $result = 'Collect from store approx ' . $preOrderReleaseDate;
        }

        return $result;
    }

    /**
     * @param string|null $shippingNote
     * @param int|null $preOrderAllowedQty
     * @param string|null $preOrderReleaseDate
     * @return string
     */
    public function renderOutOfStockInfoHtml(
        string $shippingNote = null,
        int $preOrderAllowedQty = null,
        string $preOrderReleaseDate = null
    ): string {
        $html = '';

        if (!$shippingNote) {
            return $html;
        }

        $fastestShippingServiceInfo = $this->getFastestShippingServiceInfo(
            $shippingNote,
            $preOrderAllowedQty,
            $preOrderReleaseDate
        );
        if ($fastestShippingServiceInfo) {
            $html .= '<div class="item fss">';
                $html .= '<strong>';
                $html .= 'Fastest Shipping Service: ';
                $html .= '</strong>';
                $html .= '<span>';
                $html .= $fastestShippingServiceInfo;
                $html .= '</span>';
            $html .= '</div>';
        }

        $clickCollectInfo = $this->getClickCollectInfo(
            $shippingNote,
            (int)$preOrderAllowedQty,
            $preOrderReleaseDate
        );
        if ($clickCollectInfo) {
            $html .= '<div class="item cc">';
                $html .= '<strong>';
                $html .= 'Click & Collect: ';
                $html .= '</strong>';
                $html .= '<span>';
                $html .= $clickCollectInfo;
                $html .= '</span>';
            $html .= '</div>';
        }

        $resultHtml = '';
        if (strlen($html)) {
            $resultHtml .= '<div class="additional-note">';
            $resultHtml .= $html;
            $resultHtml .= '</div>';
        }

        return $resultHtml;
    }

    /**
     * @param OrderItemInterface $orderItem
     * @param string|null $shippingNote
     * @param bool $inStock
     * @return string|null
     */
    public function getOrderItemNote(
        OrderItemInterface $orderItem,
        string $shippingNote = null,
        bool $inStock = false
    ): ?string {
        $result = null;

        try {
            if (!$order = $orderItem->getOrder()) {
                return $shippingNote;
            }
            if (!$product = $orderItem->getProduct()) {
                return $shippingNote;
            }
        } catch (\Exception $e) {
            return $shippingNote;
        }

        $shippingMethod = $order->getShippingMethod();

        if (
            !$inStock
            && !$product->getData(ResourceProduct::ATTRIBUTE_PREORDER_ALLOW_QTY)
            && ($preOrderReleaseDate = $product->getData(ResourceProduct::ATTRIBUTE_PREORDER_RELEASE_DATE))
        ) {
            $preOrderReleaseDate = date('d/m/Y', strtotime($preOrderReleaseDate));
            return sprintf(
                'PRE-ORDER - Currently Due %s, we will keep you posted if there are any changes to this date and 
                contact you when the product is available and ready for your chosen Shipping/Collection service.',
                $preOrderReleaseDate
            );
        }

        if ($shippingMethod == self::CLICK_COLLECT_SHIPPING_METHOD) {
            $result = sprintf(
                'Collect from store approx 1 Hr. %s',
                $this->getOrderConfirmationClickCollectDefaultNote()
            );
            if (!$inStock) {
                $result = sprintf(
                    '%s. %s',
                    $this->getClickCollectInfo(
                        $shippingNote,
                        (int)$product->getData(ResourceProduct::ATTRIBUTE_PREORDER_ALLOW_QTY),
                        $product->getData(ResourceProduct::ATTRIBUTE_PREORDER_RELEASE_DATE)
                    ),
                    $this->getOrderConfirmationClickCollectDefaultNote()
                );
            }
        } else if ($shippingMethod == self::FASTEST_SHIPPING_SERVICE_SHIPPING_METHOD) {
            $result = 'Express 1-2 Days';
            if (!$inStock) {
                $result = $shippingNote;
            }
        }

        return $result;
    }
}
