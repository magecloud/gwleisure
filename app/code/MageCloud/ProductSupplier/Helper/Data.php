<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Helper;

use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;

/**
 * Class Data
 * @package MageCloud\ProductSupplier\Helper
 */
class Data extends AbstractHelper
{
    /**
     * XML path
     *
     * general settings
     */
    const XML_PATH_GENERAL_ENABLED = 'magecloud_product_supplier/general/enabled';
    const XML_PATH_GENERAL_ENABLED_OVERWRITING = 'magecloud_product_supplier/general/enabled_overwriting';
    const XML_PATH_GENERAL_ENABLED_REPORT = 'magecloud_product_supplier/general/enabled_report';
    const XML_PATH_GENERAL_CRON_SCHEDULE = 'magecloud_product_supplier/general/cron_schedule';
    const XML_PATH_GENERAL_LAST_EXECUTION_DATE = 'magecloud_product_supplier/general/last_execution_date';
    const XML_PATH_GENERAL_LAST_EXECUTION_TIME = 'magecloud_product_supplier/general/last_execution_time';
    const XML_PATH_GENERAL_LAST_EXECUTION_STATUS = 'magecloud_product_supplier/general/last_execution_status';

    /**
     * Status codes
     */
    const STATUS_ERROR = 1;
    const STATUS_SUCCESS = 2;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var ReinitableConfigInterface
     */
    private $reinitableConfig;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * @param Context $context
     * @param Json $json
     * @param ConfigInterface $configInterface
     * @param ReinitableConfigInterface $reinitableConfig
     * @param DateTimeFactory $dateTimeFactory
     */
    public function __construct(
        Context $context,
        Json $json,
        ConfigInterface $configInterface,
        ReinitableConfigInterface $reinitableConfig,
        DateTimeFactory $dateTimeFactory
    ) {
        parent::__construct($context);
        $this->json = $json;
        $this->configInterface = $configInterface;
        $this->reinitableConfig = $reinitableConfig;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    /**
     * @param $store
     * @return bool
     */
    public function isEnabled($store = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param $store
     * @return bool
     */
    public function isEnabledOverwriting($store = null): bool
    {
        if ($this->isEnabled($store)) {
            return $this->scopeConfig->isSetFlag(
                self::XML_PATH_GENERAL_ENABLED_OVERWRITING,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return false;
    }

    /**
     * @param $store
     * @return bool
     */
    public function isEnabledReport($store = null): bool
    {
        if ($this->isEnabled($store)) {
            return $this->scopeConfig->isSetFlag(
                self::XML_PATH_GENERAL_ENABLED_REPORT,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return false;
    }

    /**
     * @param $store
     * @return string|null
     */
    public function getCronSchedule($store = null): ?string
    {
        if ($this->isEnabled($store)) {
            return $this->scopeConfig->getValue(
                self::XML_PATH_GENERAL_CRON_SCHEDULE,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return null;
    }

    /**
     * @param $store
     * @return string|null
     */
    public function getLastExecutionDate($store = null): ?string
    {
        if ($this->isEnabled($store)) {
            return $this->scopeConfig->getValue(
                self::XML_PATH_GENERAL_LAST_EXECUTION_DATE,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return null;
    }

    /**
     * @param $store
     * @return $this
     */
    public function setLastExecutionDate($store = null): Data
    {
        if ($this->isEnabled($store)) {
            $this->configInterface->saveConfig(
                self::XML_PATH_GENERAL_LAST_EXECUTION_DATE,
                $this->getCurrentDate(),
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                $store ?? Store::DEFAULT_STORE_ID
            );
        }
        return $this;
    }

    /**
     * @param $store
     * @return string|null
     */
    public function getLastExecutionTime($store = null): ?string
    {
        if ($this->isEnabled($store)) {
            return $this->scopeConfig->getValue(
                self::XML_PATH_GENERAL_LAST_EXECUTION_TIME,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return null;
    }

    /**
     * @param $workingTime
     * @param $store
     * @return $this
     */
    public function setLastExecutionTime($workingTime, $store = null): Data
    {
        if ($this->isEnabled($store)) {
            $this->configInterface->saveConfig(
                self::XML_PATH_GENERAL_LAST_EXECUTION_TIME,
                (string)$workingTime,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                $store ?? Store::DEFAULT_STORE_ID
            );
        }
        return $this;
    }

    /**
     * @param $store
     * @return int
     */
    public function getLastExecutionStatus($store = null): int
    {
        if ($this->isEnabled($store)) {
            return (int)$this->scopeConfig->getValue(
                self::XML_PATH_GENERAL_LAST_EXECUTION_STATUS,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return 0;
    }

    /**
     * @param int $status
     * @param $store
     * @return $this
     */
    public function setLastExecutionStatus(int $status, $store = null): Data
    {
        if ($this->isEnabled($store)) {
            $this->configInterface->saveConfig(
                self::XML_PATH_GENERAL_LAST_EXECUTION_STATUS,
                $status,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                $store ?? Store::DEFAULT_STORE_ID
            );
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getStatuses(): array
    {
        return [
            self::STATUS_ERROR => __('Error'),
            self::STATUS_SUCCESS => __('Success')
        ];
    }

    /**
     * @param $store
     * @return string|null
     */
    public function getStatus($store = null): ?string
    {
        $code = $this->getLastExecutionStatus($store);
        return (string)$this->getStatuses()[$code] ?? null;
    }

    /**
     * @return string
     */
    private function getCurrentDate(): string
    {
        $createdAtTime = $this->dateTimeFactory->create('now', new \DateTimeZone('UTC'));
        return $createdAtTime->format(DateTime::DATETIME_PHP_FORMAT);
    }

    /**
     * @param $workingTime
     * @param $status
     * @param $store
     * @return void
     */
    public function setLastExecutionInfo($workingTime = null, $status = null, $store = null): void
    {
        $this->setLastExecutionDate($store);
        if ($workingTime) {
            $this->setLastExecutionTime($workingTime, $store);
        }
        if ($status) {
            $this->setLastExecutionStatus($status);
        }
        // clear system config to display actual information
        $this->reinitableConfig->reinit();
    }
}
