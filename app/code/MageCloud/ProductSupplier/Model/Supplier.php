<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Model;

use MageCloud\ProductSupplier\Model\Supplier\DataHandlerProvider;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use MageCloud\ProductSupplier\Api\Data\SupplierInterface;
use MageCloud\ProductSupplier\Model\ResourceModel\Supplier as ResourceSupplier;
use MageCloud\ProductSupplier\Model\Supplier\DataHandlerInterface;
use MageCloud\ProductSupplier\Model\SourceProcessorFactory;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\SerializerInterface;
use Amasty\Preorder\Model\Indexer\Product\Msi\PreorderProcessor as PreorderIndexer;

/**
 * Class Supplier
 * @package MageCloud\ProductSupplier\Model
 */
class Supplier extends AbstractModel implements SupplierInterface
{
    /**#@+
     * Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    const DEFAULT_DATA_HANDLER_CODE = 'default';

    /**
     * @var SourceProcessorFactory
     */
    private $sourceProcessorFactory;

    /**
     * @var DataHandlerProvider
     */
    private $dataHandlerProvider;

    /**
     * @var PreorderIndexer
     */
    private $preorderIndexer;

    /**
     * @var SerializerInterface
     */
    private $json;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param SourceProcessorFactory $sourceProcessorFactory
     * @param DataHandlerProvider $dataHandlerProvider
     * @param PreorderIndexer $preorderIndexer
     * @param SerializerInterface $json
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        SourceProcessorFactory $sourceProcessorFactory,
        DataHandlerProvider $dataHandlerProvider,
        PreorderIndexer $preorderIndexer,
        SerializerInterface $json,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->sourceProcessorFactory = $sourceProcessorFactory;
        $this->dataHandlerProvider = $dataHandlerProvider;
        $this->preorderIndexer = $preorderIndexer;
        $this->json = $json;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceSupplier::class);
    }

    /**
     * Retrieve id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->getData(self::ID);
    }

    /**
     * Retrieve name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getData(self::NAME);
    }

    /**
     * Retrieve code
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->getData(self::CODE);
    }

    /**
     * Retrieve relative source path
     *
     * @return string
     */
    public function getRelativeSourcePath(): string
    {
        return $this->getData(self::RELATIVE_SOURCE_PATH);
    }

    /**
     * Retrieve default delivery days
     *
     * @return string
     */
    public function getDefaultDeliveryDays(): string
    {
        return $this->getData(self::DEFAULT_DELIVERY_DAYS);
    }

    /**
     * Retrieve use product delivery conditions
     *
     * @return int
     */
    public function getUseProductDeliveryConditions(): int
    {
        return $this->getData(self::USE_PRODUCT_DELIVERY_CONDITIONS);
    }

    /**
     * Retrieve delivery days per conditions
     *
     * @return string
     */
    public function getDeliveryDaysPerConditions(): string
    {
        return $this->getData(self::DELIVERY_DAYS_PER_CONDITIONS);
    }

    /**
     * Retrieve use default stock handler
     *
     * @return int
     */
    public function getUseDefaultStockHandler(): int
    {
        return $this->getData(self::USE_DEFAULT_STOCK_HANDLER);
    }

    /**
     * Retrieve additional information
     *
     * @return string
     */
    public function getAdditionalInformation(): string
    {
        return $this->getData(self::ADDITIONAL_INFORMATION);
    }

    /**
     * Retrieve created at
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Retrieve updated at
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Retrieve synced at
     *
     * @return string
     */
    public function getSyncedAt(): string
    {
        return $this->getData(self::SYNCED_AT);
    }

    /**
     * Retrieve status
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Retrieve mapped fields
     */
    public function getMappedFields(): string
    {
        return $this->getData(self::MAPPED_FIELDS);
    }

    /**
     * Retrieve add days
     */
    public function getAddDays(): int
    {
        return (int)$this->getData(self::ADD_DAYS);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return SupplierInterface
     */
    public function setId($id): SupplierInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SupplierInterface
     */
    public function setName(string $name): SupplierInterface
    {
        return $this->setData(self::NAME, $name);

    }

    /**
     * Set code
     *
     * @param string $code
     * @return SupplierInterface
     */
    public function setCode(string $code): SupplierInterface
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Set relative source path
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setRelativeSourcePath(string $value): SupplierInterface
    {
        return $this->setData(self::RELATIVE_SOURCE_PATH, $value);
    }

    /**
     * Set default delivery days
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setDefaultDeliveryDays(string $value): SupplierInterface
    {
        return $this->setData(self::DEFAULT_DELIVERY_DAYS, $value);
    }

    /**
     * Set use product delivery conditions
     *
     * @param int $value
     * @return SupplierInterface
     */
    public function setUseProductDeliveryConditions(int $value): SupplierInterface
    {
        return $this->setData(self::USE_PRODUCT_DELIVERY_CONDITIONS, $value);
    }

    /**
     * Set delivery days per conditions
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setDeliveryDaysPerConditions(string $value): SupplierInterface
    {
        return $this->setData(self::DELIVERY_DAYS_PER_CONDITIONS, $value);
    }

    /**
     * Set use default stock handler
     *
     * @param int $value
     * @return SupplierInterface
     */
    public function setUseDefaultStockHandler(int $value): SupplierInterface
    {
        return $this->setData(self::USE_DEFAULT_STOCK_HANDLER, $value);
    }

    /**
     * Set additional information
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setAdditionalInformation(string $value): SupplierInterface
    {
        return $this->setData(self::ADDITIONAL_INFORMATION, $value);
    }

    /**
     * Set created at
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setCreatedAt(string $value): SupplierInterface
    {
        return $this->setData(self::CREATED_AT, $value);
    }

    /**
     * Set updated at
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setUpdatedAt(string $value): SupplierInterface
    {
        return $this->setData(self::UPDATED_AT, $value);
    }

    /**
     * Set synced at
     *
     * @param string $value
     * @return SupplierInterface
     */
    public function setSyncedAt(string $value): SupplierInterface
    {
        return $this->setData(self::SYNCED_AT, $value);
    }

    /**
     * Set status
     *
     * @param int $status
     * @return SupplierInterface
     */
    public function setStatus(int $status): SupplierInterface
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Set mapped fields
     */
    public function setMappedFields($value): SupplierInterface
    {
        return $this->setData(self::MAPPED_FIELDS, $value);
    }

    /**
     * Set add days
     */
    public function setAddDays($value): SupplierInterface
    {
        return $this->setData(self::ADD_DAYS, $value);
    }

    /**
     * Prepare statuses
     *
     * @return array
     */
    public function getAvailableStatuses(): array
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * @return array
     */
    public function prepareFields(): array
    {
        $required = [];
        $mapped = [];
        if ($mappedData = $this->getMappedFields()) {
            if (is_string($mappedData)) {
                $mappedData = $this->json->unserialize($mappedData);
            }
            foreach ($mappedData as $rowData) {
                $sourceField = (string)$rowData['source_field'];
                $required[] = $sourceField;
                $mapped[(string)$rowData['magento_field']] = $sourceField;
            }
        }

        return [
            'required' => $required,
            'mapped' => $mapped
        ];
    }

    /**
     * Sync supplier product stock
     *
     * @param array $reportData
     * @param string|null $fileName
     * @param bool $buildOnlyMapped
     * @return void
     * @throws LocalizedException
     */
    public function syncProductStock(
        array &$reportData = [],
        string $fileName = null,
        bool $buildOnlyMapped = true
    ): void {
        $supplierCode = $this->getCode();
        /** @var SourceProcessor $sourceProcessor */
        $sourceProcessor = $this->sourceProcessorFactory->create(['fieldsProviderType' => $supplierCode]);
        if ($relativePath = $this->getRelativeSourcePath()) {
            $sourceProcessor->setRelativePath($relativePath);
        }

        $fields = $this->prepareFields();
        $sourceProcessor->setRequiredFields($fields['required']);
        $sourceProcessor->setMappedFields($fields['mapped']);

        $sources = $sourceProcessor->init($fileName, $buildOnlyMapped);
        if (!empty($sources)) {
            $dataHandlerCode = $supplierCode;
            if ($this->getUseDefaultStockHandler()) {
                $dataHandlerCode = self::DEFAULT_DATA_HANDLER_CODE;
            }
            /** @var DataHandlerInterface $dataHandler */
            $dataHandler = $this->dataHandlerProvider->getDataHandler($dataHandlerCode);
            if (!$dataHandler instanceof DataHandlerInterface) {
                return;
            }
            $productIds = $dataHandler->update($this, $sources, $reportData);
            if (!empty($productIds)) {
                $this->preorderIndexer->reindexList($productIds);
            }
        }
    }
}