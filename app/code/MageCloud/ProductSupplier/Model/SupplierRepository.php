<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model;

use MageCloud\ProductSupplier\Api\Data\SupplierInterface;
use MageCloud\ProductSupplier\Api\SupplierRepositoryInterface;
use MageCloud\ProductSupplier\Api\Data;
use MageCloud\ProductSupplier\Model\ResourceModel\Supplier as ResourceSupplier;
use MageCloud\ProductSupplier\Model\ResourceModel\Supplier\CollectionFactory as SupplierCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\EntityManager\HydratorInterface;
use MageCloud\ProductSupplier\Api\Data\SupplierInterfaceFactory;
use MageCloud\ProductSupplier\Api\Data\SupplierSearchResultsInterfaceFactory;

/**
 * Class SupplierRepository
 * @package MageCloud\ProductSupplier\Model
 */
class SupplierRepository implements SupplierRepositoryInterface
{
    /**
     * @var ResourceSupplier
     */
    private $resource;

    /**
     * @var SupplierFactory
     */
    private $supplierFactory;

    /**
     * @var SupplierCollectionFactory
     */
    private $supplierCollectionFactory;

    /**
     * @var SupplierSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * @var SupplierInterfaceFactory
     */
    protected $dataSupplierFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @param ResourceSupplier $resource
     * @param SupplierFactory $supplierFactory
     * @param SupplierInterfaceFactory $dataSupplierFactory
     * @param SupplierCollectionFactory $supplierCollectionFactory
     * @param SupplierSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface|null $collectionProcessor
     * @param HydratorInterface|null $hydrator
     */
    public function __construct(
        ResourceSupplier $resource,
        SupplierFactory $supplierFactory,
        SupplierInterfaceFactory $dataSupplierFactory,
        SupplierCollectionFactory $supplierCollectionFactory,
        SupplierSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor = null,
        ?HydratorInterface $hydrator = null
    ) {
        $this->resource = $resource;
        $this->supplierFactory = $supplierFactory;
        $this->dataSupplierFactory = $dataSupplierFactory;
        $this->supplierCollectionFactory = $supplierCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
        $this->hydrator = $hydrator ?? ObjectManager::getInstance()->get(HydratorInterface::class);
    }

    /**
     * Save supplier
     *
     * @param SupplierInterface $supplier
     * @return Supplier
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(SupplierInterface $supplier)
    {
        if ($supplier->getId() && $supplier instanceof Supplier && !$supplier->getOrigData()) {
            $supplier = $this->hydrator->hydrate($this->getById($supplier->getId()), $this->hydrator->extract($supplier));
        }

        try {
            $this->resource->save($supplier);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $supplier;
    }

    /**
     * Load supplier data by given supplier identity
     *
     * @param int $supplierId
     * @return Supplier
     * @throws NoSuchEntityException
     */
    public function getById(int $supplierId)
    {
        $supplier = $this->supplierFactory->create();
        $this->resource->load($supplier, $supplierId);
        if (!$supplier->getId()) {
            throw new NoSuchEntityException(__('The supplier with the "%1" ID doesn\'t exist.', $supplierId));
        }
        return $supplier;
    }

    /**
     * Load supplier data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \MageCloud\ProductSupplier\Api\Data\SupplierSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \MageCloud\ProductSupplier\Model\ResourceModel\Supplier\Collection $collection */
        $collection = $this->supplierCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\SupplierSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete supplier
     *
     * @param SupplierInterface $supplier
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(SupplierInterface $supplier)
    {
        try {
            $this->resource->delete($supplier);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete supplier by given supplier identity
     *
     * @param int $supplierId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $supplierId)
    {
        return $this->delete($this->getById($supplierId));
    }

    /**
     * Retrieve collection processor
     *
     * @deprecated 102.0.0
     * @return CollectionProcessorInterface
     */
    private function getCollectionProcessor()
    {
        //phpcs:disable Magento2.PHP.LiteralNamespaces
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \MageCloud\ProductSupplier\Model\Api\SearchCriteria\SupplierCollectionProcessor::class
            );
        }
        return $this->collectionProcessor;
    }
}