<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Csv;
use MageCloud\ProductSupplier\Model\SourceProcessor\FieldsProvider;
use MageCloud\ProductSupplier\Model\SourceProcessor\FieldsInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class SourceProcessor
 * @package MageCloud\ProductSupplier\Model
 */
class SourceProcessor extends DataObject
{
    const CSV_FILE_EXTENSION = 'csv';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Csv
     */
    private $csvProcessor;

    /**
     * @var FieldsProvider
     */
    private $fieldsProvider;

    /**
     * @var string
     */
    private $fieldsProviderType;

    /**
     * @var string
     */
    private $relativePath = null;

    /**
     * @var array
     */
    private $requiredFields = [];

    /**
     * @var array
     */
    private $mappedFields = [];

    /**
     * @param Filesystem $filesystem
     * @param Csv $csvProcessor
     * @param FieldsProvider $fieldsProvider
     * @param string|null $fieldsProviderType
     * @param array $data
     */
    public function __construct(
        Filesystem $filesystem,
        Csv $csvProcessor,
        FieldsProvider $fieldsProvider,
        string $fieldsProviderType = null,
        array $data = []
    ) {
        parent::__construct($data);
        $this->filesystem = $filesystem;
        $this->csvProcessor = $csvProcessor;
        $this->fieldsProvider = $fieldsProvider;
        $this->fieldsProviderType = $fieldsProviderType;
    }

    /**
     * @param $relativePath
     * @return $this
     */
    public function setRelativePath($relativePath): SourceProcessor
    {
        $this->relativePath = $relativePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getRelativePath(): string
    {
        return (string)$this->relativePath;
    }

    /**
     * @param array $fields
     * @return $this
     */
    public function setRequiredFields(array $fields = []): SourceProcessor
    {
        $this->requiredFields = $fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getRequiredFields(): array
    {
        return $this->requiredFields;
    }

    /**
     * @param array $fields
     * @return $this
     */
    public function setMappedFields(array $fields = []): SourceProcessor
    {
        $this->mappedFields = $fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getMappedFields(): array
    {
        return $this->mappedFields;
    }

    /**
     * @return $this
     */
    public function reset(): SourceProcessor
    {
        $this->requiredFields = [];
        $this->mappedFields = [];
        $this->relativePath = null;
        $this->fieldsProviderType = null;
        return $this;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function initFieldsProvider(): void
    {
        /** @var FieldsInterface $fieldsProvider */
        $fieldsProvider = $this->fieldsProvider->getProvider($this->fieldsProviderType);
        if (null === $fieldsProvider) {
            throw new \Exception("Fields provider for type {$this->fieldsProviderType} is not specified.");
        }
        // The path value can be specified in two ways.
        // If the path value is set from the backend, then it has priority and is already initialized
        // directly via main processing method.
        // Otherwise, the value will be taken from the xml configuration.
        if (!$this->getRelativePath() && ($path = $fieldsProvider->getRelativePath())) {
            $this->setRelativePath($path);
        }
        if (!$this->getRequiredFields() && ($requiredFields = $fieldsProvider->getRequired())) {
            $this->setRelativePath($requiredFields);
        }
        if (!$this->getMappedFields() && ($mappedFields = $fieldsProvider->getMapped())) {
            $this->setMappedFields($mappedFields);
        }
    }

    /**
     * Prepare and build data from specific CSV file for further processing
     *
     * @param string|null $fileName
     * @param bool $buildOnlyMapped
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    public function init(string $fileName = null, bool $buildOnlyMapped = true): array
    {
        $this->initFieldsProvider();
        $sources = $this->getSources($fileName);
        $output = [];
        foreach ($sources as $source) {
            if (!is_readable($source)) {
                throw new \Exception("File {$source} is not readable or does not exists.");
            }
            $fileRawData = $this->csvProcessor->getData($source);
            // first row of file represents headers
            $headerFields = $fileRawData[0];
            $this->fixFileDataStructure($fileRawData, $headerFields);
            $this->checkRequiredFields($headerFields);
            $validFields = $this->filterFileFields($headerFields);
            $invalidFields = array_diff_key($headerFields, $validFields);
            $fileData = $this->filterFileData($fileRawData, $invalidFields, $validFields);
            $this->prepareFileData($fileData, $buildOnlyMapped);
            $this->reset();
            $output[] = $fileData;
        }
        return $output;
    }

    /**
     * @param string|null $fileName
     * @return array
     * @throws \Exception
     */
    private function getSources(string $fileName = null): array
    {
        $varDir = $this->filesystem->getDirectoryRead(DirectoryList::PUB);
        $relativePath = trim($this->getRelativePath(), '/');
        $isRelativePathIsFile = substr($relativePath, -4) == '.' . self::CSV_FILE_EXTENSION;
        $sources = [];
        if ($fileName) {
            $fileName = trim($fileName, '/');
            $path = sprintf('%s/%s', $relativePath, $fileName);
            $sources[] = $varDir->getAbsolutePath($path);
        } else if ($isRelativePathIsFile) {
            $sources[] = $varDir->getAbsolutePath($relativePath);
        } else {
            $path = $varDir->getAbsolutePath($relativePath);
            $dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
            foreach ($dir as $file) {
                /** @var RecursiveDirectoryIterator $file */
                if ($file->isDir()) {
                    // ignore child directory
                    continue;
                }
                if ($file->getExtension() != self::CSV_FILE_EXTENSION) {
                    // ignore file with not supported format
                    throw new \Exception(
                        'The source file format in not supported. Supported format - CSV. Actual format - '
                        . $file->getExtension()
                    );
                }
                $sources[] = $file->getRealPath();
            }
        }

        return !is_array($sources) ? [$sources] : $sources;
    }

    /**
     * @param array $fileRawData
     * @param array $headerFields
     * @return void
     */
    private function fixFileDataStructure(array &$fileRawData, array $headerFields): void
    {
        foreach ($fileRawData as $key => &$rowData) {
            if (count($headerFields) == count($rowData)) {
                // omit, row is valid
                continue;
            }

            if (count($rowData) > count($headerFields)) {
                // row is not valid, remote it from further processing
                unset($fileRawData[$key]);
                continue;
            }

            foreach ($headerFields as $headerKey => $headerField) {
                if (!array_key_exists($headerKey, $rowData)) {
                    $rowData[$headerKey] = 0;
                }
            }
        }
    }

    /**
     * @param array $headerFields
     * @return void
     * @throws \Exception
     */
    private function checkRequiredFields(array $headerFields): void
    {
        $requiredFields = $this->getRequiredFields();
        if (
            count(array_map('trim', $requiredFields))
            == count(array_intersect($requiredFields, array_map('trim', $headerFields)))
        ) {
            return;
        }
        throw new \Exception(
            sprintf(
                'The file doesn\'t contain all the required columns. Missing required column(s): %s',
                implode(',', array_diff($requiredFields, $headerFields))
            )
        );
    }

    /**
     * Filter file fields (i.e. unset invalid fields)
     *
     * @param array $fileFields
     * @return string[] filtered fields
     */
    private function filterFileFields(array $fileFields): array
    {
        $filteredFields = $this->getRequiredFields();
        $requiredFieldsNum = count($this->getRequiredFields());
        $fileFieldsNum = count($fileFields);
        for ($index = $requiredFieldsNum; $index < $fileFieldsNum; $index++) {
            $titleFieldName = $fileFields[$index];
            $filteredFields[$index] = $titleFieldName;
        }
        return $filteredFields;
    }

    /**
     * Filter file data (i.e. unset all invalid fields and check consistency)
     *
     * @param array $fileRawData
     * @param array $invalidFields assoc array of invalid file fields
     * @param array $validFields assoc array of valid file fields
     * @return array
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    private function filterFileData(array $fileRawData, array $invalidFields, array $validFields): array
    {
        $validFieldsNum = count($validFields);
        foreach ($fileRawData as $rowIndex => $dataRow) {
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($fileRawData[$rowIndex]);
                continue;
            }
            // unset invalid fields from data row
            foreach ($dataRow as $fieldIndex => $fieldValue) {
                if (isset($invalidFields[$fieldIndex])) {
                    unset($fileRawData[$rowIndex][$fieldIndex]);
                }
            }
            // check if number of fields in row match with number of valid fields
            if (count($fileRawData[$rowIndex]) != $validFieldsNum) {
                throw new LocalizedException(__('Invalid file format. Row index - %1', $rowIndex));
            }
        }
        return $fileRawData;
    }

    /**
     * @param array $fileData
     * @param bool $buildOnlyMapped
     * @return void
     * @throws \Exception
     */
    private function prepareFileData(array &$fileData, bool $buildOnlyMapped = true): void
    {
        $mappedFields = $this->getMappedFields();
        $keys = array_shift($fileData);
        // apply magento column fields
        foreach ($keys as $key => $csvField) {
            if ($requiredField = array_search($csvField, $mappedFields)) {
                $keys[$key] = $requiredField;
            }
        }
        $notCorrectFormatRecords = [];
        foreach ($fileData as $i => $row) {
            if (count($keys) == count($row)) {
                $row = array_map('trim', $row);
                $data = array_combine($keys, $row);
                if ($buildOnlyMapped) {
                    // add only fields which were mapped
                    $data = array_filter($data, function($key) use ($mappedFields) {
                        return array_key_exists($key, $mappedFields);
                    }, ARRAY_FILTER_USE_KEY);
                }
                $fileData[$i] = $data;
            } else {
                $notCorrectFormatRecords[$i] = $row;
            }
        }
        if (count($notCorrectFormatRecords) > 0) {
            $issueLine = current(array_keys($notCorrectFormatRecords));
            throw new \Exception(
                sprintf(
                    'The records near line %s in input file in the incorrect format. Please check and try again.',
                    $issueLine
                )
            );
        }
    }
}