<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model;

use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Registry;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager\Product as ProductResource;
use Magento\Store\Model\StoreManagerInterface;
use Amasty\Preorder\Model\Product\Constants as AmastyPreorderConstants;

/**
 * Class ProductLabel
 * @package MageCloud\ProductSupplier\Model
 */
class ProductLabel
{
    /**
     * @var Product
     */
    private $product = null;

    /**
     * Core registry
     *
     * @var Registry
     */
    private $registry;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var array
     */
    private $shippingLabelAvailableMap = [];

    /**
     * @param Registry $registry
     * @param StockRegistryInterface $stockRegistry
     * @param ProductResource $productResource
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Registry $registry,
        StockRegistryInterface $stockRegistry,
        ProductResource $productResource,
        StoreManagerInterface $storeManager
    ) {
        $this->registry = $registry;
        $this->stockRegistry = $stockRegistry;
        $this->productResource = $productResource;
        $this->storeManager = $storeManager;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->product) {
            $this->product = $this->registry->registry('product');
        }
        return $this->product;
    }

    /**
     * @param int $productId
     * @return StockItemInterface|null
     */
    private function initStockItem(int $productId): ?StockItemInterface
    {
        $stockItem = null;
        try {
            $stockItem = $this->stockRegistry->getStockItem($productId, $this->storeManager->getStore()->getId());
        } catch (\Exception $e) {
            // omit exception
        }

        return $stockItem;
    }

    /**
     * If the stock state of the product meets certain requirements, add shipping label to the product main image.
     * Requirements:
     * - qty > 0
     * - stock status - in stock
     * - backorders - allow pre-order
     * - availability - in-stock
     * - cart label button - 'Add to Cart'
     *
     * @param Product|null $product
     * @return bool
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function isShippingLabelAvailable(Product $product = null): bool
    {
        if (null === $product) {
            $product = $this->getProduct();
        }

        $productId = (int)$product->getId();
        if (!isset($this->shippingLabelAvailableMap[$productId])) {
            if (!$stockItem = $this->initStockItem($productId)) {
                $this->shippingLabelAvailableMap[$productId] = false;
                return $this->shippingLabelAvailableMap[$productId];
            }

            $cartLabel = $product->getData(AmastyPreorderConstants::CART_LABEL)
                ? strtolower($product->getData(AmastyPreorderConstants::CART_LABEL))
                : null;

            $result = $stockItem->getQty()
                && $stockItem->getIsInStock()
                && ($stockItem->getBackorders() == AmastyPreorderConstants::BACKORDERS_PREORDER_OPTION)
                && ($product->getData(ProductResource::ATTRIBUTE_AVAILABILITY)
                    == $this->productResource->getAvailabilityValue(ProductResource::AVAILABILITY_LABEL_IN_STOCK))
                && ($cartLabel == strtolower(ProductResource::PRE_ORDER_BUTTON_LABEL_ADD_TO_CART));
            $this->shippingLabelAvailableMap[$productId] = $result;
        }

        return $this->shippingLabelAvailableMap[$productId];
    }
}