<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */

namespace MageCloud\ProductSupplier\Model;

/**
 * Declarations of core registry keys used by the ProductSupplier module
 *
 */
class RegistryConstants
{
    /**
     * Key for current supplier in registry
     */
    const CURRENT_PRODUCT_SUPPLIERS_SUPPLIER = 'current_product_suppliers_supplier';
}
