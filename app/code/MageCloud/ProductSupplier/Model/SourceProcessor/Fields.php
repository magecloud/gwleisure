<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\SourceProcessor;

/**
 * Class FieldsProvider
 * @package MageCloud\ProductSupplier\Model\SourceProcessor
 */
class Fields implements FieldsInterface
{
    /**
     * @var string
     */
    private $relativePath;

    /**
     * @var array
     */
    private $required;

    /**
     * @var array
     */
    private $mapped;

    /**
     * @param string|null $relativePath
     * @param array $required
     * @param array $mapped
     */
    public function __construct(
        string $relativePath = null,
        array $required = [],
        array $mapped = []
    ) {
        $this->relativePath = $relativePath;
        $this->required = $required;
        $this->mapped = $mapped;
    }

    /**
     * @inheritDocs
     */
    public function getRelativePath(): ?string
    {
        return $this->relativePath;
    }

    /**
     * @inheritDocs
     */
    public function getRequired(): array
    {
        return $this->required;
    }

    /**
     * @inheritDocs
     */
    public function getMapped(): array
    {
        return $this->mapped;
    }
}