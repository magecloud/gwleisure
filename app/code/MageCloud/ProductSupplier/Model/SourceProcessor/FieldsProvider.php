<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\SourceProcessor;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use MageCloud\ProductSupplier\Api\SupplierRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use MageCloud\ProductSupplier\Model\OverwritingProcessor;

/**
 * Class FieldsProvider
 * @package MageCloud\ProductSupplier\Model\SourceProcessor
 */
class FieldsProvider
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    private $objectManager = null;

    /**
     * @var FieldsInterface[]
     */
    private $providers;

    /**
     * @var SupplierRepositoryInterface
     */
    private $supplierRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var OverwritingProcessor
     */
    private $overwritingProcessor;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param SupplierRepositoryInterface $supplierRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param OverwritingProcessor $overwritingProcessor
     * @param array $providers
     * @throws LocalizedException
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        SupplierRepositoryInterface $supplierRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OverwritingProcessor $overwritingProcessor,
        array $providers = []
    ) {
        $this->objectManager = $objectManager;
        $this->supplierRepository = $supplierRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->overwritingProcessor = $overwritingProcessor;
        $this->providers = $providers;
        $this->initProviders();
    }

    /**
     * @return void
     * @throws LocalizedException
     */
    private function initProviders(): void
    {
        if (empty($this->providers)) {
            $suppliers = $this->supplierRepository->getList($this->searchCriteriaBuilder->create());
            if ($suppliers->getTotalCount()) {
                foreach ($suppliers->getItems() as $supplier) {
                    $fields = $supplier->prepareFields();
                    $this->providers[$supplier->getCode()] = [
                        'relativePath' => (string)$supplier->getRelativeSourcePath(),
                        'required' => (array)$fields['required'],
                        'mapped' => (array)$fields['mapped']
                    ];
                }
            }
            $this->providers = array_merge($this->providers, $this->overwritingProcessor->getFieldsProviderData());
        }
    }

    /**
     * Retrieve a providers list
     *
     * @return FieldsInterface[]
     */
    public function getProviders(): array
    {
        $providers = [];
        foreach ($this->providers as $code => $providerData) {
            $providers[$code] = $this->objectManager->create(Fields::class, $providerData);
        }
        return $providers;
    }

    /**
     * Retrieve a specific provider by code.
     *
     * @param string $code
     * @return FieldsInterface|null
     */
    public function getProvider(string $code): ?FieldsInterface
    {
        $providerData = $this->providers[$code] ?? null;
        if (!$providerData) {
            throw new \InvalidArgumentException(
                sprintf('Fields provider for code %s is not specified', $code)
            );
        }
        return $this->objectManager->create(Fields::class, $providerData);
    }
}