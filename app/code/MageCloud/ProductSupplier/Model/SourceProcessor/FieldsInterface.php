<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\SourceProcessor;

/**
 * Interface FieldsInterface
 * @package MageCloud\ProductSupplier\Model\SourceProcessor
 */
interface FieldsInterface
{
    /**
     * Get relative source
     *
     * @return string|null
     */
    public function getRelativePath(): ?string;

    /**
     * Get required fields for source
     *
     * @return array
     */
    public function getRequired(): array;

    /**
     * Get mapped fields for source
     *
     * @return array
     */
    public function getMapped(): array;
}