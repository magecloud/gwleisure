<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\AttributeHelper;

use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionInterface;
use Magento\Eav\Model\Entity\Attribute\OptionLabel;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Eav\Model\Entity\Attribute\Source\TableFactory;
use Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Class Option
 * @package MageCloud\ProductSupplier\Model\AttributeHelper
 */
class Option extends AbstractAttribute
{
    /**
     * @var TableFactory
     */
    private $tableFactory;

    /**
     * @var AttributeOptionManagementInterface
     */
    private $attributeOptionManagement;

    /**
     * @var AttributeOptionInterfaceFactory
     */
    private $optionFactory;

    /**
     * @var AttributeOptionLabelInterfaceFactory
     */
    private $optionLabelFactory;

    /**
     * @param AttributeFactory $attributeFactory
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeManagementInterface $attributeManagement
     * @param ProductAttributeRepositoryInterface $attributeRepository
     * @param ProductResource $productResource
     * @param Json $json
     * @param TableFactory $tableFactory
     * @param AttributeOptionManagementInterface $attributeOptionManagement
     * @param AttributeOptionLabelInterfaceFactory $optionLabelFactory
     * @param AttributeOptionInterfaceFactory $optionFactory
     */
    public function __construct(
        AttributeFactory $attributeFactory,
        EavSetupFactory $eavSetupFactory,
        AttributeManagementInterface $attributeManagement,
        ProductAttributeRepositoryInterface $attributeRepository,
        ProductResource $productResource,
        Json $json,
        TableFactory $tableFactory,
        AttributeOptionManagementInterface $attributeOptionManagement,
        AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        AttributeOptionInterfaceFactory $optionFactory
    ) {
        parent::__construct(
            $attributeFactory,
            $eavSetupFactory,
            $attributeManagement,
            $attributeRepository,
            $productResource,
            $json
        );
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
    }

    /**
     * Find or create a matching attribute option
     *
     * @param mixed $attributeCode Attribute the option should exist in
     * @param string $label Label to find or add
     * @param string $type
     * @param bool $force
     * @param int $storeId Affected store ID
     * @return int|null
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function createOrGetAttributeOptionIdByCodeAndValue(
        string $attributeCode,
        string $label,
        string $type,
        bool $force = false,
        int $storeId = 0
    ): ?int {
        if (strlen($label) < 1) {
            throw new LocalizedException(
                __('Label for %1 must not be empty.', $attributeCode)
            );
        }

        $label = trim($label);
        $optionId = $this->getOptionId($attributeCode, $label, $type, $force, $storeId);
        if (!$optionId) {
            /** @var OptionLabel $optionLabel */
            $optionLabel = $this->optionLabelFactory->create();
            $optionLabel->setStoreId($storeId);
            $optionLabel->setLabel($label);

            /** @var AttributeOptionInterface $option */
            $option = $this->optionFactory->create();
            $option->setLabel($optionLabel->getLabel());
            $option->setStoreLabels([$optionLabel]);
            $option->setSortOrder(0);
            $option->setIsDefault(false);

            $this->attributeOptionManagement->add(
                \Magento\Catalog\Model\Product::ENTITY,
                $this->initAttribute($attributeCode)->getAttributeId(),
                $option
            );

            // get the inserted ID. should be returned from the installer, but it isn't.
            $optionId = $this->getOptionId($attributeCode, $label, $type,true);
        }

        return $optionId;
    }

    /**
     * Find the ID of an option matching $label, if any
     *
     * @param string $attributeCode
     * @param string $label
     * @param string $type
     * @param bool $force
     * @param int $storeId
     * @return int|null
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getOptionId(
        string $attributeCode,
        string $label,
        string $type,
        bool $force = false,
        int $storeId = 0
    ): ?int {
        // format to the lower case, for more exact comparison, as label in system
        // and from a source can differ in the register (ex. 'Black' form source, 'black' in the system)
        $label = strtolower($label);

        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->initAttribute($attributeCode, false, $storeId, $force, $type);
        $attributeId = $attribute->getAttributeId();

        // build option array if necessary
        if ($force === true || !isset($this->attributeValues[$attributeId])) {
            $this->attributeValues[$attributeId] = [];

            // we have to generate a new sourceModel instance each time through to prevent it from
            // referencing its _options cache. No other way to get it to pick up newly-added values.
            /** @var Table $sourceModel */
            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[$attributeId][strtolower((string)$option['label'])]
                    = (int)$option['value'];
            }
        }

        if (isset($this->attributeValues[$attributeId][$label])) {
            return $this->attributeValues[$attributeId][$label];
        }

        return null;
    }

    /**
     * Find the label of an option matching ID, if any
     *
     * @param $attributeCode
     * @param $optionId
     * @return string|null
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getOptionLabelById($attributeCode, $optionId): ?string
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->initAttribute($attributeCode);
        if (!$attribute) {
            return null;
        }
        /** @var Table $sourceModel */
        $sourceModel = $this->tableFactory->create();
        $sourceModel->setAttribute($attribute);

        return $sourceModel->getOptionText($optionId);
    }
}