<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\AttributeHelper;

use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\DB\Select;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeManagementInterface;

/**
 * Class Attribute
 * @package MageCloud\ProductSupplier\Model\AttributeHelper
 */
class Attribute extends AbstractAttribute
{
    /**
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * @var array
     */
    private $attributeIds = [];

    /**
     * @param AttributeFactory $attributeFactory
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeManagementInterface $attributeManagement
     * @param ProductAttributeRepositoryInterface $attributeRepository
     * @param ProductResource $productResource
     * @param Json $json
     * @param EavConfig $eavConfig
     */
    public function __construct(
        AttributeFactory $attributeFactory,
        EavSetupFactory $eavSetupFactory,
        AttributeManagementInterface $attributeManagement,
        ProductAttributeRepositoryInterface $attributeRepository,
        ProductResource $productResource,
        Json $json,
        EavConfig $eavConfig
    ) {
        parent::__construct(
            $attributeFactory,
            $eavSetupFactory,
            $attributeManagement,
            $attributeRepository,
            $productResource,
            $json
        );
        $this->eavConfig = $eavConfig;
    }

    /**
     * @param $attributeCode
     * @param $entityType
     * @return int|mixed
     * @throws LocalizedException
     */
    public function getAttributeIdByCode($attributeCode, $entityType = Product::ENTITY): ?int
    {
        if (!isset($this->attributeIds[$attributeCode])) {
            $connection = $this->productResource->getConnection();
            $entityTypeId = $this->eavConfig->getEntityType($entityType)
                ->getEntityTypeId();
            $select = $connection->select()
                ->from($this->productResource->getTable('eav_attribute'))
                ->where('attribute_code = ?', (string) $attributeCode)
                ->where('entity_type_id = ?', (int) $entityTypeId)
                ->reset(Select::ORDER)
                ->reset(Select::LIMIT_COUNT)
                ->reset(Select::LIMIT_OFFSET)
                ->reset(Select::COLUMNS)
                ->columns('attribute_id');
            $this->attributeIds[$attributeCode] = (int) $connection->fetchOne($select);
        }
        return (int)$this->attributeIds[$attributeCode];
    }
}