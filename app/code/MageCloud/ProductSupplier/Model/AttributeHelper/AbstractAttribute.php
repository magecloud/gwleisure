<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\AttributeHelper;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeManagementInterface;

/**
 * Class AbstractAttribute
 * @package MageCloud\ProductSupplier\Model\AttributeHelper
 */
abstract class AbstractAttribute
{
    /**
     * @var AttributeFactory
     */
    protected $attributeFactory;

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var AttributeManagementInterface
     */
    protected $attributeManagement;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var ProductResource
     */
    protected $productResource;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var array
     */
    protected $attributeValues = [];

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var int
     */
    private $cacheLimit;

    /**
     * @param AttributeFactory $attributeFactory
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeManagementInterface $attributeManagement
     * @param ProductAttributeRepositoryInterface $attributeRepository
     * @param ProductResource $productResource
     * @param Json $json
     * @param int $cacheLimit
     */
    public function __construct(
        AttributeFactory $attributeFactory,
        EavSetupFactory $eavSetupFactory,
        AttributeManagementInterface $attributeManagement,
        ProductAttributeRepositoryInterface $attributeRepository,
        ProductResource $productResource,
        Json $json,
        int $cacheLimit = 1000
    ) {
        $this->attributeFactory = $attributeFactory;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeManagement = $attributeManagement;
        $this->attributeRepository = $attributeRepository;
        $this->productResource = $productResource;
        $this->json = $json;
        $this->cacheLimit = $cacheLimit;
    }

    /**
     * @param string $attributeCode
     * @param string $type
     * @return ProductAttributeInterface|null
     * @throws NoSuchEntityException
     * @throws InputException
     * @throws LocalizedException
     * @throws \Exception
     */
    private function createAttribute(string $attributeCode, string $type): ?ProductAttributeInterface
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create();
        $entityTypeId = $eavSetup->getEntityTypeId(Product::ENTITY);

        $frontendInput = 'text';
        $backendType = 'varchar';
        $sourceModel = '';
        if ($type == 'select') {
            $frontendInput = $type;
            $backendType = 'int';
        } elseif ($type == 'boolean') {
            $frontendInput = $type;
            $backendType = 'int';
            $sourceModel = 'Magento\Eav\Model\Entity\Attribute\Source\Boolean';
        }

        $label = ucwords(str_replace('_', ' ', $attributeCode));
        $data = [
            'frontend_label' => [$label],
            'frontend_input' => $frontendInput,
            'is_required' => 0,
            'update_product_preview_image' => 0,
            'use_product_image_for_swatch' => 0,
            'visual_swatch_validation' => '',
            'visual_swatch_validation_unique' => '',
            'text_swatch_validation' => '',
            'text_swatch_validation_unique' => '',
            'dropdown_attribute_validation' => '',
            'dropdown_attribute_validation_unique' => '',
            'attribute_code' => $attributeCode,
            'is_global' => 0,
            'default_value_text' => '',
            'default_value_yesno' => '',
            'default_value_date' => '',
            'default_value_datetime' => '',
            'default_value_textarea' => '',
            'is_unique' => '',
            'frontend_class' => '',
            'is_used_in_grid' => 1,
            'is_visible_in_grid' => 1,
            'is_filterable_in_grid' => 1,
            'is_searchable' => 1,
            'is_comparable' => 1,
            'is_filterable' => 1,
            'is_filterable_in_search' => 1,
            'is_used_for_promo_rules' => '',
            'is_html_allowed_on_front' => 1,
            'is_visible_on_front' => 1,
            'used_in_product_listing' => 1,
            'used_for_sort_by' => 0,
            'swatch_input_type' => ($type == 'select') ? 'dropdown' : '',
            'is_pagebuilder_enabled' => 0,
            'source_model' => $sourceModel,
            'backend_model' => '',
            'backend_type' => $backendType,
            'default_value' => '',
            'entity_type_id' => $entityTypeId,
            'is_user_defined' => 1
        ];

        /** @var ProductAttributeInterface $model */
        $attribute = $this->attributeFactory->create();
        $attribute->addData($data);

        try {
            $attribute = $this->attributeRepository->save($attribute);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        $attributeSetIds = $eavSetup->getAllAttributeSetIds(Product::ENTITY);
        foreach ($attributeSetIds as $attributeSetId) {
            $attributeGroupId = $eavSetup->getAttributeGroupId(
                $entityTypeId,
                $attributeSetId,
                'Product Details'
            );

            if ($attributeGroupId) {
                $this->attributeManagement->assign(
                    Product::ENTITY,
                    $attributeSetId,
                    $attributeGroupId,
                    $attributeCode,
                    20
                );
            }
        }

        return $attribute;
    }

    /**
     * Get attribute by code.
     *
     * @param $attributeCode
     * @param $mode
     * @param $storeId
     * @param $force
     * @param $type
     * @return mixed|null
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function initAttribute(string $attributeCode, $mode = false, $storeId = null, $force = false, $type = null)
    {
        $cacheKey = $this->getCacheKey([$mode, $storeId]);
        if (!isset($this->attributes[$attributeCode][$cacheKey]) || $force) {
            try {
                $attribute = $this->attributeRepository->get($attributeCode);
            } catch (NoSuchEntityException $e) {
                $attribute = $this->createAttribute($attributeCode, $type);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            if ($attribute) {
                $this->cacheAttribute($cacheKey, $attribute);
            } else {
                $this->attributes[$attributeCode][$cacheKey] = null;
            }
        }
        if (!isset($this->attributes[$attributeCode])) {
            $attributeCode = trim($attributeCode);
        }

        return isset($this->attributes[$attributeCode])
			? $this->attributes[$attributeCode][$cacheKey]
			: null;
    }

    /**
     * Get key for cache
     *
     * @param array $data
     * @return string
     */
    private function getCacheKey($data)
    {
        $serializeData = [];
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            } else {
                $serializeData[$key] = $value;
            }
        }
        $serializeData = $this->json->serialize($serializeData);
        return sha1($serializeData);
    }

    /**
     * Add attribute to internal cache and truncate cache if it has more than cacheLimit elements.
     *
     * @param $cacheKey
     * @param ProductAttributeInterface $productAttribute
     */
    private function cacheAttribute($cacheKey, ProductAttributeInterface $productAttribute)
    {
        $this->attributes[$productAttribute->getAttributeCode()][$cacheKey] = $productAttribute;

        if ($this->cacheLimit && count($this->attributes) > $this->cacheLimit) {
            $offset = (int)round($this->cacheLimit / -2);
            $this->attributes = array_slice($this->attributes, $offset, null, true);
        }
    }

    /**
     * Clean internal product cache
     *
     * @return void
     */
    public function cleanCache()
    {
        $this->attributes = null;
    }
}