<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\Supplier;

/**
 * Class DataHandlerProvider
 * @package MageCloud\ProductSupplier\Model\Supplier
 */
class DataHandlerProvider
{
    /**
     * @var DataHandlerInterface[]
     */
    private $dataHandlers;

    /**
     * @param array $dataHandlers
     */
    public function __construct(
        array $dataHandlers = []
    ) {
        $this->dataHandlers = $dataHandlers;
    }

    /**
     * Retrieve a specific data handler by code.
     *
     * @param string $code
     * @return DataHandlerInterface|null
     */
    public function getDataHandler(string $code): ?DataHandlerInterface
    {
        return $this->dataHandlers[$code] ?? null;
    }
}