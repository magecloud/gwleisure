<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\Supplier\DataHandler;

use MageCloud\ProductSupplier\Model\Supplier\DataHandlerInterface;
use MageCloud\ProductSupplier\Model\Supplier;

/**
 * Class DefaultDataHandler
 * @package MageCloud\ProductSupplier\Model\Supplier\DataHandler
 */
class DefaultDataHandler extends AbstractDataHandler implements DataHandlerInterface
{
    /**
     * @inheritDocs
     */
    public function update(Supplier $supplier = null, array $sources = [], array &$reportData = []): array
    {
        $resultProductIds = [];
        foreach ($sources as $source) {
            if (empty($source)) {
                continue;
            }
            $productIds = $this->resourceProduct->updateSupplierInformation($source, $supplier, $reportData);
            $resultProductIds = array_merge($resultProductIds, $productIds);
        }

        return array_unique($resultProductIds);
    }
}