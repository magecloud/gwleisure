<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\Supplier\DataHandler;

use MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager\Product as ResourceProduct;

/**
 * Class DefaultDataHandler
 * @package MageCloud\ProductSupplier\Model\Supplier\DataHandler
 */
abstract class AbstractDataHandler
{
    /**
     * @var ResourceProduct
     */
    protected $resourceProduct;

    /**
     * @param ResourceProduct $resourceProduct
     */
    public function __construct(
        ResourceProduct $resourceProduct
    ) {
        $this->resourceProduct = $resourceProduct;
    }
}