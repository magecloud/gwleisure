<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\Supplier\Source;

use MageCloud\ProductSupplier\Model\Supplier;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 * @package MageCloud\ProductSupplier\Model\Supplier\Source
 */
class Status implements OptionSourceInterface
{
    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * @param Supplier $supplier
     */
    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->supplier->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}