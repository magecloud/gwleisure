<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\Supplier;

use MageCloud\ProductSupplier\Model\Supplier;

/**
 * Interface DataHandlerInterface
 * @package MageCloud\ProductSupplier\Model\Supplier
 */
interface DataHandlerInterface
{
    /**
     * Update products from related supplier sources
     *
     * @param Supplier|null $supplier
     * @param array $sources
     * @param array $reportData
     * @return array
     * @throws \Exception
     */
    public function update(Supplier $supplier = null, array $sources = [], array &$reportData = []): array;
}