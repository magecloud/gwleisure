<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Model;

use Amasty\Preorder\Model\Indexer\Product\Msi\PreorderProcessor as PreorderIndexer;
use MageCloud\ProductSupplier\Helper\Data as HelperData;
use MageCloud\ProductSupplier\Model\Supplier\DataHandlerProvider;
use Magento\Framework\Exception\LocalizedException;
use MageCloud\ProductSupplier\Model\Supplier\DataHandlerInterface;
use MageCloud\ProductSupplier\Model\SourceProcessorFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;

/**
 * Class OverwritingProcessor
 * @package MageCloud\ProductSupplier\Model
 */
class OverwritingProcessor
{
    /**
     * @var SourceProcessorFactory
     */
    private $sourceProcessorFactory;

    /**
     * @var DataHandlerProvider
     */
    private $dataHandlerProvider;

    /**
     * @var PreorderIndexer
     */
    private $preorderIndexer;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $relativeSourcePath;

    /**
     * @var array
     */
    private $fields;

    /**
     * @param \MageCloud\ProductSupplier\Model\SourceProcessorFactory $sourceProcessorFactory
     * @param DataHandlerProvider $dataHandlerProvider
     * @param PreorderIndexer $preorderIndexer
     * @param HelperData $helperData
     * @param MessageManagerInterface $messageManager
     * @param string|null $code
     * @param string|null $relativeSourcePath
     * @param array $fields
     */
    public function __construct(
        SourceProcessorFactory $sourceProcessorFactory,
        DataHandlerProvider $dataHandlerProvider,
        PreorderIndexer $preorderIndexer,
        HelperData $helperData,
        MessageManagerInterface $messageManager,
        string $code = null,
        string $relativeSourcePath = null,
        array $fields = []
    ) {
        $this->sourceProcessorFactory = $sourceProcessorFactory;
        $this->dataHandlerProvider = $dataHandlerProvider;
        $this->preorderIndexer = $preorderIndexer;
        $this->helperData = $helperData;
        $this->messageManager = $messageManager;
        $this->code = $code;
        $this->relativeSourcePath = $relativeSourcePath;
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getRelativeSourcePath(): string
    {
        return $this->relativeSourcePath;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param string $relativeSourcePath
     * @return $this
     */
    public function setRelativeSourcePath(string $relativeSourcePath): OverwritingProcessor
    {
        $this->relativeSourcePath = $relativeSourcePath;
        return $this;
    }

    /**
     * @param array $fields
     * @return $this
     */
    public function setFields(array $fields): OverwritingProcessor
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): OverwritingProcessor
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return array
     */
    public function getFieldsProviderData(): array
    {
        $fields = $this->getFields();

        $result = [];
        $result[$this->getCode()] = [
            'relativePath' => $this->getRelativeSourcePath(),
            'required' => array_values($fields),
            'mapped' => $fields
        ];

        return $result;
    }

    /**
     * Sync overwriting product stock
     *
     * @param string|null $fileName
     * @param bool $buildOnlyMapped
     * @return void
     * @throws LocalizedException
     */
    public function execute(string $fileName = null, bool $buildOnlyMapped = true): void
    {
        if (!$this->helperData->isEnabledOverwriting()) {
            $this->messageManager->addNoticeMessage(
                __('Overwriting is not available according to the configuration settings.')
            );
            return;
        }

        $code = $this->getCode();
        $relativePath = $this->getRelativeSourcePath();
        $fields = $this->getFields();
        if (!$code || !$relativePath || empty($fields)) {
            $this->messageManager->addErrorMessage(
                __('Incorrect DI configuration for overwriting. Missing required parameters.')
            );
            return;
        }

        /** @var SourceProcessor $sourceProcessor */
        $sourceProcessor = $this->sourceProcessorFactory->create(['fieldsProviderType' => $code]);
        $sourceProcessor->setRelativePath($relativePath)
            ->setRequiredFields(array_values($fields))
            ->setMappedFields($fields);

        $sources = $sourceProcessor->init($fileName, $buildOnlyMapped);
        if (!empty($sources)) {
            /** @var DataHandlerInterface $dataHandler */
            $dataHandler = $this->dataHandlerProvider->getDataHandler($code);
            if (!$dataHandler instanceof DataHandlerInterface) {
                return;
            }
            $productIds = $dataHandler->update(null, $sources);
            if (!empty($productIds)) {
                $this->preorderIndexer->reindexList($productIds);
            }

            $this->messageManager->addSuccessMessage(
                __('The overwrite product stock data has been synced successfully.')
            );
            return;
        }

        $this->messageManager->addNoticeMessage(__('There are no files to overwrite.'));
    }
}