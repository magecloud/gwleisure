<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use MageCloud\ProductSupplier\Helper\Data as HelperData;
use const _PHPStan_35ce48cb5\__;

/**
 * Class ReportProcessor
 * @package MageCloud\ProductSupplier\Model
 */
class ReportProcessor
{
    const SUPPLIER = 'supplier';
    const SOURCE_SKU = 'source_sku';
    const SOURCE_QTY = 'source_qty';
    const SOURCE_DATE = 'source_date';
    const PRODUCT_EXIST = 'product_exist';
    const MAPPED = 'mapped';
    const CONDITION_APPLIED = 'condition_applied';

    /**
     * CSV Processor
     *
     * @var Csv
     */
    protected $csvProcessor;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @var WriteInterface
     */
    private $mediaDirectory;

    /**
     * @var array
     */
    private $reportData;

    /**
     * @param Csv $csvProcessor
     * @param Filesystem $filesystem
     * @param DateTime $dateTime
     * @param HelperData $helperData
     * @param MessageManagerInterface $messageManager
     * @param array $reportData
     * @throws FileSystemException
     */
    public function __construct(
        Csv $csvProcessor,
        Filesystem $filesystem,
        DateTime $dateTime,
        HelperData $helperData,
        MessageManagerInterface $messageManager,
        array $reportData = []
    ) {
        $this->csvProcessor = $csvProcessor;
        $this->filesystem = $filesystem;
        $this->dateTime = $dateTime;
        $this->helperData = $helperData;
        $this->messageManager = $messageManager;
        $this->mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->reportData = $reportData;
    }

    /**
     * @return array
     */
    public function getReportData(): array
    {
        return $this->reportData;
    }

    /**
     * @param array $reportData
     * @return $this
     */
    public function setReportData(array $reportData): ReportProcessor
    {
        $this->reportData = $reportData;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        $keys = [
            self::SUPPLIER,
            self::SOURCE_SKU,
            self::SOURCE_QTY,
            self::SOURCE_DATE,
            self::PRODUCT_EXIST,
            self::MAPPED,
            self::CONDITION_APPLIED
        ];

        return array_map([$this, 'keyToHeader'], $keys);
    }

    /**
     * @param string $key
     * @return string
     */
    public function keyToHeader(string $key): string
    {
        return ucwords(str_replace('_', ' ', $key));
    }

    /**
     * @return string|null
     * @throws FileSystemException
     */
    public function execute(): ?string
    {
        if (!$this->helperData->isEnabledReport()) {
            $this->messageManager->addNoticeMessage(
                __('Report is not available according to the configuration settings.')
            );
            return null;
        }
        $reportData = $this->getReportData();
        if (empty($reportData)) {
            return null;
        }

        $path = $this->mediaDirectory->getAbsolutePath('import/supplier_reports');
        $fileName = sprintf('report_%s.csv', $this->dateTime->date('Y-m-d_H-i-s'));
        $filePath = sprintf('%s/%s', $path, $fileName);
        $this->mediaDirectory->create($path);

        array_unshift($reportData, $this->getHeaders());

        $this->csvProcessor->appendData($filePath, $reportData);
        $this->setReportData([]);

        return $filePath;
    }
}