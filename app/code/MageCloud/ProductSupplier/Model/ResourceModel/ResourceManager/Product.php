<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager;

use MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DB\Select;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Store\Model\Store;
use Magento\Framework\Exception\LocalizedException;
use Amasty\Preorder\Model\Product\Constants as AmastyPreorderConstants;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\Data\StockStatusInterface;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\ProductFactory;
use MageCloud\ProductSupplier\Model\Supplier;
use MageCloud\ProductSupplier\Model\ReportProcessor;
use MageCloud\ProductSupplier\Api\Data\SupplierInterface;

/**
 * Class Product
 * @package MageCloud\ProductSupplier\Model\ResourceModel\ResourceManager
 */
class Product extends ResourceManager
{
    /**
     * Update tables
     */
    const TABLE_CATALOG_PRODUCT_ENTITY_DATETIME = 'catalog_product_entity_datetime';
    const TABLE_CATALOG_PRODUCT_ENTITY_VARCHAR = 'catalog_product_entity_varchar';
    const TABLE_CATALOG_PRODUCT_ENTITY_INT = 'catalog_product_entity_int';
    const TABLE_CATALOG_INVENTORY_STOCK_ITEM = 'cataloginventory_stock_item';
    const TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX = 'cataloginventory_stock_status';
    const TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX_REPLICA = 'cataloginventory_stock_status_replica';

    /**
     * Update attributes
     */
    const ATTRIBUTE_AVAILABILITY = 'availability';
    const ATTRIBUTE_PREORDER_ALLOW_QTY = 'amasty_preorder_allow_qty';
    const ATTRIBUTE_PREORDER_RELEASE_DATE = 'amasty_preorder_release_date';

    /**
     * Labels
     *
     * backorders
     */
    const PRE_ORDER_BUTTON_LABEL_ADD_TO_CART = 'Add to Cart';
    const PRE_ORDER_BUTTON_LABEL_PRE_ORDER = 'Pre-Order';
    const PRE_ORDER_NOTE_ESTIMATED_DELIVERY_TEXT = 'Estimated Delivery';
    const PRE_ORDER_NOTE_ESTIMATED_DELIVERY_WITH_DAYS =
        self::PRE_ORDER_NOTE_ESTIMATED_DELIVERY_TEXT . ': {days}';
    const PRE_ORDER_NOTE_ESTIMATED_DELIVERY_DEFAULT =
        self::PRE_ORDER_NOTE_ESTIMATED_DELIVERY_TEXT . ': 3-5 Working Days';
    /**
     * availability
     */
    const AVAILABILITY_LABEL_IN_STOCK = 'In-Stock';
    const AVAILABILITY_LABEL_OUT_OF_STOCK = 'Out-of-Stock';
    const AVAILABILITY_LABEL_PRE_ORDER = 'Pre-Order';
    const AVAILABILITY_LABEL_LAST_STOCK = 'Last Stock';

    /**
     * Structure data types
     */
    const STRUCTURE_TYPE_STOCK = 'stock';
    const STRUCTURE_TYPE_EAV = 'eav';

    /**
     * Mapping for report
     *
     * @var string[]
     */
    private $yesNoMapping = [
        0 => 'No',
        1 => 'Yes'
    ];

    /**
     * @var string[]
     */
    private $stockTables = [
        self::TABLE_CATALOG_INVENTORY_STOCK_ITEM,
        self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX,
        self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX_REPLICA
    ];

    /**
     * @var array
     */
    private $productIdBySkuMap = [];

    /**
     * @var array
     */
    private $productQtyByProductIdMap = [];

    /**
     * @var array
     */
    private $availabilityValueByLabelMap = [];

    /**
     * @var array
     */
    private $structureData = [];

    /**
     * @var array
     */
    private $suppliers = [];

    /**
     * Structure data myst be cleared after each supplier processed.
     *
     * @return Product
     */
    private function resetStructureData(): Product
    {
        $this->structureData = [];
        return $this;
    }

    /**
     * @param string $sku
     * @return int|null
     */
    public function getProductIdBySku(string $sku): ?int
    {
        if (!isset($this->productIdBySkuMap[$sku])) {
            $connection = $this->getConnection();
            $select = $connection->select()
                ->from($this->getTable('catalog_product_entity'))
                ->where(ProductInterface::SKU . ' = ?', $sku)
                ->reset(Select::COLUMNS)
                ->columns('entity_id');
            $this->productIdBySkuMap[$sku] = (int)$connection->fetchOne($select);
        }
        return $this->productIdBySkuMap[$sku];
    }

    /**
     * @param int $productId
     * @param int $websiteId
     * @param int $stockId
     * @param bool $useIndexTable
     * @return int|null
     */
    public function getStoredProductQty(
        int $productId,
        int $websiteId = 0,
        int $stockId = 1,
        bool $useIndexTable = false
    ): ?int {
        if (!isset($this->productQtyByProductIdMap[$productId])) {
            $sourceTable = $this->getTable(self::TABLE_CATALOG_INVENTORY_STOCK_ITEM);
            if ($useIndexTable) {
                $sourceTable = $this->getTable(self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX);
            }

            $connection = $this->getConnection();
            $select = $connection->select()
                ->from($sourceTable)
                ->where(StockStatusInterface::PRODUCT_ID . ' = ?', $productId, \Zend_Db::INT_TYPE)
                ->where('website_id = ?', $websiteId)
                ->where(StockStatusInterface::STOCK_ID . ' = ?', $stockId, \Zend_Db::INT_TYPE)
                ->reset(Select::COLUMNS)
                ->columns(StockStatusInterface::QTY);
            $this->productQtyByProductIdMap[$productId] = (int)$connection->fetchOne($select);
        }
        return $this->productQtyByProductIdMap[$productId];
    }

    /**
     * @param $tableName
     * @param $productId
     * @param $attributeId
     * @return bool
     */
    private function isAttributeRecordExist($tableName, $productId, $attributeId): bool
    {
        $connection = $this->getConnection();
        $select = $connection->select()
            ->from($this->getTable($tableName))
            ->where('attribute_id = ?', $attributeId, \Zend_Db::INT_TYPE)
            ->where('entity_id = ?', $productId, \Zend_Db::INT_TYPE)
            ->reset(Select::COLUMNS)
            ->columns('value_id');
        return (bool)$connection->fetchOne($select);
    }

    /**
     * @param string $label
     * @return int
     * @throws LocalizedException
     * @throws InputException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function getAvailabilityValue(string $label): int
    {
        if (!isset($this->availabilityValueByLabelMap[$label])) {
            $this->availabilityValueByLabelMap[$label] =
                $this->attributeOptionHelper->createOrGetAttributeOptionIdByCodeAndValue(
                    self::ATTRIBUTE_AVAILABILITY,
                    $label,
                    'select'
                );
        }
        return (int)$this->availabilityValueByLabelMap[$label];
    }

    /**
     * @param string $value
     * @param Supplier|null $supplier
     * @return string
     */
    private function formatDate(string $value, Supplier $supplier = null): string
    {
        $addDays = 0;
        if ($supplier) {
            $addDays = (int)$supplier->getAddDays();
        }

        if (strpos($value, '/') !== false) {
            $date = \DateTime::createFromFormat('d/m/Y', $value);
            if ($addDays) {
                $date->modify('+ ' . $addDays . ' days');
            }
            $value = $date->format('Y-m-d');
        } else {
            if ($timestamp = strtotime($value)) {
                // format ex. 20 May 2024

                $date = new \DateTime();
                $date->setTimestamp($timestamp);

                if ($addDays) {
                    $date->modify('+ ' . $addDays . ' days');
                }
                $value = $date->format('Y-m-d');
            } else {
                //@TODO - need to take into account another format
            }
        }

        return $value;
    }

    /**
     * @param Supplier $supplier
     * @param int $productId
     * @return string
     */
    private function getSupplierDeliveryDaysNote(Supplier $supplier, int $productId): string
    {
        $deliveryDaysNote = self::PRE_ORDER_NOTE_ESTIMATED_DELIVERY_WITH_DAYS;
        $deliveryDays = $supplier->getDefaultDeliveryDays();
        if ($supplier->getUseProductDeliveryConditions()) {
            $conditions = $supplier->getDeliveryDaysPerConditions();
            if (!$conditions) {
                return str_replace('{days}', $deliveryDays, $deliveryDaysNote);
            }

            $conditions = explode(';', $conditions);
            foreach ($conditions as $condition) {
                $conditionData = explode('|', $condition, 2);
                $conditionString = $conditionData[0];
                $conditionDays = $conditionData[1];

                $conditionString = explode(' ', $conditionString, 3);
                $conditionAttributeCode = strtolower($conditionString[0]);
                $conditionAttributeSign = $conditionString[1];
                $conditionAttributeValue = $conditionString[2];

                /** @var ProductModel $product */
                $product = ObjectManager::getInstance()->create(ProductFactory::class)->create()
                    ->load($productId);
                $productAttributeValue = $product->getData($conditionAttributeCode);
                if (
                    $productAttributeValue
                    && ((($conditionAttributeSign == '>') && ($productAttributeValue > $conditionAttributeValue))
                    || (($conditionAttributeSign == '>=') && ($productAttributeValue >= $conditionAttributeValue))
                    || (($conditionAttributeSign == '<') && ($productAttributeValue < $conditionAttributeValue))
                    || (($conditionAttributeSign == '<=') && ($productAttributeValue <= $conditionAttributeValue))
                    || (($conditionAttributeSign == '=') && ($productAttributeValue == $conditionAttributeValue)))
                ) {
                    $deliveryDays = $conditionDays;
                }
            }
        }

        return str_replace('{days}', $deliveryDays, $deliveryDaysNote);
    }

    /**
     * Build supplier related data.
     * Must init only once, as it's the same for all products.
     *
     * @param Supplier $supplier
     * @param bool $useStock
     * @param bool $useEav
     * @return array
     * @throws \Exception
     */
    private function buildStructureData(
        Supplier $supplier = null,
        bool $useStock = true,
        bool $useEav = true
    ): array {
        if (empty($this->structureData)) {
            $connection = $this->getConnection();
            $structure = [];

            if ($useStock) {
                $structure[self::STRUCTURE_TYPE_STOCK] = [
                    self::TABLE_CATALOG_INVENTORY_STOCK_ITEM => [
                        StockItemInterface::BACKORDERS,
                        StockItemInterface::USE_CONFIG_BACKORDERS
                    ]
                ];

                array_push(
                    $structure[self::STRUCTURE_TYPE_STOCK][self::TABLE_CATALOG_INVENTORY_STOCK_ITEM],
                    StockItemInterface::IS_IN_STOCK
                );
                $structure[self::STRUCTURE_TYPE_STOCK][self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX]
                    = [StockStatusInterface::STOCK_STATUS];
                $structure[self::STRUCTURE_TYPE_STOCK][self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX_REPLICA]
                    = [StockStatusInterface::STOCK_STATUS];
            }

            if ($useEav) {
                $structure[self::STRUCTURE_TYPE_EAV] = [
                    self::TABLE_CATALOG_PRODUCT_ENTITY_INT => [
                        self::ATTRIBUTE_AVAILABILITY,
                        self::ATTRIBUTE_PREORDER_ALLOW_QTY
                    ],
                    self::TABLE_CATALOG_PRODUCT_ENTITY_VARCHAR => [
                        AmastyPreorderConstants::CART_LABEL,
                        AmastyPreorderConstants::NOTE
                    ],
                    self::TABLE_CATALOG_PRODUCT_ENTITY_DATETIME => [
                        self::ATTRIBUTE_PREORDER_RELEASE_DATE,
                    ]
                ];
            }

            foreach ($structure as $type => $data) {
                foreach ($data as $tableName => $columns) {
                    foreach ($columns as $destinationAttributeCode) {
                        $destinationAttributeId = $this->getAttributeId($destinationAttributeCode);
                        // in case if some data can be retrieved directly from the supplier
                        $destinationValue = $supplier ? ($supplier->getData($destinationAttributeCode) ?? null) : null;
                        $this->structureData[$type][$tableName][$destinationAttributeCode] = [
                            'bind' => [
                                'value' => $destinationValue
                            ],
                            'where' => [
                                $connection->quoteInto('attribute_id = ?', $destinationAttributeId, \Zend_Db::INT_TYPE)
                            ],
                            'attribute_id' => $destinationAttributeId // use for insert (if any)
                        ];
                    }
                }
            }
        }
        return $this->structureData;
    }

    /**
     * @param string $code
     * @return SupplierInterface|null
     * @throws LocalizedException
     */
    private function initSupplier(string $code): ?SupplierInterface
    {
        if (!isset($this->suppliers[$code])) {
            $codeFilter = $this->filterBuilder->setField(SupplierInterface::CODE)
                ->setValue($code)
                ->setConditionType('eq')
                ->create();

            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter($codeFilter)
                ->create();

            $result = $this->supplierRepository->getList($searchCriteria)->getItems();

            $this->suppliers[$code] = !empty($result) ? array_shift($result): null;
        }

        return $this->suppliers[$code];
    }

    /**
     * @param int $productId
     * @param array $sourceRowData
     * @param Supplier|null $supplier
     * @return array
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    private function getSourceCaseData(
        int $productId,
        array $sourceRowData,
        Supplier $supplier = null
    ): array {
        $sourceQty = $sourceRowData[self::ATTRIBUTE_PREORDER_ALLOW_QTY] ?? 0;
        $sourceReleaseDate = $sourceRowData[self::ATTRIBUTE_PREORDER_RELEASE_DATE] ?? 0;
        if ($sourceReleaseDate) {
            $sourceReleaseDate = $this->formatDate($sourceReleaseDate, $supplier);
        }

        $defaultData = [
            self::ATTRIBUTE_PREORDER_ALLOW_QTY => $sourceQty,
            StockItemInterface::BACKORDERS => AmastyPreorderConstants::BACKORDERS_PREORDER_OPTION,
            StockItemInterface::USE_CONFIG_BACKORDERS => 0,
            self::ATTRIBUTE_AVAILABILITY => $this->getAvailabilityValue(self::AVAILABILITY_LABEL_IN_STOCK),
            AmastyPreorderConstants::CART_LABEL => self::PRE_ORDER_BUTTON_LABEL_ADD_TO_CART,
        ];

        if ($sourceQty && $sourceReleaseDate) {
            // CASE 1. qty available in supplier, delivery date available in supplier
            if (!$supplier && isset($sourceRowData['supplier_code'])) {
                // overwriting
                $supplier = $this->initSupplier((string)$sourceRowData['supplier_code']);
            }

            $supplierDeliveryDaysNote = $this->getSupplierDeliveryDaysNote($supplier, $productId);

            return array_merge(
                $defaultData,
                [
                    StockItemInterface::IS_IN_STOCK => StockStatusInterface::STATUS_IN_STOCK,
                    StockStatusInterface::STOCK_STATUS => StockStatusInterface::STATUS_IN_STOCK,
                    self::ATTRIBUTE_PREORDER_RELEASE_DATE => $sourceReleaseDate,
                    AmastyPreorderConstants::NOTE => $supplierDeliveryDaysNote
                ]
            );
        } else if (!$sourceQty && $sourceReleaseDate) {
            // CASE 2: qty not available in supplier, delivery date available in supplier
            $sourceReleaseDateFormatForString = date('d/m/Y', strtotime($sourceReleaseDate));
            $preOrderNote = str_replace(
                '{days}',
                $sourceReleaseDateFormatForString,
                self::PRE_ORDER_NOTE_ESTIMATED_DELIVERY_WITH_DAYS
            );

            return array_merge(
                $defaultData,
                [
                    StockItemInterface::IS_IN_STOCK => StockStatusInterface::STATUS_IN_STOCK,
                    StockStatusInterface::STOCK_STATUS => StockStatusInterface::STATUS_IN_STOCK,
                    self::ATTRIBUTE_PREORDER_ALLOW_QTY => null,
                    AmastyPreorderConstants::CART_LABEL => self::PRE_ORDER_BUTTON_LABEL_PRE_ORDER,
                    self::ATTRIBUTE_PREORDER_RELEASE_DATE => $sourceReleaseDate,
                    AmastyPreorderConstants::NOTE => $preOrderNote,
                    self::ATTRIBUTE_AVAILABILITY => $this->getAvailabilityValue(
                        self::AVAILABILITY_LABEL_PRE_ORDER
                    )
                ]
            );
        } else if ($sourceQty && !$sourceReleaseDate) {
            // CASE 3: qty available in supplier, delivery date not available in supplier
            if (!$supplier) {
                // overwriting
                $supplier = $this->initSupplier((string)$sourceRowData['supplier_code']);
            }

            $preOrderNote = $this->getSupplierDeliveryDaysNote($supplier, $productId);

            return array_merge(
                $defaultData,
                [
                    StockItemInterface::IS_IN_STOCK => StockStatusInterface::STATUS_IN_STOCK,
                    StockStatusInterface::STOCK_STATUS => StockStatusInterface::STATUS_IN_STOCK,
                    self::ATTRIBUTE_PREORDER_RELEASE_DATE => null,
                    AmastyPreorderConstants::NOTE => $preOrderNote
                ]
            );
        } else if (!$sourceQty && !$sourceReleaseDate) {
            // CASE 4: qty not available in supplier, delivery date not available in supplier

            $result = [
                self::ATTRIBUTE_PREORDER_ALLOW_QTY => '',
                AmastyPreorderConstants::CART_LABEL => '',
                StockItemInterface::BACKORDERS => StockItemInterface::BACKORDERS_NO,
                StockItemInterface::USE_CONFIG_BACKORDERS => 1,
                self::ATTRIBUTE_PREORDER_RELEASE_DATE => null,
                AmastyPreorderConstants::NOTE => '',
                self::ATTRIBUTE_AVAILABILITY => $this->getAvailabilityValue(
                    self::AVAILABILITY_LABEL_LAST_STOCK
                )
            ];

            $storedQty = (int)$this->getStoredProductQty($productId);
            if ($storedQty <= 0) {
                $result = array_merge(
                    $result,
                    [
                        StockItemInterface::IS_IN_STOCK => StockStatusInterface::STATUS_OUT_OF_STOCK,
                        StockStatusInterface::STOCK_STATUS => StockStatusInterface::STATUS_OUT_OF_STOCK,
                    ]
                );
            }

            return $result;

//            return array_merge(
//                $defaultData,
//                [
//                    StockItemInterface::IS_IN_STOCK => StockStatusInterface::STATUS_OUT_OF_STOCK,
//                    StockStatusInterface::STOCK_STATUS => StockStatusInterface::STATUS_OUT_OF_STOCK,
//                    self::ATTRIBUTE_PREORDER_ALLOW_QTY => '',
//                    AmastyPreorderConstants::CART_LABEL => '',
//                    StockItemInterface::BACKORDERS => StockItemInterface::BACKORDERS_NO,
//                    StockItemInterface::USE_CONFIG_BACKORDERS => 1,
//                    self::ATTRIBUTE_PREORDER_RELEASE_DATE => null,
//                    AmastyPreorderConstants::NOTE => '',
//                    self::ATTRIBUTE_AVAILABILITY => $this->getAvailabilityValue(
//                        self::AVAILABILITY_LABEL_LAST_STOCK
//                    )
//                ]
//            );
        }

        return [];
    }

    /**
     * @param string $tableName
     * @param int $productId
     * @param array $bindData
     * @return void
     */
    private function applyUpdate(
        string $tableName,
        int $productId,
        array $bindData
    ): void {
        $connection = $this->getConnection();
        $updateResult = $connection->update($tableName, $bindData['bind'], $bindData['where']);
        if (
            !$updateResult
            && isset($bindData['attribute_id'])
            && !in_array($tableName, $this->stockTables)
        ) {
            $attributeId = (int)$bindData['attribute_id'];

            if (!$this->isAttributeRecordExist($tableName, $productId, $attributeId)) {
                // update result may also return 0 when record exist but has the same data as update
                // insert only in case if record doesn't exist
                $insertBind = array_merge(
                    [
                        'attribute_id' => $bindData['attribute_id'],
                        'store_id' => Store::DEFAULT_STORE_ID,
                        'entity_id' => $productId
                    ],
                    $bindData['bind']
                );
                $connection->insert(
                    $this->getTable($tableName),
                    $insertBind
                );
            }
        }
    }

    /**
     * @param int $productId
     * @param array $stockCaseData
     * @param Supplier|null $supplier
     * @return array
     * @throws \Exception
     */
    private function buildDataForProcess(int $productId, array $stockCaseData, Supplier $supplier = null): array
    {
        $connection = $this->getConnection();
        $structureData = $this->buildStructureData($supplier);

        $result = [];
        $removeStockStatusUpdate = false;
        foreach ($structureData as $type => $data) {
            foreach ($data as $tableName => $binds) {
                $preparedBind = [];
                $preparedWhereClauses = [];
                $preparedData = [];
                foreach ($binds as $attributeCode => $bindData) {
                    if (!isset($bindData['bind']) || !isset($bindData['where'])) {
                        continue;
                    }

                    $value = null;
                    $bind = $bindData['bind'];
                    if (!$bind['value'] && array_key_exists($attributeCode, $stockCaseData)) {
                        $value = $stockCaseData[$attributeCode];
                    }

                    if ($type == self::STRUCTURE_TYPE_STOCK) {
                        $idColumn = 'product_id';
                        $bindData['where'] = [];
                        if (($attributeCode == StockItemInterface::IS_IN_STOCK) && ($value === null)) {
                            // don't include stock status column if it does not need to be updated
                            $removeStockStatusUpdate = true;
                            continue;
                        }
                        $bind[$attributeCode] = $value;
                        unset($bind['value']);

                        $preparedBind = array_merge($preparedBind, $bind);
                        $preparedData['bind'] = $preparedBind;
                        // init where clause only once, as it's the same for one table
                        if (empty($preparedWhereClauses)) {
                            $preparedWhereClauses = array_merge(
                                $bindData['where'],
                                [
                                    $connection->quoteInto($idColumn . ' = ?', $productId, \Zend_Db::INT_TYPE)
                                ]
                            );
                            $preparedData['where'] = $preparedWhereClauses;
                        }
                    } elseif ($type == self::STRUCTURE_TYPE_EAV) {
                        $idColumn = 'entity_id';
                        $bind['value'] = $value;

                        $preparedBind[$attributeCode] = $bind;
                        $preparedWhereClauses[$attributeCode] = array_merge(
                            $bindData['where'],
                            [
                                $connection->quoteInto($idColumn . ' = ?', $productId, \Zend_Db::INT_TYPE)
                            ]
                        );

                        $preparedData[$attributeCode] = [
                            'bind' => $preparedBind[$attributeCode],
                            'where' => $preparedWhereClauses[$attributeCode],
                            'attribute_id' => $bindData['attribute_id'],
                        ];
                    }
                }

                $result[$tableName] = $preparedData;
            }
        }

        if ($removeStockStatusUpdate) {
            unset($result[self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX]);
            unset($result[self::TABLE_CATALOG_INVENTORY_STOCK_ITEM_INDEX_REPLICA]);
        }

        return $result;
    }

    /**
     * @param array $source
     * @param Supplier|null $supplier
     * @param array $reportData
     * @return array
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function updateSupplierInformation(array $source, Supplier $supplier = null, array &$reportData = []): array
    {
        $productIds = [];
        foreach ($source as $row) {
            $sku = $row[ProductInterface::SKU] ?? null;
            if (!$sku) {
                continue;
            }

            $sku = trim($sku);

            $rowReportData = [];
            $rowReportData[ReportProcessor::SUPPLIER] = '';
            if ($supplier) {
                $rowReportData[ReportProcessor::SUPPLIER] = $supplier->getName();
            }
            $rowReportData[ReportProcessor::SOURCE_SKU] = $sku;
            $rowReportData[ReportProcessor::SOURCE_QTY] = $row[self::ATTRIBUTE_PREORDER_ALLOW_QTY] ?? 0;
            $rowReportData[ReportProcessor::SOURCE_DATE] = $row[self::ATTRIBUTE_PREORDER_RELEASE_DATE] ?? 0;

            $noLabel = $this->yesNoMapping[0];
            $yesLabel = $this->yesNoMapping[1];

            if (!$productId = (int)$this->getProductIdBySku($sku)) {
                $rowReportData[ReportProcessor::PRODUCT_EXIST] = $noLabel;
                $rowReportData[ReportProcessor::MAPPED] = $noLabel;
                $rowReportData[ReportProcessor::CONDITION_APPLIED] = $noLabel;
                $reportData[$sku] = $rowReportData;
                continue;
            }

            $rowReportData[ReportProcessor::PRODUCT_EXIST] = $yesLabel;

            $sourceCaseData = $this->getSourceCaseData($productId, $row, $supplier);
            if (empty($sourceCaseData)) {
                $rowReportData[ReportProcessor::MAPPED] = $noLabel;
                $rowReportData[ReportProcessor::CONDITION_APPLIED] = $noLabel;
                $reportData[$sku] = $rowReportData;
                continue;
            }

            $preparedData = $this->buildDataForProcess($productId, $sourceCaseData, $supplier);
            foreach ($preparedData as $tableName => $binds) {
                if (isset($binds['bind']) && isset($binds['where'])) {
                    // stock tables
                    $this->applyUpdate($tableName, $productId, $binds);
                    continue;
                }

                // eav tables
                foreach ($binds as $attributeBindData) {
                    $this->applyUpdate($tableName, $productId, $attributeBindData);
                }
            }

            $productIds[] = $productId;
            $rowReportData[ReportProcessor::MAPPED] = $yesLabel;
            $rowReportData[ReportProcessor::CONDITION_APPLIED] = $yesLabel;
            $reportData[$sku] = $rowReportData;
        }

        $this->resetStructureData();

        return $productIds;
    }
}