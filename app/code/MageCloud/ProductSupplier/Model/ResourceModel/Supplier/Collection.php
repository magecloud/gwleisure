<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Model\ResourceModel\Supplier;

use MageCloud\ProductSupplier\Model\ResourceModel\AbstractCollection;

/**
 * Class Collection
 * @package MageCloud\ProductSupplier\Model\ResourceModel\Supplier
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'product_supplier_supplier_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'supplier_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageCloud\ProductSupplier\Model\Supplier::class,
            \MageCloud\ProductSupplier\Model\ResourceModel\Supplier::class
        );
    }

    /**
     * Add collection filters by identifiers
     *
     * @param $recordId
     * @param bool $exclude
     * @return $this
     */
    public function addIdFilter($recordId, $exclude = false)
    {
        if (empty($recordId)) {
            $this->_setIsLoaded(true);
            return $this;
        }
        if (is_array($recordId)) {
            if (!empty($recordId)) {
                if ($exclude) {
                    $condition = ['nin' => $recordId];
                } else {
                    $condition = ['in' => $recordId];
                }
            } else {
                $condition = '';
            }
        } else {
            if ($exclude) {
                $condition = ['neq' => $recordId];
            } else {
                $condition = $recordId;
            }
        }
        $this->addFieldToFilter($this->_idFieldName, $condition);
        return $this;
    }

    /**
     * @return $this
     */
    public function addStatusFilter(int $status)
    {
        $this->addFieldToFilter('status', $status);
        return $this;
    }
}