<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model;

use MageCloud\ProductSupplier\Helper\Data as HelperData;
use MageCloud\ProductSupplier\Model\OverwritingProcessor;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Psr\Log\LoggerInterface;
use MageCloud\ProductSupplier\Api\SupplierRepositoryInterface;
use Magento\Framework\Stdlib\DateTime;
use MageCloud\ProductSupplier\Model\OverwritingProcessorFactory;
use MageCloud\ProductSupplier\Model\ReportProcessor;
use MageCloud\ProductSupplier\Model\ReportProcessorFactory;
use MageCloud\ProductSupplier\Api\Data\SupplierInterface;

/**
 * Class SupplierProcessor
 * @package MageCloud\ProductSupplier\Model
 */
class SupplierProcessor
{
    /**
     * @var SupplierRepositoryInterface
     */
    private $supplierRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var OverwritingProcessorFactory
     */
    private $overwritingProcessorFactory;

    /**
     * @var ReportProcessorFactory
     */
    private $reportProcessorFactory;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param SupplierRepositoryInterface $supplierRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param OverwritingProcessorFactory $overwritingProcessorFactory
     * @param ReportProcessorFactory $reportProcessorFactory
     * @param HelperData $helperData
     * @param LoggerInterface $logger
     */
    public function __construct(
        SupplierRepositoryInterface $supplierRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        OverwritingProcessorFactory $overwritingProcessorFactory,
        ReportProcessorFactory $reportProcessorFactory,
        HelperData $helperData,
        LoggerInterface $logger
    ) {
        $this->supplierRepository = $supplierRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->overwritingProcessorFactory = $overwritingProcessorFactory;
        $this->reportProcessorFactory = $reportProcessorFactory;
        $this->helperData = $helperData;
        $this->logger = $logger;
    }

    /**
     * Update products via related supplier source
     *
     * @param $code
     * @return void
     * @throws \Exception
     */
    public function execute($code = null): void
    {
        $timer = microtime(true);
        $status = HelperData::STATUS_SUCCESS;

        if ($code) {
            $codeFilter = $this->filterBuilder->setField(SupplierInterface::CODE)
                ->setValue($code)
                ->setConditionType('eq')
                ->create();

            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter($codeFilter)
                ->create();
        } else {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        }

        $result = $this->supplierRepository->getList($searchCriteria);
        if ($result->getTotalCount() > 0) {
            $reportData = [];
            /** @var Supplier $supplier */
            foreach ($result->getItems() as $supplier) {
                if ($supplier->getStatus() != Supplier::STATUS_ENABLED) {
                    continue;
                }

                $supplierName = $supplier->getName();
                try {
                    $this->logger->info("Start processing supplier {$supplierName}.");
                    $supplier->syncProductStock($reportData);
                    $supplier->setSyncedAt(date(DateTime::DATETIME_PHP_FORMAT));
                    $this->supplierRepository->save($supplier);
                    $this->logger->info("Supplier {$supplierName} was processed successfully.");
                } catch (\Exception $e) {
                    $status = HelperData::STATUS_ERROR;
                    $this->logger->error(
                        "Supplier {$supplierName} was processing with error. {$e->getMessage()}"
                    );
                }
            }

            if ($this->helperData->isEnabledOverwriting()) {
                $this->logger->info("Start processing overwriting.");
                /** @var OverwritingProcessor $overwritingProcessor */
                $overwritingProcessor = $this->overwritingProcessorFactory->create();
                $overwritingProcessor->execute();
                $this->logger->info("Overwriting was processed successfully.");
            }

            if (!empty($reportData) && $this->helperData->isEnabledReport()) {
                $this->logger->info("Start processing report.");
                /** @var ReportProcessor $reportProcessor */
                $reportProcessor = $this->reportProcessorFactory->create(
                    [
                        'reportData' => $reportData
                    ]
                );
                $reportProcessor->execute();
                $this->logger->info("Report was processed successfully.");
            }
        }

        $end = microtime(true);
        $workingTime = round($end - $timer, 2);
        $this->helperData->setLastExecutionInfo($workingTime, $status);
    }
}