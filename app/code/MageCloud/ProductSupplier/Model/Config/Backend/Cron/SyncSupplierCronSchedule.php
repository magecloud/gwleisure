<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Model\Config\Backend\Cron;

use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class SyncSupplierCronSchedule
 * @package MageCloud\ProductSupplier\Model\Config\Backend\Cron
 */
class SyncSupplierCronSchedule extends Value
{
    const CRON_GENERAL_STRING_PATH = 'crontab/default/jobs/magecloud_product_supplier_processing/schedule/cron_expr';
    const CRON_GENERAL_MODEL_PATH = 'crontab/default/jobs/magecloud_product_supplier_processing/run/model';

    /**
     * @var ValueFactory
     */
    protected $configValueFactory;

    /**
     * @var string
     */
    protected $runModelPath = '';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param $runModelPath
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        $runModelPath = '',
        array $data = []
    ) {
        $this->runModelPath = $runModelPath;
        $this->configValueFactory = $configValueFactory;
        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Cron settings after save
     *
     * @return Value
     * @throws LocalizedException
     */
    public function afterSave()
    {
        $this->updateConfigValues($this->getValue());
        return parent::afterSave();
    }

    /**
     * @param string $cronExprString
     * @throws LocalizedException
     */
    private function updateConfigValues($cronExprString = '')
    {
        try {
            /** @var \Magento\Framework\App\Config\Value $value */
            $value = $this->configValueFactory->create();
            $value->load(self::CRON_GENERAL_STRING_PATH, 'path');
            $value->setValue($cronExprString)
                ->setPath(self::CRON_GENERAL_STRING_PATH);
            $value->save();

            /** @var \Magento\Framework\App\Config\Value $value */
            $value = $this->configValueFactory->create();
            $value->load(self::CRON_GENERAL_MODEL_PATH, 'path');
            $value->setValue($this->runModelPath)
                ->setPath(self::CRON_GENERAL_MODEL_PATH);
            $value->save();
        } catch (\Exception $e) {
            throw new LocalizedException(__(
                'Can\'t save the cron expression: %1', $cronExprString
            ));
        }
    }
}