<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State as AppState;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Helper\ProgressBarFactory;
use MageCloud\ProductSupplier\Model\SupplierProcessor;
use MageCloud\ProductSupplier\Model\SupplierProcessorFactory;
use Magento\Store\Model\Store;

/**
 * Class UpdateProducts
 * @package MageCloud\ProductSupplier\Console\Command
 */
class UpdateProducts extends AbstractCommand
{
    /**#@+
     * Constant for current command.
     */
    const COMMAND = 'magecloud:product-supplier:update';
    const SUPPLIER_ARGUMENT = 'supplier';

    /**
     * @var SupplierProcessorFactory
     */
    private $supplierProcessorFactory;

    /**
     * @param AppState $appState
     * @param StoreManagerInterface $storeManager
     * @param ProgressBarFactory $progressBarFactory
     * @param SupplierProcessorFactory $supplierProcessorFactory
     */
    public function __construct(
        AppState $appState,
        StoreManagerInterface $storeManager,
        ProgressBarFactory $progressBarFactory,
        SupplierProcessorFactory $supplierProcessorFactory
    ) {
        parent::__construct(
            $appState,
            $storeManager,
            $progressBarFactory
        );
        $this->supplierProcessorFactory = $supplierProcessorFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription('Update product supplier information from source file.')
            ->addArgument(
                self::SUPPLIER_ARGUMENT,
                InputArgument::OPTIONAL,
                'Supplier code. Processing all suppliers if supplier code is not provided.'
            )
            ->addOption(
                self::STORE_INPUT_OPTION_KEY,
                's',
                InputOption::VALUE_REQUIRED,
                'Use the specific Store View',
                Store::DEFAULT_STORE_ID
            );

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->beforeProcess($input, $output);

        /** @var SupplierProcessor $supplierProcessor */
        $supplierProcessor = $this->supplierProcessorFactory->create();
        $supplierProcessor->execute($input->getArgument(self::SUPPLIER_ARGUMENT));

        $this->afterProcess($output);
    }
}