<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State as AppState;
use Magento\Framework\App\Area;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\SimpleDataObjectConverter;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\ProgressBarFactory;

/**
 * Class AbstractCommand
 * @package MageCloud\Nisa\Console\Command
 */
abstract class AbstractCommand extends Command
{
    /**#@+
     * Constant for commands
     */
    const STORE_INPUT_OPTION_KEY = 'store';
    /**#@-*/

    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProgressBarFactory
     */
    private $progressBarFactory;

    /**
     * @var null
     */
    private $timerStart = null;

    /**
     * @var null
     */
    private $progressBar = null;

    /**
     * @var null|int
     */
    private $affectedStore = null;

    /**
     * AbstractCommand constructor.
     * @param AppState $appState
     * @param StoreManagerInterface $storeManager
     * @param ProgressBarFactory $progressBarFactory
     */
    public function __construct(
        AppState $appState,
        StoreManagerInterface $storeManager,
        ProgressBarFactory $progressBarFactory
    ) {
        $this->appState = $appState;
        $this->storeManager = $storeManager;
        $this->progressBarFactory = $progressBarFactory;
        parent::__construct();
    }

    /**
     * Try to set area code in case if it was not set before
     *
     * @return $this
     */
    protected function initAreaCode(): AbstractCommand
    {
        try {
            $this->appState->setAreaCode(Area::AREA_GLOBAL);
        } catch (LocalizedException $e) {
            // area code already set
        }
        return $this;
    }

    /**
     * @return float|null
     */
    public function getTimerStart(): ?float
    {
        return $this->timerStart;
    }

    /**
     * @param $start
     * @return $this
     */
    public function setTimerStart($start): AbstractCommand
    {
        $this->timerStart = $start;
        return $this;
    }

    /**
     * @return $this
     */
    public function resetTimer(): AbstractCommand
    {
        $this->timerStart = null;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAffectedStore(): ?int
    {
        return $this->affectedStore;
    }

    /**
     * @param InputInterface $input
     * @param $storeId
     * @return $this
     * @throws NoSuchEntityException
     */
    public function setAffectedStore(InputInterface $input, $storeId = null): AbstractCommand
    {
        if (null == $storeId) {
            $storeId = $input->getOption(self::STORE_INPUT_OPTION_KEY);
            if (!$storeId) {
                $storeId = $this->getDefaultStoreId();
            }
        }
        $this->affectedStore = $storeId;
        return $this;
    }

    /**
     * @return $this
     */
    public function resetAffectedStore(): AbstractCommand
    {
        $this->affectedStore = null;
        return $this;
    }

    /**
     * @param OutputInterface $output
     * @param array $data
     * @return AbstractCommand
     */
    private function prepareProgressBar(OutputInterface $output, array $data = []): AbstractCommand
    {
        if (null === $this->progressBar) {
            /** @var ProgressBar $progress */
            $progress = $this->progressBarFactory->create(
                [
                    'output' => $output,
                    'max' => count($data)
                ]
            );
            $progress->setFormat(
                "%current%/%max% [%bar%] %percent:3s%% %elapsed% %memory:6s% \t| <info>%message%</info>"
            );
            if ($output->getVerbosity() !== OutputInterface::VERBOSITY_NORMAL) {
                $progress->setOverwrite(false);
            }
            $this->progressBar = $progress;
        }
        return $this;
    }

    /**
     * @return ProgressBar|null
     */
    public function getProgressBar(): ?ProgressBar
    {
        return $this->progressBar;
    }

    /**
     * Reset progress bar to initial state
     */
    private function resetProgressBar()
    {
        $this->progressBar = null;
    }

    /**
     * Perform actions before processing specific command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param null $storeId
     * @param array $data
     * @return $this
     * @throws NoSuchEntityException
     */
    protected function beforeProcess(
        InputInterface $input,
        OutputInterface $output,
        $storeId = null,
        array $data = []
    ): AbstractCommand {
        $this->initAreaCode();
        $this->setTimerStart(microtime(true));
        $this->prepareProgressBar($output, $data);
        $this->setAffectedStore($input, $storeId);
        $output->writeln('');
        if ($affectedStore = $this->getAffectedStore()) {
            $storeName = $this->getStoreNameById($affectedStore);
            $output->writeln(
                "<info>Performing operation(s) for store with ID {$affectedStore} ({$storeName}):</info>"
            );
        } else {
            $output->writeln("<info>Performing operation(s) for ALL stores:</info>");
        }

        return $this;
    }

    /**
     * Perform actions after processing specific command
     *
     * @param OutputInterface $output
     * @param array $arguments
     * @return $this
     * @throws NoSuchEntityException
     */
    protected function afterProcess(OutputInterface $output, array $arguments = []): AbstractCommand
    {
        $end = microtime(true);
        $workingTime = round($end - $this->getTimerStart(), 2);
        if ($affectedStore = $this->getAffectedStore()) {
            $storeName = $this->getStoreNameById($affectedStore);
            $output->writeln("");
            $output->writeln(
                "<info>Operation(s) for store ID {$affectedStore} ({$storeName}) were performed successfully.</info>"
            );
        } else {
            $output->writeln("<info>Operation(s) for ALL stores were performed successfully.</info>");
        }
        $output->writeln("<info>Working time: {$workingTime}</info>");
        $this->reset();
        return $this;
    }

    /**
     * @param $storeCode
     * @param StoreInterface[] $stores
     * @return int
     */
    protected function getStoreIdByCode($storeCode, array $stores): int
    {
        foreach ($stores as $store) {
            if ($store->getCode() == $storeCode) {
                return $store->getId();
            }
        }
        return Store::DEFAULT_STORE_ID;
    }

    /**
     * @return int
     * @throws NoSuchEntityException
     */
    protected function getDefaultStoreId(): int
    {
        return (int)$this->storeManager->getStore()->getId();
    }

    /**
     * @param int|null $storeId
     * @return string
     * @throws NoSuchEntityException
     */
    protected function getStoreNameById(int $storeId = null): string
    {
        return $this->storeManager->getStore($storeId)->getName();
    }

    /**
     * @param $value
     * @return string
     */
    protected function formatToSnakeCase($value): string
    {
        return SimpleDataObjectConverter::camelCaseToSnakeCase($value);
    }

    /**
     * @param $value
     * @return string
     */
    protected function formatToTitle($value): string
    {
        return ucwords(str_replace('_', ' ', $this->formatToSnakeCase($value)));
    }

    /**
     * @return $this
     */
    protected function reset(): AbstractCommand
    {
        $this->resetAffectedStore();
        $this->resetProgressBar();
        $this->resetTimer();
        return $this;
    }
}