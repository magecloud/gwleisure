<!--
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
-->

define([
    'uiComponent',
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'MageCloud_ProductSupplier/minicart/item/details/preorder-note',
            displayArea: 'preorder'
        },

        /**
         * @param quoteItemData
         * @return {string}
         */
        getValue: function (quoteItemData) {
            let result = '';

            if (quoteItemData['preorder_note']) {
                result = quoteItemData['preorder_note'];
            }

            return result;
        }
    });
});
