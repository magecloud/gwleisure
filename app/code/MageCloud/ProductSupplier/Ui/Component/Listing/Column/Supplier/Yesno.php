<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Ui\Component\Listing\Column\Supplier;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Status
 * @package MageCloud\ProductSupplier\Ui\Component\Listing\Column\Supplier
 */
class Yesno extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item['status'] = $this->decorateOption((int)$item['status']);
            }
        }
        return $dataSource;
    }

    /**
     * @param int $option
     * @return string
     */
    private function decorateOption(int $option): string
    {
        $decoratorClassPath = 'minor';
        $title = __('Disabled');
        if ($option) {
            $decoratorClassPath = 'notice';
            $title = __('Enabled');
        }
        return '<span class="grid-severity-' . $decoratorClassPath .'"><span>' . __($title) . '</span></span>';
    }
}