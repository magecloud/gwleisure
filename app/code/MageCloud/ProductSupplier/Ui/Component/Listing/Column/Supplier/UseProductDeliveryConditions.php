<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
declare(strict_types=1);

namespace MageCloud\ProductSupplier\Ui\Component\Listing\Column\Supplier;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class UseProductDeliveryConditions
 * @package MageCloud\ProductSupplier\Ui\Component\Listing\Column\Supplier
 */
class UseProductDeliveryConditions extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item['use_product_delivery_conditions'] = $this->decorateOption((int)$item['use_product_delivery_conditions']);
            }
        }
        return $dataSource;
    }

    /**
     * @param int $option
     * @return string
     */
    private function decorateOption(int $option): string
    {
        return $option ? (string)__('Yes') : (string)__('No');
    }
}