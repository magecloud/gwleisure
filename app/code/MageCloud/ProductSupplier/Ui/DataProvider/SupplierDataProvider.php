<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_ProductSupplier
 */
namespace MageCloud\ProductSupplier\Ui\DataProvider;

use MageCloud\ProductSupplier\Model\RegistryConstants;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use MageCloud\ProductSupplier\Model\ResourceModel\Supplier\Collection;
use MageCloud\ProductSupplier\Model\ResourceModel\Supplier\CollectionFactory;
use MageCloud\ProductSupplier\Model\Supplier;
use Magento\Framework\Registry;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class SupplierDataProvider
 * @package MageCloud\ProductSupplier\Ui\DataProvider
 */
class SupplierDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var SerializerInterface
     */
    private $json;

    /**
     * Initialize dependencies.
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param Registry $registry
     * @param SerializerInterface $json
     * @param array $meta
     * @param array $data
     * @param DataPersistorInterface|null $dataPersistor
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        Registry $registry,
        SerializerInterface $json,
        array $meta = [],
        array $data = [],
        DataPersistorInterface $dataPersistor = null
    ) {
        $this->collection = $collectionFactory->create();
        $this->coreRegistry = $registry;
        $this->json = $json;
        $this->dataPersistor = $dataPersistor ?? ObjectManager::getInstance()->get(
            DataPersistorInterface::class
        );
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var Supplier $message */
        foreach ($items as $item) {
            $item->load($item->getId());
            if ($mappedFields = $item->getMappedFields()) {
                if (is_string($mappedFields)) {
                    $mappedFields = $this->json->unserialize($mappedFields);
                }
                $item->setMappedFields($mappedFields);
            }
            $this->loadedData[$item->getId()] = $item->getData();
        }
        $data = $this->dataPersistor->get(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER);
        if (!empty($data)) {
            $item = $this->collection->getNewEmptyItem();
            $item->setData($data);
            $this->loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear(RegistryConstants::CURRENT_PRODUCT_SUPPLIERS_SUPPLIER);
        }

        return $this->loadedData;
    }
}