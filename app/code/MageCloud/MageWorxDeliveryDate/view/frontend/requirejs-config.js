/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_MageWorxDeliveryDate
 */

var config = {
    "map": {
        "*": {
            "MageWorx_DeliveryDate/js/checkout/mixin/select-billing-address-mixin": "MageCloud_MageWorxDeliveryDate/js/checkout/mixin/select-billing-address-mixin",
            "MageWorx_DeliveryDate/js/checkout/mixin/set-shipping-information-mixin": "MageCloud_MageWorxDeliveryDate/js/checkout/mixin/set-shipping-information-mixin"
        }
    }
};
