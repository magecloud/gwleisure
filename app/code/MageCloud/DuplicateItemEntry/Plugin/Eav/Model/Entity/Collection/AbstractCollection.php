<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_DuplicateItemEntry
 */
namespace MageCloud\DuplicateItemEntry\Plugin\Eav\Model\Entity\Collection;

use Magento\Eav\Model\Entity\Collection\AbstractCollection as DefaultAbstractCollection;
use Magento\Framework\DataObject;

/**
 * Class AbstractCollection
 * @package MageCloud\DuplicateItemEntry\Plugin\Eav\Model\Entity\Collection
 */
class AbstractCollection
{
    /**
     * @param DefaultAbstractCollection $subject
     * @param \Closure $process
     * @param DataObject $item
     * @return $this|mixed
     */
    public function aroundAddItem(
        DefaultAbstractCollection $subject,
        \Closure $process,
        DataObject $item
    ) {
        try {
            return $process($item);
        } catch ( \Exception $e) {
            return $this;
        }
    }
}