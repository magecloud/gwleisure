<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_DuplicateItemEntry
 */
namespace MageCloud\DuplicateItemEntry\Plugin\Framework\Data;

use Magento\Framework\Data\Collection as DefaultCollection;
use Magento\Framework\DataObject;

/**
 * Class AbstractCollection
 * @package MageCloud\DuplicateItemEntry\Plugin\Eav\Model\Entity\Collection
 */
class Collection
{
    /**
     * @param DefaultCollection $subject
     * @param \Closure $process
     * @param DataObject $item
     * @return $this|mixed
     */
    public function aroundAddItem(
        DefaultCollection $subject,
        \Closure $process,
        DataObject $item
    ) {
        try {
            return $process($item);
        } catch ( \Exception $e) {
            return $this;
        }
    }
}