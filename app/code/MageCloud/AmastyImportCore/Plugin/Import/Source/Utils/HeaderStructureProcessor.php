<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_AmastyImportCore
 */
declare(strict_types=1);

namespace MageCloud\AmastyImportCore\Plugin\Import\Source\Utils;

use Amasty\ImportCore\Api\Source\SourceDataStructureInterface;
use Amasty\ImportCore\Import\Source\Utils\HeaderStructureProcessor as DefaultHeaderStructureProcessor;

/**
 * Class HeaderStructureProcessor
 * @package MageCloud\AmastyImportCore\Plugin\Import\Source\Utils
 */
class HeaderStructureProcessor
{
    const CATALOG_PRODUCT_ATTRIBUTE_ENTITY_TYPE = 'catalog_product_attribute';

    /**
     * @param DefaultHeaderStructureProcessor $subject
     * @param $result
     * @param SourceDataStructureInterface $dataStructure
     * @param array $headerRow
     * @param string $prefixDelimiter
     * @return mixed
     */
    public function afterGetHeaderStructure(
        DefaultHeaderStructureProcessor $subject,
        $result,
        SourceDataStructureInterface $dataStructure,
        array $headerRow,
        string $prefixDelimiter
    ) {
        $this->validateHeaderStructure($result, $headerRow, $prefixDelimiter);
        return $result;
    }

    /**
     * @param array $result
     * @param array $headerRow
     * @param string $prefixDelimiter
     * @return void
     */
    private function validateHeaderStructure(array &$result, array $headerRow, string $prefixDelimiter): void
    {
        if (empty($prefixDelimiter)) {
            $prefixDelimiter = DefaultHeaderStructureProcessor::VIRTUAL_PREFIX_DELIMITER;
        }

        foreach ($result as $key => &$data) {
            if ($key !== self::CATALOG_PRODUCT_ATTRIBUTE_ENTITY_TYPE) {
                // don't process other mapping fields except product attributes
                continue;
            }

            if (!is_array($data) && is_object($data)) {
                // don't process fields that don't meet the requirements
                continue;
            }

            foreach ($headerRow as $headerValue) {
                if (!strpos($headerValue, $prefixDelimiter)) {
                    /**
                     * Omit default (root) fields without entity type specified
                     * Example syntax of the default field types:
                     * - entity_id;
                     * - sku.
                     *
                     * Example syntax of the product attributes fields:
                     * - catalog_product_attribute{$prefixDelimiter}price;
                     * - catalog_product_attribute{$prefixDelimiter}name;
                     */
                    continue;
                }

                $headerValueData = explode($prefixDelimiter, $headerValue, 2);
                if (!isset($headerValueData[0]) || !isset($headerValueData[1])) {
                    continue;
                }

                $entityType = (string)$headerValueData[0]; // entity type (ex. catalog_product_attribute)
                $field = (string)$headerValueData[1]; // mapped field (ex. store_id)



                if (($key == $entityType) && !array_key_exists($field, $result[$entityType])) {
                    // add missing header value
                    $data[$field] = '';
                }
            }
        }
    }
}