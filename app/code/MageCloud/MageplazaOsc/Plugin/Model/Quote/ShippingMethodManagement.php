<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_MageplazaOsc
 */
declare(strict_types=1);

namespace MageCloud\MageplazaOsc\Plugin\Model\Quote;

use Closure;
use Exception;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\EstimateAddressInterface;
use Magento\Quote\Model\ShippingMethodManagement as QuoteShippingMethodManagement;
use Magento\Customer\Api\Data\AddressInterface as CustomerAddressInterface;
use Magento\Quote\Model\Quote;

/**
 * Class ShippingMethodManagement
 * @package MageCloud\MageplazaOsc\Plugin\Model\Quote
 */
class ShippingMethodManagement
{
    /**
     * Quote repository.
     *
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * Customer Address repository
     *
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @param CartRepositoryInterface $quoteRepository
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        AddressRepositoryInterface $addressRepository
    ) {
        $this->quoteRepository   = $quoteRepository;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param QuoteShippingMethodManagement $subject
     * @param Closure $proceed
     * @param $cartId
     * @param EstimateAddressInterface $address
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function aroundEstimateByAddress(
        QuoteShippingMethodManagement $subject,
        Closure $proceed,
                                      $cartId,
        EstimateAddressInterface $address
    ) {
        $this->saveAddress($cartId, $address);

        return $proceed($cartId, $address);
    }

    /**
     * @param QuoteShippingMethodManagement $subject
     * @param Closure $proceed
     * @param $cartId
     * @param AddressInterface $address
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function aroundEstimateByExtendedAddress(
        QuoteShippingMethodManagement $subject,
        Closure $proceed,
        $cartId,
        AddressInterface $address
    ) {
        foreach ($address->getCustomAttributes() as $attribute) {
            if (isset($attribute->getValue()['value'])) {
                $attribute->setValue($attribute->getValue()['value']);
            }
        }

        $this->saveAddress($cartId, $address);

        return $proceed($cartId, $address);
    }

    /**
     * @param QuoteShippingMethodManagement $subject
     * @param Closure $proceed
     * @param $cartId
     * @param $addressId
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function aroundEstimateByAddressId(
        QuoteShippingMethodManagement $subject,
        Closure $proceed,
        $cartId,
        $addressId
    ) {
        $address = $this->addressRepository->getById($addressId);
        $this->saveAddress($cartId, $address);

        return $proceed($cartId, $addressId);
    }

    /**
     * @param $cartId
     * @param $address
     * @return $this
     * @throws NoSuchEntityException
     */
    private function saveAddress($cartId, $address)
    {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);

        if (!$quote->isVirtual()) {
            $addressData = [
                AddressInterface::KEY_COUNTRY_ID      => $address->getCountryId(),
                AddressInterface::KEY_POSTCODE        => $address->getPostcode(),
                AddressInterface::KEY_REGION_ID       => $address->getRegionId(),
                AddressInterface::KEY_STREET          => $address->getStreet(),
                AddressInterface::KEY_CITY            => $address->getCity(),
                AddressInterface::CUSTOMER_ADDRESS_ID => $address->getId()
            ];

            $shippingAddress = $quote->getShippingAddress();
            if (method_exists($address, 'getData')) {
                $customFieldData = [
                    'mposc_field_1' => $address->getData('mposc_field_1'),
                    'mposc_field_2' => $address->getData('mposc_field_2'),
                    'mposc_field_3' => $address->getData('mposc_field_3')
                ];
                foreach ($customFieldData as $key => $value) {
                    $shippingAddress->setCustomAttribute($key, $value);
                }
            }

            try {
                $shippingAddress->addData($addressData)
                    ->save();
                $this->quoteRepository->save($quote);
            } catch (Exception $e) {
                return $this;
            }
        }

        return $this;
    }
}