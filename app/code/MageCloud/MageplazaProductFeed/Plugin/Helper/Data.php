<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_MageplazaProductFeed
 */
namespace MageCloud\MageplazaProductFeed\Plugin\Helper;

use Magento\Framework\UrlInterface as StoreUrlInterface;
use Mageplaza\ProductFeed\Helper\Data as DefaultHelperData;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as ProductAttributeCollectionFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Locale\Resolver;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;
use Mageplaza\ProductFeed\Model\Feed;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Url;
use Magento\Framework\App\State as AppState;
use Magento\Framework\App\Area;
use Magento\Bundle\Model\Product\Type as BundleType;

/**
 * Class Data
 * @package MageCloud\MageplazaProductFeed\Plugin\Helper
 */
class Data
{
    const BACKORDERS_PRE_ORDER_OPTION_VALUE = 101;

    const PREORDER_ALLOW_QTY_ATTRIBUTE = 'amasty_preorder_release_date';

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var ProductAttributeCollectionFactory
     */
    private $productAttributeCollectionFactory;

    /**
     * @var StockRegistryInterface
     */
    private $stockState;

    /**
     * @type StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Resolver
     */
    private $resolver;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var AppState
     */
    private $appState;

    /**
     * @param ProductFactory $productFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param ProductAttributeCollectionFactory $productAttributeCollectionFactory
     * @param StockRegistryInterface $stockState
     * @param StoreManagerInterface $storeManager
     * @param Resolver $resolver
     * @param ScopeConfigInterface $scopeConfig
     * @param ProductRepository $productRepository
     * @param AppState $appState
     */
    public function __construct(
        ProductFactory $productFactory,
        ProductCollectionFactory $productCollectionFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        ProductAttributeCollectionFactory $productAttributeCollectionFactory,
        StockRegistryInterface $stockState,
        StoreManagerInterface $storeManager,
        Resolver $resolver,
        ScopeConfigInterface $scopeConfig,
        ProductRepository $productRepository,
        AppState $appState
    ) {
        $this->productFactory = $productFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->productAttributeCollectionFactory = $productAttributeCollectionFactory;
        $this->stockState = $stockState;
        $this->storeManager = $storeManager;
        $this->resolver = $resolver;
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->appState = $appState;
    }

    /**
     * Try to set area code in case if it was not set before
     *
     * @return $this
     */
    private function initAreaCode()
    {
        try {
            $this->appState->setAreaCode(Area::AREA_GLOBAL);
        } catch (LocalizedException $e) {
            // area code already set
        }
        return $this;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @return null
     */
    private function getAvailabilityDate(
        Product $product,
        \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
    ) {
        try {
            $product = $this->productFactory->create()->load($product->getId());
        } catch (\Exception $e) {
            return null;
        }

        if (
            ($stockItem->getQty() <= 0)
            && ($stockItem->getBackorders() == self::BACKORDERS_PRE_ORDER_OPTION_VALUE)
            && ($releaseDate = $product->getData(self::PREORDER_ALLOW_QTY_ATTRIBUTE))
        ) {
            return date('Y-m-d', strtotime($releaseDate));
        }
        return null;
    }

    /**
     * @param $subject
     * @param $product
     * @param $storeId
     * @return array|mixed|string|string[]
     * @throws NoSuchEntityException
     */
    private function getProductLink($subject, $product, $storeId)
    {
        $productLink = $subject->getProductUrl($product, $storeId);
        $storeBaseUrl = $this->storeManager->getStore($storeId)->getBaseUrl(StoreUrlInterface::URL_TYPE_LINK);
        if (!$productLink || ($productLink == $storeBaseUrl)) {
            $productLink = $product->getProductUrl();
        }
        if ($productLink) {
            $productLink = preg_replace('/\\?.*/', '', $productLink);
        }

        return $productLink;
    }

    /**
     * @param DefaultHelperData $subject
     * @param \Closure $proceed
     * @param Feed $feed
     * @param array $productAttributes
     * @param array $productIds
     * @return array
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function aroundGetProductsData(
        DefaultHelperData $subject,
        \Closure $proceed,
        $feed,
        $productAttributes = [],
        $productIds = []
    ) {
        $this->initAreaCode();

        $categoryMap = $subject->unserialize($feed->getCategoryMap());
        $storeId = $feed->getStoreId() ?: $this->storeManager->getDefaultStoreView()->getId();

        $allCategory = $this->categoryCollectionFactory->create();
        $allCategory->setStoreId($storeId)->addAttributeToSelect('name');
        $categoriesName = [];
        /** @var $item Category */
        foreach ($allCategory as $item) {
            $categoriesName[$item->getId()] = $item->getName();
        }

        $allSelectProductAttributes = $this->productAttributeCollectionFactory->create()
            ->addFieldToFilter('frontend_input', ['in' => ['multiselect', 'select']])
            ->getColumnValues('attribute_code');

        if (!empty($productAttributes)) {
            $productAttributes = array_merge($productAttributes, [self::PREORDER_ALLOW_QTY_ATTRIBUTE]);
        }

        $matchingProductIds = !empty($productIds) ? $productIds : $feed->getMatchingProductIds();
        $productCollection = $this->productFactory->create()->getCollection()
            ->addAttributeToSelect($productAttributes)->addStoreFilter($storeId)
            ->addFieldToFilter('entity_id', ['in' => $matchingProductIds])->addMediaGalleryData();

        $result = [];
        /** @var $product Product */
        foreach ($productCollection as $product) {
            $typeInstance = $product->getTypeInstance();
            $childProductCollection = $typeInstance->getAssociatedProducts($product);
            if ($childProductCollection) {
                $associatedData = [];
                foreach ($childProductCollection as $item) {
                    $associatedData = $item->getData();
                }
                $product->setAssociatedProducts($associatedData);
            } else {
                $product->setAssociatedProducts([]);
            }

            $stockItem = $this->stockState->getStockItem(
                $product->getId(),
                $feed->getStoreId()
            );
            $qty = $stockItem->getQty();
            $categories = $product->getCategoryCollection()->addAttributeToSelect('*');
            $relatedProducts = [];
            foreach ($product->getRelatedProducts() as $item) {
                $relatedProducts[] = $item->getData();
            }
            $crossSellProducts = [];
            foreach ($product->getCrossSellProducts() as $item) {
                $crossSellProducts[] = $item->getData();
            }
            $upSellProducts = [];
            foreach ($product->getUpSellProducts() as $item) {
                $upSellProducts[] = $item->getData();
            }
            try {
                $oriProduct = $this->productRepository->getById($product->getId(), false, $storeId);
            } catch (\Exception $e) {
                $oriProduct = $this->productFactory->create()->setStoreId($storeId)->load($product->getId());
            }

            $finalPrice = $subject->convertPrice($oriProduct->getFinalPrice(), $storeId);
            if (!$finalPrice && $oriProduct->getTypeId() == BundleType::TYPE_CODE) {
                $regularPrice = $oriProduct->getPriceInfo()->getPrice('regular_price')->getMinimalPrice()->getValue();
                $specialPrice = $oriProduct->getPriceInfo()->getPrice('final_price')->getMinimalPrice()->getValue();
                $finalPrice = ($specialPrice && ($specialPrice < $regularPrice)) ? $specialPrice : $regularPrice;
                $finalPrice = $subject->convertPrice($finalPrice, $storeId);
            }
            $productLink = $this->getProductLink($subject, $product, $storeId);
            $imageLink = $oriProduct->getImage() ? $this->storeManager->getStore($storeId)
                    ->getBaseUrl(Url::URL_TYPE_MEDIA)
                . 'catalog/product' . $oriProduct->getImage() : '';
            $images = $oriProduct->getMediaGalleryImages()->getSize() ?
                $oriProduct->getMediaGalleryImages() : [[]];
            if (is_object($images)) {
                $imagesData = [];
                foreach ($images->getItems() as $item) {
                    $imagesData[] = $item->getData();
                }
                $images = $imagesData;
            }

            /** @var $category Category */
            $lv = 0;
            $categoryPath = '';
            $cat = new DataObject();
            $categoriesData = [];
            foreach ($categories as $category) {
                if ($lv < $category->getLevel()) {
                    $lv = $category->getLevel();
                    $cat = $category;
                }
            }

            $mapping = '';
            if (isset($categoryMap[$cat->getId()])) {
                $mapping = $categoryMap[$cat->getId()];
            }
            $catPaths = array_reverse(explode(',', $cat->getPathInStore()));
            foreach ($catPaths as $index => $catId) {
                if ($index === (count($catPaths) - 1)) {
                    $categoryPath .= isset($categoriesName[$catId]) ? $categoriesName[$catId] : '';
                } else {
                    $categoryPath .= (isset($categoriesName[$catId]) ? $categoriesName[$catId] : '') . ' > ';
                }
            }

            $oriProduct->isAvailable() ? $product->setData('quantity_and_stock_status', 'in stock')
                : $product->setData('quantity_and_stock_status', 'out of stock');

            $noneAttr = [
                'categoryCollection',
                'relatedProducts',
                'crossSellProducts',
                'upSellProducts',
                'final_price',
                'link',
                'image_link',
                'images',
                'category_path',
                'mapping',
                'qty',
            ];

            // Convert attribute value to attribute text
            foreach ($productAttributes as $attributeCode) {
                try {
                    if ($attributeCode === 'quantity_and_stock_status'
                        || in_array($attributeCode, $noneAttr, true)
                        || !in_array($attributeCode, $allSelectProductAttributes, true)
                        || !$product->getData($attributeCode)
                    ) {
                        continue;
                    }
                    $attributeText = $product->getResource()->getAttribute($attributeCode)
                        ->setStoreId($feed->getStoreId())->getFrontend()->getValue($product);
                    if (is_array($attributeText)) {
                        $attributeText = implode(',', $attributeText);
                    }
                    if ($attributeText) {
                        $product->setData($attributeCode, $attributeText);
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }

            $product->setData('categoryCollection', $categoriesData);
            $product->setData('relatedProducts', $relatedProducts);
            $product->setData('crossSellProducts', $crossSellProducts);
            $product->setData('upSellProducts', $upSellProducts);
            $product->setData('final_price', $finalPrice);
            $product->setData('link', $productLink);
            $product->setData('image_link', $imageLink);
            $product->setData('images', $images);
            $product->setData('category_path', $categoryPath);
            $product->setData('mapping', $mapping);
            $product->setData('qty', $qty);
            $product->setData('availability_date', $this->getAvailabilityDate($product, $stockItem));
			$product->setData(
                'short_description',
                preg_replace('/[^(\x20-\x7F)]*/','', $product->getData('short_description') ?? '')
            );
		
            $result[] = $subject::jsonDecode($subject::jsonEncode($product->getData()));
            if ($subject->useGoogleShoppingApi($feed)) {
                $subject->syncProductToGoogleShopping($feed, $product);
            }
        }

        return $result;
    }
}
