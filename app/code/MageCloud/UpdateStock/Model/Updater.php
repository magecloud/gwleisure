<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_UpdateStock
 */
declare(strict_types=1);

namespace MageCloud\UpdateStock\Model;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Action;
use Magento\Framework\DataObject;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use MageCloud\UpdateStock\Helper\Data as HelperData;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Class Updater
 * @package MageCloud\UpdateStock\Model
 */
class Updater extends DataObject
{
    const DEFAULT_BATCH_SIZE = 200;
    const BACKORDERS_ALLOW_QTY_BELOW_ZERO_OPTION_VALUE = 1;
    const BACKORDERS_PRE_ORDER_OPTION_VALUE = 101;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepository;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var TypeListInterface
     */
    private $cacheTypeList;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var string
     */
    private $availabilityAttribute;

    /**
     * @var string
     */
    private $preorderAllowQtyAttribute;

    /**
     * @var null
     */
    private $storeId = null;

    /**
     * @var array
     */
    private $entityIds = [];

    /**
     * @var null|ProgressBar
     */
    private $progress = null;

    /**
     * @var array
     */
    private $availabilityAttributeOptions = [];

    /**
     * @var array
     */
    private $storeIds = [];

    /**
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param StockRegistryInterface $stockRegistry
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     * @param Action $productAction
     * @param TypeListInterface $cacheTypeList
     * @param StoreManagerInterface $storeManager
     * @param HelperData $helperData
     * @param string $availabilityAttribute
     * @param string $preorderAllowQtyAttribute
     * @param string|null $storeId
     * @param array $entityIds
     * @param $progress
     * @param array $data
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        StockRegistryInterface $stockRegistry,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        Action $productAction,
        TypeListInterface $cacheTypeList,
        StoreManagerInterface $storeManager,
        HelperData $helperData,
        string $availabilityAttribute,
        string $preorderAllowQtyAttribute,
        string $storeId = null,
        array $entityIds = [],
        $progress = null,
        array $data = []
    ) {
        parent::__construct($data);
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->stockRegistry = $stockRegistry;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productAction = $productAction;
        $this->cacheTypeList = $cacheTypeList;
        $this->storeManager = $storeManager;
        $this->helperData = $helperData;
        $this->availabilityAttribute = $availabilityAttribute;
        $this->preorderAllowQtyAttribute = $preorderAllowQtyAttribute;
        $this->storeId = $storeId;
        $this->entityIds = $entityIds;
        $this->progress = $progress;
    }

    /**
     * @return int|string|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getStoreId()
    {
        return $this->storeId ?? null;
    }

    /**
     * @return array
     */
    private function getStoreIds()
    {
        if (empty($this->storeIds)) {
            foreach ($this->storeManager->getStores() as $store) {
                $this->storeIds[] = $store->getId();
            }
        }
        return $this->storeIds;
    }

    /**
     * Adds stock filtering for collection
     *
     * @param $collection
     * @param bool $inStock
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function addStockFilterToCollection($collection, $inStock = true): void
    {
        $stockStatus = $inStock ? 1 : 0;
        $manageStock = $this->helperData->isProductManageStock($this->getStoreId());
        $cond = [
            '{{table}}.use_config_manage_stock = 0 AND {{table}}.manage_stock=1 AND {{table}}.is_in_stock=' . $stockStatus,
            '{{table}}.use_config_manage_stock = 0 AND {{table}}.manage_stock=0'
        ];

        if ($manageStock) {
            $cond[] = '{{table}}.use_config_manage_stock = 1 AND {{table}}.is_in_stock=' . $stockStatus;
        } else {
            $cond[] = '{{table}}.use_config_manage_stock = 1';
        }

        $collection->joinField(
            'inventory_in_stock',
            'cataloginventory_stock_item',
            'is_in_stock',
            'product_id=entity_id',
            '(' . join(') OR (', $cond) . ')'
        );
    }

    /**
     * @return array
     */
    private function getAvailabilityAttributeOptions()
    {
        if (empty($this->availabilityAttributeOptions)) {
            try {
                /** @var \Magento\Catalog\Api\Data\ProductAttributeInterface $attribute */
                $attribute = $this->productAttributeRepository->get($this->availabilityAttribute);
                foreach ($attribute->getOptions() as $option) {
                    $this->availabilityAttributeOptions[(int)$option->getValue()] = trim((string)$option->getLabel());
                }
            } catch (\Exception $e) {
                // omit exception
            }
        }
        return $this->availabilityAttributeOptions;
    }

    /**
     * @param $label
     * @return int|string|null
     */
    private function getAvailabilityOptionValueByLabel($label)
    {
        foreach ($this->getAvailabilityAttributeOptions() as $optionValue => $optionLabel) {
            if ($optionValue && (trim($label) == trim($optionLabel))) {
                return $optionValue;
            }
        }
        return null;
    }

    /**
     * @param array $fields
     * @param array $filters
     * @param $storeId
     * @return ProductCollection
     */
    private function prepareCollection(array $fields, array $filters = [], $storeId = null): ProductCollection
    {
        /** @var ProductCollection $collection */
        $collection = $this->productCollectionFactory->create();
        if (!empty($this->entityIds)) {
            $collection->addIdFilter($this->entityIds);
        }
        if ($storeId) {
            $collection->addStoreFilter($this->storeId);
        }
        $collection->addAttributeToSelect(
            [
                ProductInterface::TYPE_ID,
                ProductInterface::STATUS,
                $this->preorderAllowQtyAttribute
            ]
        )
        ->addAttributeToFilter(ProductInterface::STATUS, Status::STATUS_ENABLED)
        ->addAttributeToFilter(ProductInterface::TYPE_ID, ['eq' => Type::TYPE_SIMPLE]); // only simple manage stock

        foreach ($fields as $field) {
            $collection->addAttributeToSelect($field);
        }
        foreach ($filters as $attribute => $condition) {
            $collection->addAttributeToFilter($attribute, $condition);
        }

        return $collection;
    }

    /**
     * @param Product $product
     * @return int
     */
    private function initAvailabilityOptionValue(Product $product)
    {
        try {
            /** @var StockItemInterface $stockItem */
            $stockItem = $this->stockRegistry->getStockItem($product->getId(), $this->getStoreId());
        } catch (\Exception $e) {
            return false;
        }

        // detect the needed option value by option label. available another way?
        $value = $this->getAvailabilityOptionValueByLabel('In-Stock');
        $backorders = $stockItem->getBackorders();

        if ($stockItem->getQty() <= 0) {
            $value = $this->getAvailabilityOptionValueByLabel('Out-of-Stock');

            if ($backorders == self::BACKORDERS_PRE_ORDER_OPTION_VALUE) {
                $value = $this->getAvailabilityOptionValueByLabel('Pre-Order');
            } else if ($backorders == self::BACKORDERS_ALLOW_QTY_BELOW_ZERO_OPTION_VALUE) {
                $value = $this->getAvailabilityOptionValueByLabel('In-Stock');
            }
        }

        return (int)$value;
    }

    /**
     * @param ProductCollection $collection
     * @param array $storeIds
     * @return void
     */
    private function processing(ProductCollection $collection, array $storeIds = []): void
    {
        if ($this->progress) {
            $this->progress->setMaxSteps($collection->getSize());
        }
        if (empty($storeIds)) {
            $storeIds = $this->getStoreIds();
        }

        $collection->setPageSize(self::DEFAULT_BATCH_SIZE);
        $pageCount = $collection->getLastPageNumber();
        $currentPage = 1;
        while ($currentPage <= $pageCount) {
            $collection->setCurPage($currentPage);
            if ($this->progress) {
                $this->progress->setMessage(
                    sprintf('Processing batch %d (batch size %d)', $currentPage, self::DEFAULT_BATCH_SIZE)
                    
                );
            }
            foreach ($collection as $product) {
                /** @var Product $product */
                try {
                    if ($value = $this->initAvailabilityOptionValue($product)) {
                        $productId = $product->getId();

                        foreach ($storeIds as $storeId) {
                            $this->productAction->updateAttributes(
                                [$productId],
                                [$this->availabilityAttribute => $value],
                                $storeId
                            );
                        }
                    }
                } catch (\Exception $e) {
                    // catch exception
                }
                if ($this->progress) {
                    $this->progress->advance();
                }
            }
            $collection->clear();
            $currentPage++;
        }
        if ($this->progress) {
            $this->progress->finish();
        }
    }

    /**
     * @return $this
     */
    private function clearConfigurationCache()
    {
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        return $this;
    }

    /**
     * @param $store
     * @return $this
     */
    private function setLastUpdateExecutionDate($store = null)
    {
        $this->helperData->setLastUpdateExecutionDate();
        return $this;
    }

    /**
     * @param $storeId
     * @return $this
     */
    private function postProcessing($storeId = null)
    {
        $this->setLastUpdateExecutionDate($storeId);
        $this->clearConfigurationCache();
        return $this;
    }

    /**
     * @param array $fields
     * @param array $filters
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(array $fields = [], array $filters = []): void
    {
        $storeId = $this->getStoreId();
        if (!$this->helperData->isEnabledProductStockUpdate($storeId)) {
            return;
        }
        $collection = $this->prepareCollection($fields, $filters, $storeId);
        $this->processing($collection, $storeId ? [$storeId] : []);
        $this->postProcessing($storeId);
    }
}