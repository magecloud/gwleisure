<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_UpdateStock
 */
declare(strict_types=1);

namespace MageCloud\UpdateStock\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State as AppState;
use Magento\Framework\App\Area;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\SimpleDataObjectConverter;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\ProgressBarFactory;

/**
 * Class AbstractCommand
 * @package MageCloud\UpdateStock\Console\Command
 */
abstract class AbstractCommand extends Command
{
    /**#@+
     * Constant for commands
     */
    const STORE_INPUT_OPTION_KEY = 'store';
    /**#@-*/

    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProgressBarFactory
     */
    private $progressBarFactory;

    /**
     * @var null
     */
    private $timerStart = null;

    /**
     * @var null
     */
    private $progressBar = null;

    /**
     * AbstractCommand constructor.
     * @param AppState $appState
     * @param StoreManagerInterface $storeManager
     * @param ProgressBarFactory $progressBarFactory
     */
    public function __construct(
        AppState $appState,
        StoreManagerInterface $storeManager,
        ProgressBarFactory $progressBarFactory
    ) {
        $this->appState = $appState;
        $this->storeManager = $storeManager;
        $this->progressBarFactory = $progressBarFactory;
        parent::__construct();
    }

    /**
     * Try to set area code in case if it was not set before
     *
     * @return $this
     */
    protected function initAreaCode()
    {
        try {
            $this->appState->setAreaCode(Area::AREA_GLOBAL);
        } catch (LocalizedException $e) {
            // area code already set
        }
        return $this;
    }

    /**
     * @param $start
     * @return $this
     */
    public function setTimerStart($start)
    {
        $this->timerStart = $start;
        return $this;
    }

    /**
     * @return |null
     */
    public function getTimerStart()
    {
        return $this->timerStart;
    }

    /**
     * @return $this
     */
    public function resetTimer()
    {
        $this->timerStart = null;
        return $this;
    }

    /**
     * @param OutputInterface $output
     * @param array $data
     * @return ProgressBar|null
     */
    private function prepareProgressBar(OutputInterface $output, array $data = [])
    {
        if (null === $this->progressBar) {
            /** @var ProgressBar $progress */
            $progress = $this->progressBarFactory->create(
                [
                    'output' => $output,
                    'max' => count($data)
                ]
            );
            $progress->setFormat(
                "%current%/%max% [%bar%] %percent:3s%% %elapsed% %memory:6s% \t| <info>%message%</info>"
            );
            if ($output->getVerbosity() !== OutputInterface::VERBOSITY_NORMAL) {
                $progress->setOverwrite(false);
            }
            $this->progressBar = $progress;
        }
        return $this->progressBar;
    }

    /**
     * @return ProgressBar|null
     */
    public function getProgressBar()
    {
        return $this->progressBar;
    }

    /**
     * Reset progress bar to initial state
     */
    private function resetProgressBar()
    {
        $this->progressBar = null;
    }

    /**
     * Perform actions before processing specific command
     *
     * @param OutputInterface $output
     * @param null $storeId
     * @param array $data
     * @return $this
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function beforeProcess($output, $storeId = null, array $data = [])
    {
        $this->prepareProgressBar($output, $data);
        $this->setTimerStart(microtime(true));
        $storeName = $this->getStoreNameById($storeId);
        $output->writeln("<info>Performing operations for store with ID {$storeId} ({$storeName}):</info>");
        return $this;
    }

    /**
     * Perform actions after processing specific command
     *
     * @param OutputInterface $output
     * @param null $storeId
     * @param array $data
     * @return $this
     */
    protected function afterProcess($output, $storeId = null, array $data = [])
    {
        $end = microtime(true);
        $workingTime = round($end - $this->getTimerStart(), 2);
        $output->writeln("<info>Operations were performed successfully.</info>");
        $output->writeln("<info>Working time: {$workingTime}</info>");
        $this->resetTimer()
            ->resetProgressBar();
        return $this;
    }

    /**
     * @param $storeCode
     * @param \Magento\Store\Api\Data\StoreInterface[] $stores
     * @return int
     */
    protected function getStoreIdByCode($storeCode, $stores)
    {
        foreach ($stores as $store) {
            if ($store->getCode() == $storeCode) {
                return $store->getId();
            }
        }
        return Store::DEFAULT_STORE_ID;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getDefaultStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * @param string $storeId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getStoreNameById($storeId = null)
    {
        return $this->storeManager->getStore($storeId)->getName();
    }

    /**
     * @param $value
     * @return string
     */
    protected function formatToSnakeCase($value)
    {
        return SimpleDataObjectConverter::camelCaseToSnakeCase($value);
    }

    /**
     * @param $value
     * @return string
     */
    protected function formatToTitle($value)
    {
        return ucwords(str_replace('_', ' ', $this->formatToSnakeCase($value)));
    }
}