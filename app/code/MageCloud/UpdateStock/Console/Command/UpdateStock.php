<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_UpdateStock
 */
declare(strict_types=1);

namespace MageCloud\UpdateStock\Console\Command;

use MageCloud\UpdateStock\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State as AppState;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Helper\ProgressBarFactory;
use Magento\Framework\Exception\LocalizedException;
use MageCloud\UpdateStock\Model\UpdaterFactory;

/**
 * Class UpdateStock
 * @package MageCloud\UpdateStock\Console\Command
 */
class UpdateStock extends AbstractCommand
{
    /**#@+
     * Constant for current command.
     */
    const COMMAND = 'magecloud:inventory:update-stock';
    const ENTITY_IDS_ARGUMENT = 'entity_ids';
    /**#@-*/

    /**
     * @var UpdaterFactory
     */
    private $updaterFactory;

    /**
     * @param AppState $appState
     * @param StoreManagerInterface $storeManager
     * @param ProgressBarFactory $progressBarFactory
     * @param UpdaterFactory $updaterFactory
     */
    public function __construct(
        AppState $appState,
        StoreManagerInterface $storeManager,
        ProgressBarFactory $progressBarFactory,
        UpdaterFactory $updaterFactory
    ) {
        parent::__construct(
            $appState,
            $storeManager,
            $progressBarFactory
        );
        $this->updaterFactory = $updaterFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription('Update product stock attributes.')
            ->addArgument(
                self::ENTITY_IDS_ARGUMENT,
                InputArgument::IS_ARRAY,
                'Affected product IDs'
            )
            ->addOption(
                self::STORE_INPUT_OPTION_KEY,
                's',
                InputOption::VALUE_REQUIRED,
                'Use the specific Store View',
                Store::DEFAULT_STORE_ID
            );

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initAreaCode();
        $storeId = $input->getOption(self::STORE_INPUT_OPTION_KEY);
        if (!$storeId) {
            $storeId = $this->getDefaultStoreId();
        }
        $this->beforeProcess($output, $storeId);

        /** @var \MageCloud\UpdateStock\Model\Updater $updater */
        $updater = $this->updaterFactory->create(
            [
                'entityIds' => (array) $input->getArgument(self::ENTITY_IDS_ARGUMENT),
                'storeId' => (int) $storeId,
                'progress' => $this->getProgressBar()
            ]
        );
        $updater->execute();

        $this->afterProcess($output, $storeId);
    }
}