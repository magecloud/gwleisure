<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_UpdateStock
 */
namespace MageCloud\UpdateStock\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class Data
 * @package MageCloud\UpdateStock\Helper
 */
class Data extends AbstractHelper
{
    /**
     * XML path
     */
    const XML_PRODUCT_STOCK_UPDATE_ENABLED = 'magecloud_update_stock/general/enabled_product_stock_update';
    const XML_PRODUCT_STOCK_UPDATE_LAST_EXECUTION_DATE = 'magecloud_update_stock/general/last_execution_date';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param StoreManagerInterface $storeManager
     * @param ConfigInterface $config
     * @param DateTimeFactory $dateTimeFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        StoreManagerInterface $storeManager,
        ConfigInterface $config,
        DateTimeFactory $dateTimeFactory
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    /**
     * @return string
     */
    private function getCurrentDate()
    {
        $createdAtTime = $this->dateTimeFactory->create('now', new \DateTimeZone('UTC'));
        return $createdAtTime->format(DateTime::DATETIME_PHP_FORMAT);
    }

    /**
     * @param $store
     * @return bool
     */
    public function isEnabledProductStockUpdate($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PRODUCT_STOCK_UPDATE_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param $store
     * @return mixed
     */
    public function isProductManageStock($store = null)
    {
        return $this->scopeConfig->getValue(
            \Magento\CatalogInventory\Model\Configuration::XML_PATH_MANAGE_STOCK,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return bool
     */
    public function getLastUpdateExecutionDate($store = null)
    {
        if ($this->isEnabledProductStockUpdate($store)) {
            return $this->scopeConfig->getValue(
                self::XML_PRODUCT_STOCK_UPDATE_LAST_EXECUTION_DATE,
                ScopeInterface::SCOPE_STORE,
                $store
            );
        }
        return null;
    }

    /**
     * @param null $store
     * @return $this
     */
    public function setLastUpdateExecutionDate($store = null)
    {
        if ($this->isEnabledProductStockUpdate($store)) {
            $this->config->saveConfig(
                self::XML_PRODUCT_STOCK_UPDATE_LAST_EXECUTION_DATE,
                $this->getCurrentDate()
            );
        }
        return $this;
    }
}