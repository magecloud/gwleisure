<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_UpdateStock
 */
declare(strict_types=1);

namespace MageCloud\UpdateStock\Cron;

use MageCloud\UpdateStock\Model\UpdaterFactory;

/**
 * Class UpdateProductStock
 * @package MageCloud\UpdateStock\Cron
 */
class UpdateProductStock
{
    /**
     * @var UpdaterFactory
     */
    private $updaterFactory;

    /**
     * @param UpdaterFactory $updaterFactory
     */
    public function __construct(
        UpdaterFactory $updaterFactory
    ) {
        $this->updaterFactory = $updaterFactory;
    }

    /**
     * Mass products stock attributes update
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(): void
    {
        /** @var \MageCloud\UpdateStock\Model\Updater $updater */
        $updater = $this->updaterFactory->create();
        $updater->execute();
    }
}