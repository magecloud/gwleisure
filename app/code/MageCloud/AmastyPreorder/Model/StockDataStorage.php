<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_AmastyPreorder
 */
declare(strict_types=1);

namespace MageCloud\AmastyPreorder\Model;

use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

/**
 * Class StockDataStorage
 * @package MageCloud\AmastyPreorder\Model
 */
class StockDataStorage
{
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var array
     */
    private $stockData = [];

    /**
     * @param StockRegistryInterface $stockRegistry
     */
    public function __construct(
        StockRegistryInterface $stockRegistry
    ) {
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @param $productId
     * @return StockItemInterface|null
     */
    public function getStockItem($productId): ?StockItemInterface
    {
        $stockItem = null;
        try {
            $stockItem = $this->stockRegistry->getStockItem($productId);
        } catch (\Exception $e) {
            // omit exception
        }
        return $stockItem;
    }

    /**
     * @param int $productId
     * @return void
     */
    public function setStockData(int $productId): void
    {
        if ($stockItem = $this->getStockItem($productId)) {
            $this->stockData[$productId] = $stockItem->getQty();
        }
    }

    /**
     * @return array
     */
    public function getStockData(): array
    {
        return $this->stockData;
    }

    /**
     * @return void
     */
    public function reset(): void
    {
        $this->stockData = [];
    }
}