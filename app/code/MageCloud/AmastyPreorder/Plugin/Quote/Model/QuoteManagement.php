<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_AmastyPreorder
 */
declare(strict_types=1);

namespace MageCloud\AmastyPreorder\Plugin\Quote\Model;

use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteManagement as DefaultQuoteManagement;
use MageCloud\AmastyPreorder\Model\StockDataStorage;

/**
 * Class QuoteManagement
 * @package MageCloud\AmastyPreorder\Plugin\Quote\Model
 */
class QuoteManagement
{
    /**
     * @var StockDataStorage
     */
    private $stockDataStorage;

    /**
     * @param StockDataStorage $stockDataStorage
     */
    public function __construct(
        StockDataStorage $stockDataStorage
    ) {
        $this->stockDataStorage = $stockDataStorage;
    }

    /**
     * @param DefaultQuoteManagement $subject
     * @param Quote $quote
     * @param $orderData
     * @return null
     */
    public function beforeSubmit(
        DefaultQuoteManagement $subject,
        Quote $quote,
        $orderData = []
    ) {
        foreach ($quote->getAllItems() as $item) {
            // save stock data before place order
            $this->stockDataStorage->setStockData((int)$item->getProductId());
        }
        return null;
    }
}