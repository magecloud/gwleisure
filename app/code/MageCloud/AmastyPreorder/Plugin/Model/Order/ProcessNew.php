<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_AmastyPreorder
 */
declare(strict_types=1);

namespace MageCloud\AmastyPreorder\Plugin\Model\Order;

use Amasty\Preorder\Model\Order\Item\GetPreorderInformation as GetPreorderItemInformation;
use Amasty\Preorder\Model\Order\ProcessNew as DefaultProcessNew;
use Amasty\Preorder\Model\Order\ProcessNew\SaveOrderFlagInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use MageCloud\AmastyPreorder\Model\StockDataStorage;
use Magento\Sales\Model\Order\Item;

/**
 * Class ProcessNew
 * @package MageCloud\AmastyPreorder\Plugin\Model\Order
 */
class ProcessNew
{
    /**
     * @var GetPreorderItemInformation
     */
    private $getPreorderInformation;

    /**
     * @var SaveOrderFlagInterface
     */
    private $saveOrderFlag;

    /**
     * @var StockDataStorage
     */
    private $stockDataStorage;

    /**
     * @param GetPreorderItemInformation $getPreorderInformation
     * @param SaveOrderFlagInterface $saveOrderFlag
     * @param StockDataStorage $stockDataStorage
     */
    public function __construct(
        GetPreorderItemInformation $getPreorderInformation,
        SaveOrderFlagInterface $saveOrderFlag,
        StockDataStorage $stockDataStorage
    ) {
        $this->getPreorderInformation = $getPreorderInformation;
        $this->saveOrderFlag = $saveOrderFlag;
        $this->stockDataStorage = $stockDataStorage;
    }

    /**
     * @param DefaultProcessNew $subject
     * @param \Closure $proceed
     * @param OrderInterface $order
     * @return void
     * @throws NoSuchEntityException
     * @throws \Zend_Log_Exception
     */
    public function aroundExecute(
        DefaultProcessNew $subject,
        \Closure $proceed,
        OrderInterface $order
    ): void {
        $orderIsPreorder = false;

        $itemCollection = $order->getItemsCollection();
        foreach ($itemCollection as $item) {
            $orderIsPreorder |= $this->getPreorderInformation->execute($item)->isPreorder();
        }

        if ($orderIsPreorder) {
            $stockDataBeforeSubmit = $this->stockDataStorage->getStockData();
            /** @var Item $item */
            foreach ($itemCollection as $item) {
                $productId = $item->getProductId();
                if (!$stockItem = $this->stockDataStorage->getStockItem((int)$productId)) {
                    continue;
                }

                $currentQty = $stockItem->getQty();
                if (
                    isset($stockDataBeforeSubmit[$productId])
                    && (($stockDataBeforeSubmit[$productId] > 0) && ($currentQty >= 0))
                ) {
                    $orderIsPreorder = false;
                    break;
                }
            }

            $this->stockDataStorage->reset();
            if ($orderIsPreorder) {
                $this->saveOrderFlag->execute($order);
            }
        }
    }
}