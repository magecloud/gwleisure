<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_SeoRatingSnippet
 */
namespace MageCloud\SeoRatingSnippet\ViewModel;

use MageCloud\SeoRatingSnippet\ViewModel\Category as CategoryViewModel;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\Data\Collection as DefaultCollection;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Tax\Api\TaxCalculationInterface;
use Magento\Catalog\Model\Product\Type;

/**
 * Class Offers
 * @package MageCloud\SeoRatingSnippet\ViewModel
 */
class Offers extends CategoryViewModel
{
    const DEFAULT_TAX_RATE = 20;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Current category object
     *
     * @var null
     */
    private $currentCategory = null;

    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * Local cache for product collection
     *
     * @var Collection|null
     */
    private $productCollection = null;

    /**
     * @var TaxCalculationInterface
     */
    private $taxCalculationInterface;

    /**
     * @var string[]
     */
    private $filterableAttributes = [
        'manufacturer',
        'product_category',
        'availability',
        'frame_type',
        'season_type',
        'awning_type'
    ];

    /**
     * Offers constructor.
     * @param Registry $registry
     * @param RequestInterface $request
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $productCollectionFactory
     * @param TaxCalculationInterface $taxCalculationInterface
     */
    public function __construct(
        Registry $registry,
        RequestInterface $request,
        StoreManagerInterface $storeManager,
        CollectionFactory $productCollectionFactory,
        TaxCalculationInterface $taxCalculationInterface
    ) {
        parent::__construct($registry, $request);
        $this->storeManager = $storeManager;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->currentCategory = $this->getCategory();
        $this->taxCalculationInterface = $taxCalculationInterface;
        $this->request = $request;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPriceCurrency(): string
    {
        $result = '';
        if ($this->isCategoryPage()) {
            $result = $this->storeManager->getStore()->getCurrentCurrency()->getCode();
        }
        return $result;
    }

    /**
     * @return false|Category|null
     */
    private function getCurrentCategory()
    {
        return $this->currentCategory;
    }

    /**
     * @param Collection $collection
     * @return void
     */
    private function filterCollectionByAttributes($collection)
    {
        foreach ($this->filterableAttributes as $attributeCode) {
            if ($attributeParamValues = $this->request->getParam($attributeCode, null)) {
                $values = explode(',', $attributeParamValues);
                if (!empty($values)) {
                    $collection->addAttributeToFilter($attributeCode, ['in' => $values]);
                }
            }
        }
    }

    /**
     * Retrieve product collection for current category. Init of needed.
     *
     * @return Collection|null
     */
    public function getProductCollection()
    {
        if (null === $this->productCollection) {
            /** @var Collection $collection */
            $collection = $this->productCollectionFactory->create();
            $this->productCollection = $collection->addFinalPrice()
                ->addCategoryFilter($this->getCurrentCategory())
                ->addAttributeToFilter(ProductInterface::VISIBILITY, Visibility::VISIBILITY_BOTH)
                ->addAttributeToFilter(ProductInterface::STATUS, Status::STATUS_ENABLED);

            $this->filterCollectionByAttributes($this->productCollection);
        }
        return $this->productCollection;
    }

    /**
     * @param bool $includeTax
     * @return array
     */
    public function getPriceRange($includeTax = false)
    {
        $minPrice = $this->getProductCollection()->getMinPrice();
        $maxPrice = $this->getProductCollection()->getMaxPrice();

        if ($includeTax) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->getProductCollection()->getFirstItem();
            $rate = self::DEFAULT_TAX_RATE;
            if ($taxClassId = $product->getTaxClassId()) {
                $rate = $this->taxCalculationInterface->getCalculatedRate($taxClassId);
            }
            $minPrice = $minPrice + (($minPrice * $rate) / 100);
            $maxPrice = $maxPrice + (($maxPrice * $rate) / 100);
        }

        return [
            'min' => $minPrice,
            'max' => $maxPrice
        ];
    }
}
