<?php
/**
 * Copyright (c) 2020. Volodymyr Hryvinskyi.  All rights reserved.
 * @author: <mailto:volodymyr@hryvinskyi.com>
 * @github: <https://github.com/hryvinskyi>
 */

declare(strict_types=1);

namespace MageCloud\SeoRatingSnippet\ViewModel;

use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Class Category
 */
class Category implements ArgumentInterface
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * RatingData constructor.
     *
     * @param Registry $registry
     * @param RequestInterface $request
     */
    public function __construct(Registry $registry, RequestInterface $request)
    {
        $this->registry = $registry;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function isCategoryPage(): bool
    {
        return $this->getCategory() && $this->request->getFullActionName() == 'catalog_category_view';
    }

    /**
     * @return false|CategoryModel
     */
    public function getCategory(): ?CategoryModel
    {
        return $this->registry->registry('current_category');
    }
}
