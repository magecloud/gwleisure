<?php
/**
 * Copyright (c) 2020. Volodymyr Hryvinskyi.  All rights reserved.
 * @author: <mailto:volodymyr@hryvinskyi.com>
 * @github: <https://github.com/hryvinskyi>
 */

declare(strict_types=1);

namespace MageCloud\SeoRatingSnippet\ViewModel;

use MageCloud\SeoRatingSnippet\Model\Config;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Page\Title;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class RatingData
 */
class RatingData implements ArgumentInterface
{
    /**
     * @var Title
     */
    private $pageTitle;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Http
     */
    private $request;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var array
     */
    private $pageTypes;

    /**
     * RatingData constructor.
     * @param Title $pageTitle
     * @param Config $config
     * @param Http $request
     * @param StoreManagerInterface $storeManager
     * @param array $pageTypes
     */
    public function __construct(
        Title $pageTitle,
        Config $config,
        Http $request,
        StoreManagerInterface $storeManager,
        array $pageTypes = []
    ) {
        $this->pageTitle = $pageTitle;
        $this->config = $config;
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->pageTypes = $pageTypes;
    }

    /**
     * @param null $store
     * @return int
     * @throws NoSuchEntityException
     */
    private function getStoreId($store = null)
    {
        return $this->storeManager->getStore($store)->getId();
    }

    /**
     * @return string
     */
    public function getPageTitle(): string
    {
        return (string)$this->pageTitle->getShort();
    }

    /**
     * @param null $storeId
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->config->isEnabled($storeId ?: $this->getStoreId());
    }

    /**
     * @param null $storeId
     * @return string
     * @throws NoSuchEntityException
     */
    public function getValue($storeId = null): string
    {
        return $this->config->getValue($storeId ?: $this->getStoreId());
    }

    /**
     * @param null $storeId
     * @return string
     * @throws NoSuchEntityException
     */
    public function getCount($storeId = null): string
    {
        return $this->config->getCount($storeId ?: $this->getStoreId());
    }

    /**
     * @return bool
     */
    public function isShowRating(): bool
    {
        return in_array($this->request->getFullActionName(), $this->pageTypes);
    }
}