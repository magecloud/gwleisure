<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_MageplazaFaqs
 */
namespace MageCloud\MageplazaFaqs\Plugin;

use Mageplaza\Faqs\Plugin\FaqSchemaMarkup as DefaultFaqSchemaMarkup;
use Mageplaza\Faqs\Helper\Data;

/**
 * Class FaqSchemaMarkup
 * @package MageCloud\MageplazaFaqs\Plugin
 */
class FaqSchemaMarkup
{
    /**
     * @var Data
     */
    private $helperData;

    /**
     * @param Data $helperData
     */
    public function __construct(
        Data $helperData
    ) {
        $this->helperData = $helperData;
    }

    /**
     * @param DefaultFaqSchemaMarkup $subject
     * @param \Closure $proceed
     * @param $listArticle
     * @return mixed|string
     */
    public function aroundAddFaqsSchema(
        DefaultFaqSchemaMarkup $subject,
        \Closure $proceed,
        $listArticle
    ) {
        $maxQuestions = $this->helperData->getSchemaConfig('number_question') ?? 5;
        $listArticle->setOrder('views', 'desc')->setPageSize($maxQuestions)->setCurPage(1);
        if (count($listArticle)) {
            return $proceed($listArticle);
        }
        return '';
    }
}