<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_MageplazaSeoRule
 */
namespace MageCloud\MageplazaSeoRule\Plugin\Helper;

use Mageplaza\SeoRule\Helper\Data as DefaultHelperData;

/**
 * Class Data
 * @package MageCloud\MageplazaSeoRule\Plugin\Helper
 */
class Data
{
    /**
     * @param DefaultHelperData $subject
     * @param $metaTemplate
     * @param $dataAttributeFilter
     * @param bool $isKeyWord
     * @return array
     */
    public function beforeGenerateMetaTemplateForLayer(
        DefaultHelperData $subject,
        $metaTemplate,
        $dataAttributeFilter,
        $isKeyWord = false
    ): array {
        $attributeRandomData = $subject->processRandomOptions($metaTemplate, []);
        $metaTemplate = $subject->replaceDataTemplate($metaTemplate, $attributeRandomData, $isKeyWord);

        return [$metaTemplate, $dataAttributeFilter, $isKeyWord];
    }
}