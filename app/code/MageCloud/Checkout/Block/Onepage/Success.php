<?php
/**
 * @author andy
 * @email andyworkbase@gmail.com
 * @team MageCloud
 * @package MageCloud_Checkout
 */
namespace MageCloud\Checkout\Block\Onepage;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;

/**
 * Class Success
 * @package MageCloud\Checkout\Block\Onepage
 */
class Success extends Template
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @param Context $context
     * @param Session $checkoutSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return Success
     */
    protected function _beforeToHtml()
    {
        $this->prepareBlockData();
        return parent::_beforeToHtml();
    }

    /**
     * Prepares block data
     *
     * @return void
     */
    private function prepareBlockData()
    {
        $order = $this->checkoutSession->getLastRealOrder();
        if (!$incrementId = $order->getIncrementId()) {
            return;
        }

        $this->addData(
            [
                'increment_id' => $incrementId,
                'base_subtotal' => $order->getBaseSubtotal()
            ]
        );
    }
}
