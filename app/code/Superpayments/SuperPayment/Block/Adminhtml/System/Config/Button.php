<?php

declare(strict_types=1);

namespace Superpayments\SuperPayment\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Widget\Button as WidgetButton;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

class Button extends Field
{
    /**
     * @inheritdoc
     * @throws LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $this->setElement($element);
        $url = $this->getUrl('superpayment/system_config/validate');

        return $this->getLayout()->createBlock(WidgetButton::class)
            ->setType('button')
            ->setClass('scalable')
            ->setId('super_payments_validate_button')
            ->setLabel('Validate API Key')
            ->setDataAttribute(['url' => $url])
            ->toHtml();
    }
}
