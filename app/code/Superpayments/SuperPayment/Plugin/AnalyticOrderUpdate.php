<?php

declare(strict_types=1);

namespace Superpayments\SuperPayment\Plugin;

use Magento\Sales\Model\ResourceModel\Order;
use Psr\Log\LoggerInterface;
use Superpayments\SuperPayment\Gateway\Service\OrderCreatedService;
use Superpayments\SuperPayment\Gateway\Service\OrderStatusChangedService;
use Throwable;

class AnalyticOrderUpdate
{
    /** @var OrderCreatedService */
    private $orderCreatedService;

    /** @var OrderStatusChangedService */
    private $orderStatusChangedService;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        OrderCreatedService $orderCreatedService,
        OrderStatusChangedService $orderStatusChangedService,
        LoggerInterface $logger
    ) {
        $this->orderCreatedService = $orderCreatedService;
        $this->orderStatusChangedService = $orderStatusChangedService;
        $this->logger = $logger;
    }

    public function afterSave(Order $subject, $result, $object)
    {
        try {
            $oldStatus = $object->getOrigData('status');
            $status = $object->getData('status');
            $data = [
                'order' => $object,
            ];
            if (empty($status) || $oldStatus == $status) {
                return $result;
            }

            if (empty($oldStatus)) {
                $this->orderCreatedService->execute($data);
            } else {
                $this->orderStatusChangedService->execute($data);
            }
        } catch (Throwable $e) {
            $this->logger->error(
                '[SuperPayments] AnalyticOrderUpdate ' . $e->getMessage() ."\n". $e->getTraceAsString()
            );
        }

        return $result;
    }
}
