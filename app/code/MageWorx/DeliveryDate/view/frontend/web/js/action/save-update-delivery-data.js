define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'MageWorx_DeliveryDate/js/checkout/action/saveShippingInformation',
        'MageWorx_DeliveryDate/js/checkout/action/saveDeliveryDateInformation',
        'underscore',
        'jquery/ui'
    ],
    function (ko, $, Component, saveShippingInformation, saveDeliveryDateInformation, _) {
        "use strict";

        return Component.extend({
            defaults: {
                extraChargeOldValue: '',
                imports: {
                    deliveryDateExtraChargeHasBeenChanged: "${ $.provider }:extraCharge",
                    deliveryDate: "${ $.provider }:delivery_date"
                }
            },

            observableProperties: [
                "deliveryDateExtraChargeHasBeenChanged",
                "extraChargeOldValue",
                "deliveryDate"
            ],

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                this._super();
                this.initSubscribers();

                return this;
            },

            /** @inheritdoc */
            initObservable: function () {
                this._super()
                    .observe(this.observableProperties);

                return this;
            },

            initSubscribers: function () {

                this.deliveryDateExtraChargeHasBeenChanged.subscribe(function(oldValue) {
                    if (oldValue === undefined || oldValue === null) {
                        this.extraChargeOldValue('');
                    } else {
                        this.extraChargeOldValue(oldValue);
                    }
                }, this, "beforeChange");

                this.deliveryDateExtraChargeHasBeenChanged.subscribe(function (value) {
                    if (String(this.extraChargeOldValue()) !== String(value)) {
                        saveShippingInformation();
                    }
                }, this);

                this.deliveryDate.subscribe(function (value) {
                    saveDeliveryDateInformation();
                }, this);
            }
        });
    }
)
