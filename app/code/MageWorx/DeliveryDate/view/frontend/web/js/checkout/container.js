define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'uiRegistry',
        'MageWorx_DeliveryDate/js/checkout/delivery_date_manager',
        'underscore',
        'jquery/ui'
    ],
    function (ko, $, Component, registry, ddManager, _) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'MageWorx_DeliveryDate/checkout/container',
                isVisible: false
            },

            /**
             * Returns path to the template
             * defined for a current display mode.
             *
             * @returns {String} Path to the template.
             */
            getTemplate: function () {
                return this.template;
            },

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                this._super();

                registry.set('deliveryDateContainer', this);

                return this;
            },

            /** @inheritdoc */
            initObservable: function () {
                this._super()
                    .observe('isVisible');

                return this;
            }
        });
    }
);
