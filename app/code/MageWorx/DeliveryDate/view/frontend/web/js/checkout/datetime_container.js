define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'underscore',
        'jquery/ui'
    ],
    function (ko, $, Component, _) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'MageWorx_DeliveryDate/checkout/container/for_datetime',
                isVisible: true,
                hasButtonsClass: false
            },

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                this._super();

                return this;
            },

            /** @inheritdoc */
            initObservable: function () {
                this._super()
                    .observe('isVisible hasButtonsClass');

                if (!_.isEmpty(window.checkoutConfig.mageworx.delivery_date)) {
                    if (window.checkoutConfig.mageworx.delivery_date.time.input_type === 'buttons' ||
                        window.checkoutConfig.mageworx.delivery_date.day.input_type === 'buttons') {
                        this.hasButtonsClass(true);
                    } else {
                        this.hasButtonsClass(false);
                    }
                }

                return this;
            }
        });
    }
);
