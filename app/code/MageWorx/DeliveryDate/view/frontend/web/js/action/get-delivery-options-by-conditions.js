define(
    [
        'jquery',
        'MageWorx_DeliveryDate/js/model/url-builder',
        'mage/storage'
    ],
    function (
        $,
        urlBuilder,
        storage
    ) {
        "use strict";

        return {
            getDeliveryOptions: function (conditions) {
                return storage.post(
                    urlBuilder.createUrl('/delivery-date-limits-by-conditions', {}),
                    JSON.stringify({"deliveryOptionConditions": conditions})
                );
            }
        }
    }
)
