/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Checkout/js/action/set-shipping-information',
    'MageWorx_DeliveryDate/js/checkout/action/reloadShippingMethods',
], function (setShippingInfoAction, reloadShippingMethods) {
    'use strict';

    var timerId;

    return function () {
        // Prevent many saves on each change
        clearTimeout(timerId);
        timerId = setTimeout(function () {
            setShippingInfoAction().done(function (result) {
                reloadShippingMethods();
            });
        }, 200);
    };
});
