"use strict";
define(
    [
        'ko',
        'Magento_Ui/js/form/element/abstract',
        './deliveryDayUtils',
        'uiRegistry',
        'underscore',
        'jquery',
        'mage/translate',
        'MageWorx_DeliveryDate/js/checkout/action/saveShippingInformation',
        'Magento_Checkout/js/model/quote',
        'jquery/ui'
    ],
    function (
        ko,
        Element,
        DeliveryDayUtils,
        registry,
        _,
        $,
        $t,
        saveShippingInformation,
        quote
    ) {
        "use strict";

        return Element.extend({

            defaults: {
                exports: {
                    realDateValue: 'checkout.delivery_date_manager:selectedDay' // parent of parent
                },
                imports: {
                    activeMethodData: 'checkout.delivery_date_manager:activeMethodData',
                    dayLimits: 'checkout.delivery_date_manager:dayLimits',
                },
                defaultValueWasSet: false,
                pickerDateTimeFormat: 'YYYY-MM-DD'
            },

            observableProperties: [
                'realDateValue',
                'dateFormat',
                'activeMethodData',
                'defaultValueWasSet',
                'dayLimits',
                'selectedDay',
                'additionalCharge',
                'additionalChargeMessage',
                'uid'
            ],

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                return this._super();
            },

            initObservable: function () {
                this._super();
                this.observe(this.observableProperties);
                this.dateFormat(window.checkoutConfig.mageworx.delivery_date.day_label_format);

                // Check corresponding radio button when selected day was set
                this.selectedDay.subscribe(function (value) {
                    var intDayIndex = parseInt(value);
                    $('input[type="radio"][name="delivery_day_selector"][value="' + intDayIndex + '"]')
                        .prop('checked', true);
                }, this);

                return this;
            },

            /**
             * Validates itself by it's validation rules using validator object.
             * If validation of a rule did not pass, writes it's message to
             * 'error' observable property.
             *
             * @returns {Object} Validate information.
             */
            validate: function () {
                var data = this.activeMethodData && this.activeMethodData();
                if (_.isEmpty(data) || _.isEmpty(data['day_limits'])) {
                    // Skip validation in case there no delivery option
                    return {
                        valid: true,
                        target: this
                    };
                } else {
                    return this._super();
                }
            },
            
            /**
             * Validate selected delivery date
             *
             * @param date
             */
            validateInput: function validateInput(date) {
                var self = this,
                    index = DeliveryDayUtils.getDayIndexFromToday(date),
                    activeMethodData = this.activeMethodData();
                if (activeMethodData && activeMethodData.length > 0) {
                    var dayLimits = activeMethodData['day_limits'] || [],
                        selectedDay = dayLimits[index] || null,
                        dateUnavailableError = $t('Selected date is not available');
                    if (!selectedDay && this.value() != '') {
                        this.value('');
                        setTimeout(function () {
                            self.error(dateUnavailableError);
                        }, 10);
                    }
                }
            },

            /**
             * Days diff from today
             *
             * @param day
             * @returns {*}
             */
            dayFromToday: function (day) {
                var dayLimits = this.dayLimits(),
                    dateFormatted = dayLimits[day]['date_formatted'];

                return dateFormatted;
            },

            /**
             * Select day
             *
             * @param day
             * @param value
             * @param Event
             * @returns {boolean}
             */
            selectDay: function (day, value, Event) {
                var dateValue = DeliveryDayUtils.createDateObjectFromDayIndexFromToday(day, this.pickerDateTimeFormat);
                this.selectedDay(day);
                this.value(dateValue);
                $('[name="delivery_day_selector"][value="'+day+'"]').prop("checked", true);
                if (dateValue) {
                    this.validateInput(dateValue);
                    if (this.value() && quote.shippingMethod()) {
                        var realDateValue = this.getRealDateValue();
                        this.realDateValue(realDateValue);
                        this.source.delivery_date.real_date_value = realDateValue;
                        saveShippingInformation();
                    }
                }

                return true;
            },

            /**
             * Set default value if needed
             */
            initDefaultValue: function () {
                this.setDefaultValue();
            },

            /**
             * Change value to default one
             */
            setDefaultValue: function () {
                var dayLimits = this.dayLimits() ?
                    this.dayLimits() :
                    [],
                    dayIndex = Object.keys(dayLimits)[0];
                this.selectDay(dayIndex);
            },

            /**
             * Returns date value as a date string (not like an day-index from today)
             *
             * @returns {number}
             */
            getRealDateValue: function () {
                return this.value();
            }
        });
    }
);
