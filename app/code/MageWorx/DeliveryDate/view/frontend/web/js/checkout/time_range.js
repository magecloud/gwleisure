define(
    [
        'ko',
        'Magento_Ui/js/form/element/select',
        'uiRegistry',
        'underscore',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'MageWorx_DeliveryDate/js/checkout/action/saveShippingInformation',
        'jquery/ui'
    ],
    function (
        ko,
        Element,
        registry,
        _,
        $,
        quote,
        saveShippingInformation
    ) {
        "use strict";

        return Element.extend({

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                this._super();

                if (!_.isUndefined(window.checkoutConfig.mageworx.delivery_date.time)) {
                    for (var key in window.checkoutConfig.mageworx.delivery_date.time) {
                        if (window.checkoutConfig.mageworx.delivery_date.time.hasOwnProperty(key)) {
                            this[key] = window.checkoutConfig.mageworx.delivery_date.time[key];
                        }
                    }
                }

                registry.set('deliveryTimeSelect', this);

                this.initSubscribers();

                return this;
            },

            initSubscribers: function () {
                // Save shipping information on value change
                this.value.subscribe(function (val) {
                    if (val && quote.shippingMethod()) {
                        // Prevent many saves on each change
                        saveShippingInformation();
                    }
                });
            },

            /** @inheritdoc */
            initObservable: function () {
                this._super()
                    .observe('visible value');
                this.visible(false);

                return this;
            },

            updateRadioValue: function (item, event) {
                var time = $(event.target).val();
                registry.get('deliveryTimeSelect').value(time);

                return true;
            },

            selectTime: function (obj, time) {
                this.value(time);
            },

            reset: function () {
                this._super();
                var shippingAddress = quote.shippingAddress();
                if (shippingAddress) {
                    if (shippingAddress.customAttributes) {
                        delete shippingAddress.customAttributes['delivery_time'];
                    }

                    if (shippingAddress['extensionAttributes']) {
                        delete shippingAddress['extensionAttributes']['delivery_time'];
                    }
                }
            }
        });
    }
);
