"use strict";
define(
    [
        'ko',
        'Magento_Ui/js/form/element/date',
        './deliveryDayUtils',
        'uiRegistry',
        'underscore',
        'jquery',
        'mage/translate',
        'moment',
        'moment-timezone-with-data',
        'MageWorx_DeliveryDate/js/checkout/action/saveShippingInformation',
        'Magento_Checkout/js/model/quote',
        'jquery/ui'
    ],
    function (
        ko,
        Element,
        DeliveryDayUtils,
        registry,
        _,
        $,
        $t,
        moment,
        tz,
        saveShippingInformation,
        quote
    ) {
        "use strict";

        return Element.extend({

            defaults: {
                // exports: {
                //     realDateValue: '${ $.parentName.split(\'.\').slice(0, -1).join(\'.\', 2) }.delivery_date_manager:selectedDay' // parent of parent
                // },
                // imports: {
                //     activeMethodData: '${ $.parentName.split(\'.\').slice(0, -1).join(\'.\', 2) }.delivery_date_manager:activeMethodData',
                //     dayLimits: '${ $.parentName.split(\'.\').slice(0, -1).join(\'.\', 2) }.delivery_date_manager:dayLimits',
                // },
                exports: {
                    realDateValue: 'checkout.delivery_date_manager:selectedDay' // parent of parent
                },
                imports: {
                    activeMethodData: 'checkout.delivery_date_manager:activeMethodData',
                    dayLimits: 'checkout.delivery_date_manager:dayLimits',
                },
                inputDateFormat: 'y-MM-dd',
                outputDateFormat: 'y-MM-dd',
                pickerDateTimeFormat: window.checkoutConfig.mageworx.delivery_date.day_label_format,
                defaultValueWasSet: false,
                storeTimeZone: window.checkoutConfig.mageworx.delivery_date.timezone,
                autocomplete: 'off',
                options: {
                    firstDay: window.checkoutConfig.mageworx.delivery_date.firstDay ?? 0,
                    dateFormat: window.checkoutConfig.mageworx.delivery_date.day_label_format
                }
            },

            observableProperties: [
                'realDateValue',
                'dateFormat',
                'activeMethodData',
                'defaultValueWasSet',
                'dayLimits',
                'selectedDay',
                'placeholder',
                'shiftedValue',
                'uid',
                'additionalCharge',
                'additionalChargeMessage'
            ],

            /**
             * Invokes initialize method of parent class,
             * contains initialization logic
             */
            initialize: function () {
                var self = this;

                this._super();

                this.options.beforeShowDay = function (date) {
                    var diff = DeliveryDayUtils.getDayIndexFromToday(date),
                        data = self.activeMethodData();

                    if (_.isUndefined(data) ||
                        _.isUndefined(data.day_limits) ||
                        _.isUndefined(data.day_limits[diff])) {
                        return [false, "", $t("Unavailable")];
                    } else if (diff === -1) {
                        return [false, "", $t("Unavailable")];
                    } else {
                        return [true, "", $t("Available")];
                    }
                };

                moment.locale(window.checkoutConfig.mageworx.delivery_date.locale);
                var momentLocaleData = moment.localeData(),
                    config = {};

                config = {
                    monthNames: momentLocaleData._months.standalone ?? momentLocaleData._months,
                    dayNames: momentLocaleData._weekdays.standalone ?? momentLocaleData._weekdays,
                    dayNamesShort: momentLocaleData._weekdaysMin,
                    dayNamesMin: momentLocaleData._weekdaysMin,
                };

                if (momentLocaleData._monthsShort) {
                    config.monthNamesShort = momentLocaleData._monthsShort.standalone ?? momentLocaleData._monthsShort;
                }

                $.datepicker.regional['user'] = config;

                $.datepicker.setDefaults($.datepicker.regional['user']);

                return this;
            },

            initObservable: function () {
                this._super();

                this.observe(this.observableProperties);

                if (!_.isUndefined(window.checkoutConfig.mageworx.delivery_date.day_label_format)) {
                    this.dateFormat(window.checkoutConfig.mageworx.delivery_date.day_label_format);
                }
                if (!_.isUndefined(window.checkoutConfig.mageworx.delivery_date.day.placeholder)) {
                    this.placeholder(window.checkoutConfig.mageworx.delivery_date.day.placeholder);
                }

                return this;
            },

            /**
             * Prepares and sets date/time value that will be displayed
             * in the input field.
             *
             * @param {String} value
             */
            onValueChange: function (value) {
                // Replace _super
                if (value !== this.shiftedValue()) {
                    this.shiftedValue(value);
                }

                if (value) {
                    this.validateInput(value);
                    if (this.value() && quote.shippingMethod()) {
                        var realDateValue = this.getRealDateValue();
                        this.realDateValue(realDateValue);
                        this.source.delivery_date.real_date_value = realDateValue;
                        saveShippingInformation();
                    }
                }
            },

            /**
             * Prepares and sets date/time value that will be sent
             * to the server.
             *
             * @param {String} shiftedValue
             */
            onShiftedValueChange: function (shiftedValue) {
                // Replace _super
                if (shiftedValue !== this.value()) {
                    this.value(shiftedValue);
                }

                $('input[name="extension_attributes\[delivery_day\]"]').each(function (i, el) {
                    el.value = shiftedValue;
                });
            },

            /**
             * Validates itself by it's validation rules using validator object.
             * If validation of a rule did not pass, writes it's message to
             * 'error' observable property.
             *
             * @returns {Object} Validate information.
             */
            validate: function () {
                var data = this.activeMethodData && this.activeMethodData();
                if (_.isEmpty(data) || _.isEmpty(data['day_limits'])) {
                    // Skip validation in case there no delivery option
                    return {
                        valid: true,
                        target: this
                    };
                } else {
                    return this._super();
                }
            },

            /**
             * Validate selected delivery date
             *
             * @param date
             */
            validateInput: function validateInput(date) {
                var self = this,
                    inputDate = moment(date, this.pickerDateTimeFormat),
                    inputDateFormatted = inputDate.format(this.inputDateFormat),
                    index = DeliveryDayUtils.getDayIndexFromToday(inputDateFormatted),
                    activeMethodData = this.activeMethodData();
                if (activeMethodData && Object.keys(activeMethodData).length > 0) {
                    var dayLimits = activeMethodData['day_limits'] || [],
                        selectedDay = dayLimits[index] || null,
                        dateUnavailableError = $t('Selected date is not available');
                    if (!selectedDay && this.value() != '') {
                        this.value('');
                        $('#' + this.uid()).val($t('Please, select a desired delivery date from the list of available'));
                        setTimeout(function () {
                            self.error(dateUnavailableError);
                        }, 10);
                    }
                }
            },

            /**
             * Select day
             *
             * @param day
             * @param value
             * @param Event
             * @returns {boolean}
             */
            selectDay: function (day, value, Event) {
                var date = DeliveryDayUtils.createDateObjectFromDayIndexFromToday(
                    day,
                    this.pickerDateTimeFormat
                );

                if (typeof date !== 'undefined' && date !== '') {
                    this.selectedDay(day);
                    this.value(date);
                    if (date !== '' && date !== null) {
                        $('[id=' + this.uid() + ']').val(date);
                        this.validateInput(date);
                        if (this.value() && quote.shippingMethod()) {
                            saveShippingInformation();
                        }
                    }
                }

                return true;
            },

            /**
             * Set default value if needed
             */
            initDefaultValue: function () {
                this.setDefaultValue();
            },

            /**
             * Change value to default one
             */
            setDefaultValue: function () {
                var dayLimits = this.dayLimits() ? this.dayLimits() : [],
                    index = Object.keys(dayLimits)[0];

                if (typeof index !== 'undefined') {
                    this.selectDay(index);
                    this.defaultValueWasSet(true);
                }
            },

            /**
             * Returns date value as a date string (not like an day-index from today)
             *
             * @returns {number}
             */
            getRealDateValue: function () {
                return moment(this.value(), this.pickerDateTimeFormat).locale('en').format(this.inputDateFormat);
            }
        });
    }
);
