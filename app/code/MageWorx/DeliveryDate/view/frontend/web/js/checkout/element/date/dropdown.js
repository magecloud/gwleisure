"use strict";
define(
    [
        'ko',
        'Magento_Ui/js/form/element/select',
        './deliveryDayUtils',
        'uiRegistry',
        'underscore',
        'jquery',
        'mage/translate',
        'MageWorx_DeliveryDate/js/checkout/action/saveShippingInformation',
        'Magento_Checkout/js/model/quote',
        'jquery/ui'
    ],
    function (
        ko,
        Element,
        DeliveryDayUtils,
        registry,
        _,
        $,
        $t,
        saveShippingInformation,
        quote
    ) {
        "use strict";

        /**
         * Parses incoming options, considers options with undefined value property
         *     as caption
         *
         * @param  {Array} nodes
         * @return {Object}
         */
        function parseOptions(nodes, captionValue) {
            var caption,
                value;

            nodes = _.map(nodes, function (node) {
                value = node.value;

                if (value === null || value === captionValue) {
                    if (_.isUndefined(caption)) {
                        caption = node.label;
                    }
                } else {
                    return node;
                }
            });

            return {
                options: _.compact(nodes),
                caption: _.isString(caption) ? caption : false
            };
        }

        /**
         * Recursively set to object item like value and item.value like key.
         *
         * @param {Array} data
         * @param {Object} result
         * @returns {Object}
         */
        function indexOptions(data, result) {
            var value;

            result = result || {};

            data.forEach(function (item) {
                value = item.value;

                if (Array.isArray(value)) {
                    indexOptions(value, result);
                } else {
                    result[value] = item;
                }
            });

            return result;
        }

        return Element.extend({

            defaults: {
                exports: {
                    realDateValue: 'checkout.delivery_date_manager:selectedDay'
                },
                imports: {
                    activeMethodData: 'checkout.delivery_date_manager:activeMethodData',
                    dayLimits: 'checkout.delivery_date_manager:dayLimits',
                },
                links: {
                    value: '', // Unset value link to data provider
                    realDateValue: '${ $.provider }:${ $.dataScope }', // Use real date value instead
                },
                defaultValueWasSet: false,
                pickerDateTimeFormat: 'YYYY-MM-DD',
                caption: '',
                options: [],
                initialValue: ''
            },

            observableProperties: [
                'realDateValue',
                'dateFormat',
                'activeMethodData',
                'defaultValueWasSet',
                'dayLimits',
                'selectedDay',
                'additionalCharge',
                'additionalChargeMessage',
                'uid'
            ],

            initObservable: function () {
                this.options = [];
                this._super();
                this.observe(this.observableProperties);
                this.dateFormat(window.checkoutConfig.mageworx.delivery_date.day_label_format);
                this.initSubscribers();

                return this;
            },

            setOptions: function (data) {
                var captionValue = this.captionValue || '',
                    result = parseOptions(data, captionValue);

                this.indexedOptions = indexOptions(result.options);

                this.options(result.options);

                return this;
            },

            /**
             * All subscribers used in this class
             */
            initSubscribers: function () {
                // Update options
                this.dayLimits.subscribe(function (value) {
                    if (!value) {
                        return;
                    }

                    if (value.length < 1) {
                        return;
                    }

                    var values = [];
                    for (var key in value) {
                        values.push({
                            'label': value.hasOwnProperty(key) && value[key].date_formatted ?
                                value[key].date_formatted :
                                this.dayFromToday(key),
                            'value': key
                        });
                    }
                    this.setOptions(values);
                    if (values.length > 0) {
                        this.clear();
                    }
                }, this);
            },

            /**
             * Validates itself by it's validation rules using validator object.
             * If validation of a rule did not pass, writes it's message to
             * 'error' observable property.
             *
             * @returns {Object} Validate information.
             */
            validate: function () {
                var data = this.activeMethodData && this.activeMethodData();
                if (_.isEmpty(data) || _.isEmpty(data['day_limits'])) {
                    // Skip validation in case there no delivery option
                    return {
                        valid: true,
                        target: this
                    };
                } else {
                    return this._super();
                }
            },

            /**
             * Validate selected delivery date
             *
             * @param date
             */
            validateInput: function validateInput(date) {
                var self = this,
                    index = DeliveryDayUtils.getDayIndexFromToday(date),
                    activeMethodData = this.activeMethodData();
                if (activeMethodData && activeMethodData.length > 0) {
                    var dayLimits = activeMethodData['day_limits'] || [],
                        selectedDay = dayLimits[index] || null,
                        dateUnavailableError = $t('Selected date is not available');
                    if (!selectedDay && this.value() != '') {
                        this.value('');
                        setTimeout(function () {
                            self.error(dateUnavailableError);
                        }, 10);
                    }
                }
            },

            /**
             * Days diff from today
             *
             * @param day
             * @returns {*}
             */
            dayFromToday: function (day) {
                var dayLimits = this.dayLimits(),
                    dayParsed = _.isFunction(day) ? day() : day,
                    dateFormatted = null;

                if (dayParsed) {
                    dateFormatted = !_.isUndefined(dayLimits[dayParsed]) ?
                        dayLimits[dayParsed]['date_formatted'] :
                        null;
                }

                return dateFormatted;
            },

            /**
             * Select day
             *
             * @param date
             * @param Event
             * @returns {boolean}
             */
            selectDay: function (date, Event) {
                var day = Event ? $(Event.target).val() : date,
                    dateValue = DeliveryDayUtils.createDateObjectFromDayIndexFromToday(
                        day,
                        this.pickerDateTimeFormat
                    );
                this.selectedDay(day);
                this.value(day);
                this.setRealValue(day);
                if (dateValue) {
                    this.validateInput(dateValue);
                    if (this.value() && quote.shippingMethod()) {
                        var realDateValue = this.getRealDateValue();
                        this.realDateValue(realDateValue);
                        this.source.delivery_date.real_date_value = realDateValue;
                        saveShippingInformation();
                    }
                }

                return true;
            },

            /**
             * Set default value if needed
             */
            initDefaultValue: function () {
                this.setDefaultValue();
            },

            /**
             * Change value to default one
             */
            setDefaultValue: function () {
                var dayLimits = this.dayLimits() ?
                    this.dayLimits() :
                    [],
                    dayIndex = Object.keys(dayLimits)[0];

                this.selectDay(dayIndex);
            },

            /**
             * Update select value using jQuery manually, because when element not rendered yet value is not updated
             * and always corresponds caption-value.
             */
            processAfterRender: function () {
                var $this = $('#' + this.uid()),
                    self = this;

                $this.val(this.value());
                $this.on('change', function () {
                    self.value($this.val());
                });
            },

            /**
             * Returns date value as a date string (not like an day-index from today)
             *
             * @returns {number}
             */
            getRealDateValue: function () {
                var dayLimits = this.dayLimits() ?
                    this.dayLimits() :
                    [],
                    value = this.value();

                return _.isUndefined(dayLimits[value]) || _.isUndefined(dayLimits[value].date_formatted) ? -1 : DeliveryDayUtils.createDateObjectFromDayIndexFromToday(
                    value,
                    this.pickerDateTimeFormat
                );
            },

            setRealValue: function (value) {
                $('#' + this.uid()).val(value);
            },

            /**
             * Callback that fires when 'value' property is updated.
             */
            onUpdate: function (value) {
                this._super();

                var realDateValue = this.getRealDateValue();
                this.realDateValue(realDateValue);
                this.source.delivery_date.real_date_value = realDateValue;
            },
        });
    }
);
