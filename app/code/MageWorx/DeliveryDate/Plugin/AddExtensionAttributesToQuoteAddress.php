<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\DeliveryDate\Plugin;

use Magento\Quote\Api\Data\AddressExtensionInterfaceFactory;
use MageWorx\DeliveryDate\Api\Data\QueueDataInterface;

class AddExtensionAttributesToQuoteAddress
{
    /**
     * @var AddressExtensionInterfaceFactory
     */
    private $extensionFactory;

    /**
     * @param AddressExtensionInterfaceFactory $extensionFactory
     */
    public function __construct(AddressExtensionInterfaceFactory $extensionFactory)
    {
        $this->extensionFactory = $extensionFactory;
    }

    /**
     * @param \Magento\Quote\Model\ResourceModel\Quote\Address\Collection $subject
     * @param \Magento\Quote\Model\ResourceModel\Quote\Address\Collection $result
     * @param \Magento\Framework\DataObject|\Magento\Quote\Model\Quote\Address $item
     * @return \Magento\Quote\Model\ResourceModel\Quote\Address\Collection
     */
    public function afterAddItem(
        \Magento\Quote\Model\ResourceModel\Quote\Address\Collection $subject,
        \Magento\Quote\Model\ResourceModel\Quote\Address\Collection $result,
        \Magento\Framework\DataObject $item
    ) {
        // Return if address type is BILLING (only for SHIPPING)
        if ($item->getAddressType() === \Magento\Quote\Model\Quote\Address::ADDRESS_TYPE_BILLING) {
            return $result;
        }

        if (!$item instanceof \Magento\Quote\Api\Data\AddressInterface) {
            return $result;
        }

        /** @var \Magento\Quote\Api\Data\AddressExtension $extension */
        $extension = $item->getExtensionAttributes();

        $extension->setDeliveryDay($item->getData(QueueDataInterface::DELIVERY_DAY_KEY));
        $extension->setDeliveryHoursFrom($item->getData(QueueDataInterface::DELIVERY_HOURS_FROM_KEY) ?? '00');
        $extension->setDeliveryMinutesFrom($item->getData(QueueDataInterface::DELIVERY_MINUTES_FROM_KEY) ?? '00');
        $extension->setDeliveryHoursTo($item->getData(QueueDataInterface::DELIVERY_HOURS_TO_KEY) ?? '00');
        $extension->setDeliveryMinutesTo($item->getData(QueueDataInterface::DELIVERY_MINUTES_TO_KEY) ?? '00');
        $extension->setDeliveryComment($item->getData(QueueDataInterface::DELIVERY_COMMENT_KEY));
        $extension->setDeliveryOptionId($item->getData('delivery_option_id'));

        $extension->setDeliveryTime(
            implode(
                QueueDataInterface::TIME_DELIMITER,
                [
                    $extension->getDeliveryHoursFrom(),
                    $extension->getDeliveryMinutesFrom(),
                ]
            )
            . '_' .
            implode(
                QueueDataInterface::TIME_DELIMITER,
                [
                    $extension->getDeliveryHoursTo(),
                    $extension->getDeliveryMinutesTo(),
                ]
            )
        );

        return $result;
    }
}
