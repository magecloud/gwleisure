/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
var config = {
    map: {
        '*': {
            'mwSlick'       : 'MageWorx_ShippingRules/js/slick',
            'mwSlickWrapper': 'MageWorx_ShippingRules/js/slickWrapper'
        }
    }
};
