define([
    'Magento_Rule/rules',
    'prototype'
], function (rulesForm) {
    'use strict';

    return function (config) {
        console.log(config);
        var ruleConditionsFieldset = new rulesForm(config.jsObjectName, config.childUrl);

        /**
         * Does not allow editing rule items if this feature is disabled for the current admin
         */
        if (config.isReadonly !== '') {
            ruleConditionsFieldset.setReadonly(true);
        }

        window[config.jsObjectName] = ruleConditionsFieldset;
    };
});
