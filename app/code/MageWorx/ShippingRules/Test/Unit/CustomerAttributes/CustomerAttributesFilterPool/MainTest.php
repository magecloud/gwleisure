<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\CustomerAttributesFilterPool;

class MainTest extends \PHPUnit\Framework\TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Rule\Attributes\Customer\CustomerAttributesFilterPool';

    /**
     * @var \MageWorx\ShippingRules\Model\Rule\Attributes\Customer\CustomerAttributesFilterPool
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        // Magento\Eav\Model\Entity\Attribute
        $this->attributeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Attribute::class)
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->customFilter1Mock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface::class
        )
                                        ->disableOriginalConstructor()
                                        ->getMock();

        $this->customFilter2Mock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface::class
        )
                                        ->disableOriginalConstructor()
                                        ->getMock();

        $this->pool = [
            'customFilter1' => $this->customFilter1Mock,
            'customFilter2' => $this->customFilter2Mock
        ];

        $this->model = $objectManager->getObject(
            static::TESTED_CLASS_NAME,
            [
                'pool' => $this->pool
            ]
        );
    }

    /**
     * @return void
     */
    public function testAttributeIsUnavailable()
    {
        $attribute = $this->attributeMock;
        $this->customFilter1Mock->expects($this->once())
                                ->method('check')
                                ->with($attribute)
                                ->willReturn(false);

        $this->customFilter2Mock->expects($this->never())
                                ->method('check');

        $result = $this->model->isAvailable($attribute);

        $this->assertEquals(false, $result);
    }

    /**
     * @return void
     */
    public function testAttributeIsAvailable()
    {
        $this->customFilter1Mock->expects($this->once())
                                ->method('check')
                                ->with($this->attributeMock)
                                ->willReturn(true);
        $this->customFilter2Mock->expects($this->once())
                                ->method('check')
                                ->with($this->attributeMock)
                                ->willReturn(true);
        $result = $this->model->isAvailable($this->attributeMock);

        $this->assertEquals(true, $result);
    }
}
