<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\CustomerResolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\CustomerResolver';

    /**
     * @var Quote\Address|\PHPUnit\Framework\MockObject\MockObject
     */
    private $quoteAddressMock;

    /**
     * @var Quote|\PHPUnit\Framework\MockObject\MockObject
     */
    private $quoteMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $sessionMock;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customerRepositoryMock;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customerFactoryMock;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customerMock;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|\Psr\Log\LoggerInterface
     */
    private $loggerMock;

    /**
     * @var \MageWorx\ShippingRules\Model\CustomerResolver
     */
    private $model;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->sessionMock = $this->getMockForAbstractClass(
            \Magento\Framework\Session\SessionManagerInterface::class,
            [],
            '',
            false,
            true,
            true,
            ['getCustomerId']
        );

        $this->customerRepositoryMock = $this->getMockBuilder(\Magento\Customer\Api\CustomerRepositoryInterface::class)
                                             ->disableOriginalConstructor()
                                             ->getMock();

        $this->customerFactoryMock = $this->getMockBuilder(\Magento\Customer\Api\Data\CustomerInterfaceFactory::class)
                                          ->disableOriginalConstructor()
                                          ->getMock();

        $this->customerMock = $this->getMockBuilder(\Magento\Customer\Api\Data\CustomerInterface::class)
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $this->loggerMock = $this->getMockBuilder(\Psr\Log\LoggerInterface::class)
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $this->quoteMock = $this->getMockBuilder(\Magento\Quote\Model\Quote::class)
                                ->addMethods(['getQuote', 'getCustomerId'])
                                ->onlyMethods(['getId', 'getCustomer', 'setCustomer'])
                                ->disableOriginalConstructor()
                                ->getMock();

        $this->quoteAddressMock = $this->getMockBuilder(\Magento\Quote\Model\Quote\Address::class)
                                       ->addmethods(['getCustomer'])
                                       ->onlyMethods(['getId', 'getQuote'])
                                       ->disableOriginalConstructor()
                                       ->getMock();

        $args = [
            'session' => $this->sessionMock,
            'customerRepository' => $this->customerRepositoryMock,
            'customerFactory' => $this->customerFactoryMock,
            'logger' => $this->loggerMock
        ];

        $this->model = $this->objectManager->getObject(
            static::TESTED_CLASS_NAME,
            $args
        );
    }

    /**
     * Customer from quote address entity must be taken from the repository directly
     *
     * @return void
     */
    public function testWithQuoteAddressModel()
    {
        $customerId = 103;

        $this->customerMock->expects($this->any())
                           ->method('getId')
                           ->willReturn($customerId);
        $this->quoteMock->expects($this->never())
                        ->method('getCustomer')
                        ->willReturn($this->customerMock);
        $this->quoteMock->expects($this->atLeastOnce())
                        ->method('getCustomerId')
                        ->willReturn($customerId);
        $this->quoteAddressMock->expects($this->atLeastOnce())
                               ->method('getCustomer')
                               ->willReturn(null);
        $this->quoteAddressMock->expects($this->atLeastOnce())
                               ->method('getQuote')
                               ->willReturn($this->quoteMock);
        $this->customerRepositoryMock->expects($this->atLeastOnce())
                                     ->method('getById')
                                     ->with($customerId)
                                     ->willReturn($this->customerMock);
        $this->customerFactoryMock->expects($this->never())
                                  ->method('create');

        $result = $this->model->resolve($this->quoteAddressMock);

        $this->assertIsObject($result);
        $this->assertEquals($customerId, $result->getId());
    }

    /**
     * If quote object passed to the method, but has no customer set, customer must be created using factory.
     *
     * @return void
     */
    public function testWithQuoteModelWithNoCustomerSet()
    {
        $this->quoteMock->expects($this->atLeastOnce())
                        ->method('getCustomer')
                        ->willReturn(null);
        $this->quoteMock->expects($this->atLeastOnce())
                        ->method('getQuote')
                        ->willReturn(null);
        $this->customerFactoryMock->expects($this->once())
                                  ->method('create')
                                  ->willReturn($this->customerMock);

        $result = $this->model->resolve($this->quoteMock);

        // As a result we get empty customer entity
        $this->assertIsObject($result);
        $this->assertNull($result->getId());
    }

    /**
     * If quote object passed to the method it must return correct customer object from that object
     * and must not search for customer in other models.
     *
     * @return void
     */
    public function testWithQuoteModel()
    {
        $customerId = 102;

        $this->customerMock->expects($this->atLeastOnce())
                           ->method('getId')
                           ->willReturn($customerId);
        $this->quoteMock->expects($this->atLeastOnce())
                        ->method('getCustomer')
                        ->willReturn($this->customerMock);
        $this->quoteMock->expects($this->never())
                        ->method('getQuote');

        $result = $this->model->resolve($this->quoteMock);

        $this->assertIsObject($result);
        $this->assertEquals($customerId, $result->getId());
    }

    /**
     * Resolved customer instance must be an object with correct id from session
     * if customer session returns the correct customer id.
     *
     * @return void
     */
    public function testWithoutProvidedModelWithCorrectIdFromSession()
    {
        $customerId = 101;

        $this->sessionMock->expects($this->atLeastOnce())
                          ->method('getCustomerId')
                          ->willReturn($customerId);

        $customerArgs = [];
        $customer     = $this->objectManager->getObject(
            \Magento\Customer\Model\Data\Customer::class,
            $customerArgs
        );
        $customer->setId($customerId);

        $this->customerFactoryMock->expects($this->never())
                                  ->method('create');
        $this->loggerMock->expects($this->never())
                         ->method('error');

        $this->customerRepositoryMock->expects($this->atLeastOnce())
                                     ->method('getById')
                                     ->with($customerId)
                                     ->willReturn($customer);

        $result = $this->model->resolve(null);

        $this->assertIsObject($result);
        $this->assertEquals($customerId, $result->getId());
    }

    /**
     * Resolved customer instance must be an empty object
     * if customer session returns the incorrect customer id.
     *
     * @return void
     */
    public function testWithoutProvidedModelWithIncorrectIdFromSession()
    {
        $this->sessionMock->expects($this->atLeastOnce())
                          ->method('getCustomerId')
                          ->willReturn(333);

        $customerArgs = [];
        $customer     = $this->objectManager->getObject(
            \Magento\Customer\Model\Data\Customer::class,
            $customerArgs
        );

        $this->customerFactoryMock->expects($this->atLeastOnce())
                                  ->method('create')
                                  ->willReturn($customer);

        $this->customerRepositoryMock->expects($this->atLeastOnce())
                                     ->method('getById')
                                     ->willThrowException(new LocalizedException(__('Dummy string')));

        $this->loggerMock->expects($this->atLeastOnce())
                         ->method('error');

        $result = $this->model->resolve(null);

        // As a result we get empty customer entity
        $this->assertIsObject($result);
        $this->assertEmpty($result->getId());
    }

    /**
     * Resolved customer instance must be an empty object
     * if customer session does not return the customer id.
     *
     * @return void
     */
    public function testWithoutProvidedModelAndNoIdFromSession()
    {
        $this->sessionMock->expects($this->atLeastOnce())
                          ->method('getCustomerId')
                          ->willReturn(null);

        $this->customerFactoryMock->expects($this->once())
                                  ->method('create')
                                  ->willReturn($this->customerMock);

        $result = $this->model->resolve(null);

        // As a result we get empty customer entity
        $this->assertIsObject($result);
        $this->assertEmpty($result->getId());
    }
}
