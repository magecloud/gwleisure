<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\CustomerAttributesFilter;

class ByDataTest extends \PHPUnit\Framework\TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\ByData';

    /**
     * @var \MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\ByData
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->attributeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Attribute::class)
                                    ->addMethods(
                                        [
                                            'getFrontendLabel'
                                        ]
                                    )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->attributeWithoutGetLabelMethodMock = $this->getMockBuilder(\Magento\Eav\Api\Data\AttributeInterface::class)
                                                         ->disableOriginalConstructor()
                                                         ->getMock();

        $this->model = $objectManager->getObject(
            static::TESTED_CLASS_NAME,
            []
        );
    }

    /**
     * Attribute without the getFrontendLabel method must be unavailable in conditions
     *
     * @return void
     */
    public function testAttributeWithoutGetLabelMethod()
    {
        $result = $this->model->check($this->attributeWithoutGetLabelMethodMock);

        $this->assertFalse($result);
    }

    /**
     * Attribute with the LABEL must be available in conditions
     *
     * @return void
     */
    public function testAttributeWithLabel()
    {
        $this->attributeMock->expects($this->once())
                            ->method('getFrontendLabel')
                            ->willReturn('Dummy Label');

        $result = $this->model->check($this->attributeMock);

        $this->assertTrue($result);
    }

    /**
     * Attribute without the LABEL must be unavailable in conditions
     *
     * @return void
     */
    public function testAttributeWithoutLabel()
    {
        $this->attributeMock->expects($this->once())
                            ->method('getFrontendLabel')
                            ->willReturn(null);

        $result = $this->model->check($this->attributeMock);

        $this->assertFalse($result);
    }
}
