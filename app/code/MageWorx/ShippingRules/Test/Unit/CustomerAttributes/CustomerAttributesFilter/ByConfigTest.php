<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\CustomerAttributesFilter;

class ByConfigTest extends \PHPUnit\Framework\TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\ByConfig';

    /**
     * @var \MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\ByConfig
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->attributeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Attribute::class)
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->scopeConfigMock = $this->getMockBuilder(\Magento\Framework\App\Config\ScopeConfigInterface::class)
                                      ->disableOriginalConstructor()
                                      ->getMock();

        $this->model = $objectManager->getObject(
            static::TESTED_CLASS_NAME,
            [
                'scopeConfig' => $this->scopeConfigMock
            ]
        );
    }

    /**
     * @return void
     */
    public function testReturnCorrectValueWhenAttributeInDisabledListAsString()
    {
        $this->scopeConfigMock->expects($this->once())
                              ->method('getValue')
                              ->with($this->model::XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES)
                              ->willReturn('disabled_attribute_1,disabled_attribute_2,disabled_attribute_3');
        $this->attributeMock->expects($this->atLeastOnce())
                            ->method('getAttributeCode')
                            ->willReturn('disabled_attribute_2');

        $result = $this->model->check($this->attributeMock);

        $this->assertFalse($result);
    }

    /**
     * @return void
     */
    public function test2ReturnCorrectValueWhenAttributeInDisabledListAsString()
    {
        $this->scopeConfigMock->expects($this->once())
                              ->method('getValue')
                              ->with($this->model::XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES)
                              ->willReturn('disabled_attribute_1,disabled_attribute_2,disabled_attribute_3');
        $this->attributeMock->expects($this->atLeastOnce())
                            ->method('getAttributeCode')
                            ->willReturn('disabled_attribute_100');

        $result = $this->model->check($this->attributeMock);

        $this->assertTrue($result);
    }

    /**
     * @return void
     */
    public function testReturnCorrectValueWhenAttributeInDisabledListAsArray()
    {
        $this->scopeConfigMock->expects($this->once())
                              ->method('getValue')
                              ->with($this->model::XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES)
                              ->willReturn(['disabled_attribute_100', 'disabled_attribute_200']);
        $this->attributeMock->expects($this->atLeastOnce())
                            ->method('getAttributeCode')
                            ->willReturn('disabled_attribute_200');

        $result = $this->model->check($this->attributeMock);

        $this->assertFalse($result);
    }

    /**
     * @return void
     */
    public function test2ReturnCorrectValueWhenAttributeInDisabledListAsArray()
    {
        $this->scopeConfigMock->expects($this->once())
                              ->method('getValue')
                              ->with($this->model::XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES)
                              ->willReturn(['disabled_attribute_100', 'disabled_attribute_200']);
        $this->attributeMock->expects($this->atLeastOnce())
                            ->method('getAttributeCode')
                            ->willReturn('disabled_attribute_9');

        $result = $this->model->check($this->attributeMock);

        $this->assertTrue($result);
    }

    /**
     * @return void
     */
    public function testConfigValues()
    {
        $this->scopeConfigMock->expects($this->once())
                              ->method('getValue')
                              ->with($this->model::XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES)
                              ->willReturn(
                                  'disabled_attribute_1,disabled_attribute_2,disabled_attribute_3, disabled_attribute_4'
                              );

        $result = $this->model->getDisabledAttributes();

        $this->assertArrayHasKey(0, $result);
        $this->assertArrayHasKey(1, $result);
        $this->assertArrayHasKey(2, $result);
        $this->assertArrayHasKey(3, $result);
        $this->assertArrayNotHasKey(4, $result);
    }

    /**
     * @return void
     */
    public function testDisabledAttributesFromConfigType()
    {
        $result = $this->model->getDisabledAttributes();

        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }
}
