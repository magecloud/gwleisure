<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\CustomerAttributesFilter;

class ByInputTypeTest extends \PHPUnit\Framework\TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\ByInputType';

    /**
     * @var \MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\ByInputType
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->attributeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Attribute::class)
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->model = $objectManager->getObject(
            static::TESTED_CLASS_NAME,
            []
        );
    }

    /**
     * Attributes with file input type must be unavailable in conditions
     *
     * @return void
     */
    public function testFileType()
    {
        $this->attributeMock->expects($this->once())
            ->method('getFrontendInput')
            ->willReturn('file');

        $result = $this->model->check($this->attributeMock);

        $this->assertFalse($result);
    }

    /**
     * Attributes with image input type must be unavailable in conditions
     *
     * @return void
     */
    public function testImageType()
    {
        $this->attributeMock->expects($this->once())
                            ->method('getFrontendInput')
                            ->willReturn('image');

        $result = $this->model->check($this->attributeMock);

        $this->assertFalse($result);
    }

    /**
     * Attributes with text input type can be available in conditions
     *
     * @return void
     */
    public function testTextType()
    {
        $this->attributeMock->expects($this->once())
                            ->method('getFrontendInput')
                            ->willReturn('text');

        $result = $this->model->check($this->attributeMock);

        $this->assertTrue($result);
    }

    /**
     * Attributes with select input type can be available in conditions
     *
     * @return void
     */
    public function testSelectType()
    {
        $this->attributeMock->expects($this->once())
                            ->method('getFrontendInput')
                            ->willReturn('select');

        $result = $this->model->check($this->attributeMock);

        $this->assertTrue($result);
    }
}
