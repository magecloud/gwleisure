<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\CustomerAttributesFilter;

class GeneralFilterTest extends \PHPUnit\Framework\TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\General';

    /**
     * @var \MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters\General
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->attributeMock = $this->getMockBuilder(\Magento\Eav\Model\Entity\Attribute::class)
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->disabledAttributesArray = [
            'attribute_1',
            'attribute_2',
            'attribute_3',
        ];

        $this->model = $objectManager->getObject(
            static::TESTED_CLASS_NAME,
            [
                'disabledAttributes' => $this->disabledAttributesArray
            ]
        );
    }

    /**
     * Disabled attribute check must return false
     *
     * @return void
     */
    public function testDisabledAttribute()
    {
        $this->attributeMock->expects($this->once())
            ->method('getAttributeCode')
            ->willReturn('attribute_2');

        $result = $this->model->check($this->attributeMock);

        $this->assertFalse($result);
    }

    /**
     * Not disabled attribute check must return true
     *
     * @return void
     */
    public function testNotDisabledAttribute()
    {
        $this->attributeMock->expects($this->once())
                            ->method('getAttributeCode')
                            ->willReturn('attribute_4');

        $result = $this->model->check($this->attributeMock);

        $this->assertTrue($result);
    }
}
