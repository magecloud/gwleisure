<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\CustomerAttributes\Condition;

use Magento\Quote\Model\Quote;
use PHPUnit\Framework\TestCase;

/**
 * Test validation rules (conditions)
 */
class ValidationTest extends TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Rule\Condition\Customer';

    /**
     * Operators in constants for better code readability
     *
     * '==' => __('is'),
     * '!=' => __('is not'),
     * '>=' => __('equals or greater than'),
     * '<=' => __('equals or less than'),
     * '>' => __('greater than'),
     * '<' => __('less than'),
     * '{}' => __('contains'),
     * '!{}' => __('does not contain'),
     * '()' => __('is one of'),
     * '!()' => __('is not one of'),
     * '<=>' => __('is undefined'),
     */
    const OPERATOR_IS                     = '==';
    const OPERATOR_IS_NOT                 = '!=';
    const OPERATOR_EQUALS_OR_GREATER_THAN = '>=';
    const OPERATOR_EQUALS_OR_LESS_THAN    = '<=';
    const OPERATOR_GREATER_THAN           = '>';
    const OPERATOR_LESS_THAN              = '<';
    const OPERATOR_CONTAINS               = '{}';
    const OPERATOR_DOES_NOT_CONTAIN       = '!{}';
    const OPERATOR_IS_ONE_OF              = '()';
    const OPERATOR_IS_NOT_ONE_OF          = '!()';
    const OPERATOR_IS_UNDEFINED           = '<=>';

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customerResourceMock;

    /**
     * @var \MageWorx\ShippingRules\Api\CustomerResolverInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customerResolverMock;

    /**
     * @var Quote\Address|\PHPUnit\Framework\MockObject\MockObject
     */
    private $quoteAddressMock;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customerMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    private $objectManager;

    /**
     * @var \MageWorx\ShippingRules\Model\Rule\Condition\Customer
     */
    private $model;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->customerMock = $this->getMockBuilder(\Magento\Customer\Model\Data\Customer::class)
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $this->quoteAddressMock = $this->getMockBuilder(\Magento\Quote\Model\Quote\Address::class)
                                       ->addmethods(['getCustomer'])
                                       ->onlyMethods(['getId', 'getQuote'])
                                       ->disableOriginalConstructor()
                                       ->getMock();

        $this->customerResolverMock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Api\CustomerResolverInterface::class
        )
                                           ->disableOriginalConstructor()
                                           ->getMock();

        $this->customerResourceMock = $this->getMockBuilder(\Magento\Customer\Model\ResourceModel\Customer::class)
                                           ->disableOriginalConstructor()
                                           ->getMock();

        $this->customerAttributeMock = $this->getMockBuilder(\Magento\Eav\Api\Data\AttributeInterface::class)
                                            ->disableOriginalConstructor()
                                            ->getMock();

        $this->eavConfigMock = $this->getMockBuilder(\Magento\Eav\Model\Config::class)
                                    ->disableOriginalConstructor()
                                    ->getMock();

        /**
         * These methods will be called directly from the __construct method, so they must be defined before we create
         * object using OM
         */
        $this->customerResourceMock->expects($this->any())
                                   ->method('getAttributesByCode')
                                   ->willReturn([]);
        $this->customerResourceMock->expects($this->any())
                                   ->method('loadAllAttributes')
                                   ->willReturnSelf();

        $args = [
            'customerResolver' => $this->customerResolverMock,
            'customerResource' => $this->customerResourceMock
        ];

        $this->model = $this->objectManager->getObject(
            static::TESTED_CLASS_NAME,
            $args
        );
    }

    /**
     * Test validation method
     *
     * @return void
     * @dataProvider attributeValidationDataProvider @dataProvider
     */
    public function testValidation(array $attributeData, bool $expectedResult)
    {
        $testAttributeCode            = $attributeData['code'];
        $testAttributeOperator        = $attributeData['operator'];
        $testAttributeConditionValue  = $attributeData['condition_value'];
        $testAttributeValidationValue = $attributeData['value'];
        $testAttributeFrontendInput   = $attributeData['frontend_input'];
        $testAttributeValidationRules = $attributeData['validation_rules'] ?? [];
        $customerData                 = [
            $testAttributeCode => $testAttributeValidationValue
        ];

        $this->eavConfigMock->expects($this->any())
                            ->method('getAttribute')
                            ->with(['customer', $testAttributeCode])
                            ->willReturn($this->customerAttributeMock);

        $this->customerAttributeMock->expects($this->any())
                                    ->method('getAttributeCode')
                                    ->willReturn($testAttributeCode);
        $this->customerAttributeMock->expects($this->any())
                                    ->method('getFrontendInput')
                                    ->willReturn($testAttributeFrontendInput);
        $this->customerAttributeMock->expects($this->any())
                                    ->method('getValidationRules')
                                    ->willReturn($testAttributeValidationRules);

        $this->customerMock->expects($this->once())
                           ->method('__toArray')
                           ->willReturn($customerData);
        $this->customerResourceMock->expects($this->any())
                                   ->method('getAttributesByCode')
                                   ->willReturn([$this->customerAttributeMock]);
        $this->customerResourceMock->expects($this->any())
                                   ->method('loadAllAttributes')
                                   ->willReturnSelf();
        $this->customerResolverMock->expects($this->once())
                                   ->method('resolve')
                                   ->with($this->quoteAddressMock)
                                   ->willReturn($this->customerMock);

        $this->model->setAttribute($testAttributeCode);
        $this->model->setValue($testAttributeConditionValue);
        $this->model->setOperator($testAttributeOperator);

        $result = $this->model->validate($this->quoteAddressMock);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    public function attributeValidationDataProvider(): array
    {
        return [
            // Customer has 33, and we are looking for 33 to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '33',
                    'value'           => '33',
                    'operator'        => self::OPERATOR_IS,
                    'frontend_input'  => 'text'
                ],
                'expectedResult' => true
            ],
            // Customer has 33, and we are looking for not 33 to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '33',
                    'value'           => '33',
                    'operator'        => self::OPERATOR_IS_NOT,
                    'frontend_input'  => 'text'
                ],
                'expectedResult' => false
            ],
            // Customer has 35, and we are looking for 34 to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '34',
                    'value'           => '35',
                    'operator'        => self::OPERATOR_IS,
                    'frontend_input'  => 'text'
                ],
                'expectedResult' => false
            ],
            // Customer has 35, and we are looking for 34 or MORE to trigger a rule
            [
                'attributeData'  => [
                    'code'             => 'age',
                    'condition_value'  => '34',
                    'value'            => '35',
                    'operator'         => self::OPERATOR_EQUALS_OR_GREATER_THAN,
                    'frontend_input'   => 'text',
                    'validation_rules' => [
                        'input_validation' => 'numeric'
                    ]
                ],
                'expectedResult' => true
            ],
            // Customer has 35, and we are looking for 34 or LESS to trigger a rule
            [
                'attributeData'  => [
                    'code'             => 'age',
                    'condition_value'  => '34',
                    'value'            => '35',
                    'operator'         => self::OPERATOR_EQUALS_OR_LESS_THAN,
                    'frontend_input'   => 'text',
                    'validation_rules' => [
                        'input_validation' => 'numeric'
                    ]
                ],
                'expectedResult' => false
            ],
            // Customer has 34, and we are looking for 35, 33, 40 or 1000 years old to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '35,33,40,1000',
                    'value'           => '34',
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => false
            ],
            // Customer has 34, and we are looking for any age EXCEPT the 35, 33, 40 or 1000 years old to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '35,33,40,1000',
                    'value'           => '34',
                    'operator'        => self::OPERATOR_IS_NOT_ONE_OF,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // Customer has 40, and we are looking for 35, 33, 40 or 1000 years old to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '35,33,40,1000',
                    'value'           => '40',
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // Customer has 40, and we are looking for any age EXCEPT 35, 33, 40 or 1000 years old to trigger a rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => '35,33,40,1000',
                    'value'           => '40',
                    'operator'        => self::OPERATOR_IS_NOT_ONE_OF,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => false
            ],
            // Customer has not selected a gender, and we are looking for customer with undefined gender to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'gender',
                    'condition_value' => null,
                    'value'           => null,
                    'operator'        => self::OPERATOR_IS_UNDEFINED,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // Customer has set an age, but we are looking for customer with an undefined age to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'age',
                    'condition_value' => null,
                    'value'           => '100',
                    'operator'        => self::OPERATOR_IS_UNDEFINED,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => false
            ],
            // Customer has set a favorite numbers as 100 and 200 (as array of strings), and we are looking for customer
            // with favorite numbers is 100, 200 or 300 (as a string from DB) to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => '100,200,300',
                    'value'           => ['100', '200'],
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'multiselect',
                ],
                'expectedResult' => true
            ],
            // Customer has set a favorite numbers as 100 and 200 (as array of strings), and we are looking for customer
            // with favorite numbers is 100, 200 or 300 (as array of strings from DB) to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => ['100', '200', '300'],
                    'value'           => ['100', '200'],
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'multiselect',
                ],
                'expectedResult' => true
            ],
            // Customer has set a favorite numbers as 100 and 200 (as array of strings), and we are looking for customer
            // with favorite numbers is 100, 200 or 300 (as array of int from DB) to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => [100, 200, 300],
                    'value'           => ['100', '200'],
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'multiselect',
                ],
                'expectedResult' => true
            ],
            // Customer has set a favorite numbers as 100 and 200 (as array of int), and we are looking for customer
            // with favorite numbers is 100, 200 or 300 (as array of int from DB) to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => [100, 200, 300],
                    'value'           => [100, 200],
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'multiselect',
                ],
                'expectedResult' => true
            ],
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => '100,200,300',
                    'value'           => ['100', '200'],
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'multiselect',
                ],
                'expectedResult' => true
            ],
            // Customer has set a favorite numbers as 100 (as strings in select), and we are looking for customer
            // with favorite numbers is 100, 200 or 300 (as array of int from DB) to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => [100, 200, 300],
                    'value'           => '100',
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'select',
                ],
                'expectedResult' => true
            ],
            // Customer has set a favorite numbers as 110 (as strings in select), and we are looking for customer
            // with favorite numbers is 100, 200 or 300 (as array of int from DB) to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_numbers',
                    'condition_value' => [100, 200, 300],
                    'value'           => '110',
                    'operator'        => self::OPERATOR_IS_ONE_OF,
                    'frontend_input'  => 'select',
                ],
                'expectedResult' => false
            ],
            // Customer has set a favorite number as 213, and we are looking for customers which favorite number
            // contains 1 to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_number',
                    'condition_value' => '1',
                    'value'           => '213',
                    'operator'        => self::OPERATOR_CONTAINS, // 213 (customer value) contains 1 (condition value)
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // Customer has set a favorite number as 321, and we are looking for customers which favorite number
            // does not contain 213 to trigger the rule
            [
                'attributeData'  => [
                    'code'            => 'favorite_number',
                    'condition_value' => '213',
                    'value'           => '321',
                    'operator'        => self::OPERATOR_DOES_NOT_CONTAIN,
                    // 321 (customer value) does not contain 213 (condition value)
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // The customer wrote about himself in the about me section, and we are looking for a customer who hates
            // something to trigger a shipping rule with extra charge
            [
                'attributeData'  => [
                    'code'            => 'about_me',
                    'condition_value' => 'hate',
                    'value'           => 'I hate the Magento 2 and all software developers! >:|',
                    'operator'        => self::OPERATOR_CONTAINS,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // The customer wrote about himself in the about me section, and we are looking for a customer who
            // hates nothing to trigger a shipping rule with free shipping
            [
                'attributeData'  => [
                    'code'            => 'about_me',
                    'condition_value' => 'hate',
                    'value'           => 'I love the Magento 2 and all the developers are awesome! :)',
                    'operator'        => self::OPERATOR_DOES_NOT_CONTAIN,
                    'frontend_input'  => 'text',
                ],
                'expectedResult' => true
            ],
            // Date comparison: customer will be born tomorrow, and we are looking for customers born yesterday
            [
                'attributeData'  => [
                    'code'            => 'birthday',
                    'condition_value' => date("Y-m-d", strtotime('-1 days')),
                    'value'           => date("Y-m-d", strtotime('+1 days')),
                    'operator'        => self::OPERATOR_EQUALS_OR_GREATER_THAN,
                    'frontend_input'  => 'date',
                ],
                'expectedResult' => true
            ],
            // Date comparison: customer has been registered 30 years ago, but we are looking for customers registered
            // ten days ago or later
            [
                'attributeData'  => [
                    'code'            => 'birthday',
                    'condition_value' => date("Y-m-d", strtotime('-10 days')),
                    'value'           => date("Y-m-d", strtotime('-30 years')),
                    'operator'        => self::OPERATOR_EQUALS_OR_GREATER_THAN,
                    'frontend_input'  => 'date',
                ],
                'expectedResult' => false
            ]
        ];
    }

    /**
     * Test validation method with empty customer
     *
     * @return void
     */
    public function testValidationWithoutCustomer()
    {
        $this->customerResourceMock->expects($this->any())
                                   ->method('getAttributesByCode')
                                   ->willReturn([]);
        $this->customerResourceMock->expects($this->any())
                                   ->method('loadAllAttributes')
                                   ->willReturnSelf();
        $this->customerResolverMock->expects($this->once())
                                   ->method('resolve')
                                   ->with($this->quoteAddressMock)
                                   ->willReturn($this->customerMock);

        $result = $this->model->validate($this->quoteAddressMock);

        $this->assertFalse($result);
    }
}
