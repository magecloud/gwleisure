<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\Plugin\CollectValidMethods;


class ShippingMethodCodeComparisonTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Catalog\Model\Product|\PHPUnit\Framework\MockObject\MockObject
     */
    private $productMock;

    /**
     * @var \Magento\Quote\Model\Quote\Item|\PHPUnit\Framework\MockObject\MockObject
     */
    private $quoteItemMock;

    /**
     * @var \MageWorx\ShippingRules\Model\Config\Source\Shipping\Methods|\PHPUnit\Framework\MockObject\MockObject
     */
    private $shippingMethodsSourceMock;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product|\PHPUnit\Framework\MockObject\MockObject
     */
    private $productResourceModelMock;

    /**
     * @var \MageWorx\ShippingRules\Model\Plugin\CollectValidMethods
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->productResourceModelMock = $this->getMockBuilder(\Magento\Catalog\Model\ResourceModel\Product::class)
                                               ->disableOriginalConstructor()
                                               ->getMock();

        $this->shippingMethodsSourceMock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Model\Config\Source\Shipping\Methods::class
        )
                                                ->disableOriginalConstructor()
                                                ->getMock();

        $this->quoteItemMock = $this->getMockBuilder(\Magento\Quote\Model\Quote\Item::class)
                                    ->setMethods(
                                        [
                                            'getStoreId',
                                            'getProduct'
                                        ]
                                    )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->productMock = $this->getMockBuilder(\Magento\Catalog\Model\Product::class)
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->model = $objectManager->getObject(
            \MageWorx\ShippingRules\Model\Plugin\CollectValidMethods::class,
            [
                'productResource' => $this->productResourceModelMock,
                'shippingMethodsSource' => $this->shippingMethodsSourceMock
            ]
        );
    }

    /**
     * Empty items with empty base methods will return empty array of available shipping methods
     *
     * @return void
     */
    public function testCollectAvailableShippingMethodsForItemsWithEmptyItems()
    {
        $methods = [];
        $items   = [];

        $this->shippingMethodsSourceMock->expects($this->atLeastOnce())
                                        ->method('toArray')
                                        ->willReturn($methods);

        $result = $this->model->collectAvailableShippingMethodsForItems($items);

        $this->assertEquals([], $result);
    }

    /**
     * Test intersection of the shipping methods from config and from product (in different letter case adn with extra
     * space). As result methods which will be available is the 'test_method' and 'test2_method1'.
     *
     * @return void
     */
    public function testCollectAvailableShippingMethodsForItems()
    {
        $productId = 1;
        $storeId   = 1;

        $methodsFromProduct = [
            'test_method',
            'another_test',
            ' TeST2_MeTHoD1'
        ];
        $methods            = [
            'method_x',
            'tEsT_MeTHOD ',
            'other_method',
            'other_method2',
            'tEst2_mEthOd1'
        ];
        $items              = [
            $this->quoteItemMock
        ];

        $this->productMock->expects($this->atLeastOnce())
                          ->method('getId')
                          ->willReturn($productId);

        $this->quoteItemMock->expects($this->atLeastOnce())
                            ->method('getProduct')
                            ->willReturn($this->productMock);

        $this->quoteItemMock->expects($this->atLeastOnce())
                            ->method('getStoreId')
                            ->willReturn($storeId);

        $this->productResourceModelMock->expects($this->once())
                                       ->method('getAttributeRawValue')
                                       ->with($productId, 'available_shipping_methods', $storeId)
                                       ->willReturn($methodsFromProduct);


        $this->shippingMethodsSourceMock->expects($this->atLeastOnce())
                                        ->method('toArray')
                                        ->willReturn($methods);

        $result = $this->model->collectAvailableShippingMethodsForItems($items);

        $result = array_values($result);

        $this->assertEquals(['test_method', 'test2_method1'], $result);
    }
}
