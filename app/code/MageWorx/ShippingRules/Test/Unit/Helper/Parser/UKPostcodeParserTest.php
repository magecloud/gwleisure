<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\Helper\Parser;

class UKPostcodeParserTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \MageWorx\ShippingRules\Helper\Data
     */
    private $model;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $args          = [];
        $this->model   = $objectManager->getObject(
            \MageWorx\ShippingRules\Helper\Data::class,
            $args
        );
    }

    /**
     * @param string $zipCode tested zip code
     * @param array $expectedResult parsed zip code as parts
     *
     * @return void
     * @dataProvider UKZipCodesInputDataProvider @dataProvider
     */
    public function testUKZipCodesParser(string $zipCode, array $expectedResult): void
    {
        $result = $this->model->parseUkPostCode($zipCode);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    public function UKZipCodesInputDataProvider(): array
    {
        return [
            // Normal zip: uppercase
            [
                'zip'            => 'BT41 1AA',
                'expectedResult' => [
                    'uk_area'      => 'BT',
                    'uk_district'  => '41',
                    'uk_sector'    => '1',
                    'uk_unit'      => 'AA',
                    'uk_outcode'   => 'BT41',
                    'uk_incode'    => '1AA',
                    'uk_full_code' => 'BT41 1AA'
                ]
            ],
            // Normal zip: lowercase
            [
                'zip'            => 'bt41 1aa',
                'expectedResult' => [
                    'uk_area'      => 'BT',
                    'uk_district'  => '41',
                    'uk_sector'    => '1',
                    'uk_unit'      => 'AA',
                    'uk_outcode'   => 'BT41',
                    'uk_incode'    => '1AA',
                    'uk_full_code' => 'BT41 1AA'
                ]
            ],
            // Normal zip: without space lowercase
            [
                'zip'            => 'bt411aa',
                'expectedResult' => [
                    'uk_area'      => 'BT',
                    'uk_district'  => '41',
                    'uk_sector'    => '1',
                    'uk_unit'      => 'AA',
                    'uk_outcode'   => 'BT41',
                    'uk_incode'    => '1AA',
                    'uk_full_code' => 'BT411AA'
                ]
            ],
            // Normal zip: without space uppercase
            [
                'zip'            => 'BT411AA',
                'expectedResult' => [
                    'uk_area'      => 'BT',
                    'uk_district'  => '41',
                    'uk_sector'    => '1',
                    'uk_unit'      => 'AA',
                    'uk_outcode'   => 'BT41',
                    'uk_incode'    => '1AA',
                    'uk_full_code' => 'BT411AA'
                ]
            ],
            // Incorrect zip: one more symbol at the end (empty result)
            [
                'zip'            => 'BT411AAX',
                'expectedResult' => [
                    'uk_area'      => '',
                    'uk_district'  => '',
                    'uk_sector'    => '',
                    'uk_unit'      => '',
                    'uk_outcode'   => '',
                    'uk_incode'    => '',
                    'uk_full_code' => 'BT411AAX'
                ]
            ],
            // Partial zip code: only outcode
            [
                'zip'            => 'SS7',
                'expectedResult' => [
                    'uk_area'      => 'SS',
                    'uk_district'  => '7',
                    'uk_sector'    => '',
                    'uk_unit'      => '',
                    'uk_outcode'   => 'SS7',
                    'uk_incode'    => '',
                    'uk_full_code' => 'SS7'
                ]
            ],
            // Partial zip code: only outcode with one space
            [
                'zip'            => 'SS7 ',
                'expectedResult' => [
                    'uk_area'      => 'SS',
                    'uk_district'  => '7',
                    'uk_sector'    => '',
                    'uk_unit'      => '',
                    'uk_outcode'   => 'SS7',
                    'uk_incode'    => '',
                    'uk_full_code' => 'SS7'
                ]
            ],
            // Partial zip code: only outcode with many spaces
            [
                'zip'            => '  SS7  ',
                'expectedResult' => [
                    'uk_area'      => 'SS',
                    'uk_district'  => '7',
                    'uk_sector'    => '',
                    'uk_unit'      => '',
                    'uk_outcode'   => 'SS7',
                    'uk_incode'    => '',
                    'uk_full_code' => 'SS7'
                ]
            ],
            // Partial zip code: only incode (empty result)
            [
                'zip'            => '6AA',
                'expectedResult' => [
                    'uk_area'      => '',
                    'uk_district'  => '',
                    'uk_sector'    => '',
                    'uk_unit'      => '',
                    'uk_outcode'   => '',
                    'uk_incode'    => '',
                    'uk_full_code' => '6AA'
                ]
            ],
            // Normal zip: Northern Ireland
            [
                'zip'            => 'BT69 6AA',
                'expectedResult' => [
                    'uk_area'      => 'BT',
                    'uk_district'  => '69',
                    'uk_sector'    => '6',
                    'uk_unit'      => 'AA',
                    'uk_outcode'   => 'BT69',
                    'uk_incode'    => '6AA',
                    'uk_full_code' => 'BT69 6AA'
                ]
            ],
        ];
    }
}
