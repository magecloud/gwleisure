<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Test\Unit\ArtificialCarrier;

use MageWorx\ShippingRules\Model\Carrier\Method\Rate as MageWorxRate;
use PHPUnit\Framework\TestCase;

class CollectRatesTest extends TestCase
{
    const TESTED_CLASS_NAME = 'MageWorx\ShippingRules\Model\Carrier\Artificial';

    /**
     * @var \MageWorx\ShippingRules\Model\Carrier\Artificial
     */
    protected $model;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateRequest|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateRequestMock;

    /**
     * @var \MageWorx\ShippingRules\Model\CarrierFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $carrierFactoryMock;

    /**
     * @var \MageWorx\ShippingRules\Model\Carrier|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $carrierMock;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateResultFactoryMock;

    /**
     * @var \Magento\Shipping\Model\Rate\Result|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateResultMock;

    /**
     * @var \MageWorx\ShippingRules\Model\Carrier\Method|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $methodMock1;

    /**
     * @var \MageWorx\ShippingRules\Model\Carrier\Method|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $methodMock2;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateMethodFactoryMock;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\Method|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateMethodMock;

    /**
     * @var \MageWorx\ShippingRules\Model\ResourceModel\Rate\CollectionFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateCollectionFactoryMock;

    /**
     * @var \MageWorx\ShippingRules\Model\ResourceModel\Rate\Collection|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateCollectionMock;

    /**
     * @var \MageWorx\ShippingRules\Model\Carrier\Method\Rate|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $rateOneMock;

    /**
     * @var \MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterfaceFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $cartPriceCalcConfigFactoryMock;

    /**
     * @var \MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $cartPriceCalcConfigMock;

    /**
     * @var \MageWorx\ShippingRules\Api\CartPriceCalculationInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $cartPriceCalculationMock;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $methods = \array_merge(
            \get_class_methods(\Magento\Quote\Model\Quote\Address\RateRequest::class),
            ['getAllItems', 'getStoreId']
        );

        $this->rateRequestMock = $this->getMockBuilder(\Magento\Quote\Model\Quote\Address\RateRequest::class)
                                      ->setMethods($methods)
                                      ->disableOriginalConstructor()
                                      ->getMock();

        $this->rateResultFactoryMock = $this->getMockBuilder('\Magento\Shipping\Model\Rate\ResultFactory')
                                            ->disableOriginalConstructor()
                                            ->getMock();

        $this->rateResultMock = $this->getMockBuilder(\Magento\Shipping\Model\Rate\Result::class)
                                     ->disableOriginalConstructor()
                                     ->getMock();

        $this->carrierFactoryMock = $this->getMockBuilder('\MageWorx\ShippingRules\Model\CarrierFactory')
                                         ->disableOriginalConstructor()
                                         ->getMock();

        $this->carrierMock = $this->getMockBuilder(\MageWorx\ShippingRules\Model\Carrier::class)
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->methodMock1 = $this->getMockBuilder(\MageWorx\ShippingRules\Model\Carrier\Method::class)
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->methodMock2 = $this->getMockBuilder(\MageWorx\ShippingRules\Model\Carrier\Method::class)
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->rateMethodFactoryMock = $this->getMockBuilder(
            '\Magento\Quote\Model\Quote\Address\RateResult\MethodFactory'
        )
                                            ->disableOriginalConstructor()
                                            ->getMock();

        $this->rateMethodMock = $this->getMockBuilder(\Magento\Quote\Model\Quote\Address\RateResult\Method::class)
                                     ->disableOriginalConstructor()
                                     ->getMock();

        $this->rateCollectionFactoryMock = $this->getMockBuilder(
            '\MageWorx\ShippingRules\Model\ResourceModel\Rate\CollectionFactory'
        )
                                                ->disableOriginalConstructor()
                                                ->getMock();

        $this->rateCollectionMock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Model\ResourceModel\Rate\Collection::class
        )
                                         ->disableOriginalConstructor()
                                         ->getMock();

        $this->rateOneMock = $this->getMockBuilder(\MageWorx\ShippingRules\Model\Carrier\Method\Rate::class)
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->cartPriceCalcConfigFactoryMock = $this->getMockBuilder(
            '\MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterfaceFactory'
        )
                                                     ->disableOriginalConstructor()
                                                     ->getMock();

        $this->cartPriceCalcConfigMock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterface::class
        )
                                              ->disableOriginalConstructor()
                                              ->getMock();

        $this->cartPriceCalculationMock = $this->getMockBuilder(
            \MageWorx\ShippingRules\Api\CartPriceCalculationInterface::class
        )
                                               ->disableOriginalConstructor()
                                               ->getMock();
        $args                           = [
            'carrierFactory'             => $this->carrierFactoryMock,
            'rateResultFactory'          => $this->rateResultFactoryMock,
            'rateMethodFactory'          => $this->rateMethodFactoryMock,
            'rateCollectionFactory'      => $this->rateCollectionFactoryMock,
            'cartPriceCalcConfigFactory' => $this->cartPriceCalcConfigFactoryMock,
            'cartPriceCalculation'       => $this->cartPriceCalculationMock
        ];

        /** @var \MageWorx\ShippingRules\Model\Carrier\Artificial model */
        $this->model = $this->objectManager->getObject(
            self::TESTED_CLASS_NAME,
            $args
        );
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testCollectRatesWithEmptyMethods()
    {
        $requestItems = [];
        $storeId      = 300;

        $this->rateRequestMock->expects($this->atLeastOnce())
                              ->method('getAllItems')
                              ->willReturn($requestItems);

        $this->rateRequestMock->expects($this->atLeastOnce())
                              ->method('getStoreId')
                              ->willReturn($storeId);

        $this->rateResultFactoryMock->expects($this->atLeastOnce())
                                    ->method('create')
                                    ->willReturn($this->rateResultMock);

        $this->carrierFactoryMock->expects($this->atLeastOnce())
                                 ->method('create')
                                 ->willReturn($this->carrierMock);

        $this->carrierMock->expects($this->atLeastOnce())
                          ->method('load')
                          ->withAnyParameters()
                          ->willReturnSelf();

        $carrierData = [];
        $methods     = [];

        $this->carrierMock->expects($this->atLeastOnce())
                          ->method('getData')
                          ->willReturn($carrierData);

        $this->carrierMock->expects($this->atLeastOnce())
                          ->method('getMethods')
                          ->willReturn($methods);

        $this->model->collectRates($this->rateRequestMock);
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testCollectRates()
    {
        $requestItems = [];
        $storeId      = 300;

        $this->rateRequestMock->expects($this->atLeastOnce())
                              ->method('getAllItems')
                              ->willReturn($requestItems);

        $this->rateRequestMock->expects($this->atLeastOnce())
                              ->method('getStoreId')
                              ->willReturn($storeId);

        $this->rateResultFactoryMock->expects($this->atLeastOnce())
                                    ->method('create')
                                    ->willReturn($this->rateResultMock);

        $this->carrierFactoryMock->expects($this->atLeastOnce())
                                 ->method('create')
                                 ->willReturn($this->carrierMock);

        $this->carrierMock->expects($this->atLeastOnce())
                          ->method('load')
                          ->withAnyParameters()
                          ->willReturnSelf();

        $carrierData = [];
        $methods     = [
            $this->methodMock1,
            $this->methodMock2
        ];

        $this->carrierMock->expects($this->atLeastOnce())
                          ->method('getData')
                          ->willReturn($carrierData);

        $this->carrierMock->expects($this->atLeastOnce())
                          ->method('getMethods')
                          ->willReturn($methods);

        // Method 1 will be skipped as inactive
        $this->methodMock1->expects($this->once())
                          ->method('getActive')
                          ->willReturn(false);

        // The only active method with rates
        $this->methodMock2->expects($this->once())
                          ->method('getActive')
                          ->willReturn(true);

        // Calculate available rates by max priority
        $this->methodMock2->expects($this->atLeast(2))
                          ->method('getMultipleRatesPrice')
                          ->willReturn(MageWorxRate::MULTIPLE_RATES_PRICE_CALCULATION_MAX_PRIORITY);

        $this->rateMethodFactoryMock->expects($this->once())
                                    ->method('create')
                                    ->willReturn($this->rateMethodMock);

        $this->rateCollectionFactoryMock->expects($this->once())
                                        ->method('create')
                                        ->willReturn($this->rateCollectionMock);

        $rates = [
            $this->rateOneMock
        ];

        $this->rateCollectionMock->expects($this->once())
                                 ->method('getItems')
                                 ->willReturn($rates);

        $this->cartPriceCalcConfigFactoryMock->expects($this->once())
                                             ->method('create')
                                             ->willReturn($this->cartPriceCalcConfigMock);

        $basePrice = 0.0;
        $this->cartPriceCalculationMock->expects($this->once())
                                       ->method('calculateBasePrice')
                                       ->willReturn($basePrice);

        $this->model->collectRates($this->rateRequestMock);
    }
}
