<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Model\Attribute\Frontend;

/**
 * Class AvailableShippingMethods
 *
 * Frontend model of the available_shipping_methods product's attribute
 */
class AvailableShippingMethods extends \Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend
{
    /**
     * @param \Magento\Framework\DataObject $object
     * @return mixed|string
     */
    public function getValue(\Magento\Framework\DataObject $object)
    {
        $value = parent::getValue($object);

        return $value;
    }

    /**
     * Get select options in case it's select box and options source is defined.
     * Convert 2 level multiple select array of options to one level to prevent error on frontend.
     *
     * @return array
     */
    public function getSelectOptions()
    {
        $optionsMultiple = parent::getSelectOptions();
        $optionsOneLevel = [];

        foreach ($optionsMultiple as $optionMultiple) {
            if (is_array($optionMultiple['value'])) {
                foreach ($optionMultiple['value'] as $option) {
                    $optionsOneLevel[] = $option;
                }
            } else {
                $optionsOneLevel[] = $optionMultiple;
            }
        }

        return $optionsOneLevel;
    }
}
