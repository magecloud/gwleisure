<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Attribute;

use Magento\Framework\DataObject;
use MageWorx\ShippingRules\Api\Data\RuleAttributeInterface;

/**
 * Attribute blank for any non-real EAV attribute which can be used in the conditions.
 */
class RuleAttributeBlank extends DataObject implements RuleAttributeInterface
{
    /**
     * Attribute unique code
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->getData('code');
    }

    /**
     * Attribute label
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->getData('label');
    }

    /**
     * Input type for validation
     *
     * @return string
     */
    public function getInputType(): string
    {
        return $this->getData('input_type') ?? 'string';
    }

    /**
     * Value type for condition input
     *
     * @return string
     */
    public function getValueType(): string
    {
        return $this->getData('value_type') ?? 'text';
    }

    /**
     * Attribute options
     *
     * @return string|null
     */
    public function getSourceModel(): ?string
    {
        return $this->getData('source_model');
    }

    /**
     * Attribute unique code
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setCode(string $value): RuleAttributeInterface
    {
        return $this->setData('code');
    }

    /**
     * Attribute label
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setLabel(string $value): RuleAttributeInterface
    {
        return $this->setData('label');
    }

    /**
     * Attribute input type (for validation)
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setInputType(string $value): RuleAttributeInterface
    {
        return $this->setData('input_type');
    }

    /**
     * Attribute value type (for condition input)
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setValueType(string $value): RuleAttributeInterface
    {
        return $this->setData('value_type');
    }

    /**
     * Attribute options
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setSourceModel(string $value): RuleAttributeInterface
    {
        return $this->setData('source_model');
    }
}
