<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Action\Rate;

class PerXUnitsOfWeightAfterY extends AbstractRate
{
    const PER_X_UNITS_OF_WEIGHT_KEY   = 'x';
    const AFTER_Y_UNITS_OF_WEIGHT_KEY = 'y';

    /**
     * Calculate fixed amount
     *
     * @return AbstractRate
     */
    protected function fixed(): PerXUnitsOfWeightAfterY
    {
        $weight            = $this->getWeight();
        $amountValue       = $this->getAmountValue();
        $resultAmountValue = $amountValue * $weight;
        $this->_setAmountValue($resultAmountValue);

        return $this;
    }

    /**
     * Get all items row weight
     * Note: $item->getRowWeight() works very strangely, use a regular weight & qty instead
     * Note: Per X units of weight after Y units of weight
     *
     * @return float
     */
    protected function getWeight(): float
    {
        $condition = $this->getCondition();
        $weight    = 0;
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($this->validItems as $item) {
            $qty = (float)$item->getQty();
            if ($item->getParentItem() && $item->getParentItem()->getQty()) {
                $qty *= (float)$item->getParentItem()->getQty();
            }
            $weight += (float)$item->getWeight() * $qty;
        }

        $weight           -= $condition[static::AFTER_Y_UNITS_OF_WEIGHT_KEY] ?? 0;
        $perWeightX       = (float)($condition[static::PER_X_UNITS_OF_WEIGHT_KEY] ?? 1);
        $perWeightX       = $perWeightX != 0 ? abs($perWeightX) : 1;
        $resultMultiplier = ceil((float)$weight / (float)$perWeightX);

        return $resultMultiplier > 0 ? $resultMultiplier : 0;
    }

    /**
     * Calculate percent of amount
     *
     * @return AbstractRate
     */
    protected function percent(): PerXUnitsOfWeightAfterY
    {
        $rate        = $this->getRate();
        $amountValue = $this->getAmountValue() ? $this->getAmountValue() / 100 : 0;
        $amount      = (float)$rate->getPrice() * $amountValue;

        $weight            = $this->getWeight();
        $resultAmountValue = $amount * $weight;
        $this->_setAmountValue($resultAmountValue);

        return $this;
    }
}
