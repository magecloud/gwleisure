<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Model\Rule\Action\Rate;

/**
 * Class PerXProducts
 */
class PerXProducts extends AbstractRate
{
    /**
     * Calculate fixed amount
     *
     * @return AbstractRate
     */
    protected function fixed()
    {
        $itemsCount  = $this->getValidItemsCount();
        $amountValue = $this->getAmountValue();
        if ($itemsCount > 0) {
            $resultAmountValue = $amountValue * $itemsCount;
        } else {
            if ($this->getApplyMethod() === 'overwrite') {
                $resultAmountValue = $this->getRate()->getPrice();
            } else {
                $resultAmountValue = 0;
            }
        }

        $this->_setAmountValue($resultAmountValue);

        return $this;
    }

    /**
     * Get valid items count using multiplier
     *
     * @return float
     */
    protected function getValidItemsCount(): float
    {
        $baseItemsCount = 0;
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($this->validItems as $item) {
            if ($item->getParentItem()) {
                $qty = (float)$item->getQty() * (float)$item->getParentItem()->getQty();
            } else {
                $qty = (float)$item->getQty();
            }
            $baseItemsCount += $qty;
        }

        if ($baseItemsCount <= 0) {
            return 0;
        }

        $condition = (float)$this->getCondition();
        $perXItems = abs($condition);
        if ($perXItems <= 0) {
            return 0;
        }

        $resultMultiplier = ceil((float)$baseItemsCount / (float)$perXItems);

        return $resultMultiplier > 0 ? $resultMultiplier : 0;
    }

    /**
     * Calculate percent of amount
     *
     * @return AbstractRate
     */
    protected function percent()
    {
        $amountValue = $this->getAmountValue() ? $this->getAmountValue() / 100 : 0;
        $price       = 0;
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($this->validItems as $item) {
            if ($item->getParentItem()) {
                $price += (float)$item->getParentItem()->getRowTotal();
            } else {
                $price += (float)$item->getRowTotal();
            }
        }

        $itemsCount = $this->getValidItemsCount();
        $amount     = $price * ($amountValue * $itemsCount);

        $this->_setAmountValue($amount);

        return $this;
    }
}
