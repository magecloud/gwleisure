<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters;

/**
 * Disable customer attributes which has unsupported input type
 */
class ByInputType implements \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface
{
    /**
     * Check is attribute disabled for conditions
     *
     * @param \Magento\Eav\Api\Data\AttributeInterface $attribute
     * @return bool
     */
    public function check(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool
    {
        if (in_array($attribute->getFrontendInput(), ['file', 'image'])) {
            return false;
        }

        return true;
    }
}
