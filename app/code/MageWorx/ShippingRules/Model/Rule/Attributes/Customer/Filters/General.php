<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters;

/**
 * Disable system customer attributes and service attributes.
 * They must not be used in conditions.
 */
class General implements \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface
{
    /**
     * @var string[]
     */
    protected $disabledAttributes = [];

    /**
     * @param array $disabledAttributes
     */
    public function __construct(
        array $disabledAttributes = []
    ) {
        $this->disabledAttributes = $disabledAttributes;
    }

    /**
     * Check is attribute disabled for conditions
     *
     * @param \Magento\Eav\Api\Data\AttributeInterface $attribute
     * @return bool
     */
    public function check(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool
    {
        $disabledAttributes = $this->getDisabledAttributesCodes();
        if (in_array($attribute->getAttributeCode(), $disabledAttributes)) {
            return false;
        }

        return true;
    }

    /**
     * @return string[]
     */
    public function getDisabledAttributesCodes(): array
    {
        return $this->disabledAttributes;
    }
}
