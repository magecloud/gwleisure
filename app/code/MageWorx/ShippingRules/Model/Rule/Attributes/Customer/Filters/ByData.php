<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters;

use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;

/**
 * Disable customer attributes which has no label.
 */
class ByData implements \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface
{
    /**
     * Skip attributes without label
     *
     * @param AttributeInterface|AbstractAttribute $attribute
     * @return bool
     */
    public function check(AttributeInterface $attribute): bool
    {
        $getLabelMethodAvailable = $attribute instanceof AbstractAttribute
            || method_exists($attribute,'getFrontendLabel');
        $labelExists = $getLabelMethodAvailable && $attribute->getFrontendLabel();
        if (!$labelExists) {
            return false;
        }

        return true;
    }
}
