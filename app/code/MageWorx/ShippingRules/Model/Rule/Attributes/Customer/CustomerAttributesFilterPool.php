<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Attributes\Customer;

use \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface;

/**
 * Pool of filters for customer attributes used in the shipping rules condition.
 * Filters check the attribute one by one and returns false in case attribute is unavailable for condition.
 * More filters can be added through di.xml
 */
class CustomerAttributesFilterPool implements \MageWorx\ShippingRules\Api\CustomerAttributesFilterPoolInterface
{
    /**
     * @var array|CustomerAttributesFilterInterface[]
     */
    private $pool = [];

    /**
     * @param array|CustomerAttributesFilterInterface[] $pool
     */
    public function __construct(
        array $pool = []
    ) {
        $this->pool = $pool;
    }

    /**
     * @inheritDoc
     */
    public function isAvailable(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool
    {
        foreach ($this->getPool() as $filter) {
            if (!$filter->check($attribute)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get pool of filters
     *
     * @return array|CustomerAttributesFilterInterface[]
     */
    public function getPool(): array
    {
        return $this->pool;
    }
}
