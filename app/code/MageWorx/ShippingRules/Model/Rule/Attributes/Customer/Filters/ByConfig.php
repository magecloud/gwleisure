<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Attributes\Customer\Filters;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;

/**
 * Disable customer attributes which were disabled in the module configuration.
 */
class ByConfig implements \MageWorx\ShippingRules\Api\CustomerAttributesFilterInterface
{
    const XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES = 'mageworx_shippingrules/validation/customer/hidden_attributes';

    /**
     * @var array|null
     */
    protected $disabledAttributes;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param LoggerInterface $logger
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface      $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger      = $logger;
    }

    /**
     * Check is attribute disabled for conditions
     *
     * @param \Magento\Eav\Api\Data\AttributeInterface $attribute
     * @return bool
     */
    public function check(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool
    {
        $disabledAttributes = $this->getDisabledAttributes();
        $attributeCode      = $attribute->getAttributeCode();
        if (in_array($attributeCode, $disabledAttributes)) {
            return false;
        }

        return true;
    }

    /**
     * Get disabled attributes from config
     *
     * @return array
     */
    public function getDisabledAttributes(): array
    {
        if (!is_array($this->disabledAttributes)) {
            $disabledAttributes = $this->scopeConfig->getValue(
                static::XML_PATH_DISABLED_CUSTOMER_ATTRIBUTES
            );

            if ($disabledAttributes === null) {
                $disabledAttributes = [];
            } elseif (!is_array($disabledAttributes)) {
                $disabledAttributes = explode(',', $disabledAttributes);
                if ($disabledAttributes === false) {
                    $this->logger->error(__('Unable to explode attributes string from config.'));
                    $disabledAttributes = [];
                }
            }

            $this->disabledAttributes = $disabledAttributes;
        }

        return $this->disabledAttributes;
    }
}
