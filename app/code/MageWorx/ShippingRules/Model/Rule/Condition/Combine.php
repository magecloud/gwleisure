<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Model\Rule\Condition;

/**
 * Class Combine
 */
class Combine extends \Magento\SalesRule\Model\Rule\Condition\Combine
{
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var Customer
     */
    protected $conditionCustomer;

    /**
     * Combine constructor.
     *
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param Address $conditionAddress
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \MageWorx\ShippingRules\Model\Rule\Condition\Address $conditionAddress,
        \MageWorx\ShippingRules\Model\Rule\Condition\Customer $conditionCustomer,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        array $data = []
    ) {
        $this->dataObjectFactory = $dataObjectFactory;
        parent::__construct($context, $eventManager, $conditionAddress, $data);
        $this->conditionCustomer = $conditionCustomer;
        $this->setType('MageWorx\ShippingRules\Model\Rule\Condition\Combine');
    }

    /**
     * Get new child select options
     *
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $addressAttributes = $this->_conditionAddress->loadAttributeOptions()->getAttributeOption();
        $cartAttributes        = [];
        foreach ($addressAttributes as $code => $label) {
            $cartAttributes[] = [
                'value' => 'MageWorx\ShippingRules\Model\Rule\Condition\Address|' . $code,
                'label' => $label,
            ];
        }

        $customerAttributesRaw = $this->conditionCustomer->loadAttributeOptions()->getAttributeOption();
        $customerAttributes        = [];
        foreach ($customerAttributesRaw as $code => $label) {
            $customerAttributes[] = [
                'value' => 'MageWorx\ShippingRules\Model\Rule\Condition\Customer|' . $code,
                'label' => $label,
            ];
        }

        $conditions = [
            [
                'value' => '',
                'label' => __('Please choose a condition to add.'),
            ],
            [
                'value' => 'MageWorx\ShippingRules\Model\Rule\Condition\Product\Found',
                'label' => __('Product attribute combination'),
            ],
            [
                'value' => 'MageWorx\ShippingRules\Model\Rule\Condition\Product\Subselect',
                'label' => __('Products subselection'),
            ],
            [
                'value' => 'MageWorx\ShippingRules\Model\Rule\Condition\Combine',
                'label' => __('Conditions combination'),
            ],
            [
                'value' => $cartAttributes,
                'label' => __('Cart Attribute'),
            ],
            [
                'value' => $customerAttributes,
                'label' => __('Customer Attributes'),
            ]
        ];

        /** @var \Magento\Framework\DataObject $additional */
        $additional = $this->dataObjectFactory->create();

        $this->_eventManager->dispatch(
            'salesrule_rule_condition_combine',
            ['additional' => $additional]
        );
        $this->_eventManager->dispatch(
            'shipping_rule_condition_combine',
            ['additional' => $additional]
        );

        $additionalConditions = $additional->getConditions();
        if ($additionalConditions) {
            $conditions = array_merge_recursive(
                $conditions,
                $additionalConditions
            );
        }

        return $conditions;
    }
}
