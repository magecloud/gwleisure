<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Rule\Condition;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Eav\Api\Data\AttributeInterface;

class Customer extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer|mixed
     */
    protected $customerResource;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @var \MageWorx\ShippingRules\Api\CustomerResolverInterface
     */
    protected $customerResolver;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var \MageWorx\ShippingRules\Api\CustomerAttributesFilterPoolInterface
     */
    protected $customerAttributesFilterPool;

    /**
     * @var \MageWorx\ShippingRules\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Rule\Model\Condition\Context                             $context,
        \Magento\Customer\Model\ResourceModel\Customer                    $customerResource,
        \Magento\Eav\Model\Config                                         $eavConfig,
        \MageWorx\ShippingRules\Api\CustomerResolverInterface             $customerResolver,
        \Magento\Framework\DataObjectFactory                              $dataObjectFactory,
        \MageWorx\ShippingRules\Api\CustomerAttributesFilterPoolInterface $customerAttributesFilterPool,
        \MageWorx\ShippingRules\Helper\Data                               $helper,
        array                                                             $data = []
    ) {
        $this->customerResource             = $customerResource;
        $this->eavConfig                    = $eavConfig;
        $this->customerResolver             = $customerResolver;
        $this->dataObjectFactory            = $dataObjectFactory;
        $this->customerAttributesFilterPool = $customerAttributesFilterPool;
        $this->helper                       = $helper;
        parent::__construct($context, $data);
        $this->setType(\MageWorx\ShippingRules\Model\Rule\Condition\Customer::class);
    }

    /**
     * Validate model.
     *
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        /** @var \Magento\Customer\Model\Data\Customer|CustomerInterface $customer */
        $customer = $this->customerResolver->resolve($model);

        if (!$customer->getId() && $this->helper->isIgnoreCustomerConditionsForVisitors()) {
            return true;
        }

        $attributes = $customer->__toArray();

        $attributeValue = isset($attributes[$this->getAttribute()]) ? $attributes[$this->getAttribute()] : null;
        if ($attributeValue === null && isset($attributes['custom_attributes'][$this->getAttribute()])) {
            $attributeValue = isset($attributes['custom_attributes'][$this->getAttribute()]['value'])
                ? $attributes['custom_attributes'][$this->getAttribute()]['value']
                : null;
        }

        if ($attributeValue === null && isset($attributes['extension_attributes'][$this->getAttribute()])) {
            $attributeValue = $attributes['extension_attributes'][$this->getAttribute()];
        }

        if ($this->getOperator() === '<=>') { // is undefined operator
            if ($attributeValue === null) {
                return true;
            }

            return false;
        }

        return $this->validateAttribute($attributeValue);
    }

    /**
     * Get inherited conditions selectors
     *
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $attributes = $this->loadAttributeOptions()->getAttributeOption();
        $conditions = [];
        foreach ($attributes as $code => $label) {
            $conditions[] = ['value' => $this->getType() . '|' . $code, 'label' => $label];
        }

        return $conditions;
    }

    /**
     * Load condition options for castomer attributes
     *
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $customerAttributes = $this->customerResource->loadAllAttributes()->getAttributesByCode();

        $attributes = [];

        /** @var  $attribute */
        foreach ($customerAttributes as $attribute) {
            if (!$this->isAttributeAvailable($attribute)) {
                continue;
            }

            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }

        asort($attributes);
        $this->setAttributeOption($attributes);

        return $this;
    }

    /**
     * Retrieve attribute object
     *
     * @return \Magento\Eav\Model\Entity\Attribute|\Magento\Eav\Model\Entity\Attribute\AbstractAttribute
     */
    public function getAttributeObject()
    {
        return $this->eavConfig->getAttribute('customer', $this->getAttribute());
    }

    /**
     * Retrieve select option values
     *
     * @return array
     */
    public function getValueSelectOptions()
    {
        if (!$this->getData('value_select_options') && is_object($this->getAttributeObject())) {
            if ($this->getAttributeObject()->usesSource()) {
                if ($this->getAttributeObject()->getFrontendInput() == 'multiselect') {
                    $addEmptyOption = false;
                } else {
                    $addEmptyOption = true;
                }
                $optionsArr = $this->getAttributeObject()->getSource()->getAllOptions($addEmptyOption);
                $this->setData('value_select_options', $optionsArr);
            }
        }

        return $this->getData('value_select_options');
    }

    /**
     * Get input type for attribute operators.
     *
     * @return string
     */
    public function getInputType()
    {
        $attribute = $this->getAttributeObject();
        if (!is_object($attribute)) {
            return 'string';
        }
        $input = $attribute->getFrontendInput();
        switch ($input) {
            case 'boolean':
                return 'select';
            case 'select':
            case 'multiselect':
            case 'date':
                return $input;
            case 'text':
                if ($this->isNumericAttributeType($attribute)) {
                    return 'numeric';
                }
            default:
                return 'string';
        }
    }

    /**
     * @param AttributeInterface $attribute
     * @return bool
     */
    protected function isNumericAttributeType(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool
    {
        $validationRules = $attribute->getValidationRules();
        if (!empty($validationRules)
            && isset($validationRules['input_validation'])
            && $validationRules['input_validation'] === 'numeric'
        ) {
            return true;
        }

        return false;
    }

    /**
     * Get attribute value input element type
     *
     * @return string
     */
    public function getValueElementType(): string
    {
        $attribute = $this->getAttributeObject();
        if (!is_object($attribute)) {
            return 'text';
        }
        $input = $attribute->getFrontendInput();
        switch ($input) {
            case 'boolean':
                return 'select';
            case 'select':
            case 'multiselect':
            case 'date':
                return $input;
            default:
                return 'text';
        }
    }

    /**
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     */
    public function getAttributeElement(): \Magento\Framework\Data\Form\Element\AbstractElement
    {
        return parent::getAttributeElement()->setShowAsText(true);
    }

    /**
     * Customize default operator input by type mapper for some types.
     *
     * @return array
     */
    public function getDefaultOperatorInputByType()
    {
        if (null === $this->_defaultOperatorInputByType) {
            parent::getDefaultOperatorInputByType();
            $this->_defaultOperatorInputByType['numeric']     = ['==', '!=', '>=', '>', '<=', '<', '<=>'];
            $this->_defaultOperatorInputByType['string']      = ['==', '!=', '{}', '!{}', '<=>'];
            $this->_defaultOperatorInputByType['multiselect'] = ['==', '!=', '[]', '![]', '<=>'];
            $this->_defaultOperatorInputByType['date']        = ['==', '>=', '<=', '<=>'];
        }

        return $this->_defaultOperatorInputByType;
    }

    /**
     * Default operator options getter.
     *
     * Provides all possible operator options.
     *
     * @return array
     */
    public function getDefaultOperatorOptions()
    {
        if (null === $this->_defaultOperatorOptions) {
            $this->_defaultOperatorOptions = parent::getDefaultOperatorOptions();

            $this->_defaultOperatorOptions['[]']  = __('contains');
            $this->_defaultOperatorOptions['![]'] = __('does not contains');
        }

        return $this->_defaultOperatorOptions;
    }

    /**
     * Check if attribute value should be explicit
     *
     * @return bool
     */
    public function getExplicitApply()
    {
        if (is_object($this->getAttributeObject())) {
            switch ($this->getAttributeObject()->getFrontendInput()) {
                case 'date':
                    return true;
            }
        }

        return false;
    }

    /**
     * Check: is customer attribute disabled
     *
     * @param AttributeInterface $attribute
     * @return bool
     */
    public function isAttributeAvailable(AttributeInterface $attribute): bool
    {
        return $this->customerAttributesFilterPool->isAvailable($attribute);
    }
}
