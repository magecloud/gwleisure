<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Config\Source;

/**
 * Create source models dynamically
 */
class SourceModelFactory implements \MageWorx\ShippingRules\Api\SourceModelFactoryInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }
    /**
     * @inheritDoc
     */
    public function create(string $className, ...$arguments): \Magento\Framework\Data\OptionSourceInterface
    {
        return $this->objectManager->create($className, $arguments);
    }
}
