<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Config\Source\Attributes;

use Magento\Framework\Data\OptionSourceInterface;

class Customer implements OptionSourceInterface
{
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $customerResource;

    /**
     * @var \MageWorx\ShippingRules\Api\CustomerAttributesFilterPoolInterface
     */
    protected $customerAttributesFilterPool;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @param \Magento\Eav\Model\Config $eavConfig
     */
    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer                    $customerResource,
        \MageWorx\ShippingRules\Api\CustomerAttributesFilterPoolInterface $customerAttributesFilterPool
    ) {
        $this->customerResource             = $customerResource;
        $this->customerAttributesFilterPool = $customerAttributesFilterPool;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray(): array
    {
        if (empty($this->options)) {
            $customerAttributes = $this->customerResource->loadAllAttributes()->getAttributesByCode();

            $attributes = [];
            /** @var  $attribute */
            foreach ($customerAttributes as $attribute) {
                if (!$this->customerAttributesFilterPool->isAvailable($attribute)) {
                    continue;
                }

                $attributes[] = [
                    'label' => $attribute->getFrontendLabel(),
                    'value' => $attribute->getAttributeCode()
                ];
            }

            $this->options = $attributes;
        }

        return $this->options;
    }
}
