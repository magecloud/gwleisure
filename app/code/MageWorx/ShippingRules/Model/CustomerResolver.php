<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Quote\Api\Data\CartInterface;
use Psr\Log\LoggerInterface;

/**
 * Resolve customer from quote, quote address or from session object.
 * Used in the shipping rules conditions by customer.
 */
class CustomerResolver implements \MageWorx\ShippingRules\Api\CustomerResolverInterface
{
    /**
     * @var SessionManagerInterface
     */
    protected $session;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CustomerInterfaceFactory
     */
    protected $customerFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param SessionManagerInterface $session
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerInterfaceFactory $customerFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        SessionManagerInterface     $session,
        CustomerRepositoryInterface $customerRepository,
        CustomerInterfaceFactory    $customerFactory,
        LoggerInterface             $logger
    ) {
        $this->session            = $session;
        $this->customerRepository = $customerRepository;
        $this->customerFactory    = $customerFactory;
        $this->logger             = $logger;
    }

    /**
     * @inheritDoc
     */
    public function resolve($model = null): CustomerInterface
    {
        if ($model === null) {
            $customer = $this->resolveFromSession();
        } elseif ($model->getCustomer() && $model->getCustomer() instanceof CustomerInterface) {
            $customer = $model->getCustomer();
        } elseif ($model->getQuote() && $model->getQuote() instanceof CartInterface) {
            $customer = $this->resolveFromCart($model->getQuote());
        } else {
            $customer = $this->customerFactory->create();
        }

        return $customer;
    }

    /**
     * Resolve current customer using session
     *
     * @return CustomerInterface
     */
    public function resolveFromSession(): CustomerInterface
    {
        $customerId = (int)$this->session->getCustomerId();
        if ($customerId) {
            $customer = $this->getCustomerById($customerId);
        } else {
            $customer = $this->customerFactory->create();
        }

        return $customer;
    }

    /**
     * Get current customer from cart object
     *
     * @param CartInterface $quote
     * @return CustomerInterface
     */
    public function resolveFromCart(CartInterface $quote): CustomerInterface
    {
        $customerId = (int)$quote->getCustomerId();
        if ($customerId) {
            $customer = $this->getCustomerById($customerId);
        } else {
            $customer = $this->customerFactory->create();
        }

        return $customer;
    }

    /**
     * @param int $customerId
     * @return CustomerInterface
     */
    private function getCustomerById(int $customerId): CustomerInterface
    {
        try {
            $customer = $this->customerRepository->getById($customerId);
        } catch (NoSuchEntityException|LocalizedException $exception) {
            $this->logger->error($exception->getLogMessage());
            $customer = $this->customerFactory->create();
        }

        return $customer;
    }
}
