<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Event\ManagerInterface as EventManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Profiler;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateRequest as QuoteAddressRateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory as RateResultErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method as QuoteAddressRateResultMethod;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory as RateResultMethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory as RateResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\StoreResolver;
use MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterfaceFactory as CartPriceCalculationConfigFactory;
use MageWorx\ShippingRules\Api\CartPriceCalculationInterface as CartPriceCalculation;
use MageWorx\ShippingRules\Helper\Data as ShippingRulesHelper;
use MageWorx\ShippingRules\Model\Carrier as MageWorxCarrierModel;
use MageWorx\ShippingRules\Model\Carrier\Method as MageWorxMethodModel;
use MageWorx\ShippingRules\Model\Carrier\Method\Rate as MageWorxRate;
use MageWorx\ShippingRules\Model\CarrierFactory as MageWorxCarrierFactory;
use MageWorx\ShippingRules\Model\ResourceModel\Carrier\CollectionFactory as MageWorxCarrierCollectionFactory;
use MageWorx\ShippingRules\Model\ResourceModel\Rate\CollectionFactory as MageWorxRateCollectionFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Artificial
 *
 * Describes all carriers created by MageWorx ShippingRules (custom shipping carriers/methods).
 * Include rates validation and implementation.
 */
class Artificial extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = null;

    /**
     * @var MageWorxCarrierFactory
     */
    protected $carrierFactory;

    /**
     * @var MageWorxCarrierCollectionFactory
     */
    protected $carrierCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var RateRequest
     */
    protected $request;

    /**
     * @var CartPriceCalculationConfigFactory
     */
    protected $cartPriceCalcConfigFactory;

    /**
     * @var MageWorxRateCollectionFactory
     */
    protected $rateCollectionFactory;
    /**
     * @var StoreResolver
     */
    protected $storeResolver;

    /**
     * @var RateResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var RateResultMethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * @var ShippingRulesHelper
     */
    protected $helper;

    /**
     * @var CartPriceCalculation
     */
    protected $cartPriceCalculation;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param RateResultErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param RateResultFactory $rateResultFactory
     * @param RateResultMethodFactory $rateMethodFactory
     * @param MageWorxCarrierFactory $carrierFactory
     * @param MageWorxCarrierCollectionFactory $carrierCollectionFactory
     * @param MageWorxRateCollectionFactory $rateCollectionFactory
     * @param StoreManagerInterface $storeManager
     * @param StoreResolver $storeResolver
     * @param State $state
     * @param EventManagerInterface $eventManager
     * @param ShippingRulesHelper $helper
     * @param CartPriceCalculationConfigFactory $cartPriceCalcConfigFactory
     * @param CartPriceCalculation $cartPriceCalculation
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface              $scopeConfig,
        RateResultErrorFactory            $rateErrorFactory,
        LoggerInterface                   $logger,
        RateResultFactory                 $rateResultFactory,
        RateResultMethodFactory           $rateMethodFactory,
        MageWorxCarrierFactory            $carrierFactory,
        MageWorxCarrierCollectionFactory  $carrierCollectionFactory,
        MageWorxRateCollectionFactory     $rateCollectionFactory,
        StoreManagerInterface             $storeManager,
        StoreResolver                     $storeResolver,
        State                             $state,
        EventManagerInterface             $eventManager,
        ShippingRulesHelper               $helper,
        CartPriceCalculationConfigFactory $cartPriceCalcConfigFactory,
        CartPriceCalculation              $cartPriceCalculation,
        array                             $data = []
    ) {
        $this->rateResultFactory          = $rateResultFactory;
        $this->rateMethodFactory          = $rateMethodFactory;
        $this->carrierFactory             = $carrierFactory;
        $this->carrierCollectionFactory   = $carrierCollectionFactory;
        $this->rateCollectionFactory      = $rateCollectionFactory;
        $this->storeManager               = $storeManager;
        $this->storeResolver              = $storeResolver;
        $this->appState                   = $state;
        $this->eventManager               = $eventManager;
        $this->helper                     = $helper;
        $this->cartPriceCalcConfigFactory = $cartPriceCalcConfigFactory;
        $this->cartPriceCalculation       = $cartPriceCalculation;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Set _code when set id
     *
     * @param string $id
     * @return $this
     */
    public function setId(string $id): Artificial
    {
        $this->setData('id', $id);
        if ($this->_code === null) {
            $this->_code = $id;
        }

        return $this;
    }

    /**
     * @param RateRequest $request
     * @return Result
     * @throws NoSuchEntityException
     */
    public function collectRates(RateRequest $request): Result
    {
        $this->prepareRequest($request);
        $this->setRequest($request);

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();

        /** @var MageWorxCarrierModel $carrier */
        $carrier = $this->findCarrier();
        if (!$carrier) {
            return $result;
        }

        $this->addData($carrier->getData());
        $this->_code = $carrier->getData('carrier_code');

        $storeId = $request->getStoreId();
        $methods = $carrier->getMethods($storeId);
        if (empty($methods)) {
            return $result;
        }

        /** @var MageWorxMethodModel $methodData */
        foreach ($methods as $methodData) {
            if (!$methodData->getActive()) {
                continue;
            }

            /** @var QuoteAddressRateResultMethod $method */
            $method = $this->rateMethodFactory->create();
            $method->setCarrier($this->getId());
            $method->setCarrierTitle($carrier->getTitle());
            $method->setCarrierSortOrder($carrier->getSortOrder());
            $method->setMethod($methodData->getData('code'));
            $method->setCost($methodData->getData('cost'));
            $method = $this->applyRates($method, $methodData);

            if ($method) {
                if ($methodData->getAllowFreeShipping() && $request->getFreeShipping() === true) {
                    $method->setPrice('0.00');
                }

                if ($methodData->getDescription()) {
                    $method->setData('method_description', $methodData->getDescription());
                }
                $result->append($method);
            }
        }

        return $result;
    }

    /**
     * Find corresponding carrier in the collection
     *
     * @return MageWorxCarrierModel|null
     */
    protected function findCarrier(): ?MageWorxCarrierModel
    {
        $carrier = $this->carrierFactory
            ->create()
            ->load($this->getData('id'), 'carrier_code');

        return $carrier;
    }

    /**
     * @param QuoteAddressRateResultMethod $method
     * @param MageWorxMethodModel $methodData
     * @return QuoteAddressRateResultMethod|null
     * @throws NoSuchEntityException
     * @throws LocalizedException
     * @throws \Zend_Db_Select_Exception
     */
    protected function applyRates(
        QuoteAddressRateResultMethod $method,
        MageWorxMethodModel          $methodData
    ): ?QuoteAddressRateResultMethod {
        $disableMethodWithoutValidRates = $methodData->getDisabledWithoutValidRates();

        $request = $this->getRequest();
        $rates   = $this->getSuitableRatesAccordingRequest($request, $methodData);
        if (empty($rates)) {
            $rates = [];
        }

        $method->setMethodTitle($methodData->getData('title'));
        $method->setPrice($methodData->getData('price'));

        if ($rates) {
            $filteredRates = $this->filterRatesBeforeApply($rates, $request, $methodData);
            /** @var MageWorxRate $validRate */
            foreach ($filteredRates as $validRate) {
                $method = $validRate->applyRateToMethod($method, $request, $methodData);
            }
        } elseif ($disableMethodWithoutValidRates) {
            return null;
        }

        if ($methodData->isNeedToDisplayEstimatedDeliveryTime()) {
            $titleWithDate = $method->getMethodTitle() .
                $methodData->getEstimatedDeliveryTimeMessageFormatted(' (', ')');
            $method->setMethodTitle($titleWithDate);
        }

        return $method;
    }

    /**
     * @return RateRequest
     */
    protected function getRequest(): RateRequest
    {
        return $this->request;
    }

    /**
     * @param RateRequest $request
     * @return $this
     */
    protected function setRequest(RateRequest $request): Artificial
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Find all suitable rates for current method ($methodData argument) according request
     *
     *
     * @param QuoteAddressRateRequest $request
     * @param MageWorxMethodModel $methodData
     * @return \Magento\Framework\DataObject[]|\MageWorx\ShippingRules\Api\Data\RateInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws NoSuchEntityException
     * @throws \Zend_Db_Select_Exception
     *
     * @note Since your rate doesn't work (not applied/applied incorrect rate) start debugging from here.
     *
     */
    protected function getSuitableRatesAccordingRequest(
        QuoteAddressRateRequest $request,
        MageWorxMethodModel     $methodData
    ): array {
        Profiler::start('load_rates_collection_for_method_' . $methodData->getCode());

        /** @var \MageWorx\ShippingRules\Model\ResourceModel\Rate\Collection $ratesCollection */
        $ratesCollection = $this->rateCollectionFactory->create();

        // General filters should always present
        $ratesCollection->addStoreFilter($request->getStoreId());
        $ratesCollection->addFieldToFilter('active', 1);
        $ratesCollection->addFieldToFilter('method_code', $methodData->getCode());

        // Filters for validate request
        $ratesCollection->addDestinationZipCodeFilters((string)$request->getDestPostcode());
        $ratesCollection->addDestinationCountryFilter($request->getDestCountryId());

        $actualPrice = $this->getPriceForRequestCalculation($request, $methodData);
        $ratesCollection->addPriceFilter($actualPrice);

        $ratesCollection->addWeightFilter($request->getPackageWeight());
        $ratesCollection->addQtyFilter($request->getPackageQty());

        if ($request->getDestRegionId()) {
            $ratesCollection->addDestinationRegionIdFilter($request->getDestRegionId());
        } elseif ($request->getDestRegionCode()) {
            $ratesCollection->addDestinationRegionFilter($request->getDestRegionCode());
        } else {
            $ratesCollection->addDestinationRegionIdFilter('');
            $ratesCollection->addDestinationRegionFilter('');
        }

        $this->eventManager->dispatch(
            'mageworx_suitable_rates_collection_load_before',
            [
                'rates_collection' => $ratesCollection,
                'method'           => $methodData,
                'request'          => $request,
            ]
        );

        if ($this->appState->getMode() == State::MODE_DEVELOPER && $this->helper->isSQLLogsEnabled()) {
            $sqlDump = $ratesCollection->getSelectSql(true);
            $this->_logger->log(100, $sqlDump);
        }

        $rates = $ratesCollection->getItems();

        Profiler::stop('load_rates_collection_for_method_' . $methodData->getCode());

        return $rates;
    }

    /**
     * Detect most suitable rate according to the rate's setting
     *
     * @param array $rates
     * @param RateRequest $request
     * @param Method $methodData
     * @return array
     * @throws NoSuchEntityException
     */
    protected function filterRatesBeforeApply(
        array               $rates,
        RateRequest         $request,
        MageWorxMethodModel $methodData
    ): array {
        if (!$rates) {
            return $rates;
        }

        if ($methodData->getMultipleRatesPrice() !== -1) {
            $multipleRatesCalculationType = $methodData->getMultipleRatesPrice();
        } else {
            $multipleRatesCalculationType = $this->storeManager
                ->getStore()
                ->getConfig('mageworx_shippingrules/main/multiple_rates_price');
        }

        switch ($multipleRatesCalculationType) {
            case MageWorxRate::MULTIPLE_RATES_PRICE_CALCULATION_MAX_PRIORITY:
                $resultRate = $this->getRateWithMaxPriority($rates);
                break;
            case MageWorxRate::MULTIPLE_RATES_PRICE_CALCULATION_MAX_PRICE:
                $resultRate = $this->getRateWithMaxPrice($rates, $request, $methodData);
                break;
            case MageWorxRate::MULTIPLE_RATES_PRICE_CALCULATION_MIN_PRICE:
                $resultRate = $this->getRateWithMinPrice($rates, $request, $methodData);
                break;
            case MageWorxRate::MULTIPLE_RATES_PRICE_CALCULATION_SUM_UP:
            default:
                return $rates;
        }

        $resultRates = [$resultRate->getId() => $resultRate];

        return $resultRates;
    }

    /**
     * Find rate with max priority in array of rates
     *
     * @param array $rates
     * @return MageWorxRate
     */
    protected function getRateWithMaxPriority(array $rates): MageWorxRate
    {
        /** @var MageWorxRate $currentRate */
        /** @var MageWorxRate $rate */
        foreach ($rates as $currentRate) {
            if (!isset($rate) || $rate->getPriority() <= $currentRate->getPriority()) {
                $rate = $currentRate;
            }
        }

        return $rate;
    }

    /**
     * Find rate with max price in array of rates
     *
     * @param array $rates
     * @return MageWorxRate
     */
    protected function getRateWithMaxPrice(
        array               $rates,
        RateRequest         $request,
        MageWorxMethodModel $methodData
    ): MageWorxRate {
        /** @var MageWorxRate $currentRate */
        /** @var MageWorxRate $rate */
        $actualRateCalculatedPrice = 0;
        foreach ($rates as $currentRate) {
            $currentRatePrice = $currentRate->getCalculatedPrice($request, $methodData);
            if (!isset($rate) || $actualRateCalculatedPrice <= $currentRatePrice) {
                $rate                      = $currentRate;
                $actualRateCalculatedPrice = $currentRatePrice;
            }
        }

        return $rate;
    }

    /**
     * Find rate with min price in array of rates
     *
     * @param array $rates
     * @return MageWorxRate
     */
    protected function getRateWithMinPrice(
        array               $rates,
        RateRequest         $request,
        MageWorxMethodModel $methodData
    ): MageWorxRate {
        /** @var MageWorxRate $currentRate */
        /** @var MageWorxRate $rate */
        $actualRateCalculatedPrice = 0;
        foreach ($rates as $currentRate) {
            $currentRatePrice = $currentRate->getCalculatedPrice($request, $methodData);
            if (!isset($rate) || $actualRateCalculatedPrice >= $currentRatePrice) {
                $rate                      = $currentRate;
                $actualRateCalculatedPrice = $currentRatePrice;
            }
        }

        return $rate;
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     * @api
     */
    public function getAllowedMethods(): array
    {
        $carrier = $this->findCarrier();
        if (!$carrier) {
            return [];
        }

        /** @var \MageWorx\ShippingRules\Api\Data\MethodInterface[] $methods */
        $methods        = $carrier->getMethods();
        $allowedMethods = [];
        foreach ($methods as $method) {
            $allowedMethods[$method->getCode()] = $method->getTitle();
        }

        return $allowedMethods;
    }

    /**
     * When enabled the tax will be added to the subtotal for the rate validation.
     *
     * @return bool
     */
    public function includeTaxInSubtotal(): bool
    {
        return $this->helper->isIncludeTaxInSubtotalForRatesValidation();
    }

    /**
     * Add custom data to request before calculations
     *
     * @param RateRequest $request
     * @return void
     */
    protected function prepareRequest(RateRequest $request): void
    {
        if (!$request->getData('mw_is_prepared')) {
            $data = [
                'mageworx_base_subtotal'                    => 0.0,
                'mageworx_base_tax'                         => 0.0,
                'mageworx_base_discount'                    => 0.0,
                'mageworx_package_qty'                      => 0.0,
                'mageworx_base_subtotal_phisical_products'  => 0.0,
                'mageworx_base_tax_phisical_products'       => 0.0,
                'mageworx_base_discount_phisical_products'  => 0.0,
                'mageworx_package_weight_physical_products' => 0.0,
                'mageworx_package_qty_physical_products'    => 0.0,
                'mw_is_prepared'                            => true
            ];

            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($request->getAllItems() as $item) {
                // Do not take into account child products separately
                if ($item->getParentItemId()) {
                    continue;
                }

                // For any product in cart
                $data['mageworx_base_subtotal'] += $item->getBaseRowTotal();
                $data['mageworx_base_tax']      += ($item->getBaseRowTotalInclTax() - $item->getBaseRowTotal());
                $data['mageworx_base_discount'] += $item->getBaseDiscountAmount();
                $data['mageworx_package_qty']   += $item->getQty();

                if ($item->getIsVirtual()) {
                    continue;
                }

                // For physical product only
                $data['mageworx_base_subtotal_phisical_products']  += $item->getBaseRowTotal();
                $data['mageworx_base_tax_phisical_products']       += (
                    $item->getBaseRowTotalInclTax() - $item->getBaseRowTotal()
                );
                $data['mageworx_base_discount_phisical_products']  += $item->getBaseDiscountAmount();
                $data['mageworx_package_weight_physical_products'] += $item->getRowWeight()
                    ? $item->getRowWeight()
                    : $item->getWeight() * $item->getQty();
                $data['mageworx_package_qty_physical_products']    = $item->getQty();
            }

            $request->addData($data);
        }
    }

    /**
     * Get an actual price for which we must get a rate.
     * May include tax or not;
     * May include price of virtual products in cart or not;
     *
     * @param RateRequest $request
     * @param Method $methodData
     * @return float
     */
    protected function getPriceForRequestCalculation(
        QuoteAddressRateRequest $request,
        MageWorxMethodModel     $methodData
    ): float {
        $config = $this->cartPriceCalcConfigFactory->create(
            [
                'includeTax'            => $this->includeTaxInSubtotal() && $request->getBaseSubtotalInclTax(),
                'ignoreVirtualProducts' => $methodData->getIgnoreVirtualProductsPrice(),
                'useDiscount'           => $methodData->getUsePriceWithDiscount()
            ]
        );

        return $this->cartPriceCalculation->calculateBasePrice($request, $config);
    }
}
