<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Carrier\CartPriceCalculation;

use MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterface;

class Config implements CartPriceCalculationConfigInterface
{
    /**
     * @var bool
     */
    protected $isTaxIncluded = false;

    /**
     * @var bool
     */
    protected $ignoreVirtualProducts = false;

    /**
     * @var bool
     */
    protected $useDiscount = false;

    /**
     * @param bool $includeTax
     * @param bool $ignoreVirtualProducts
     * @param bool $useDiscount
     */
    public function __construct(
        bool $includeTax = false,
        bool $ignoreVirtualProducts = false,
        bool $useDiscount = false
    ) {
        $this->isTaxIncluded         = $includeTax;
        $this->ignoreVirtualProducts = $ignoreVirtualProducts;
        $this->useDiscount           = $useDiscount;
    }

    /**
     * Does the Subtotal include tax in the rate request?
     *
     * @return bool
     */
    public function isTaxIncluded(): bool
    {
        return $this->isTaxIncluded;
    }

    /**
     * Does the virtual products affect the rate request?
     *
     * @return bool
     */
    public function isVirtualProductsIgnored(): bool
    {
        return $this->ignoreVirtualProducts;
    }

    /**
     * Does the subtotal subtract the discount?
     *
     * @return bool
     */
    public function isDiscountIncluded(): bool
    {
        return $this->useDiscount;
    }
}
