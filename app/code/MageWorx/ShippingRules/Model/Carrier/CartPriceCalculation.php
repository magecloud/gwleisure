<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Carrier;

use MageWorx\ShippingRules\Api\CartPriceCalculationInterface;

class CartPriceCalculation implements CartPriceCalculationInterface
{
    /**
     * @inheritDoc
     */
    public function calculateBasePrice(
        \Magento\Quote\Model\Quote\Address\RateRequest                  $request,
        \MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterface $calculationConfig
    ): float {
        $calculationPrice = 0.0;

        if ($calculationConfig->isVirtualProductsIgnored()) {
            $subtotal = (float)$request->getData('mageworx_base_subtotal_phisical_products');
            $tax      = (float)$request->getData('mageworx_base_tax_phisical_products');
            $discount = (float)$request->getData('mageworx_base_discount_phisical_products');
        } else {
            $subtotal = (float)$request->getData('mageworx_base_subtotal');
            $tax      = (float)$request->getData('mageworx_base_tax');
            $discount = (float)$request->getData('mageworx_base_discount');
        }

        $calculationPrice += $subtotal;

        if ($calculationConfig->isTaxIncluded()) {
            $calculationPrice += $tax;
        }

        if ($calculationConfig->isDiscountIncluded()) {
            $calculationPrice -= $discount;
        }

        return (float)$calculationPrice;
    }
}
