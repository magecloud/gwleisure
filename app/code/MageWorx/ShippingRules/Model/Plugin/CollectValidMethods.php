<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Model\Plugin;

use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Framework\Profiler;
use MageWorx\ShippingRules\Model\Config\Source\Shipping\Methods as ShippingMethodsSource;

/**
 * Class CollectValidMethods
 */
class CollectValidMethods
{
    /**
     * @var array
     */
    private $availableShippingMethodsOverall = [];

    /**
     * @var ProductResourceModel
     */
    private $productResource;

    /**
     * @var ShippingMethodsSource
     */
    private $shippingMethodsSource;

    /**
     * @var bool
     */
    private $isCollected = false;

    /**
     * CollectValidMethods constructor.
     *
     * @param ProductResourceModel $productResource
     * @param ShippingMethodsSource $shippingMethodsSource
     */
    public function __construct(
        ProductResourceModel $productResource,
        ShippingMethodsSource $shippingMethodsSource
    ) {
        $this->productResource       = $productResource;
        $this->shippingMethodsSource = $shippingMethodsSource;
    }

    /**
     * @param \Magento\Shipping\Model\Shipping $subject
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return array
     */
    public function beforeCollectRates(
        \Magento\Shipping\Model\Shipping $subject,
        \Magento\Quote\Model\Quote\Address\RateRequest $request
    ): array {
        /** @var \Magento\Quote\Model\Quote\Item[] $allItems */
        $allItems = $request->getAllItems();
        if (empty($allItems)) {
            return [$request];
        }

        $this->collectAvailableShippingMethodsForItems($allItems);

        return [$request];
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item[] $items
     * @return array
     */
    public function collectAvailableShippingMethodsForItems(array $items): array
    {
        Profiler::start('collect_available_shipping_methods_for_items');

        // By default, all shipping methods are available.
        // Then we will remove the methods which were not set for the product as an available
        // (one-by-one for each product). Attribute name is: 'available_shipping_methods'
        $availableShippingMethods = $this->shippingMethodsSource->toArray();
        // Trim all values and make them all in lowercase for better comparison
        $availableShippingMethods = array_map(function ($value) {
            return trim(strtolower((string)$value ?? ''));
        }, $availableShippingMethods);

        $productsShippingMethods = [];

        foreach ($items as $quoteItem) {
            Profiler::start('check_available_shipping_methods_in_item_' . $quoteItem->getId());
            $product = $quoteItem->getProduct();
            $productAvailableShippingMethods = $this->productResource->getAttributeRawValue(
                $product->getId(),
                'available_shipping_methods',
                $quoteItem->getStoreId()
            );
            if (empty($productAvailableShippingMethods)) {
                continue; // No one method selected means no restriction!
            } elseif (!is_array($productAvailableShippingMethods)) {
                $productAvailableShippingMethods = \explode(',', (string)$productAvailableShippingMethods);
            }

            // Trim all values and make them all in lowercase for better comparison
            $productAvailableShippingMethods = array_map(function ($value) {
                return trim(strtolower((string)$value ?? ''));
            }, $productAvailableShippingMethods);

            $productsShippingMethods[$product->getId()] = $productAvailableShippingMethods;
            Profiler::stop('check_available_shipping_methods_in_item_' . $quoteItem->getId());
        }

        if (!empty($productsShippingMethods)) {
            // Searching for only intersected values (method codes), removing other from both sides.
            $availableShippingMethods = \array_intersect($availableShippingMethods, ...$productsShippingMethods);
        }

        $this->availableShippingMethodsOverall = \array_unique($availableShippingMethods);
        $this->isCollected                     = true;

        Profiler::stop('collect_available_shipping_methods_for_items');

        return $this->availableShippingMethodsOverall;
    }

    /**
     * Returns array of all available shipping methods calculated from products (from request)
     *
     * @return array
     */
    public function getAvailableShippingMethods(): array
    {
        return $this->availableShippingMethodsOverall;
    }

    /**
     * Check is available shipping methods was collected
     *
     * @return bool
     */
    public function getIsCollected(): bool
    {
        return $this->isCollected;
    }
}
