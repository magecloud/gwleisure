<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Model\Plugin;

use Magento\Framework\App\Config as AppConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json as ObjectSerializer;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\StoreResolver;
use MageWorx\ShippingRules\Cache\Type\ShippingCarriers as ShippingCarriersCache;
use MageWorx\ShippingRules\Model\CarrierFactory;
use MageWorx\ShippingRules\Model\ResourceModel\Carrier\CollectionFactory as CarrierCollectionFactory;

/**
 * Class AddMethods
 */
class AddMethods
{
    /**
     * @var CarrierFactory
     */
    protected $carrierFactory;

    /**
     * @var CarrierCollectionFactory
     */
    protected $carrierCollectionFactory;

    /**
     * @var \MageWorx\ShippingRules\Model\ResourceModel\Carrier\Collection
     */
    protected $carriersCollection;

    /**
     * @var array
     */
    protected $loadedCarriers = [];

    /**
     * @var StoreResolver
     */
    private $storeResolver;

    /**
     * @var State
     */
    private $state;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ShippingCarriersCache
     */
    private $shippingCarriersCache;

    /**
     * @var ObjectSerializer
     */
    private $serializer;

    /**
     * @var array
     */
    private $cache;

    /**
     * @var int
     */
    protected $storeId;

    /**
     * @param CarrierFactory $carrierFactory
     * @param CarrierCollectionFactory $collectionFactory
     * @param StoreResolver $storeResolver
     * @param State $state
     * @param StoreManagerInterface $storeManager
     * @param RequestInterface $request
     * @param ShippingCarriersCache $shippingCarriersCache
     * @param ObjectSerializer $serializer
     */
    public function __construct(
        CarrierFactory $carrierFactory,
        CarrierCollectionFactory $collectionFactory,
        StoreResolver $storeResolver,
        State $state,
        StoreManagerInterface $storeManager,
        RequestInterface $request,
        ShippingCarriersCache $shippingCarriersCache,
        ObjectSerializer $serializer
    ) {
        $this->carrierFactory           = $carrierFactory;
        $this->carrierCollectionFactory = $collectionFactory;
        $this->storeResolver            = $storeResolver;
        $this->state                    = $state;
        $this->storeManager             = $storeManager;
        $this->request                  = $request;
        $this->shippingCarriersCache    = $shippingCarriersCache;
        $this->serializer               = $serializer;
    }

    /**
     * @param AppConfig $subject
     * @param callable $proceed
     * @param null $path
     * @param string $scope
     * @param null $scopeCode
     * @return mixed|null
     */
    public function aroundGetValue(
        AppConfig $subject,
        callable $proceed,
        $path = null,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        $returnValue   = $proceed($path, $scope, $scopeCode);
        $filterByStore = (int)($scopeCode !== null) || $scope !== ScopeConfigInterface::SCOPE_TYPE_DEFAULT;

        if (mb_stripos((string)$path, 'carriers') === 0) {
            $pathParts  = explode('/', (string)$path);
            $partsCount = count($pathParts);

            // Do not process existing in the config value, because it has highest priority
            if ($partsCount > 1 && $returnValue !== null) {
                return $returnValue;
            }

            if (!$filterByStore) {
                $this->storeId = 0; // default
            }

            try {
                switch ($partsCount) {
                    case 1:
                        $this->prepareCarriers();
                        $returnValue = $this->addCarriers($returnValue, $this->getStoreId());
                        break;
                    case 2:
                        $this->prepareCarriers();
                        $code        = $pathParts[1];
                        $returnValue = $this->getSpecificCarrierData($code);
                        break;
                    case 3:
                        $this->prepareCarriers();
                        $code        = $pathParts[1];
                        $param       = $pathParts[2];
                        $returnValue = $param == '' ? null : $this->getSpecificCarrierData($code, $param);
                        break;
                }
            } catch (LocalizedException $localizedException) {
                return $returnValue;
            }
        }

        return $returnValue;
    }

    /**
     * Prepare carriers collection & load items
     *
     * @throws LocalizedException
     */
    protected function prepareCarriers()
    {
        $storeId = $this->getStoreId();
        $cached  = $this->getFromCache($storeId);
        if (!empty($cached)) {
            $this->loadedCarriers[$storeId] = $cached;
        }

        if (empty($this->loadedCarriers[$storeId])) {
            /** @var \MageWorx\ShippingRules\Model\ResourceModel\Carrier\Collection $carriersCollection */
            $this->carriersCollection = $this->carrierCollectionFactory->create();

            // Default store workaround for settings page (not order creation page)
            if ($storeId) {
                $this->carriersCollection->addStoreFilter($storeId);
            }

            $this->carriersCollection->load();
            $carriersArray = $this->carriersCollection->toArray();
            $this->loadedCarriers[$storeId] = $carriersArray['items'];
            $this->saveInCache($storeId, $this->loadedCarriers[$storeId]);
        }

        // In case you want to display a store specific labels for backend you can do it here
    }

    /**
     * Load carriers from cache
     *
     * @param int $storeId
     * @return array
     */
    private function getFromCache(int $storeId): array
    {
        if (empty($this->cache)) {
            $serializedCache = $this->shippingCarriersCache->load(ShippingCarriersCache::TYPE_IDENTIFIER);
            if (!empty($serializedCache)) {
                $this->cache = $this->serializer->unserialize($serializedCache);
            }
        }

        return $this->cache[$storeId] ?? [];
    }

    /**
     * @param int $storeId
     * @param array $items
     * @return bool
     */
    private function saveInCache(int $storeId, array $items): bool
    {
        if (empty($this->cache)) {
            $serializedCache = $this->shippingCarriersCache->load(ShippingCarriersCache::TYPE_IDENTIFIER);
            if (!empty($serializedCache)) {
                $this->cache = $this->serializer->unserialize($serializedCache);
            }
        }

        $this->cache[$storeId] = $items;
        $this->shippingCarriersCache->save(
            $this->serializer->serialize($this->cache),
            ShippingCarriersCache::TYPE_IDENTIFIER,
            [ShippingCarriersCache::CACHE_TAG],
            ShippingCarriersCache::DEFAULT_CACHE_LIFETIME
        );

        return true;
    }

    /**
     * Add all available carriers to the result
     *
     * @param mixed $returnValue
     * @param int $storeId
     * @return mixed
     */
    protected function addCarriers($returnValue, int $storeId)
    {
        $carriers = $this->getCarriersByStoreId($storeId);
        foreach ($carriers as $carrier) {
            if (empty($carrier['carrier_code'])) {
                continue;
            }

            $code = $carrier['carrier_code'];
            if (isset($returnValue[$code])) {
                continue;
            }

            $returnValue[$code] = $carrier;
        }

        return $returnValue;
    }

    /**
     * Get all data of the carrier specified by code (carrier_code)
     * It's possible to get the specified parameter ($param) of the carrier
     *
     * @param string $code
     * @param null $param
     * @return mixed|null
     * @throws LocalizedException
     */
    protected function getSpecificCarrierData($code, $param = null)
    {
        $carrierData = $this->getCarrierDataByCode($code);
        if (empty($carrierData)) {
            return null;
        }

        if (!$param) {
            return $carrierData;
        }

        return $carrierData[$param] ?? null;
    }

    /**
     * Get carriers for specific store using cache
     *
     * @param int $storeId
     * @return array
     */
    private function getCarriersByStoreId(int $storeId): array
    {
        $carriers = $this->getFromCache($storeId);

        return $carriers;
    }

    /**
     * @param string $carrierCode
     * @return array
     * @throws LocalizedException
     */
    private function getCarrierDataByCode(string $carrierCode): array
    {
        $storeId  = $this->getStoreId();
        $carriers = $this->getCarriersByStoreId($storeId);
        $carrier  = [];

        foreach ($carriers as $carrierData) {
            if (!empty($carrierData['carrier_code']) && $carrierData['carrier_code'] == $carrierCode) {
                $carrier = $carrierData;
                break;
            }
        }

        return $carrier;
    }

    /**
     * @return int
     * @throws LocalizedException
     */
    public function getStoreId(): int
    {
        if (!$this->storeId) {
            $this->storeId = $this->resolveStoreId();
        }

        return $this->storeId;
    }

    /**
     * @return int
     * @throws LocalizedException
     */
    private function resolveStoreId(): int
    {
        try {
            if ($this->state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
                // in admin area
                /** @var RequestInterface $request */
                $storeId = (int)$this->request->getParam('store_id', 0);
            } else {
                $storeId = (int)$this->storeResolver->getCurrentStoreId();
            }
        } catch (LocalizedException $localizedException) {
            $storeId = $this->storeManager->getStore()->getId();
        }

        return $storeId;
    }
}
