<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Setup\Patch\Schema;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Setup\Exception;
use MageWorx\ShippingRules\Api\Data\RateInterface;
use MageWorx\ShippingRules\Model\Carrier;
use MageWorx\ShippingRules\Model\ZipCodeManager;
use Psr\Log\LoggerInterface;

class TransferData260 implements SchemaPatchInterface, PatchVersionInterface
{
    const MAX_ITERATIONS_COUNT = 1000;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ZipCodeManager
     */
    protected $zipCodeManager;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        LoggerInterface $logger,
        ZipCodeManager $zipCodeManager
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->logger          = $logger;
        $this->zipCodeManager  = $zipCodeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->logger->debug('Transfer data start:');
        try {
            // Transfer countries
            $this->transferCountriesFromRateToIndexTable();

            // Transfer regions and region ids
            $this->transferRegionsFromRateToIndexTable();
            $this->transferRegionIdsFromRateToIndexTable();

            // Transfer Zip Codes (plain & diapasons)
            $this->transferZipsFromRateToIndexTables();

            // Create dump of the rates table
            $this->createRatesTableDump();

            // Remove abandoned data
            $this->dropUnnecessaryRatesConditionsColumns();
            $this->removeCarrierIdColumnFromMethodsTable();
            $this->removeMethodIdColumnFromRatesTable();
        } catch (\Exception $e) {
            $this->logger->debug('Exception: ' . $e->getMessage());
            $this->logger->debug($e->getTraceAsString());

            throw $e;
        }
        $this->logger->debug('Transfer data ends;');
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.1.5';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Transfer countries data by rate id from old column to the new index table
     */
    private function transferCountriesFromRateToIndexTable()
    {
        $connection             = $this->moduleDataSetup->getConnection();
        $ratesTable             = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);
        $ratesCountriesIdxTable = $this->moduleDataSetup->getTable(RateInterface::RATE_COUNTRY_TABLE_NAME);

        if (!$connection->tableColumnExists($ratesTable, 'country_id')) {
            return;
        }

        $select  = $connection->select()->from($ratesTable, [RateInterface::ENTITY_ID_FIELD_NAME, 'country_id'])
                              ->where("country_id > ''");
        $rawData = $connection->fetchAll($select);
        if (empty($rawData)) {
            return;
        }

        $data = [];
        foreach ($rawData as $datum) {
            $rateId    = $datum[RateInterface::ENTITY_ID_FIELD_NAME];
            $countries = explode(',', (string)$datum['country_id']);
            foreach ($countries as $country) {
                $data[] = [
                    'rate_id'      => $rateId,
                    'country_code' => $country
                ];
            }
        }

        $connection->insertOnDuplicate($ratesCountriesIdxTable, $data);
    }

    /**
     * Transfer regions data by rate id from old column to the new index table
     */
    private function transferRegionsFromRateToIndexTable()
    {
        $connection           = $this->moduleDataSetup->getConnection();
        $ratesTable           = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);
        $ratesRegionsIdxTable = $this->moduleDataSetup->getTable(RateInterface::RATE_REGION_TABLE_NAME);

        if (!$connection->tableColumnExists($ratesTable, 'region')) {
            return;
        }

        $select  = $connection->select()->from($ratesTable, [RateInterface::ENTITY_ID_FIELD_NAME, 'region'])
                              ->where("region > ''");
        $rawData = $connection->fetchAll($select);
        if (empty($rawData)) {
            return;
        }

        $data = [];
        foreach ($rawData as $datum) {
            $rateId  = $datum[RateInterface::ENTITY_ID_FIELD_NAME];
            $regions = explode(',', (string)$datum['region']);
            foreach ($regions as $region) {
                $data[] = [
                    'rate_id' => $rateId,
                    'region'  => $region
                ];
            }
        }

        $connection->insertOnDuplicate($ratesRegionsIdxTable, $data);
    }

    /**
     * Transfer region ids data by rate id from old column to the new index table
     */
    private function transferRegionIdsFromRateToIndexTable()
    {
        $connection             = $this->moduleDataSetup->getConnection();
        $ratesTable             = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);
        $ratesRegionIdsIdxTable = $this->moduleDataSetup->getTable(RateInterface::RATE_REGION_ID_TABLE_NAME);

        if (!$connection->tableColumnExists($ratesTable, 'region_id')) {
            return;
        }

        $select  = $connection->select()->from($ratesTable, [RateInterface::ENTITY_ID_FIELD_NAME, 'region_id'])
                              ->where("region_id > ''");
        $rawData = $connection->fetchAll($select);
        if (empty($rawData)) {
            return;
        }

        $data = [];
        foreach ($rawData as $datum) {
            $rateId    = $datum[RateInterface::ENTITY_ID_FIELD_NAME];
            $regionIds = explode(',', (string)$datum['region_id']);
            foreach ($regionIds as $regionId) {
                $data[] = [
                    'rate_id'   => $rateId,
                    'region_id' => $regionId
                ];
            }
        }

        $connection->insertOnDuplicate($ratesRegionIdsIdxTable, $data);
    }

    /**
     * Get old records (zip-codes rules) from the rates table and transfer it to own specific tables
     *
     * @throws LocalizedException
     */
    private function transferZipsFromRateToIndexTables()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $ratesTable = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);

        if (!$connection->tableColumnExists($ratesTable, 'zip_from')) {
            return;
        }

        if (!$connection->tableColumnExists($ratesTable, 'zip_to')) {
            return;
        }

        $page = 0;
        do {
            $select  = $connection->select()->from(
                $ratesTable,
                [RateInterface::ENTITY_ID_FIELD_NAME, 'zip_from', 'zip_to']
            )
                                  ->where("zip_from IS NOT NULL")
                                  ->limitPage($page, 100);
            $rawData = $connection->fetchAll($select);
            if (empty($rawData)) {
                return;
            }

            $this->logger->debug(sprintf('Page: %d', $page));

            $this->doTransferZips($rawData);
            $page++;
        } while (!empty($rawData) && $page < 10000);
    }

    /**
     * Transfer zip method
     *
     * @param array $rawData
     * @throws LocalizedException
     */
    private function doTransferZips($rawData)
    {
        $dataPlainZips                 = [];
        $dataDiapasonZips              = [];
        $plainZipValidationRateIds     = [];
        $diapasonZipsValidationRateIds = [];
        $diapasonZipsFormats           = [];

        foreach ($rawData as $datum) {
            $rateId  = $datum[RateInterface::ENTITY_ID_FIELD_NAME];
            $zipFrom = $datum['zip_from'];
            $zipTo   = $datum['zip_to'];

            // Parse zip from
            if (empty($zipFrom) && $zipFrom !== '0') {
                continue;
            } else {
                $zipFromArray = explode(',', (string)$zipFrom);
            }

            // Parse zip to
            if (empty($zipTo)) {
                $isDiapason = false;
            } else {
                $isDiapason = true;
                $zipToArray = explode(',', (string)$zipTo);
            }

            if ($isDiapason) {
                // Working with diapason
                $count = min(count($zipFromArray), count($zipToArray));
                $i     = 0;
                while ($i < $count) {
                    $zipFromData = $zipFromArray[$i];
                    $zipToData   = $zipToArray[$i];

                    $zipFromDataWithoutSpaces = trim($zipFromData);
                    if (stripos($zipFromDataWithoutSpaces, '!') === 0) {
                        $inverted     = true;
                        $cleanZipFrom = str_ireplace('!', '', $zipFromDataWithoutSpaces);
                    } else {
                        $inverted     = false;
                        $cleanZipFrom = $zipFromDataWithoutSpaces;
                    }

                    $cleanZipTo = str_ireplace('!', '', trim($zipToData));

                    if ($cleanZipFrom != $cleanZipTo) {
                        $formatFrom = $this->zipCodeManager->detectFormat($cleanZipFrom);
                        $formatTo   = $this->zipCodeManager->detectFormat($cleanZipTo);
                        if ($formatFrom === $formatTo) {
                            $dataDiapasonZips[$formatFrom][]    = [
                                'rate_id'  => $rateId,
                                'from'     => $cleanZipFrom,
                                'to'       => $cleanZipTo,
                                'inverted' => $inverted
                            ];
                            $diapasonZipsValidationRateIds[]    = $rateId;
                            $diapasonZipsFormats[$formatFrom][] = $rateId;
                        }
                    } else {
                        // Correcting in case zip from equals zip to
                        $dataPlainZips[]             = [
                            'rate_id'  => $rateId,
                            'zip'      => $cleanZipFrom,
                            'inverted' => $inverted
                        ];
                        $plainZipValidationRateIds[] = $rateId;
                    }

                    $i++;
                }
            } else {
                // Working with plain zip codes
                foreach ($zipFromArray as $zipFromData) {
                    $zipFromDataWithoutSpaces = trim($zipFromData);
                    if (stripos($zipFromDataWithoutSpaces, '!') === 0) {
                        $inverted = true;
                        $cleanZip = str_ireplace('!', '', $zipFromDataWithoutSpaces);
                    } else {
                        $inverted = false;
                        $cleanZip = $zipFromDataWithoutSpaces;
                    }

                    $dataPlainZips[] = [
                        'rate_id'  => $rateId,
                        'zip'      => $cleanZip,
                        'inverted' => $inverted
                    ];

                    $plainZipValidationRateIds[] = $rateId;
                }
            }
        }

        // Insert data
        $this->insertPlainZips($dataPlainZips);
        $this->insertDiapasonsZip($dataDiapasonZips);
        $this->insertZipValidationModeForRates(
            $plainZipValidationRateIds,
            RateInterface::ZIP_VALIDATION_MODE_PLAIN
        );
        $this->insertZipValidationModeForRates(
            $diapasonZipsValidationRateIds,
            RateInterface::ZIP_VALIDATION_MODE_DIAPASON
        );
        $this->insertDiapasonsZipFormatForRates($diapasonZipsFormats);

        // Log Result
        $allRecordsCount = count($rawData);
        $diapasonsCount  = 0;
        foreach ($dataDiapasonZips as $dataDiapasonZip) {
            $diapasonsCount += count($dataDiapasonZip);
        }
        $plainZipsCount          = count($dataPlainZips);
        $unprocessedRecordsCount = $allRecordsCount - ($diapasonsCount + $plainZipsCount);

        $this->logger->debug(sprintf('Overall Records: %d', $allRecordsCount));
        $this->logger->debug(sprintf('Diapasons: %d', $diapasonsCount));
        $this->logger->debug(sprintf('Plain Zips: %d', $plainZipsCount));
        $this->logger->debug(sprintf('Not Processed: %d', $unprocessedRecordsCount));
    }

    /**
     * Insert zip_format values in the rates table
     *
     * @param array $diapasonZipsFormats
     */
    private function insertDiapasonsZipFormatForRates(array $diapasonZipsFormats)
    {
        if (empty($diapasonZipsFormats)) {
            return;
        }

        $connection = $this->moduleDataSetup->getConnection();
        $ratesTable = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);

        foreach ($diapasonZipsFormats as $format => $zipsForFormat) {
            if (!empty($zipsForFormat)) {
                $zipsForFormat = array_unique($zipsForFormat);
                $whereAsString = 'rate_id IN (' . implode(',', $zipsForFormat) . ')';
                $connection->update(
                    $ratesTable,
                    [
                        'zip_format' => $format
                    ],
                    $whereAsString
                );
            }
        }
    }

    /**
     * Insert zip_validation_mode values in the rates table
     *
     * @param array $rateIds
     * @param int $mode
     */
    private function insertZipValidationModeForRates(array $rateIds, $mode)
    {
        if (empty($rateIds)) {
            return;
        }

        $connection = $this->moduleDataSetup->getConnection();
        $ratesTable = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);

        $rateIds       = array_unique($rateIds);
        $whereAsString = 'rate_id IN (' . implode(',', $rateIds) . ')';

        $connection->update(
            $ratesTable,
            [
                'zip_validation_mode' => $mode
            ],
            $whereAsString
        );
    }

    /**
     * Insert zip diapasons
     *
     * @param array $dataDiapasonZips
     * @throws LocalizedException
     */
    private function insertDiapasonsZip(array $dataDiapasonZips)
    {
        // Insert diapasons according its format
        foreach ($dataDiapasonZips as $format => $formatZipsData) {
            if (empty($formatZipsData)) {
                continue;
            }

            try {
                $formatter = $this->zipCodeManager->getFormatter($format);
                $formatter->bulkInsertUpdate($formatZipsData);
            } catch (LocalizedException $exception) {
                $this->logger->debug($exception->getMessage());

                throw $exception;
            }
        }
    }

    /**
     * Insert plain zips
     *
     * @param array $dataPlainZips
     */
    private function insertPlainZips(array $dataPlainZips)
    {
        if (empty($dataPlainZips)) {
            return;
        }

        $plainZipsTable = $this->moduleDataSetup->getTable(RateInterface::RATE_ZIPS_TABLE_NAME);
        $connection     = $this->moduleDataSetup->getConnection();

        $dataPlainZipsChunks = array_chunk($dataPlainZips, 256);
        foreach ($dataPlainZipsChunks as $chunk) {
            $connection->insertOnDuplicate($plainZipsTable, $chunk);
        }
    }

    /**
     * @throws Exception
     */
    private function createRatesTableDump()
    {
        $connection     = $this->moduleDataSetup->getConnection();
        $ratesTable     = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);
        $ratesTableDump = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME . '_dump');
        $i              = 1;
        while ($connection->isTableExists($ratesTableDump)) {
            $ratesTableDump = $this->moduleDataSetup->getTable(sprintf(Carrier::RATE_TABLE_NAME . '_dump_%d', $i));
            $i++;
            if ($i >= self::MAX_ITERATIONS_COUNT) {
                throw new Exception('Something goes wrong during creation dump of the rates table.');
            }
        }
        $sql = sprintf('CREATE TABLE %1$s DEFAULT CHARSET=utf8 AS SELECT * FROM %2$s;', $ratesTableDump, $ratesTable);
        $connection->query($sql);
    }

    /**
     * Drop old columns from the rates table
     */
    private function dropUnnecessaryRatesConditionsColumns()
    {
        $connection         = $this->moduleDataSetup->getConnection();
        $ratesTable         = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);
        $unnecessaryColumns = [
            'region',
            'region_id',
            'country_id',
            'zip_from',
            'zip_to'
        ];

        foreach ($unnecessaryColumns as $column) {
            $connection->dropColumn($ratesTable, $column);
        }
    }

    /**
     * Remove the `carrier_id` column from the methods table (preferred way: using `carrier_code` column instead)
     */
    private function removeCarrierIdColumnFromMethodsTable()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $table      = $this->moduleDataSetup->getTable(Carrier::METHOD_TABLE_NAME);
        $connection->dropColumn($table, 'carrier_id');
    }

    /**
     * Remove the `method_id` column from the rates table (preferred way: using `method_code` column instead)
     */
    private function removeMethodIdColumnFromRatesTable()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $table      = $this->moduleDataSetup->getTable(Carrier::RATE_TABLE_NAME);
        $connection->dropColumn($table, 'method_id');
    }
}
