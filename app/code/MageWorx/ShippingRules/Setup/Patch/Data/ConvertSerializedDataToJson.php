<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Setup\Patch\Data;

use Magento\Framework\DB\AggregatedFieldDataConverter;
use Magento\Framework\DB\DataConverter\SerializedToJson;
use Magento\Framework\DB\FieldToConvert;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use MageWorx\ShippingRules\Api\Data\RuleInterface;
use MageWorx\ShippingRules\Model\Rule;
use MageWorx\ShippingRules\Model\Zone;
use MageWorx\ShippingRules\Api\Data\ZoneInterface;

class ConvertSerializedDataToJson implements DataPatchInterface
{
    /**
     * @var MetadataPool
     */
    protected $metadataPool;

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var AggregatedFieldDataConverter
     */
    private $aggregatedFieldDataConverter;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        AggregatedFieldDataConverter $aggregatedFieldDataConverter,
        MetadataPool $metadataPool
    ) {
        $this->moduleDataSetup              = $moduleDataSetup;
        $this->aggregatedFieldDataConverter = $aggregatedFieldDataConverter;
        $this->metadataPool                 = $metadataPool;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->convertRuleSerializedDataToJson();
        $this->convertZoneSerializedDataToJson();
    }

    private function convertRuleSerializedDataToJson()
    {
        $metadata = $this->metadataPool->getMetadata(RuleInterface::class);
        $this->aggregatedFieldDataConverter->convert(
            [
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'conditions_serialized'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'actions_serialized'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'amount'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'action_type'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'shipping_methods'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'disabled_shipping_methods'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'enabled_shipping_methods'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'store_errmsgs'
                ),
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Rule::TABLE_NAME),
                    $metadata->getLinkField(),
                    'changed_titles'
                ),
            ],
            $this->moduleDataSetup->getConnection()
        );
    }

    private function convertZoneSerializedDataToJson()
    {
        $metadata = $this->metadataPool->getMetadata(ZoneInterface::class);
        $this->aggregatedFieldDataConverter->convert(
            [
                new FieldToConvert(
                    SerializedToJson::class,
                    $this->moduleDataSetup->getTable(Zone::ZONE_TABLE_NAME),
                    $metadata->getLinkField(),
                    'conditions_serialized'
                ),
            ],
            $this->moduleDataSetup->getConnection()
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
