<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\ShippingRules\Setup\Patch\Data;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use MageWorx\ShippingRules\Model\Region as RegionModel;

class AddDefaultValuesForDefaultRegions160 implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection           = $this->moduleDataSetup->getConnection();
        $regionsTable         = $this->moduleDataSetup->getTable('directory_country_region');
        $extendedRegionsTable = $this->moduleDataSetup->getTable(RegionModel::EXTENDED_REGIONS_TABLE_NAME);
        $select               = $connection->select()->from($regionsTable, ['region_id']);
        $query                = $connection->insertFromSelect(
            $select,
            $extendedRegionsTable,
            ['region_id'],
            AdapterInterface::INSERT_IGNORE
        );
        $connection->query($query);
        $connection->update($extendedRegionsTable, ['is_active' => 1, 'is_custom' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
