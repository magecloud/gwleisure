<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Magento\Eav\Setup\EavSetupFactory;

class AddShippingPerProductAttributes240 implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create();

        $availableShippingMethodsAttribute = $eavSetup->getAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'available_shipping_methods'
        );
        if (empty($availableShippingMethodsAttribute)) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'available_shipping_methods',
                [
                    'group'                    => 'General',
                    'type'                     => 'text',
                    'label'                    => 'Available Shipping Methods',
                    'input'                    => 'multiselect',
                    'required'                 => false,
                    'sort_order'               => 40,
                    'global'                   => ScopedAttributeInterface::SCOPE_STORE,
                    'is_used_in_grid'          => true,
                    'is_visible_in_grid'       => true,
                    'is_filterable_in_grid'    => true,
                    'visible'                  => true,
                    'is_html_allowed_on_front' => false,
                    'visible_on_front'         => false,
                    'system'                   => 0,
                    // should be 0 to access this attribute everywhere
                    'user_defined'             => false,
                    // should be false to prevent deleting from admin-side interface
                    'source'                   =>
                        \MageWorx\ShippingRules\Model\Attribute\Source\AvailableShippingMethods::class,
                    'frontend'                 =>
                        \MageWorx\ShippingRules\Model\Attribute\Frontend\AvailableShippingMethods::class,
                    'backend'                  =>
                        \MageWorx\ShippingRules\Model\Attribute\Backend\AvailableShippingMethods::class,
                    // Extends Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend
                ]
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    public static function getVersion()
    {
        return '2.4.0';
    }
}
