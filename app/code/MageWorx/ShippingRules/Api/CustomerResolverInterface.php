<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Quote\Api\Data\CartInterface;

interface CustomerResolverInterface
{
    /**
     * Resolve current customer (from model or session)
     *
     * @param mixed $model
     * @return CustomerInterface
     */
    public function resolve($model = null): CustomerInterface;

    /**
     * Resolve current customer using session
     *
     * @return CustomerInterface
     */
    public function resolveFromSession(): CustomerInterface;

    /**
     * Get current customer from cart object
     *
     * @param CartInterface $quote
     * @return CustomerInterface
     */
    public function resolveFromCart(CartInterface $quote): CustomerInterface;
}
