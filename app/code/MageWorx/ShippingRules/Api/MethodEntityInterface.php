<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

use Magento\Framework\DataObject;

/**
 * Interface MethodEntityInterface
 *
 * Describes custom shipping method
 */
interface MethodEntityInterface extends Data\MethodInterface
{
    /**
     * Validate model data
     *
     * @param DataObject $dataObject
     * @return bool|array
     */
    public function validateData(DataObject $dataObject);

    /**
     * Get Method label by specified store.
     * The $force flag make it applicable in the adminhtml area.
     *
     * @param \Magento\Store\Model\Store|int|bool|null $store
     * @return string|bool
     */
    public function getStoreLabel($store = null);

    /**
     * Set if not yet and retrieve method store labels
     *
     * @return mixed[]
     */
    public function getStoreLabels(): array;

    /**
     * Initialize method model data from array.
     * Set store labels if applicable.
     *
     * @param mixed[] $data
     * @return $this
     */
    public function loadPost(array $data): \MageWorx\ShippingRules\Api\MethodEntityInterface;

    /**
     * @param \MageWorx\ShippingRules\Model\ResourceModel\Rate\Collection $rates
     * @return $this
     */
    public function setRatesCollection(
        \MageWorx\ShippingRules\Model\ResourceModel\Rate\Collection $rates
    ): \MageWorx\ShippingRules\Api\MethodEntityInterface;

    /**
     * Display or not the estimated delivery time message
     *
     * @return bool
     */
    public function isNeedToDisplayEstimatedDeliveryTime(): bool;

    /**
     * Returns formatted estimated delivery time message
     * string will be formatted as $prefix + message + $ending
     *
     * @param string $prefix
     * @param string $ending
     * @return string
     */
    public function getEstimatedDeliveryTimeMessageFormatted(string $prefix = '', string $ending = ''): string;

    /**
     * Get min estimated delivery time by rate (overwritten default value)
     *
     * @return float
     */
    public function getEstimatedDeliveryTimeMinByRate(): float;

    /**
     * Get max estimated delivery time by rate (overwritten default value)
     *
     * @return float
     */
    public function getEstimatedDeliveryTimeMaxByRate(): float;

    /**
     * Set min estimated delivery time by rate (overwrite default value)
     *
     * @param float $value
     * @return $this
     */
    public function setEstimatedDeliveryTimeMinByRate(float $value): \MageWorx\ShippingRules\Api\MethodEntityInterface;

    /**
     * Set max estimated delivery time by rate (overwrite default value)
     *
     * @param float $value
     * @return $this
     */
    public function setEstimatedDeliveryTimeMaxByRate(float $value): \MageWorx\ShippingRules\Api\MethodEntityInterface;
}
