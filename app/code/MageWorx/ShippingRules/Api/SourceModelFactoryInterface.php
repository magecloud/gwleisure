<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

/**
 * Create source models dynamically
 */
interface SourceModelFactoryInterface
{
    /**
     * Create specific source model
     *
     * @param string $className
     * @param ...$arguments
     * @return \Magento\Framework\Data\OptionSourceInterface
     */
    public function create(string $className, ...$arguments): \Magento\Framework\Data\OptionSourceInterface;
}
