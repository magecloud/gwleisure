<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

use Magento\Quote\Model\Quote\Address\RateRequest;

interface CartPriceCalculationInterface
{
    /**
     * Base price for rate conditions. Based on this value suitable rate will be selected.
     *
     * @param RateRequest $request
     * @param CartPriceCalculationConfigInterface $calculationConfig
     * @return float
     */
    public function calculateBasePrice(
        \Magento\Quote\Model\Quote\Address\RateRequest $request,
        \MageWorx\ShippingRules\Api\CartPriceCalculationConfigInterface $calculationConfig
    ): float;
}
