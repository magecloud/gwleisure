<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

/**
 * Manage customer attributes availability in shipping rules conditions
 */
interface CustomerAttributesFilterPoolInterface
{
    /**
     * Walk through all filters and check is attribute available for shipping rules condition usage
     *
     * @param \Magento\Eav\Api\Data\AttributeInterface $attribute
     * @return bool
     */
    public function isAvailable(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool;

    /**
     * Get pool of filters
     *
     * @return array|CustomerAttributesFilterInterface[]
     */
    public function getPool(): array;
}
