<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

/**
 * Manage customer attributes availability in shipping rules conditions
 */
interface CustomerAttributesFilterInterface
{
    /**
     * Check is attribute disabled for conditions
     *
     * @param \Magento\Eav\Api\Data\AttributeInterface $attribute
     * @return bool
     */
    public function check(\Magento\Eav\Api\Data\AttributeInterface $attribute): bool;
}
