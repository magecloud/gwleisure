<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api;

interface CartPriceCalculationConfigInterface
{
    /**
     * Does the Subtotal include tax in the rate request?
     *
     * @return bool
     */
    public function isTaxIncluded(): bool;

    /**
     * Does the virtual products affect the rate request?
     *
     * @return bool
     */
    public function isVirtualProductsIgnored(): bool;

    /**
     * Does the subtotal subtract the discount?
     *
     * @return bool
     */
    public function isDiscountIncluded(): bool;
}
