<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api\Data;

/**
 * Interface MethodInterface
 */
interface MethodInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    const EDT_DISPLAY_TYPE_DAYS           = 0;
    const EDT_DISPLAY_TYPE_HOURS          = 1;
    const EDT_DISPLAY_TYPE_DAYS_AND_HOURS = 2;

    const EDT_PLACEHOLDER_MIN = '{{min}}';
    const EDT_PLACEHOLDER_MAX = '{{max}}';

    const ENTITY_ID_FIELD_NAME = 'entity_id';

    /**
     * Retrieve method title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Flag: is the title replacement allowed
     * In case it is allowed - the title from a most prior rate will be used
     * (in a case valid rate is exists)
     *
     * @return bool
     */
    public function getReplaceableTitle(): bool;

    /**
     * Retrieve method code
     *
     * @return string
     */
    public function getCode(): string;

    /**
     * Retrieve method ID
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Check is method active
     *
     * @return bool
     */
    public function getActive(): bool;

    /**
     * Default method price
     *
     * @return float (12,2)
     */
    public function getPrice(): float;

    /**
     * Get Max price threshold
     *
     * @return float
     */
    public function getMaxPriceThreshold(): float;

    /**
     * Get Min price threshold
     *
     * @return float
     */
    public function getMinPriceThreshold(): float;

    /**
     * Flag: is need to ignore price of the virtual product in cart during rate validation
     *
     * @return bool
     */
    public function getIgnoreVirtualProductsPrice(): bool;

    /**
     * Flag: is price condition include discount
     *
     * @return bool
     */
    public function getUsePriceWithDiscount(): bool;

    /**
     * Default method cost
     *
     * @return float (12,2)
     */
    public function getCost(): float;

    /**
     * Get created at date
     *
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * Get last updated date
     *
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * Check is we should disable this method when there are no valid rates
     *
     * @return bool
     */
    public function getDisabledWithoutValidRates(): bool;

    /**
     * Multiple rates price calculation method
     *
     * @see \MageWorx\ShippingRules\Model\Config\Source\MultipleRatesPrice::toOptionArray()
     *
     * @return int
     */
    public function getMultipleRatesPrice(): int;

    /**
     * Is free shipping by a third party extension allowed (like sales rule)
     *
     * @return bool
     */
    public function getAllowFreeShipping(): bool;

    /**
     * Min estimated delivery time (can be overwritten by a value form a rate, visible at checkout & cart)
     *
     * @return float
     */
    public function getEstimatedDeliveryTimeMin(): float;

    /**
     * Max estimated delivery time (can be overwritten by a value form a rate, visible at checkout & cart)
     *
     * @return float
     */
    public function getEstimatedDeliveryTimeMax(): float;

    /**
     * Flag: is replacing of the estimated delivery time allowed (from a valid rates)
     *
     * @return bool
     */
    public function getReplaceableEstimatedDeliveryTime(): bool;

    /**
     * How an estimated delivery time values would be visible for the customer?
     *
     * Possible values:
     * DAYS (rounded) - MethodInterface::EDT_DISPLAY_TYPE_DAYS
     * HOURS - MethodInterface::EDT_DISPLAY_TYPE_HOURS
     * DAYS & HOURS - MethodInterface::EDT_DISPLAY_TYPE_DAYS_AND_HOURS
     *
     * @return int
     */
    public function getEstimatedDeliveryTimeDisplayType(): int;

    /**
     * Flag: should be the Estimated Delivery Time displayed for the customer or not
     *
     * @return bool
     */
    public function getShowEstimatedDeliveryTime(): bool;

    /**
     * Markup for the EDT message.
     * You can use variables {{min}} {{max}} which will be replaced by a script to the corresponding values
     * from a method or rate.
     *
     * {{min}} - MethodInterface::EDT_PLACEHOLDER_MIN
     * {{max}} - MethodInterface::EDT_PLACEHOLDER_MAX
     *
     * @return string
     */
    public function getEstimatedDeliveryTimeMessage(): string;

    /**
     * Get associated store Ids
     *
     * @return int[]
     */
    public function getStoreIds(): array;

    /**
     * Get corresponding carrier code (relation)
     *
     * @return string
     */
    public function getCarrierCode(): string;

    /**
     * Get method store specific labels
     *
     * @return string[]
     */
    public function getStoreLabels(): array;

    /**
     * Set if not yet and retrieve method store specific EDT messages
     *
     * @return string[]
     */
    public function getEdtStoreSpecificMessages(): array;

    //_______________________________________________SETTERS___________________________________________________________

    /**
     * Set method title
     *
     * @param string $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setTitle(string $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Flag: is the title replacement allowed
     * In case it is allowed - the title from a most prior rate will be used
     * (in a case valid rate is exists)
     *
     * @param bool $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setReplaceableTitle(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set method code
     *
     * @param string $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setCode(string $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set corresponding carrier id
     *
     * @param int $id
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setCarrierId(int $id): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set method ID
     *
     * @param int $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setEntityId($value);

    /**
     * Set is method active
     *
     * @param bool $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setActive(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set default method price
     *
     * @param float $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setPrice(float $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Max price threshold
     *
     * @param float $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setMaxPriceThreshold(float $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Min price threshold
     *
     * @param float $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setMinPriceThreshold(float $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Flag: is need to ignore price of the virtual product in cart during rate validation
     *
     * @param bool $value
     * @return MethodInterface
     */
    public function setIgnoreVirtualProductsPrice(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Flag: is price condition include discount
     *
     * @param bool $value
     * @return MethodInterface
     */
    public function setUsePriceWithDiscount(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Default method cost
     *
     * @param float $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setCost(float $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set created at date
     *
     * @param mixed $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setCreatedAt($value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set last updated date
     *
     * @param mixed $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setUpdatedAt($value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set is we should disable this method when there are no valid rates
     *
     * @param bool $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setDisabledWithoutValidRates(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Multiple rates price calculation method
     *
     * @see \MageWorx\ShippingRules\Model\Config\Source\MultipleRatesPrice::toOptionArray($value)
     *
     * @param int $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setMultipleRatesPrice(int $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Is free shipping by a third party extension allowed (like sales rule)
     *
     * @param bool $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setAllowFreeShipping(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Min estimated delivery time (can be overwritten by a value form a rate, visible at checkout & cart)
     *
     * @param float $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setEstimatedDeliveryTimeMin(float $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Max estimated delivery time (can be overwritten by a value form a rate, visible at checkout & cart)
     *
     * @param float $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setEstimatedDeliveryTimeMax(float $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Flag: is replacing of the estimated delivery time allowed (from a valid rates)
     *
     * @param bool $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setReplaceableEstimatedDeliveryTime(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * How an estimated delivery time values would be visible for the customer?
     *
     * Possible values:
     * DAYS (rounded) - MethodInterface::EDT_DISPLAY_TYPE_DAYS
     * HOURS - MethodInterface::EDT_DISPLAY_TYPE_HOURS
     * DAYS & HOURS - MethodInterface::EDT_DISPLAY_TYPE_DAYS_AND_HOURS
     *
     * @param int $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setEstimatedDeliveryTimeDisplayType(int $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Flag: should be the Estimated Delivery Time displayed for the customer or not
     *
     * @param bool $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setShowEstimatedDeliveryTime(bool $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set estimated Delivery Time Message
     *
     * Markup for the EDT message.
     * You can use variables {{min}} {{max}} which will be replaced by a script to the corresponding values
     * from a method or rate.
     *
     * {{min}} - MethodInterface::EDT_PLACEHOLDER_MIN
     * {{max}} - MethodInterface::EDT_PLACEHOLDER_MAX
     *
     * @param string|null $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setEstimatedDeliveryTimeMessage(?string $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set associated store Ids
     *
     * @param int[] $value
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setStoreIds(array $value): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set corresponding carrier code
     *
     * @param string $code
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setCarrierCode(string $code): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set store specific labels (title)
     *
     * @param string[] $storeLabels
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setStoreLabels(array $storeLabels = []): \MageWorx\ShippingRules\Api\Data\MethodInterface;

    /**
     * Set Store Specific Estimated Delivery Time Messages
     *
     * @param string[] $messages
     * @return \MageWorx\ShippingRules\Api\Data\MethodInterface
     */
    public function setEdtStoreSpecificMessages(array $messages = []): \MageWorx\ShippingRules\Api\Data\MethodInterface;
}
