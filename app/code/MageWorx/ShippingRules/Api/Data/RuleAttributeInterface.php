<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
declare(strict_types=1);

namespace MageWorx\ShippingRules\Api\Data;

interface RuleAttributeInterface
{
    /**
     * Attribute unique code
     *
     * @return string
     */
    public function getCode(): string;

    /**
     * Attribute label
     *
     * @return string
     */
    public function getLabel(): string;

    /**
     * Input type for validation.
     * "string" by default.
     *
     * @return string
     */
    public function getInputType(): string;

    /**
     * Value type for condition input.
     * "text" by default.
     *
     * @return string
     */
    public function getValueType(): string;

    /**
     * Attribute options
     *
     * @return string|null
     */
    public function getSourceModel(): ?string;

    /**
     * Attribute unique code
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setCode(string $value): RuleAttributeInterface;

    /**
     * Attribute label
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setLabel(string $value): RuleAttributeInterface;

    /**
     * Attribute input type (for validation).
     * "string" by default.
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setInputType(string $value): RuleAttributeInterface;

    /**
     * Attribute value type (for condition input).
     * "text" by default.
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setValueType(string $value): RuleAttributeInterface;

    /**
     * Attribute options
     *
     * @param string $value
     * @return RuleAttributeInterface
     */
    public function setSourceModel(string $value): RuleAttributeInterface;
}
