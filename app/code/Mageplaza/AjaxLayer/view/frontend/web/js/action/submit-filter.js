/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AjaxLayer
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'mage/storage',
        'Mageplaza_AjaxLayer/js/model/loader',
        'mage/apply/main'
    ],
    function ($, storage, loader, mage) {
        'use strict';

        var productContainer = $('#layer-product-list'),
            layerContainer = $('.layered-filter-block-container'),
            quickViewContainer = $('#mpquickview-popup');

        return function (submitUrl, isChangeUrl, method) {
            /** save active state */
            var actives = [],
                data;
            $('.filter-options-item').each(function (index) {
                if ($(this).hasClass('active')) {
                    actives.push($(this).attr('attribute'));
                }
            });
            window.layerActiveTabs = actives;

            /** start loader */
            loader.startLoader();

            /** change browser url */
            if (typeof window.history.pushState === 'function' && (typeof isChangeUrl === 'undefined')) {
                window.history.pushState({url: submitUrl}, '', submitUrl);
            }
            if (method === 'post') {// For 'add to wishlist' & 'add to compare' event
                return storage.post(submitUrl).done(
                ).fail(
                    function () {
                        window.location.reload();
                    }
                ).always(function () {
                    loader.stopLoader();
                });
            }

            return storage.get(submitUrl).done(
                function (response) {
                    if (response.backUrl) {
                        window.location = response.backUrl;
                        return;
                    }
                    if (response.navigation) {
                        layerContainer.html(response.navigation);
                    }
                    if (response.products) {
                        productContainer.html(response.products);
                    }
                    if (response.quickview) {
                        quickViewContainer.html(response.quickview);
                    }

                    $('.main .products.grid .product-items li.product-item:nth-child(2n)').addClass('nth-child-2n');
                    $('.main .products.grid .product-items li.product-item:nth-child(2n+1)').addClass('nth-child-2np1');
                    $('.main .products.grid .product-items li.product-item:nth-child(3n)').addClass('nth-child-3n');
                    $('.main .products.grid .product-items li.product-item:nth-child(3n+1)').addClass('nth-child-3np1');
                    $('.main .products.grid .product-items li.product-item:nth-child(4n)').addClass('nth-child-4n');
                    $('.main .products.grid .product-items li.product-item:nth-child(4n+1)').addClass('nth-child-4np1');
                    $('.main .products.grid .product-items li.product-item:nth-child(5n)').addClass('nth-child-5n');
                    $('.main .products.grid .product-items li.product-item:nth-child(5n+1)').addClass('nth-child-5np1');
                    $('.main .products.grid .product-items li.product-item:nth-child(6n)').addClass('nth-child-6n');
                    $('.main .products.grid .product-items li.product-item:nth-child(6n+1)').addClass('nth-child-6np1');
                    $('.main .products.grid .product-items li.product-item:nth-child(7n)').addClass('nth-child-7n');
                    $('.main .products.grid .product-items li.product-item:nth-child(7n+1)').addClass('nth-child-7np1');
                    $('.main .products.grid .product-items li.product-item:nth-child(8n)').addClass('nth-child-8n');
                    $('.main .products.grid .product-items li.product-item:nth-child(8n+1)').addClass('nth-child-8np1');
                    
                    $("#layer-product-list img.porto-lazyload:not(.porto-lazyload-loaded)").lazyload({effect:"fadeIn"});
                    if ($('#layer-product-list .porto-lazyload:not(.porto-lazyload-loaded)').closest('.owl-carousel').length) {
                        $('#layer-product-list .porto-lazyload:not(.porto-lazyload-loaded)').closest('.owl-carousel').on('initialized.owl.carousel', function() {
                            $(this).find('.porto-lazyload:not(.porto-lazyload-loaded)').trigger('appear');
                        });
                        $('#layer-product-list .porto-lazyload:not(.porto-lazyload-loaded)').closest('.owl-carousel').on('changed.owl.carousel', function() {
                            $(this).find('.porto-lazyload:not(.porto-lazyload-loaded)').trigger('appear');
                        });
                    }

                    if (mage) {
                        mage.apply();
                    }
                }
            ).fail(
                function () {
                    window.location.reload();
                }
            ).always(
                function () {

                    var colorAttr = $('.filter-options .filter-options-item .color .swatch-option-link-layered .swatch-option');

                    colorAttr.each(function(){
                        var el  = $(this),
                            hex = el.attr('data-option-tooltip-value');
                        if(typeof hex != "undefined"){
                            if (hex.charAt(0) === '#') {
                                hex = hex.substr(1);
                            }
                            if ((hex.length < 2) || (hex.length > 6)) {
                                el.attr('style','background: '+el.attr('data-option-label')+';');
                            }
                            var values = hex.split(''),
                                r,
                                g,
                                b;

                            if (hex.length === 2) {
                                r = parseInt(values[0].toString() + values[1].toString(), 16);
                                g = r;
                                b = r;
                            } else if (hex.length === 3) {
                                r = parseInt(values[0].toString() + values[0].toString(), 16);
                                g = parseInt(values[1].toString() + values[1].toString(), 16);
                                b = parseInt(values[2].toString() + values[2].toString(), 16);
                            } else if (hex.length === 6) {
                                r = parseInt(values[0].toString() + values[1].toString(), 16);
                                g = parseInt(values[2].toString() + values[3].toString(), 16);
                                b = parseInt(values[4].toString() + values[5].toString(), 16);
                            } else {
                                el.attr('style','background: '+el.attr('data-option-label')+';');
                            }

                            el.attr('style','background: center center no-repeat rgb('+[r, g, b]+');');
                        }

                    });

                    //selected

                    var filterCurrent = $('.layered-filter-block-container .filter-current .items .item .filter-value');

                    filterCurrent.each(function(){
                        var el         = $(this),
                            colorLabel = el.html(),
                            colorAttr  = $('.filter-options .filter-options-item .color .swatch-option-link-layered .swatch-option');

                        colorAttr.each(function(){
                            var elA = $(this);
                            if(elA.attr('data-option-label') === colorLabel && !elA.hasClass('selected')){
                                elA.addClass('selected');
                            }
                        });
                    });

                    loader.stopLoader();
                }
            );
        };
    }
);
