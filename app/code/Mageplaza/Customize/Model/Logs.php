<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AbandonedCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Customize\Model;

use Closure;
use Magento\Quote\Model\Quote;
use Mageplaza\AbandonedCart\Model\ResourceModel\LogsFactory;

class Logs
{
    /**
     * @var LogsFactory
     */
    private $logsFactory;

    /**
     * Logs constructor.
     *
     * @param LogsFactory $logsFactory
     */
    public function __construct(
        LogsFactory $logsFactory
    ) {
        $this->logsFactory     = $logsFactory;
    }

    /**
     * @param Logs $subject
     * @param $result
     * @param Quote $quote
     * @param string $customerEmail
     * @param string $customerName
     * @param string $sender
     * @param $subjectAbandonedCart
     * @param string $body
     * @param bool $status
     * @param null $couponCode
     */
    public function aroundSaveLogs(
        \Mageplaza\AbandonedCart\Model\Logs $subject,
        Closure                             $process,
        Quote                               $quote,
        $customerEmail,
        $customerName,
        $sender,
        $subjectAbandonedCart,
        $body,
        $status = false,
        $couponCode = null
    ) {
        $phone_number=$quote->getShippingAddress()->getTelephone();
        $subject->setSubject($subjectAbandonedCart)
            ->setCustomerEmail($customerEmail)
            ->setCouponCode($couponCode)
            ->setQuoteId($quote->getId())
            ->setSender($sender)
            ->setCustomerName($customerName)
            ->setSequentNumber(1)
            ->setEmailContent(htmlspecialchars($body))
            ->setStatus($status)
            ->setPhoneNumber($phone_number)
            ->save();
    }
}
