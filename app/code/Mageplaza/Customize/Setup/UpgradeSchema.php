<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_GiftCard
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Customize\Setup;

use Magento\Customer\Model\ResourceModel\Customer as CustomerResource;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Zend_Db_Exception;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var CustomerResource
     */
    protected $customerResource;

    /**
     * UpgradeSchema constructor.
     *
     * @param CustomerResource $customerResource
     */
    public function __construct(
        CustomerResource $customerResource
    ) {
        $this->customerResource = $customerResource;
    }

    /**
     * {@inheritdoc}
     * @throws Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $connection->addColumn($setup->getTable('mageplaza_abandonedcart_logs'), 'phone_number', [
                'type'    => Table::TYPE_TEXT,
                'length'  => null,
                'comment' => 'Phone Number',
            ]);
        }

        $installer->endSetup();
    }
}
