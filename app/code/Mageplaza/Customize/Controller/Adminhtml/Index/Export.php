<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AbandonedCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Customize\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Mageplaza\AbandonedCart\Model\ResourceModel\Logs\Collection;

/**
 * Class Clear
 * @package Mageplaza\AbandonedCart\Controller\Adminhtml\Index
 */
class Export extends Action
{
    /**
     * @var Collection
     */
    protected $logsCollection;
    /**
     * @var WriteInterface
     */
    private WriteInterface $directory;
    /**
     * @var FileFactory
     */
    private FileFactory $_fileFactory;

    /**
     * @throws FileSystemException
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        FileFactory $fileFactory,
        Collection $logsCollection,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->logsCollection =$logsCollection;
        parent::__construct($context);
    }
    public function execute()
    {
        $name = date('m_d_Y_H_i_s');
        $filepath = 'export/custom' . $name . '.csv';
        $this->directory->create('export');
        /* Open file */
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        $columns = $this->getColumnHeader();
        foreach ($columns as $column) {
            $header[] = $column;
        }
        /* Write Header */
        $stream->writeCsv($header);
        $logsCollection = $this->logsCollection->getData();
        foreach ($logsCollection as $logs) {
            $logsData =[];
            $logsData[] = $logs['id'];
            $logsData[] = $logs['subject'];
            $logsData[] = $logs['customer_email'];
            $logsData[] = $logs['phone_number'];
            $logsData[] = $logs['coupon_code'];
            $logsData[] = $logs['sequent_number'];
            $logsData[] = $logs['updated_at'];
            if ((int)$logs['status'] === 1) {
                $logsData[] ='SENT';
                $stream->writeCsv($logsData);
            }
        }
        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder

        $csvfilename = 'AbandonedCart.xlsx';
        $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
        $this->_redirect('abandonedcart/*/report');
    }

    /* Header Columns */
    public function getColumnHeader()
    {
        $headers = ['ID','Subject','Recipient','Phone Number','Coupon','Sequent number','Sent Date','Status'];
        return $headers;
    }
}
