<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ThankYouPage
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ThankYouPage\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class NewsletterBlock
 * @package Mageplaza\ThankYouPage\Model\Config\Source
 */
class NewsletterBlock implements ArrayInterface
{
    const COUPON         = 'coupon';
    const SOCIAL_SHARING = 'social_sharing';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::COUPON,
                'label' => __('Coupon')
            ],
            [
                'value' => self::SOCIAL_SHARING,
                'label' => __('Social Sharing')
            ]
        ];

        return $options;
    }
}
