<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ThankYouPage
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ThankYouPage\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 * @package Mageplaza\ThankYouPage\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $moduleContext
     *
     * @throws Zend_Db_Exception
     * @SuppressWarnings(Unused)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $moduleContext)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('mageplaza_thankyoupage_templates')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('mageplaza_thankyoupage_templates'))
                ->addColumn('template_id', Table::TYPE_INTEGER, null, [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true
                ], 'Template Id')
                ->addColumn('name', Table::TYPE_TEXT, 255, [], 'Name')
                ->addColumn('page_type', Table::TYPE_TEXT, 255, [], 'Page Type')
                ->addColumn('status', Table::TYPE_INTEGER, 1, ['nullable' => false], 'Status')
                ->addColumn('store_ids', Table::TYPE_TEXT, 255, ['nullable' => false], 'Store')
                ->addColumn('customer_group_ids', Table::TYPE_TEXT, 255, ['nullable' => false], 'Customer Group')
                ->addColumn('priority', Table::TYPE_INTEGER, null, ['nullable' => false], 'Priority')
                ->addColumn('conditions_serialized', Table::TYPE_TEXT, '2M', [], 'Conditions Serialized')
                ->addColumn('style', Table::TYPE_TEXT, 64, [], 'Template Type')
                ->addColumn('custom_style', Table::TYPE_INTEGER, 1, [], 'Custom Style')
                ->addColumn('title', Table::TYPE_TEXT, 255, [], 'Title')
                ->addColumn('sub_title', Table::TYPE_TEXT, 255, [], 'Sub Title')
                ->addColumn('description', Table::TYPE_TEXT, '2M', [], 'Description')
                ->addColumn('continue_button', Table::TYPE_INTEGER, 1, [], 'Show continue shopping button')
                ->addColumn('block', Table::TYPE_TEXT, 64, [], 'Enable Block(s)')
                ->addColumn('custom_html', Table::TYPE_TEXT, '2M', [], 'Custom HTML')
                ->addColumn('custom_css', Table::TYPE_TEXT, '2M', [], 'Custom CSS')
                ->addColumn('static_block_1', Table::TYPE_INTEGER, null, [], 'Static Block 1')
                ->addColumn('static_block_2', Table::TYPE_INTEGER, null, [], 'Static Block 2')
                ->addColumn('social_share', Table::TYPE_INTEGER, 1, [], 'Enable Social Share')
                ->addColumn('enable_coupon', Table::TYPE_INTEGER, 1, [], 'Enable coupon')
                ->addColumn('rule_id', Table::TYPE_INTEGER, 64, [], 'Rule ID')
                ->addColumn('coupon_pattern', Table::TYPE_TEXT, 64, [], 'Coupon Pattern')
                ->addColumn('coupon_label', Table::TYPE_TEXT, '2M', [], 'Coupon Label')
                ->addColumn('enable_faq', Table::TYPE_INTEGER, 1, [], 'Enable FAQ')
                ->addColumn('faq_title', Table::TYPE_TEXT, 64, [], 'FAQ Title')
                ->addColumn('faq_category', Table::TYPE_INTEGER, 64, [], 'FAQ Title')
                ->addColumn('faq_limit', Table::TYPE_INTEGER, 64, ['default' => 5], 'FAQ Limit')
                ->addColumn('enable_product_slider', Table::TYPE_INTEGER, 1, [], 'Enable Product Slider')
                ->addColumn('product_slider_id', Table::TYPE_TEXT, 64, [], 'Product Slider')
                ->addColumn('product_slider_title', Table::TYPE_TEXT, 255, [], 'Product Slider Title')
                ->addColumn('product_slider_limit', Table::TYPE_INTEGER, 64, ['default' => 10], 'Product limit')
                ->addColumn('product_slider_additional', Table::TYPE_TEXT, 255, [], 'Product Slider Additional')
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Creation Time'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Update Time'
                )
                ->addIndex(
                    $installer->getIdxName('mageplaza_thankyoupage_templates', ['status', 'priority']),
                    ['status', 'priority']
                );

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
