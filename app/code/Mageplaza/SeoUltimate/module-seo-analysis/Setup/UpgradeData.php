<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SeoRule
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SeoAnalysis\Setup;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Zend_Validate_Exception;

/**
 * Class UpgradeData
 * @package Mageplaza\SeoAnalysis\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CategorySetupFactory
     */
    protected $categorySetupFactory;

    /**
     * @param CategorySetupFactory $categorySetupFactory
     */
    public function __construct(
        CategorySetupFactory $categorySetupFactory
    ) {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '2.0.2', '<')) {
            $attributes = [
                [
                    'label'      => 'Meta Data Preview',
                    'value'      => 'mp_meta_data_preview',
                    'sort_order' => 5,
                ],
                [
                    'label'      => 'Focus Keyword',
                    'value'      => 'mp_main_keyword',
                    'sort_order' => 60
                ],
                [
                    'label'      => 'SEO insights',
                    'value'      => 'mp_seo_insights',
                    'sort_order' => 70
                ]
            ];
            $this->removeAttributes($categorySetup, $attributes);
            $this->addAttributes($categorySetup, $attributes);
        }
        $setup->endSetup();
    }

    /**
     * @param CategorySetup $categorySetup
     * @param array $attributes
     *
     * @return $this
     */
    public function removeAttributes($categorySetup, $attributes)
    {
        foreach ($attributes as $attribute) {
            if ($categorySetup->getAttributeId(Category::ENTITY, $attribute['value'])) {
                $categorySetup->removeAttribute(Category::ENTITY, $attribute['value']);
            }
        }

        return $this;
    }

    /**
     * @param CategorySetup $categorySetup
     * @param array $attributes
     *
     * @return $this
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function addAttributes($categorySetup, $attributes)
    {
        foreach ($attributes as $attribute) {
            $categorySetup->addAttribute(Category::ENTITY, $attribute['value'], [
                'type'             => 'varchar',
                'label'            => $attribute['label'],
                'input'            => 'text',
                'backend'          => '',
                'frontend'         => '',
                'class'            => '',
                'source'           => '',
                'global'           => Attribute::SCOPE_STORE,
                'visible'          => true,
                'required'         => false,
                'user_defined'     => false,
                'group'            => 'Search Engine Optimization',
                'default'          => '',
                'apply_to'         => '',
                'sort_order'       => $attribute['sort_order'],
                'visible_on_front' => false,
                'note'             => ''
            ]);
        }

        return $this;
    }
}
