<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SeoCrosslinks
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SeoCrosslinks\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Mageplaza\SeoCrosslinks\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * upgrade
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $setup->getConnection();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $transactionTableName = 'mageplaza_seocrosslinks_term';
            $connection->dropColumn($transactionTableName, 'from_date');
            $connection->dropColumn($transactionTableName, 'to_date');
        }

        if (version_compare($context->getVersion(), '2.0.1' , '<')) {
            $transactionTableName = 'mageplaza_seocrosslinks_term';
            $connection->dropColumn($transactionTableName, 'ref_static_url');
            $connection->addColumn($transactionTableName, 'ref_static_url', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Custom Url',
                'after' => 'reference'
            ]);
        }

        $installer->endSetup();
    }
}
