<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Mageplaza
 * @package   Mageplaza_LazyLoading
 * @copyright Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license   https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ImageOptimizer\Plugin\Model\Template;

use Exception;
use Magento\Cms\Model\Template\Filter as CmsFilter;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\ImageOptimizer\Helper\Data;

class Filter
{
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * WebpImageCategories constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param Data $helperData
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        Data $helperData
    ) {
        $this->helperData   = $helperData;
        $this->storeManager = $storeManager;
    }

    /**
     * @param CmsFilter $filter
     * @param string $result
     *
     * @return mixed
     * @throws NoSuchEntityException
     * @SuppressWarnings("Unused")
     */
    public function afterFilter(CmsFilter $filter, $result)
    {
        // try {
        //     $storeId = $this->storeManager->getStore()->getId();
        // } catch (Exception $e) {
        //     $storeId = null;
        // }

        // if ($this->helperData->isEnabled($storeId)
        //     && $this->helperData->isReplaceWebpImage()) {
        //     $webp = '.webp';
        //     $pattern = '/src=(\'|\")([^>]*)\.(png|tiff|gif|tif|bmp|jpeg|jpg)\1/';
        //     $patternPicture = '/srcset=(\'|\")\s{0,}([^>]*)\.(png|gif|webp|jpeg|jpg)\s{0,}\1/';
        //     $replacementPicture = "srcset=$1$2$webp$1";
        //     $replacement = "src=$1$2$webp$1";
        //     $result = preg_replace($pattern, $replacement, $result);
        //     $result = preg_replace($patternPicture, $replacementPicture, $result);
        //     //exception images
        //     $exceptImg = $this->helperData->getExceptionImages();

        //     if($exceptImg) {
        //         $formatImg = ['.png','.tiff','.gif','.tif','.bmp','.jpeg','.jpg'];
        //         $exceptImg = explode(',', $exceptImg);

        //         foreach ($exceptImg as $key) {
        //             $webpImg = '';
        //             foreach($formatImg as $img) {
        //                 if(stristr($key, $img)) {
        //                     $webpImg = str_replace($img, '.webp', $key);
        //                     break;
        //                 }
        //             }
        //             if($webpImg) {
        //                 $result = str_replace($webpImg, $key, $result);
        //             }
        //         }
        //     }   
        //     return $result;
        // }

        return $result;
    }
}
